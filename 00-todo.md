## Main  - Stories

## Raj Current Working - Stories
WMS-323 Csv Ingest Process to be executed as Background process (use 321)

## Redis
* http://tgrall.github.io/blog/2019/09/02/getting-with-redis-streams-and-java/

## Planning of New Stories 
* Create all unit-tests for CSV Processing
* Create all Spec test for Csv Processing
     
### TODO Under 286      
* TODO - use npmrc for env Example: https://docs.npmjs.com/files/npmrc = (1 pt) and document the use and override from ~/.npmrc
* TODO - convert data generator module to Typescript = (3 pt) using commander library - data generator is used seriously, also help unit test to import data-generator easily
* TODO - Embed Mongo for Test = (1 pt) Make Mongo Port visible and background process for node "unit" tests that way we can debug data in test db if needed with puase or debug node
* TODO - Import sa-spm-common after John/Dan is ready - (3 pt)
* TODO - Fix Frank/Dan/Sky NodeJS meet (strucure we discussed on Friday meeting) - (3 pt)

## Discuss - Stories
       
## Tech Debt
```text
1) Finish all Task end-end working
PROGRESS 1.1) Launch Mongo Test instance at 27019 usign pre-test and post test
DONE-2) Move from Application to Admin component
DONE-3) Move all Models into .model.ts everywhere
DONE-4) move site+device FielArray into config/json file
  DONE- - align fileds inside schema array as 
  DONE-- colIndex: 0, required: true, dataType: 'string', colName: 'siteid', mongoPath: 'siteId', 
5) Move SiteCsvSchema inside validation.ts
6) rename mongo_int.sh to load_mongo_seed_data.sh
6.1) Make Mongo as class pass with conection args

A) TRY-Move
test + spec to prp-service root
check build works good including package/deploy
B) TRY-Move
scripts 
    - cmd
    - swagger
    - graph add Grpah Gen to deploy.sh and edit DOCS

7) Install Windows VM and test BUILD


C) MMMMMMMMMM
 modify setField value tree in csv to use L.set instead of my own utility
 give all unit-tests

D) Refactor CSV Upload, more generic (datastage) way

```

## Finished - Stories
* DONE - WMS-321 - Maintain Application processes list/status ===  accept-doc not done
* DONE - put timeTaken across each Process, with abstract class
* DONE - validation Array fix for CsvValidation
* DONE - WMS-322 Csv Ingest Process to Maintain status using Process List API (use 321)
* DONE - WMS-324 Store Csv source and Result data files in S3 (use 316)

* Story WMS-64 SPIKE - CSV file upload by Utility 
* Task  WMS-135 Build Pipeline 
* Task  WMS-139 Document CSV format and processing 
* Task  WMS-154 Device metadata CSV Upload API 
* Task  WMS-150 Site Data CSV Upload API 
* Task  WMS-170 SPIKE - Determine data source for SA of the columns used in the RNI version 
* Task  WMS-221 PRP - Application Components and Documentation 
* STask WMS-273/WMS-262 (Task.11) Application Properties 
* DONE (1) wms-287  Application Properties - List and Reload API (old 273/262)
* DONE (2) wms-288  Mongo Index - Get Utility Summary API 
* DONE (3) wms-289  Background Task Executor - Create generic framework API  
* DONE (4) wms-290  Mongo Index - Create BackGround Task    (old 274/262) 
* DONE (5) wms-291  Mongo Index - Create OnDemand Task      (old 264/262) 
* DONE (6) wms-292  Docker File for webapp/service modules   (old 265/262) 
## Done- Discuss
* wms-226 implement 282, FilterValueService - already discussed with Joe, looks liek we are talking 2 differnt impls
* wms-164 Discuss with Aaron, Yes we have schema


## DEMO:
```text
NodeJS -  
    - Application Features
    - Application Structure
    - Language/API practice and References
Admin Features 
Background Task using Child Process   

*Overall:*
* Node Library Checklist
* Node App Structure
*Application feature/demo:*
* Implement a Background Task using child-process
* Properties reload at runtime
* Create Mongo Index - as background Job

``` 
  
* As a < type of user >, I want < some goal > so that < some reason >.
* https://xyleminc.atlassian.net/issues/?jql=project%20%3D%20WMS%20AND%20resolution%20%3D%20Unresolved%20AND%20created%20%3E%3D%20-2w%20ORDER%20BY%20created%20DESC%2C%20priority%20DESC&atlOrigin=eyJpIjoiZThiZmQ0YTIyYzg4NDJkNGI1MTQ3NjM1MDhhNjliMTEiLCJwIjoiaiJ9
 
 
 Story/Task: WMS-170
 Date: 06/04/2019
 References:
 Ref.1) Reading Defintion
 https://xyleminc.atlassian.net/wiki/spaces/SA/pages/202014721/Reading+definition
 Ref.2) Event Definition
 https://xyleminc.atlassian.net/wiki/spaces/SA/pages/202014725/Event+definition
 Ref.3) DeviceInfo Definition
 https://xyleminc.atlassian.net/wiki/spaces/SA/pages/202014729/DeviceInfo+definition
 Ref.4) RNI-504 Two-way Actions from SA
 https://xyleminc.atlassian.net/wiki/spaces/SA/pages/342983371/RNI-504+Two-
 way+Actions+from+SA
 
 
* 6/5 VSCOde GetToKnow https://dev.to/deepu105/my-vs-code-setup-making-the-most-out-of-vs-code-4enl
* VSCODE Test Explorerer https://marketplace.visualstudio.com/items?itemName=hbenl.vscode-test-explorer
* VSCODE todo tree https://marketplace.visualstudio.com/items?itemName=Gruntfuggly.todo-tree

### NEW
* https://httptoolkit.tech/view/