* Read everything from https://github.com/webpro/reveal-md

```bash
cd ~/projects/bitbucket/rpradesh/rpradesh.bitbucket.io/demoes/prjStruc-july-16
npm install -g reveal-md
reveal-md slides_v1.md --port 8005
reveal-md slides_v2.md --port 8006 --watch
```