### Project Structure and Practices

* What
    - Pointers about: Project Development Practices and Structure 
    - Documentation and Report/Code Generators
* Why
    - Help team within/outside communicate better
    - All required technical/story details are in one place
    - Standard artifacts as means of deliverables
* How
    - We will see in future slides !
 
Note:
* For project team mates to be on same page as well outside team able to understand the deliverable

---

### How: bitbucket
* Project structure as we all know it [here](https://bitbucket.org/sensusanalytics-dev/sa-wms-prp/src/master/) 
    - `service` and `webapp` will have only prod code/artifacts
    - `tools` can have code/data generators, seed data, config/properties load scripts
    - tools can still relative-ref prod code
    - `docs` folder will be project/application wiki in md [here](https://bitbucket.org/sensusanalytics-dev/sa-wms-prp/src/master/docs)
    
Note:
* The code inisde tools can relative reference prod code like model/interfaces

---

### How: Docs
* The static docs rendering with "Search" [index page](https://sensusanalytics-dev.bitbucket.io/) and [pages here](https://sensusanalytics-dev.bitbucket.io/sa/sa-wms-prp/#/) 
* [Unit Test](https://rpradesh.bitbucket.io/sa-docs/sa-wms-prp/coverage/lcov-report/index.html)  Reports
* [Integration Test](https://rpradesh.bitbucket.io/sa-docs/sa-wms-prp/spec-report/spec.html) Reports

Note:
    * All reports with ZERO extra code
    
---

### How: Docs Highlights
* Dev/Non-Dev notes, External Links
* Data Generator/Loader scripts example for [Site Data](https://bitbucket.org/sensusanalytics-dev/sa-wms-prp/src/master/prp-tools/scripts/lib/sitedatagen.js)
* notice swagger file generator and features [here](https://sensusanalytics-dev.bitbucket.io/sa/sa-wms-prp/#/tools/swagger_gen) live swagger [here](http://localhost:3000/api-docs)
* The Source for Swagger is just function types and signature [here](https://bitbucket.org/sensusanalytics-dev/sa-wms-prp/src/master/prp-tools/swagger-gen/src/routes/SiteRouter.ts)
* Notice How this aligns with real "Router" code [here](https://bitbucket.org/sensusanalytics-dev/sa-wms-prp/src/master/prp-service/src/routes/SiteRouter.ts)

Note: 
* Advantage compared to Postman is about Data-Types and Optional fileds

---

### How: Story SubTasks/Acceptance
* Example brakdown [here](https://sensusanalytics-dev.bitbucket.io/sa/sa-wms-prp/#/plan/story_planning?id=typical-story-breakdown-into-subtasks) of subtasks
* Example acceptance Doc [wms-154](https://xyleminc.atlassian.net/browse/WMS-154) 
* Example acceptance Doc usign swagger [wms-172](https://xyleminc.atlassian.net/browse/WMS-172) 

Note: 
* Acceptance Doc - helps QA+anyone who is accepting story
* Helps the author of story too to check once.

---


### Any Questions

Thanks for not asking questions :)