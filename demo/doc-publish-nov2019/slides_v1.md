### Publish Docs in BitBucket

#### Project and Documentation: What, Why and How
- <!-- .element: class="fragment" --> Why
    * Jira maps the requirments to Technical Tasks 
        - We have basic details
                 
- <!-- .element: class="fragment" --> What 
    * Docs - almost 80% of application/business needs come into picture

- <!-- .element: class="fragment" --> How
    * Code - need more time to read the abstrct/inherited code to understand the details

Note:
- You might have heard of Microsoft/IBM who are very big proponents of Documentaion

---

### Basic Elements of Doc
- <!-- .element: class="fragment" --> Install/Launch of App from Zero
- <!-- .element: class="fragment" --> Basic Features of Application
- <!-- .element: class="fragment" --> Config & Properties
- <!-- .element: class="fragment" --> Taking app from Local to Production
- <!-- .element: class="fragment" --> All Application related links and docs

---

### PRP Docs
- <!-- .element: class="fragment" -->  Workspace https://bitbucket.org/sensusanalytics-dev
- <!-- .element: class="fragment" -->  Doc Repo at https://bitbucket.org/sensusanalytics-dev/sensusanalytics-dev.bitbucket.io/src/master/
- <!-- .element: class="fragment" -->  publish at https://sensusanalytics-dev.bitbucket.io/

---

### Docsify
- <!-- .element: class="fragment" -->  Project Docs is at https://bitbucket.org/sensusanalytics-dev/sa-wms-prp/src/master/
- <!-- .element: class="fragment" -->  Docsify Index is at https://bitbucket.org/sensusanalytics-dev/sensusanalytics-dev.bitbucket.io/src/master/sa/sa-wms-prp/
- <!-- .element: class="fragment" -->  Points to RAW files at https://bitbucket.org/rpradesh/rpradesh.bitbucket.io/raw/master/sa-docs/sa-wms-prp/docs
- <!-- .element: class="fragment" -->  Docsify https://github.com/docsifyjs/docsify https://docsify.js.org/#/quickstart
   
Note:
* https://confluence.atlassian.com/bitbucket/publishing-a-website-on-bitbucket-cloud-221449776.html 
  
---

### Any Questions

* Each Answer costs Amazon gift card of 20$

