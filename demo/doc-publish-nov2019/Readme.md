## Steps to publish static website in BitBucket
* https://confluence.atlassian.com/bitbucket/publishing-a-website-on-bitbucket-cloud-221449776.html


## Look at
* workspace https://bitbucket.org/rpradesh
* Doc Repo at  https://bitbucket.org/rpradesh/rpradesh.bitbucket.io/src/master/
* Render/Publish at rpradesh.bitbucket.io

### Sesus Dev workspace
* Repository https://bitbucket.org/sensusanalytics-dev
* Doc Repo at https://bitbucket.org/sensusanalytics-dev/sensusanalytics-dev.bitbucket.io/src/master/
* publish at https://sensusanalytics-dev.bitbucket.io/

### PRP
* Project Docs is at https://bitbucket.org/sensusanalytics-dev/sa-wms-prp/src/master/
* Docsify Index is at https://bitbucket.org/sensusanalytics-dev/sensusanalytics-dev.bitbucket.io/src/master/sa/sa-wms-prp/
* Points to RAW files at https://bitbucket.org/rpradesh/rpradesh.bitbucket.io/raw/master/sa-docs/sa-wms-prp/docs

## Docsify
* https://github.com/docsifyjs/docsify

## Other Examples
* https://github.com/njleonzhang/vue-data-tables/ https://www.njleonzhang.com/vue-data-tables/#/

```bash
cd /Users/rajeshpradeshik/projects/bb-rpradesh/rpradesh.bitbucket.io/demo/doc-publish-nov2019

npm install -g reveal-md
reveal-md slides_v1.md --port 8005 -w
reveal-md slides_v1.md --port 8006  --theme solarized --watch
reveal-md slides_v1.md --port 8007  --theme moon
reveal-md slides_v1.md --print slides_doc-publish-nov2019_v1.pdf

```