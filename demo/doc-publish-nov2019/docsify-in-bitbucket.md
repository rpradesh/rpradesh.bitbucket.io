### Steps to Create Docsify Docs from bitBucket.io
* Note: for this example, I have used "rpradesh" as my workspaceID and I am owner of it in bitbucket.org
* First you can crete a rpradesh.bitbucket.io (domain url) if you own the account/workspaceID called "rpradesh" in bitBucket.org
* To test it works create new public repo as "rpradesh.bitbucket.io" under https://bitbucket.org/rpradesh/ and commit an index.html as with "Test 1.0" as body and render public as http://rpradesh.bitbucket.io/index.html
* Once above io-site is tested, we are going to use this "main" hosting place to provide index.html for different "bitBucket" repo's to render Markdown documents in "/docs" folder which are residing in its "own" repositories.

#### How create link/render  "docs" folder under "my-java-app" repository
* in the application repo (say) my-java-app/docs
* edit _coverpage.md 
   * edit "BitBucket Code Repo" with repo URL
   * edit "Get Started" to sub-context or bitBucket.io directory
* edit _navbar.md
   * You can update any absolute external URL's    
* and last edit _sidebar.md is the main Left Navigation bar
   * used to provide "relative" links for ".md" docs
   
#### Link from docsify of rpradesh.bitbucket.io to bitbucket "my-java-app" repo  
* Docsify Javascipt actually downloads all the contents from the basePath of settings which go in rpradesh.bitbucket.io
* create index.html under rpradesh.bitbucket.io/my-java-app where folder name matches with repo name you are linking to
* Copy/Past contents of index.html from another project folder and edit following in index.html
    * The "title" to match Your Repo/App Name 
    * The "meta desc" to match the tags
    * Important is the basePath in window.$docsify pointing to include "docs" folder of code repo    
    * And "name" of App in the config
* Once done you should be able to browse the docs throuh io.site as follows  http://rpradesh.bitbucket.io/my-java-app/

```bash
cd /Users/rajeshpradeshik/projects/bb-rpradesh/rpradesh.bitbucket.io
./deploy.sh

```  