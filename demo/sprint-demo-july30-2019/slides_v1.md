### Project Struct / Setup / Docs

#### WMS-221
- <!-- .element: class="fragment" data-fragment-index="1" --> Project [Code Repo](https://bitbucket.org/sensusanalytics-dev/sa-wms-prp/src/master/) + Build + [Wiki](https://xyleminc.atlassian.net/wiki/spaces/SA/pages/193560637/Water+Management+System+Pressure+Profile) 
- <!-- .element: class="fragment" data-fragment-index="2" --> [Application Wiki](https://sensusanalytics-dev.bitbucket.io/sa/sa-wms-prp/#/) for new/current Dev's 
- <!-- .element: class="fragment" data-fragment-index="3" --> NodeJS Backend with Test and Integration Test setup
    * [Unit Test](https://rpradesh.bitbucket.io/sa-docs/sa-wms-prp/coverage/lcov-report/index.html)  Reports
    * [Integration Test](https://rpradesh.bitbucket.io/sa-docs/sa-wms-prp/spec-report/spec.html) Reports
  

Note:
- Logging, properties, basic Bakend App Etc.,

---

### More Application-Components
- <!-- .element: class="fragment" data-fragment-index="1" --> Data Generator Scripts [Site Data](https://sensusanalytics-dev.bitbucket.io/sa/sa-wms-prp/#/feature/feature_site?id=generate-site-data)
- <!-- .element: class="fragment" data-fragment-index="2" --> Live Swagger/Api for testing backend [here](http://localhost:3000/api-docs)
- <!-- .element: class="fragment" data-fragment-index="3" --> Next will Demo Api working

---

### Features - Completed
- <!-- .element: class="fragment" data-fragment-index="1" --> WMS-139 Have [Client Doc](PRP_Client_Data_Feeds_V1.docx) describing upload csv format
- <!-- .element: class="fragment" data-fragment-index="2" --> WMS-150 Site Upload API + Filter  
    - Demo valid + validation file using [swagger](http://localhost:3000/api-docs)
    - Site API, Acceptance Doc [here](wms_150_site_csv_ingest_acceptance_doc.docx)
- <!-- .element: class="fragment" data-fragment-index="3" --> WMS-154 Device Upload API 
   
Note:
* /home/rpradesh/projects/bitbucket/master/sa-wms-prp/prp-service/src/test/test-data/site/10-sitedata.csv
* /home/rpradesh/projects/bitbucket/master/sa-wms-prp/prp-service/src/test/test-data/site/5-invalid-sitedata.csv   
---

### Any Questions

Thanks for not asking questions :)