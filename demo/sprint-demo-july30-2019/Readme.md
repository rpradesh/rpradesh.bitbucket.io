* Read everything from https://github.com/webpro/reveal-md
* Put Revels.js options in reveal.json values from [here](https://github.com/hakimel/reveal.js#configuration)

* More Themes https://revealjs-themes.dzello.com/#/

* TO CHOOSE a them from https://github.com/hakimel/reveal.js/tree/master/css/theme
*
```bash
cd ~/projects/bitbucket/rpradesh/rpradesh.bitbucket.io/demo/sprint-demo-july30-2019
npm install -g reveal-md
reveal-md slides_v1.md --port 8005 -w
reveal-md slides_v1.md --port 8006  --theme solarized --watch
reveal-md slides_v1.md --port 8007  --theme moon
reveal-md slides_v1.md --print slides_projectdemo_july30-2019_v1.pdf
```

```text

/* eslint-disable @typescript-eslint/no-object-literal-type-assertion */
import * as Joi from '@hapi/joi';
import * as fs from 'fs';

async function test() {
  const filePath = ``;
  const cursor: Cursor = {} as Cursor;
  const fileWriteStream = fs.createWriteStream(filePath);
  cursor.forEach(async rec => {
    let newLine = '';
    let comma = '';
    for (const elem of []) {
      newLine += comma + L.get(rec, elem.mongoPath);
      comma = ', ';
    }
    newLine += '\n';
    fileWriteStream.write(newLine);
  });
  
  await fileWriteStream.on('end', () => {
    // eslint-disable-next-line no-console
    console.log('file writing end!');
  });
  //res.download(filePath);
}
```