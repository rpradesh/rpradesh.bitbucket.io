## RevelaJS
* Steps at https://github.com/hakimel/reveal.js#full-setup
* See demo working on net http://revealjs.com/
* Follow for example slides code https://github.com/hakimel/reveal.js/blob/master/demo.html
```bash
# one time to build and install
nvm install 10.16.0
nvm use 10.16.0
node -v

cd demoes/nodejs-may-2019
git clone --depth=1 https://github.com/hakimel/reveal.js.git
cd reveal.js

rm -rf ./.git
npm install
npm start -- --port=8004

# browse at http://localhost:8004/
# <!-- .element: class="fragment" -->

# just to start eveal
cd /home/rpradesh/projects/bitbucket/rpradesh/rpradesh.bitbucket.io
cd demoes/nodejs-may-2019/reveal.js
nvm use 10.16.0
npm install
npm start -- --port=8004

google-chrome http://localhost:8004 
google-chrome http://localhost:8004/?print-pdf

```
