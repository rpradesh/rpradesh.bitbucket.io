/* eslint-disable @typescript-eslint/no-parameter-properties */
import {CsvProcessor} from './csvProcessor';
import {mongo, logger} from '../../Application';
import * as utils from '../../utils';

export interface IDupeCountRecord {
  dupeCount: number;
  recPkValue: string;
}

export class DuplicateCheckProcessor {
  constructor(readonly csvProc: CsvProcessor) {}

  async process(): Promise<void> {
    const dupeArray = await this.fetchDupeRecords(this.csvProc.csvRequest.recBatchId);
    logger.info(`found dupeArray=${dupeArray.length}`);
    dupeArray.forEach(async (rec, i) => {
      await this.updateDupeRecords(this.csvProc.csvRequest.recBatchId, rec);
    });
    await utils.sleepSecs(2);
  }

  async updateDupeRecords(recBatchId: string, dupeRec: IDupeCountRecord): Promise<number> {
    const updateOps = await mongo
      .utilityDB(this.csvProc.utility)
      .collection('prp_data_stage')
      .updateMany(
        {
          recPkValue: dupeRec.recPkValue,
          recBatchId: recBatchId
        },
        {
          $push: {
            recValErrs: `record ${dupeRec.recPkValue} has ${dupeRec.dupeCount} duplicates`
          },
          $inc: {
            totalValErrCount: 1
          }
        }
      );

    return updateOps.result.n;
  }

  async fetchDupeRecords(recBatchId: string): Promise<IDupeCountRecord[]> {
    // count quey did not work, gives error so usign agg
    const aggCursor = await mongo
      .utilityDB(this.csvProc.utility)
      .collection('prp_data_stage')
      .aggregate([
        {
          $match: {
            recBatchId: recBatchId,
            srcLineIndex: {
              $ne: null
            }
          }
        },
        {
          $group: {
            _id: {
              recPkValue: '$recPkValue'
            },
            dupeCount: {
              $sum: 1
            }
          }
        },
        {
          $project: {
            recPkValue: '$_id.recPkValue',
            dupeCount: '$dupeCount'
          }
        },
        {
          $match: {
            dupeCount: {
              $gt: 1
            }
          }
        },
        {
          $project: {_id: 0, dupeCount: 1, recPkValue: 1}
        }
      ]);

    return await aggCursor.toArray();
  }
}
