/* eslint-disable no-console */
/* eslint-disable @typescript-eslint/no-parameter-properties */

import {mongo, logger} from '../../Application';
import * as L from 'lodash';
import {CsvProcessRequest, CsvProcessResponse, CsvFeatureOptions} from './model';
import {DataStageInsertProcessor} from './dataStageInsertProcessor';
import {DuplicateCheckProcessor} from './duplicateCheckProcessor';
import {InsertUpdateProcessor} from './insertUpdateProcessor';
import {ExportResultProcessor} from './exportResultProcessor';
import {FileDetailPersistProcessor} from './fileDetailPersistProcessor';
import {S3FilePersistProcessor} from './s3FilePersistProcessor';
import {ProcessStatusEnum} from '../../components/Process';

export class CsvProcessor {
  csvResponse: CsvProcessResponse = ({} as unknown) as CsvProcessResponse;

  constructor(readonly csvRequest: CsvProcessRequest, readonly csvFeature: CsvFeatureOptions) {
    this.updateResponse();
  }

  get utility(): string {
    return this.csvRequest.utility;
  }

  get collectionName(): string {
    return this.csvFeature.csvImport.CollectionName;
  }

  get collectionPkField(): string {
    return this.csvFeature.csvImport.CollectionPkField;
  }

  async process(): Promise<CsvProcessResponse> {
    logger.info(`== starting data ingest ==`); //CsvProcessRequest=${JSON.stringify(this.csvRequest)}`);
    try {
      await new DataStageInsertProcessor(this).process();
      if (this.csvRequest.processResult.totalRecordCount === 0)
        throw new Error(`given file: ${this.csvRequest.originalFileName} has Zero records to process `);
      await new DuplicateCheckProcessor(this).process();
      await new InsertUpdateProcessor(this).process();
      await this.processGetCounts();
      await new ExportResultProcessor(this).process();
      this.updateResponse();
      await new FileDetailPersistProcessor(this).process();
      await new S3FilePersistProcessor(this).process();
      this.csvRequest.processDetail.status = ProcessStatusEnum.SUCCESS;
    } catch (err) {
      logger.error(err.stack ? err.stack : err);
      this.csvRequest.processDetail.errorDetail = err.stack;
      this.csvRequest.processDetail.status = ProcessStatusEnum.FAIL;
      this.csvRequest.processResult.processError = err.message ? err.message : err;
      //console.log(`errJson.stack=`, err.stack);
    } finally {
      this.csvRequest.processResult.processEndedAt = new Date();
      this.csvRequest.processDetail.completedAt = new Date();
      this.updateResponse();
      //logger.info(`summary csvResponse= ${JSON.stringify(this.csvResponse)}`);
    }

    return this.csvResponse;
  }

  updateResponse(): void {
    const requiredRequestProps = L.omit(this.csvRequest, [
      'filePaths',
      'recBatchId',
      'csvSchema',
      'inputFilepath',
      'processDetail'
    ]);
    this.csvResponse = L.merge(this.csvResponse, requiredRequestProps);
  }

  async processGetCounts(): Promise<void> {
    // total inValid records in batch
    this.csvRequest.processResult.inValidRecordCount = await mongo
      .utilityDB(this.utility)
      .collection('prp_data_stage')
      .count({
        recBatchId: this.csvRequest.recBatchId,
        totalValErrCount: {$gt: 0}
      });

    // valid is diff of above 2
    this.csvRequest.processResult.validRecordCount =
      this.csvRequest.processResult.totalRecordCount - this.csvRequest.processResult.inValidRecordCount;
  }
} //end-class
