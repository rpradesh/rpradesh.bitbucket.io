/* eslint-disable @typescript-eslint/no-object-literal-type-assertion */
import {ICsvRequestParams, CsvProcessRequest, CsvProcessResponse, CsvFeatureOptions} from './model';
import {CsvProcessor} from './csvProcessor';
import {logger} from '../../Application';
import * as utils from '../../utils';

export async function ingestCsv(
  reqParams: ICsvRequestParams,
  csvFeature: CsvFeatureOptions
): Promise<CsvProcessResponse> {
  const timeTaken = new utils.TimeTaken();
  logger.info(`metrics START=${JSON.stringify(utils.processMetrics())}==`);

  const csvReq: CsvProcessRequest = new CsvProcessRequest(
    reqParams.utility,
    reqParams.processName,
    reqParams.originalFileName,
    reqParams.inputFilepath,
    reqParams.guid
  );
  const csvProcessor = new CsvProcessor(csvReq, csvFeature);
  const csvResponse: CsvProcessResponse = await csvProcessor.process();
  logger.info(`metrics   END=${JSON.stringify(utils.processMetrics())}== ${timeTaken.end('seconds')} secs`);

  return csvResponse;
}

export {
  /* Proc */
  CsvProcessor,
  /* Model */
  ICsvRequestParams,
  CsvFeatureOptions,
  CsvProcessResponse
};
