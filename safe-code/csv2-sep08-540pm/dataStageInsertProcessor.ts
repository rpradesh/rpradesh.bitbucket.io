/* eslint-disable @typescript-eslint/no-parameter-properties */
import {CsvProcessor} from './csvProcessor';
import * as utils from '../../utils';
import {CsvStageRecord} from './model';
import {mongo, logger} from '../../Application';

function updateFilds(rec: CsvStageRecord, foundHash: any): CsvStageRecord {
  rec.recAction = foundHash.srcJsonMd5Hash === rec.srcJsonMd5Hash ? -1 : 1; //if same : not-same
  return rec;
}
/**
 * recAction value
 * if 0 means record does not exist in prp_site, make an insert
 * if 1 means record exists with different hash, needs an update
 * if -1 means record exists with same hash, do not update
 */
export class DataStageInsertProcessor {
  private batchRecords: any[] = [];
  private fileLineCounter: number = 0;
  private insertCount = 0;
  private lookupMap: Map<string, any> = new Map();

  constructor(readonly csvProc: CsvProcessor) {}

  async process(): Promise<void> {
    for await (const line of utils.readLines(this.csvProc.csvRequest.inputFilepath)) {
      if (this.fileLineCounter < this.csvProc.csvFeature.skipHeaderLineCount) {
        this.fileLineCounter++;
      } else {
        await this.processLine(this.fileLineCounter, line);
        this.fileLineCounter++;
      }
    } //end-for

    // process last not-fully-filled batch records
    await this.persistBatch();
    this.batchRecords = [];

    // update total inserted records n data stage
    this.csvProc.csvRequest.processResult.totalRecordCount = this.insertCount;
    this.csvProc.csvRequest.processResult.totalLinesInFile = this.fileLineCounter;

    logger.info(`this.fileLineCounter=${this.fileLineCounter} this.insertCount=${this.insertCount}`);
  }

  private async processLine(lineIndex: number, srcLine: string): Promise<void> {
    const csvStageRecord: CsvStageRecord = new CsvStageRecord(lineIndex, srcLine.replace(/\r?\n|\r/g, ''));
    await csvStageRecord.parse(this.csvProc.csvRequest.csvSchema);
    this.batchRecords.push(csvStageRecord);
    // logger.info(`csvStageRecord=${JSON.stringify(csvStageRecord)}`);
    if (this.batchRecords.length === this.csvProc.csvFeature.batchSize) {
      await this.persistBatch();
      this.batchRecords = [];
    }
  }

  private async _createLookupMap(): Promise<void> {
    // run lookup query, by passing the in query
    const pkValueArray: string[] = this.batchRecords.map(rec => rec.recPkValue);
    const arrayOfTupes = await mongo
      .utilityDB(this.csvProc.utility)
      .collection(this.csvProc.collectionName)
      .find({[this.csvProc.collectionPkField]: {$in: [...pkValueArray]}})
      .project({_id: 0, [this.csvProc.collectionPkField]: 1, srcJsonMd5Hash: 1})
      .toArray();
    this.lookupMap = await utils.buildMap(arrayOfTupes, this.csvProc.collectionPkField);
    // logger.info(`pkValueArray=${JSON.stringify(pkValueArray)}, pkMap=${JSON.stringify(this.lookupMap)}`);
  }

  private async persistBatch(): Promise<void> {
    // logger.info(`metrics persistBatch START=${JSON.stringify(utils.processMetrics())}==`);
    await this._createLookupMap();

    const insertArray: any[] = this.batchRecords.map((rec: CsvStageRecord) => {
      rec.recBatchId = this.csvProc.csvRequest.recBatchId;
      rec.totalValErrCount = rec.srcValErrs.length;

      if (this.lookupMap.has(rec.recPkValue)) rec = updateFilds(rec, this.lookupMap.get(rec.recPkValue));
      else rec.recAction = 0;

      return {
        insertOne: {...rec}
      };
    });

    this.insertCount += await utils.bulkWrite('prp1', 'prp_data_stage', insertArray);
    // logger.info(`metrics persistBatch END=${JSON.stringify(utils.processMetrics())}==`);
  }
}
