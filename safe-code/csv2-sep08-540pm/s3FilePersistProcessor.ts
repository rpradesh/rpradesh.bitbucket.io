/* eslint-disable no-console */
/* eslint-disable @typescript-eslint/no-parameter-properties */
import {CsvProcessor} from './CsvProcessor';
import {mongo, logger, Constant} from '../../Application';
import {BulkWriteOpResultObject, AggregationCursor} from 'mongodb';
import * as utils from '../../utils';
import * as moment from 'moment';
import * as path from 'path';
import {FilestoreService, IIngestFileDetail, IFileProcessingDetail} from '../../components/Filestore/index';

async function saveToS3(utility: string, absFilePath: string, key: string): Promise<void> {
  // TODO: after WMS-316 S3Cient with AWS is ready, to putObject to AWS
}

export class S3FilePersistProcessor {
  constructor(readonly csvProc: CsvProcessor) {}

  async process(): Promise<void> {
    // copy file to proper source filename, called as site/download/:sourceObjectId/source
    await utils.copyFile(
      this.csvProc.csvRequest.inputFilepath,
      path.join(Constant.App.uploadTempFolder, this.csvProc.csvResponse.csvSourceFileObjectId)
    );

    // copy file to proper source filename, called as site/download/:sourceObjectId/result
    await utils.copyFile(
      this.csvProc.csvRequest.filePaths.resultFilepath,
      path.join(Constant.App.uploadTempFolder, this.csvProc.csvResponse.csvResultFileObjectId)
    );
    //await utils.deleteFile(this.csvProc.csvRequest.filePaths.resultFilepath);

    // this is for FielDashboard in Utility Portal to pull file for download, from UI popUP
    await saveToS3(
      this.csvProc.utility,
      this.csvProc.csvRequest.inputFilepath,
      this.csvProc.csvResponse.ingestFileObjectId
    );

    // this is for PRP App to download file using AWS
    // site/download/:sourceObjectId/source
    await saveToS3(
      this.csvProc.utility,
      this.csvProc.csvRequest.inputFilepath,
      this.csvProc.csvResponse.csvSourceFileObjectId
    );

    // this is for PRP App to download file using AWS
    // site/download/:sourceObjectId/result
    await saveToS3(
      this.csvProc.utility,
      this.csvProc.csvRequest.filePaths.resultFilepath,
      this.csvProc.csvResponse.csvResultFileObjectId
    );
  }
}
