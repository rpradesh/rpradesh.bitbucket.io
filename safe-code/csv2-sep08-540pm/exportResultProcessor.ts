/* eslint-disable no-console */
/* eslint-disable @typescript-eslint/no-parameter-properties */
import {CsvProcessor} from './CsvProcessor';
import {mongo, logger, app} from '../../Application';
import * as utils from '../../utils';
import * as fs from 'fs';
import * as L from 'lodash';

async function buildLine(mongoPaths: string[], rec: any): Promise<string> {
  const colData: string[] = [];
  mongoPaths.forEach((path, i) => {
    colData.push(L.get(rec, path, ''));
  });
  return colData.join(',');
}

async function buildErrorContent(rec: any): Promise<string> {
  const errData: string[] = [];
  let totalErrorCount = 0;

  if (rec.totalValErrCount > 0) {
    errData.push(rec.srcValErrs.join(';'));
    errData.push(rec.recValErrs.join(';'));
    totalErrorCount += rec.totalValErrCount;
  }

  return `, ${totalErrorCount} , ${errData.join(';').replace(',', '|')} `;
}

export class ExportResultProcessor {
  constructor(readonly csvProc: CsvProcessor) {}

  async process(): Promise<void> {
    let lineCount = 0;
    const outFilePath = this.csvProc.csvRequest.filePaths.resultFilepath;
    const fileWriteStream: fs.WriteStream = fs.createWriteStream(outFilePath);
    const mongoPaths: string[] = utils.parseFieldFromObjectArray(
      this.csvProc.csvRequest.csvSchema.schemaFields,
      'mongoPath'
    );
    logger.info(`starting to write to outFilePath=${outFilePath} mongoPaths=${mongoPaths}`);

    const cursor = await mongo
      .utilityDB(this.csvProc.utility)
      .collection('prp_data_stage')
      .find({recBatchId: this.csvProc.csvRequest.recBatchId});

    while (await cursor.hasNext()) {
      const rec = await cursor.next();
      const line = await buildLine(mongoPaths, rec.srcJson);
      const errorContent = await buildErrorContent(rec);
      // logger.info(`line=${line}`);
      fileWriteStream.write(line);
      fileWriteStream.write(errorContent);
      fileWriteStream.write('\n');
      lineCount++;
    }
    this.csvProc.csvRequest.processResult.nbrLinesWritten = lineCount;
  }
}
