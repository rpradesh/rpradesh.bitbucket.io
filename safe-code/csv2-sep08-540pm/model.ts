/* eslint-disable @typescript-eslint/no-empty-interface */
/* eslint-disable @typescript-eslint/no-parameter-properties */
import * as path from 'path';
import * as uuid from 'uuid';
import * as L from 'lodash';
import {BaseCsvSchema} from '../csv';
import * as utils from '../../utils';
import * as Joi from '@hapi/joi';
import {app, logger} from '../../Application';
import * as moment from 'moment';
import {Constant, CsvImport} from '../../Application';
import {SiteCsvSchema} from '../../components/Site/siteCsvSchema';
import {DeviceCsvSchema} from '../../components/Device/deviceCsvSchema';
import {ProcessNameEnum, ProcessDetail, ProcessActionEnum, ProcessStatusEnum} from '../../components/Process';

export interface ICsvFieldSchema {
  colIndex: number;
  colName: string;
  mongoPath: string;
  required: boolean;
  dataType: string;
  lookup?: string[];
}

export class CsvFeatureOptions {
  readonly hasUniqueFieldPath: boolean;
  readonly isPercentFeatureEnabled: boolean;
  private unknownUniqueFieldValue = 'unknownUniqueFieldValue';

  constructor(
    readonly csvImport: CsvImport,
    readonly doValidateMinPercentDataMatch: boolean = false,
    readonly validateMinPercentDataMatch: number = 0,
    readonly doValidateUniqueField: boolean = false,
    readonly skipHeaderLineCount: number = 0,
    readonly batchSize: number = 10000
  ) {
    this.hasUniqueFieldPath = !L.isEmpty(csvImport.CollectionPkField);
    this.isPercentFeatureEnabled = !(
      this.doValidateMinPercentDataMatch === false || this.validateMinPercentDataMatch === 0
    );
  }

  getUniqueFieldValue(json: any): string {
    let ufValue: string = this.unknownUniqueFieldValue;
    if (this.hasUniqueFieldPath) {
      ufValue = L.get(json, this.csvImport.CollectionPkField, this.unknownUniqueFieldValue);
    }
    return ufValue;
  }
}

export class CsvFilePaths {
  readonly sourceFilepath: string;
  readonly resultFilepath: string;
  readonly currentExportFilename: string;
  constructor(readonly guid: string, readonly processName: ProcessNameEnum) {
    const currentDataTimeStr = moment(new Date()).format('MMDDYYYY_HHmmss');
    this.sourceFilepath = path.join(
      Constant.App.uploadTempFolder,
      `${Constant.App.Name}-${processName}-${guid}-source.csv`
    );
    this.resultFilepath = path.join(
      Constant.App.uploadTempFolder,
      `${Constant.App.Name}-${processName}-${guid}-result.csv`
    );
    this.currentExportFilename = `${Constant.App.Name}-${processName}-currentExport-${currentDataTimeStr}.csv`;
  }
}

export class CsvProcessResult {
  processEndedAt?: Date;
  processError?: any;
  totalLinesInFile: number = 0;
  totalRecordCount: number = 0;
  validRecordCount: number = 0;
  insertedRecordCount: number = 0;
  updatedRecordCount: number = 0;
  inValidRecordCount: number = 0;
  nbrLinesWritten: number = 0;
  constructor(readonly processStartedAt: Date = new Date()) {}
}

class CsvSrcRecord {
  srcValErrs: string[] = [];
  srcJson?: any = {};
  srcJsonMd5Hash: string = '';

  constructor(readonly srcLineIndex: number, readonly srcLine: string) {}

  get hasSrcValidationErrors(): boolean {
    return this.srcValErrs.length > 0;
  }
}

export class CsvStageRecord extends CsvSrcRecord {
  recValErrs: string[] = [];
  totalValErrCount: number = 0;
  recBatchId: string = '';
  recAction: number = 0;
  recPkValue: string = '';
  recCreatedAt = new Date();

  get hasStageValidationErrors(): boolean {
    return this.recValErrs.length > 0;
  }

  async parse(csvSchema: BaseCsvSchema): Promise<void> {
    if (this.srcLine.split(',').length < csvSchema.schemaFields.length) {
      this.srcValErrs.push(`expected [${csvSchema.schemaFields.length}] fields`);
    } else {
      this.srcJson = csvSchema.buildModel(this.srcLine.split(','));
      this.srcJsonMd5Hash = utils.calcHexMd5(this.srcJson);
      this.recPkValue = L.get(this.srcJson, csvSchema.uniqueFieldName, '');
      await this.validate(csvSchema);
    }
  }

  private async validate(csvSchema: BaseCsvSchema): Promise<void> {
    const errMsgs: string[] = [];
    try {
      const resultPromise = Joi.validate(this.srcJson, csvSchema.getValidationSchema(), {
        abortEarly: false,
        presence: 'optional'
      });
      if (resultPromise && resultPromise.error && resultPromise.error.details) {
        const valErrItems: Joi.ValidationErrorItem[] = resultPromise.error.details;
        for (const vmsg of valErrItems) errMsgs.push(vmsg.message);
      }
      // console.log(`errMsgs=`, errMsgs)
      this.srcValErrs.push(...errMsgs);
    } catch (err) {
      logger.error(`validate-method`, err);
    }
  }
}

export interface ICsvRequestParams {
  utility: string;
  processName: ProcessNameEnum;
  originalFileName: string;
  inputFilepath: string;
  guid: string;
}

interface IBaseCsvProcessRequest {
  processResult: CsvProcessResult;
  utility: string;
  processName: ProcessNameEnum;
  originalFileName: string;
  inputFilepath: string;
  guid: string;
}

export interface CsvProcessResponse extends IBaseCsvProcessRequest {
  ingestFileObjectId?: string;
  csvSourceFileObjectId?: string;
  csvResultFileObjectId?: string;
}

export class CsvProcessRequest implements IBaseCsvProcessRequest {
  readonly processDetail: ProcessDetail;
  readonly processResult: CsvProcessResult;

  // 3 properties not in base interface
  readonly filePaths: CsvFilePaths;
  readonly recBatchId: string;
  readonly csvSchema: BaseCsvSchema;

  constructor(
    readonly utility: string,
    readonly processName: ProcessNameEnum,
    readonly originalFileName: string,
    readonly inputFilepath: string,
    readonly guid: string = uuid.v1()
  ) {
    this.processDetail = new ProcessDetail(
      guid,
      processName,
      ProcessActionEnum.CSV_INGEST,
      ProcessStatusEnum.IN_PROGRESS
    );

    this.filePaths = new CsvFilePaths(guid, processName);
    this.recBatchId = `${processName}-${this.processDetail.processAction}-${guid}`;
    this.processResult = new CsvProcessResult();
    this.csvSchema = processName === ProcessNameEnum.SITE ? new SiteCsvSchema() : new DeviceCsvSchema();
  }
}
