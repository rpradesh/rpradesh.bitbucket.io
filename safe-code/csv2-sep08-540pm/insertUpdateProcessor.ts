/* eslint-disable no-console */
/* eslint-disable @typescript-eslint/no-parameter-properties */
import {CsvProcessor} from './CsvProcessor';
import {mongo, logger} from '../../Application';
import {BulkWriteOpResultObject, AggregationCursor} from 'mongodb';
import * as utils from '../../utils';

export class InsertUpdateProcessor {
  constructor(readonly csvProc: CsvProcessor) {}

  async getInsertOrUpdateRecords(recActionValue: number, startLineIndex: number): Promise<AggregationCursor> {
    const endLineIndex = startLineIndex + this.csvProc.csvFeature.batchSize;

    return await mongo
      .utilityDB(this.csvProc.utility)
      .collection('prp_data_stage')
      .aggregate([
        {
          $match: {
            recBatchId: this.csvProc.csvRequest.recBatchId,
            totalValErrCount: 0,
            recAction: recActionValue,
            srcLineIndex: {$gte: startLineIndex, $lt: endLineIndex}
          }
        },
        {$project: {_id: 0, srcJsonMd5Hash: 1, srcJson: 1, recPkValue: 1, recCreatedAt: 1}}
      ]);
  }

  async process(): Promise<void> {
    let insertCount = 0;
    let updateCount = 0;
    let startIndex = 0;

    while (startIndex < this.csvProc.csvRequest.processResult.totalLinesInFile) {
      const insertCursor: AggregationCursor = await this.getInsertOrUpdateRecords(0, startIndex);
      insertCount += await this.createInsertRecs(insertCursor);

      const updateCursor: AggregationCursor = await this.getInsertOrUpdateRecords(1, startIndex);
      updateCount += await this.createUpdateRecs(updateCursor);

      startIndex += this.csvProc.csvFeature.batchSize;
    }

    this.csvProc.csvRequest.processResult.insertedRecordCount = insertCount;
    this.csvProc.csvRequest.processResult.updatedRecordCount = updateCount;
  }

  async createInsertRecs(cursor: AggregationCursor): Promise<number> {
    const batchRecords: any[] = [];

    while (await cursor.hasNext()) {
      const rec = await cursor.next();
      batchRecords.push({
        insertOne: {
          document: {
            srcJsonMd5Hash: rec.srcJsonMd5Hash,
            recCreatedAt: rec.recCreatedAt,
            recUpdatedAt: rec.recCreatedAt,
            ...rec.srcJson
          }
        }
      });
    }

    return await utils.bulkWrite(this.csvProc.utility, this.csvProc.collectionName, batchRecords);
  }

  async createUpdateRecs(cursor: AggregationCursor): Promise<number> {
    const batchRecords: any[] = [];

    while (await cursor.hasNext()) {
      const rec = await cursor.next();
      batchRecords.push({
        updateOne: {
          filter: {[this.csvProc.collectionPkField]: rec.recPkValue},
          update: {
            $set: {
              srcJsonMd5Hash: rec.srcJsonMd5Hash,
              recUpdatedAt: rec.recCreatedAt,
              ...rec.srcJson
            }
          }
        }
      });
    }

    return await utils.bulkWrite(this.csvProc.utility, this.csvProc.collectionName, batchRecords);
  }
}
