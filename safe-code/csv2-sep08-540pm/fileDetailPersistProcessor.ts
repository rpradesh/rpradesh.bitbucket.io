/* eslint-disable no-console */
/* eslint-disable @typescript-eslint/no-parameter-properties */
import {CsvProcessor} from './CsvProcessor';
import {mongo, logger, Constant} from '../../Application';
import {BulkWriteOpResultObject, AggregationCursor} from 'mongodb';
import * as utils from '../../utils';
import * as moment from 'moment';
import * as path from 'path';
// TODO: common classes are refering to component/classes, need to refactor not to later
import {FilestoreService, IIngestFileDetail, IFileProcessingDetail} from '../../components/Filestore/index';

async function mapToFileIngestDetail(csvProc: CsvProcessor): Promise<IIngestFileDetail> {
  const procResult = csvProc.csvRequest.processResult;

  const hasError = procResult.inValidRecordCount > 0;
  const procStart = moment(procResult.processStartedAt);
  const procEnd = moment(procResult.processEndedAt);
  const resultFileStat = await utils.fileStat(csvProc.csvRequest.inputFilepath, true);

  logger.debug(`procStart=`, procStart, `procEnd=`, procEnd);
  const ingestFile: IIngestFileDetail = {
    fileName: csvProc.csvRequest.originalFileName,
    fileProcessingDate: procResult.processStartedAt,
    fileUploadDate: moment(procResult.processStartedAt)
      .startOf('day')
      .toDate(),
    fileProcessingTime: moment.duration(procEnd.diff(procStart)).asMilliseconds(),
    fileSize: resultFileStat.sizeInKB,
    fileProcessingStatus: hasError === true ? 'HAS_ERRORS' : 'HAS_NO_ERROS',
    fileType: `${Constant.App.Name}-${csvProc.csvRequest.processName}-${csvProc.csvRequest.processDetail.processAction}`,
    fileChecksum: resultFileStat.fileMd5Checksum,
    fileNumberOfLines: procResult.totalRecordCount,
    fileNumberOfLinesProcessed: procResult.validRecordCount,
    fileNumberOfLinesFailed: procResult.inValidRecordCount,
    fileNumberDataValues: csvProc.csvRequest.csvSchema.schemaFields.length,
    fileGUID: csvProc.csvRequest.guid
  };

  return ingestFile;
}

async function mapToFileProcessDetail(
  csvProc: CsvProcessor,
  FILE_TYPE: string,
  origFilename: string
): Promise<IFileProcessingDetail> {
  const procResult = csvProc.csvRequest.processResult;
  const hasError = procResult.inValidRecordCount > 0;
  const procStart = moment(procResult.processStartedAt);

  const procDetail: IFileProcessingDetail = {
    fileName: origFilename,
    fileType: `${Constant.App.Name}-${csvProc.csvRequest.processName}-${FILE_TYPE}`,
    processingDate: procResult.processStartedAt,
    success: !(hasError === true),
    errorCode: hasError === true ? 'HAS_ERRORS' : 'HAS_NO_ERROS',
    errorDescription:
      hasError === true
        ? `File was processed successfully, but ${procResult.inValidRecordCount} records had validation errors`
        : 'File was processed successfully with no exceptions',
    fileUploadDate: moment(procStart)
      .startOf('day')
      .toDate()
  };

  return procDetail;
}

export class FileDetailPersistProcessor {
  constructor(readonly csvProc: CsvProcessor) {}

  async process(): Promise<void> {
    // insert into IngestDetail
    const ingestFileDetail: IIngestFileDetail = await mapToFileIngestDetail(this.csvProc);
    const ingestDetailObjectId = await FilestoreService.saveIngestDetail(ingestFileDetail);
    this.csvProc.csvResponse.ingestFileObjectId = ingestDetailObjectId;

    // insert source- into Proc Detail
    const procDetailOfSource: IFileProcessingDetail = await mapToFileProcessDetail(
      this.csvProc,
      'SOURCE',
      path.basename(this.csvProc.csvRequest.filePaths.sourceFilepath)
    );
    procDetailOfSource['ingest_files_id'] = ingestDetailObjectId;

    const sourceFileObjectId = await FilestoreService.saveProcessDetail(procDetailOfSource);
    this.csvProc.csvResponse.csvSourceFileObjectId = sourceFileObjectId;

    // insert result- into Proc Detail
    const procDetailOfResult: IFileProcessingDetail = await mapToFileProcessDetail(
      this.csvProc,
      'RESULT',
      path.basename(this.csvProc.csvRequest.filePaths.resultFilepath)
    );
    procDetailOfResult['ingest_files_id'] = ingestDetailObjectId;

    const resultFileObjectId = await FilestoreService.saveProcessDetail(procDetailOfResult);
    this.csvProc.csvResponse.csvResultFileObjectId = resultFileObjectId;
  }
}
