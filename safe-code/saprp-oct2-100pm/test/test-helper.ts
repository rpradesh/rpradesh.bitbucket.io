/* eslint-disable no-console */
import * as shell from 'shelljs';
import * as fs from 'fs';

export const TestUrl = 'mongodb://localhost:27017';
export const TestUtility = 'saprp1';

export function log(msg: string): void {
  fs.appendFileSync('./test.log', `${msg}\n`);
}

export function executeMongoCmd(cmd: string): void {
  shell.exec(`mongo localhost:27017/verdeeco_${TestUtility}_db --quiet --eval '${cmd}' `);
}

export function countWords(lines: string, word: string): number {
  const regex = new RegExp(word, 'g');
  const count = ((lines || '').match(regex) || []).length;
  log(`count=${count}; lines=${lines}`);
  return count;
}

function countWordsOld(lines: string, word: string): number {
  const count = lines.split(word).length;
  log(`count=${count}; lines=${lines}`);
  return count;
}

export function deleteFile(filename: string): void {
  // delete file if exists
  if (fs.existsSync(filename)) fs.unlinkSync(filename);
}

export function writeJsonArrayToFile(records: any[], filename: string): void {
  // delete file if exists
  deleteFile(filename);
  const lines: string[] = records.map(rec => JSON.stringify(rec));

  fs.appendFileSync(filename, '[');
  fs.appendFileSync(filename, lines.join(',\n'));
  fs.appendFileSync(filename, ']');
}
