/* eslint-disable no-console */
import {expect, test} from '@oclif/test';
import {TestUrl, TestUtility, executeMongoCmd, countWords, log, writeJsonArrayToFile, deleteFile} from '../test-helper';
import * as fs from 'fs';
import * as jp from 'jmespath';
import {DataGenTypeEnum as tE} from '../../src/common';

function fName(key: string): string {
  return `test--${key}--dategen.json`;
}

function loadJson(key: string): any {
  return JSON.parse(fs.readFileSync(fName(key), 'utf8'));
}

function buildArgsLine(type: tE, nbrRecords: number): string[] {
  const cmdArray = ['datagen', '-t', type, '-f', fName(type), '-n', '' + nbrRecords, '-d', '10'];
  log(`cmdArray=${cmdArray}`);

  return cmdArray;
}

describe('datagen tests for all types', () => {
  before(async () => {
    for (const k in tE) {
      const filename = fName(k);
    }
  });

  after(async () => {
    for (const k in tE) {
      const filename = fName(k);
      deleteFile(filename);
    }
  });

  test
    .stdout()
    .command(['datagen', '-t', tE.prp_site, '-f', fName(tE.prp_site), '-n', '5', '-i', '2000000'])
    .it(`should generate 5 records in file: ${fName(tE.prp_site)}`, ctx => {
      expect(ctx.stdout).contain('writeCount=5;');
      const data = loadJson(tE.prp_site);
      expect(jp.search(data, 'length([*].siteId)')).equal(5);
      expect(jp.search(data, 'min([*].siteId)')).equal('sid2000000');
      expect(jp.search(data, 'max([*].siteId)')).equal('sid2000004');
    });

  test
    .stdout()
    .command(['datagen', '-t', tE.prp_device, '-f', fName(tE.prp_device), '-n', '10', '-i', '3000000'])
    .it(`should generate 10 records in file: ${fName(tE.prp_device)}`, ctx => {
      expect(ctx.stdout).contain('writeCount=10;');
      const data = loadJson(tE.prp_device);
      expect(jp.search(data, 'length([*].deviceId)')).equal(10);
      expect(jp.search(data, 'min([*].deviceId)')).equal('deviceId3000000');
      expect(jp.search(data, 'max([*].deviceId)')).equal('deviceId3000009');
    });

  test
    .stdout()
    .command(['datagen', '-t', tE.device, '-f', fName(tE.device), '-n', '15', '-i', '4000000'])
    .it(`should generate 15 records in file: ${fName(tE.device)}`, ctx => {
      expect(ctx.stdout).contain('writeCount=15;');
      const data = loadJson(tE.device);
      expect(jp.search(data, 'length([*])')).equal(15);
      expect(jp.search(data, 'min([*]._id.deviceNumber)')).equal(4000000);
      expect(jp.search(data, 'max([*]._id.deviceNumber)')).equal(4000014);
    });

  test
    .stdout()
    .command(['datagen', '-t', tE.prp_devicedata, '-f', fName(tE.prp_devicedata), '-n', '10', '-i', '5000000'])
    .it(`should generate 10 records in file: ${fName(tE.prp_devicedata)}`, ctx => {
      expect(ctx.stdout).contain('writeCount=10;');
      const data = loadJson(tE.prp_devicedata);
      expect(jp.search(data, 'length([*])')).equal(10);
      expect(jp.search(data, 'min([*].deviceId)')).equal('5000000');
      expect(jp.search(data, 'max([*].deviceId)')).equal('5000009');
    });

  test
    .stdout()
    .command(['datagen', '-t', tE.daily_record, '-f', fName(tE.daily_record), '-n', '10', '-i', '6000000', '-d', '5'])
    .it(`should generate 10 records in file: ${fName(tE.daily_record)}`, ctx => {
      expect(ctx.stdout).contain('writeCount=50;');
      const data = loadJson(tE.daily_record);
      expect(jp.search(data, 'length([*])')).equal(50);
      expect(jp.search(data, 'min([*].device)')).equal(6000000);
      expect(jp.search(data, 'max([*].device)')).equal(6000009);

      expect(jp.search(data, 'length([0].registers[*])')).equal(24); //should be 24 hours, for first record
      expect(jp.search(data, 'length([49].registers[*])')).equal(24); //should be 24 hours, for last record
    });

  test
    .stdout()
    .command(['datagen', '-t', tE.prp_alarm, '-f', fName(tE.prp_alarm), '-n', '10', '-i', '7000000', '-d', '5'])
    .it(`should generate 10 records in file: ${fName(tE.prp_alarm)}`, ctx => {
      expect(ctx.stdout).contain('writeCount=50;');
      const data = loadJson(tE.prp_alarm);
      expect(jp.search(data, 'length([*])')).equal(50);
      expect(jp.search(data, 'min([*].device)')).equal(7000000);
      expect(jp.search(data, 'max([*].device)')).equal(7000009);

      expect(jp.search(data, 'length([0].alarms[*])')).equal(12); //should be 12 hours, for first record
      expect(jp.search(data, 'length([49].alarms[*])')).equal(12); //should be 12 hours, for last record
    });

  test
    .stdout()
    .command(['datagen', '-t', tE.prp_reads, '-f', fName(tE.prp_reads), '-n', '10', '-i', '8000000', '-d', '5'])
    .it(`should generate 10 records in file: ${fName(tE.prp_reads)}`, ctx => {
      expect(ctx.stdout).contain(`writeCount=${10 * 5 * 2};`);
      const data = loadJson(tE.prp_reads);
      expect(jp.search(data, 'length([*])')).equal(10 * 5 * 2);
      expect(jp.search(data, 'min([*].device)')).equal('8000000');
      expect(jp.search(data, 'max([*].device)')).equal('8000009');

      expect(jp.search(data, 'length([0].reads[*])')).equal(24); //should be 24 hours, for first record
      expect(jp.search(data, 'length([49].reads[*])')).equal(24); //should be 24 hours, for last record
    });
});
