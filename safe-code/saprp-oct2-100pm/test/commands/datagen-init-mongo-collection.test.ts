/* eslint-disable no-console */
import {expect, test} from '@oclif/test';
import {TestUrl, TestUtility, executeMongoCmd, countWords, log, writeJsonArrayToFile, deleteFile} from '../test-helper';
import * as L from 'lodash';
import {DataGenTypeEnum as tE} from '../../src/common';

const testColName = 'prp_site_3';

describe('datagen-init-mongo-collection', () => {
  before(async () => {
    for (const k in tE) {
      executeMongoCmd(`db.${k}.deleteMany({})`);
    }
  });
 
  test
    .stdout()
    .command(['datagen-init-mongo-collection', '-m', TestUrl, '-t', tE.prp_site, '-u', TestUtility, '-i', '100', '-n', '10'])
    .it(`should load 10 records into col: ${testColName}`, ctx => {
      expect(ctx.stdout).contain('beforeRecCount=0;');
      expect(ctx.stdout).contain('deleteCount=0;');
      expect(ctx.stdout).contain('updateCount=0;');
      expect(ctx.stdout).contain('insertCount=10;'); 
      expect(ctx.stdout).contain('currentRecCount=10;');
    });

  test
    .stdout()
    .command(['datagen-init-mongo-collection', '-t', tE.prp_site, '-u', TestUtility, '-i', '200', '-n', '10'])
    .it(`should insert additional 10 , for same by "_id" into col: ${testColName}`, ctx => {
      expect(ctx.stdout).contain('beforeRecCount=10;');
      expect(ctx.stdout).contain('deleteCount=0;');
      expect(ctx.stdout).contain('updateCount=0;');
      expect(ctx.stdout).contain('insertCount=10;');
      expect(ctx.stdout).contain('currentRecCount=20;');
    });

  test
    .stdout()
    .command([
      'datagen-init-mongo-collection',
      '-t',
      tE.prp_site,
      '-u',
      TestUtility,
      '-i',
      '200',
      '-n',
      '10',
      '--pkfield',
      'siteId'
    ])
    .it(`should update 200 siteId's records, not insert due to "pkfield": ${testColName}`, ctx => {
      expect(ctx.stdout).contain('beforeRecCount=20;');
      expect(ctx.stdout).contain('deleteCount=0;');
      expect(ctx.stdout).contain('updateCount=10;');
      expect(ctx.stdout).contain('insertCount=0;');
      expect(ctx.stdout).contain('currentRecCount=20;');
    });

  test
    .stdout()
    .command([
      'datagen-init-mongo-collection',
      '-t',
      tE.prp_site,
      '-u',
      TestUtility,
      '-i',
      '200',
      '-n',
      '10',
      '--pkfield',
      'siteId',
      '--empty',
      'true'
    ])
    .it(`should update 200 siteId's records, not insert due to "pkfield": ${testColName}`, ctx => {

        expect(ctx.stdout).contain('beforeRecCount=20;');
      expect(ctx.stdout).contain('deleteCount=20;');
      expect(ctx.stdout).contain('updateCount=0;');
      expect(ctx.stdout).contain('insertCount=10;');
      expect(ctx.stdout).contain('currentRecCount=10;');
    });
});
