import {expect, test} from '@oclif/test';

describe('hello', () => {
  test
    .stdout()
    .command(['hello'])
    .it('runs hello', ctx => {
      expect(ctx.stdout).to.contain('hello world');
    });

  test
    .stdout()
    .command(['hello', '--name', 'B6jeff'])
    .it('runs hello --name B6jeff', ctx => {
      expect(ctx.stdout).to.contain('hello B6jeff');
    });
});
