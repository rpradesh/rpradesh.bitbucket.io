/* eslint-disable no-console */
import {expect, test} from '@oclif/test';
import {TestUrl, TestUtility, executeMongoCmd, countWords, log} from '../test-helper';
import * as L from 'lodash';
import * as cmn from '../../src/common';

describe('mongo-display-records', () => {
  before(async () => {
    // let us init a dummy table
    executeMongoCmd(`db.prp_site_1.deleteMany({})`);

    L.range(0, 15).forEach(i => {
      executeMongoCmd(`db.prp_site_1.insert({ theDate: "${new Date()}", index: ${i} })`);
    });
  });

  test
    .stdout()
    .command(['mongo-display-records', '-m', TestUrl, '-u', TestUtility, '-c', 'prp_site_1'])
    .it('should display default 10 records only', ctx => {
      const currentYear = '' + new Date().getFullYear();
      log(`currentYear=${currentYear}`);
      log(`ctx.stdout=${ctx.stdout}`);
      expect(countWords(ctx.stdout, 'Index')).equal(1); // for header row
      expect(countWords(ctx.stdout, currentYear)).equal(10); // for data rows
    });

  test
    .stdout()
    .command(['mongo-display-records', '-m', TestUrl, '-u', TestUtility, '-c', 'prp_site_1', '--recordcount', '4'])
    .it('should display 4 records only, as set by new flag "recordscount" ', ctx => {
      const currentYear = '' + new Date().getFullYear();
      expect(countWords(ctx.stdout, 'Index')).equal(1); // for header row
      expect(countWords(ctx.stdout, currentYear)).equal(4); // for data rows
    });

  test
    .stdout()
    .command(['mongo-display-records', '-m', TestUrl, '-u', TestUtility, '-c', 'prp_site_ZZ'])
    .it('should display no records, for non-existing collection', ctx => {
      const currentYear = '' + new Date().getFullYear();
      log(`currentYear=${currentYear}`);
      log(`ctx.stdout=${ctx.stdout}`);
      expect(countWords(ctx.stdout, 'Index')).equal(0); // for header row
      expect(countWords(ctx.stdout, currentYear)).equal(0); // for data rows
    });
});
