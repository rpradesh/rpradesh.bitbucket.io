/* eslint-disable no-console */
import Command from '../common/base';
import * as Listr from 'listr';
import * as mu from '../mongo';
import {buildFlagsFor, buildMongoModel} from '../common';

export default class Mongo extends Command {
  static description = 'Mongo: Check Connection';
  static args = [{name: 'mongo-check-connection'}];
  static flags = {...buildFlagsFor(['help', 'mongos'])};

  static examples = [
    `$ saprp mongo-check-connection -m "mongodb://localhost:27099"    
    or have MONGO_URL set as mongodb://localhost:27017
    saprp mongo-check-connection 
    `
  ];

  async run() {
    const {args, flags} = this.parse(Mongo);
    const mongoArgs = buildMongoModel(flags);

    const tasks = new Listr([
      {
        title: `Checking Mongo Connection at: ${mongoArgs.mongoUrl}`,
        task: async () => await mu.checkConnection(mongoArgs)
      }
    ]);

    try {
      await tasks.run();
      this.log(`connected=true;`);
    } catch (err) {
      this.log(`connected=false; msg=${err}`);
    }
  } //end-run
}
