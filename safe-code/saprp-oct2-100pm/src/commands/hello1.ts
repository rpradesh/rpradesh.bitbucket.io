/* eslint-disable no-console */
import {flags} from '@oclif/command';
import {BaseCommand} from '../common/base-command';
import * as ora from 'ora';
import * as cmn from '../common';
export default class Hello extends BaseCommand {
  static description = 'describe the command here';

  static examples = [
    `$ saprp hello
hello world from ./src/hello.ts!
`
  ];

  static flags = {
    help: flags.help({char: 'h'}),
    name: flags.string({char: 'n', description: 'name to print'}),
    force: flags.boolean({char: 'f'})
  };

  static args = [{name: 'file'}];

  async run() {
    const {args, flags} = this.parse(Hello);
    let result = '';

    try {
      this.setStatusText('step 1');
      await cmn.sleepSecs(1);
      this.appendResult('collection', 'device');

      // real logic
      result = flags.name || 'world';

      this.setStatusText('step 2');
      await cmn.sleepSecs(1);
      this.appendResult('pkvalue', result);
    } catch (err) {
      this.error(err);
    } finally {
      this.stopStatus();
      this.printResult();
    }
  }
}
