/* eslint-disable no-console */
import Command from '../common/base';
import * as mu from '../mongo';
import {buildFlagsFor, buildMongoModel} from '../common';
import * as Listr from 'listr';

export default class Mongo extends Command {
  static description = 'Mongo: Delete all Records in given Collection';
  static args = [{name: 'mongo-delete-records'}];
  static flags = {...buildFlagsFor(['help', 'mongos', 'utility', 'collection'])};

  static examples = [
    `$ saprp mongo-delete-records -u prp1 -c prp_process
    or with mongo url
    saprp mongo-delete-records -m "mongodb://localhost:27099" -u prp1 -c prp_process
    `
  ];

  async run() {
    const {args, flags} = this.parse(Mongo);
    const mongoArgs = buildMongoModel(flags);
    let deleteCount = 0;
    const tasks = new Listr([
      {
        title: `Delete Count of: ${mongoArgs.displayCollectionName}`,
        task: async () => {
          deleteCount = await mu.clearCollection(mongoArgs);
        }
      }
    ]);
    await tasks.run();

    this.log(`delete count of ${mongoArgs.displayCollectionName}=${deleteCount};`);
  } //end-run
}
