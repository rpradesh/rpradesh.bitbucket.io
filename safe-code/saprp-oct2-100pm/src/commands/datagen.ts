/* eslint-disable no-console */
import * as path from 'path';
import * as ora from 'ora';
import * as Listr from 'listr';
import * as utils from '../utils';
import Command from '../common/base';
import {getDatagenInstance} from '../datagen';
import {DatagenArgs, DataGenTypeEnum, buildFlagsFor, buildDatagenModel} from '../common';
import {promisify} from 'util';
export default class Datagen extends Command {
  static genTypes = utils.enumValueAsArray(DataGenTypeEnum);
  static description = `Generate data for following types: ${Datagen.genTypes.join(',')}`;
  static args = [{name: 'datagen'}];
  static flags = {
    ...buildFlagsFor(['type', 'startindex', 'recordcount', 'output', 'days', 'notify']),
    ...buildFlagsFor(['help', 'file'])
  };

  static examples = [
    `$ saprp datagen -t PrpSite -i 1000 -n 15 -f ~/data/gen-data/test_prpsite_v1.json 
    `
  ];

  async run() {
    const {args, flags} = this.parse(Datagen);
    const datagenArgs: DatagenArgs = buildDatagenModel(flags);
    const dgInstance = getDatagenInstance(datagenArgs);

    const dirPath = path.dirname(datagenArgs.file);
    if (!(await utils.isDirectory(dirPath))) {
      throw new Error(`${dirPath} is not valid dir`);
    }

    /*
    const tasks = new Listr([
      {
        title: `Generating ${datagenArgs.json ? 'json' : 'csv'} for type: ${datagenArgs.type}`,
        task: () => dgInstance.generate()
      }
    ]);

    await tasks.run();
    */
    const o = ora('').start();
    dgInstance.generate();
    o.stop();
    this.log(`writeCount=${dgInstance.getWriteRecordCount()};`);
  } //end-run
}
