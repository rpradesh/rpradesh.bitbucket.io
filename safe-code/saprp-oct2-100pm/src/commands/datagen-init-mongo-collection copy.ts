/* eslint-disable no-console */
import Command from '../common/base';
import * as mu from '../mongo';
import * as utils from '../utils';
import * as Listr from 'listr';
import {getDatagenInstance} from '../datagen';
import {DatagenArgs, DataGenTypeEnum, buildFlagsFor, buildDatagenModel} from '../common';

export default class MongoDatagen extends Command {
  static description = 'Mongo: Init  Mongo Collection Directly without generating Json file into given Collection Name';
  static args = [{name: 'datagen-init-mongo-collection'}];
  static flags = {
    ...buildFlagsFor(['help', 'mongos', 'utility']),
    ...buildFlagsFor(['type', 'startindex', 'recordcount', 'output', 'days', 'notify', 'pkfield', 'empty'])
  };
  static examples = [
    `$ saprp datagen-init-mongo-collection -t prp_site -i 1000000 -n 10 -u prp1 --days 30    
    `
  ];

  async run() {
    const {args, flags} = this.parse(MongoDatagen);
    const mongoDatagenArgs: DatagenArgs = buildDatagenModel(flags);
    // we overwrite direct as true here
    mongoDatagenArgs.doDirectPublish = true;
    // update collection name from type
    let mongoArgs: DatagenArgs = mongoDatagenArgs;
    mongoArgs.mongoUrl = flags.mongos;
    mongoArgs.utility = flags.utility;
    mongoArgs.col = mongoDatagenArgs.type;

    // tasks results
    let deleteCount = 0;
    let beforeRecCount = 0;
    let afterRecCount = 0;
    const dgInstance = getDatagenInstance(mongoArgs);
    // tasks to execute
    const tasks = new Listr([
      {
        title: `Checking Mongo Connection at: ${mongoArgs.mongoUrl}`,
        task: async () => await mu.checkConnection(mongoArgs)
      },
      {
        title: `Before Count of: ${mongoArgs.displayCollectionName} `,
        task: async () => {
          beforeRecCount = await mu.getCollectionCount(mongoArgs);
        }
      },
      {
        title: `Do Empty (${mongoArgs.doEmpty}): ${mongoArgs.displayCollectionName} `,
        task: async () => {
          if (mongoArgs.doEmpty === 'true') deleteCount = await mu.clearCollection(mongoArgs);
        }
      },
      {
        title: `Loading into Collection: ${mongoArgs.displayCollectionName}`,
        task: async () => {
          mongoArgs = await dgInstance.persistIntoMongo();
        }
      },
      {
        title: `After Count of: ${mongoArgs.displayCollectionName} `,
        task: async () => {
          afterRecCount = await mu.getCollectionCount(mongoArgs);
        }
      }
    ]);

    await tasks.run();

    // print results
    console.log(
      ` ${mongoArgs.displayCollectionName} 
        beforeRecCount=${beforeRecCount}; 
        deleteCount=${deleteCount}; 

        updateCount=${mongoArgs.upsertCount}; 
        insertCount=${mongoArgs.insertCount};
        currentRecCount=${afterRecCount}; 
      `
    );
  } //end-run
}
