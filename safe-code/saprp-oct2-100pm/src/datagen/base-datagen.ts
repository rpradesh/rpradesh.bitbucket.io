/* eslint-disable no-console */
import * as fs from 'fs';
import * as L from 'lodash';
import {DatagenArgs, MongoArgs} from '../common';
import * as notifier from 'node-notifier';
import * as mu from '../mongo';
import {MongoClient} from 'mongodb';
import {loadRecordsIntoCollection} from '../mongo/mongo-load-util';

const WriteBatchSize = 100;

export abstract class BaseDatagen {
  readonly datagenArgs: DatagenArgs;
  records: any[] = [];
  private comma = '';
  private writeCount = 0;

  getWriteRecordCount(): number {
    return this.writeCount;
  }

  constructor(datagenArgs: DatagenArgs) {
    this.datagenArgs = datagenArgs;
  }

  generate(): void {
    // remove if file exists
    if (fs.existsSync(this.datagenArgs.file)) {
      fs.unlinkSync(this.datagenArgs.file);
    }

    // to prevent memory hog, let us write at batch size of 100
    this.printStart();

    for (let i = 0; i < this.datagenArgs.recordCount; i++) {
      this.appendNewRecordsForIndex(i);
      if (this.records.length > WriteBatchSize) this.flushRecords();
    }

    // flush any remaining
    this.flushRecords();

    this.printEnd();

    if (this.datagenArgs.doNotify === 'true') {
      notifier.notify({
        title: `Data generator: completed for type=${this.datagenArgs.type}`,
        message: `output type: "${this.datagenArgs.json ? 'json' : 'csv'}" at file: ${this.datagenArgs.file}`
      });
    }
  }

  async persistIntoMongo(): Promise<DatagenArgs> {
    // since this datagenArgs extends mongoArgs
    const client: MongoClient = await mu.getConnection(this.datagenArgs.mongoUrl);

    for (let i = 0; i < this.datagenArgs.recordCount; i++) {
      this.appendNewRecordsForIndex(i);

      // persist now
      const {upsertCount, insertCount} = await loadRecordsIntoCollection(client, this.datagenArgs, this.records);
      this.datagenArgs.upsertCount += upsertCount;
      this.datagenArgs.insertCount += insertCount;

      this.records = [];
    }

    await mu.closeConnection(client);

    return this.datagenArgs;
  }

  private flushRecords(): void {
    if (this.records.length <= 0) return;

    const lines: string[] = this.datagenArgs.json
      ? this.records.map(rec => JSON.stringify(rec))
      : this.buildStringLinesFromJson();

    this.writeCount += lines.length;

    const text = this.datagenArgs.json ? lines.join(',\n') : lines.join('\n');
    fs.appendFileSync(this.datagenArgs.file, this.comma);
    fs.appendFileSync(this.datagenArgs.file, text);
    // init comma for next cycle
    if (this.comma === '') this.comma = this.datagenArgs.json ? ',\n' : '\n';
    // clear it after writing to file
    this.records = [];
  }

  private printStart(): void {
    if (this.datagenArgs.json) fs.appendFileSync(this.datagenArgs.file, '[');
    else if (this.doGenerateHeadersForCsv() && this.getJsonPathToGenerateCsv().length > 0) {
      fs.appendFileSync(
        this.datagenArgs.file,
        `The data for "${this.datagenArgs.type}" was generated on ${new Date()} \n`
      );
      fs.appendFileSync(this.datagenArgs.file, `${this.getJsonPathToGenerateCsv().join(',')} \n`);
    }
  }

  private buildSingleStringLine(rec: any): string {
    const fields: string[] = [];

    this.getJsonPathToGenerateCsv().forEach(path => {
      const fValue: string = '' + L.get(rec, path, '');
      fields.push(fValue.replace(/,/g, '|'));
    });
    return fields.join(',');
  }

  private buildStringLinesFromJson(): string[] {
    const stringLines: string[] = [];

    this.records.forEach(rec => {
      stringLines.push(this.buildSingleStringLine(rec));
    });

    return stringLines;
  }

  private printEnd(): void {
    fs.appendFileSync(this.datagenArgs.file, this.datagenArgs.json ? '\n]' : '\n');
  }

  abstract appendNewRecordsForIndex(currentIndex: number): void;

  getJsonPathToGenerateCsv(): string[] {
    return [];
  }

  doGenerateHeadersForCsv(): boolean {
    return true;
  }
}
