import Command, {flags} from '@oclif/command';
import * as ora from 'ora';

const enable = true;
export abstract class BaseCommand extends Command {
  static usage = `${Command.name} [OPTIONS]`;
  static spinner = ora('').start();

  static resultMap = new Map();

  async catch(err: Error) {
    await super.catch(err);
    process.exit(-1);
  }

  async init() {
    // do some initialization
    BaseCommand.spinner.color = 'green';
  }

  async finally(err: Error) {
    // called after run and catch regardless of whether or not the command errored
    BaseCommand.spinner.stop();
    BaseCommand.spinner.clear();
  }

  // custom methods
  setStatusText(txt: string) {
    BaseCommand.spinner.text = txt;
  }
  stopStatus() {
    BaseCommand.spinner.stop();
  }

  appendResult(key: string, result: string | number | boolean) {
    BaseCommand.resultMap.set(key, new String(result));
  }

  printResult() {
    let output = '';
    for (const pair of BaseCommand.resultMap) output += `${pair[0]}=${pair[1]};`;
    this.log(output);
  }

  get resultMap(): Map<string, string> {
    return BaseCommand.resultMap;
  }
}
