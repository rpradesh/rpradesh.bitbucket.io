/* eslint-disable @typescript-eslint/no-parameter-properties */
import * as uuid from 'uuid';
import * as L from 'lodash';
import {BaseCsvSchema} from '../csv';
import * as utils from '../../utils';
import * as Joi from '@hapi/joi';
import {logger} from '../../Application';

export class CsvProcessOptions {
  public readonly recBatchId: string;
  public readonly resultOutputFilepath: string;

  constructor(
    public readonly csvProcessName: string,
    public readonly inputFilepath: string,
    public readonly skipHeaderLineCount: number = 0,
    public readonly guid: string = uuid.v1(),
    public readonly batchSize: number = 10000
  ) {
    this.recBatchId = `${this.csvProcessName}-${this.guid}`;
    this.resultOutputFilepath = this.inputFilepath + '.output.csv';
  }
}

export class CsvFeatureOptions {
  readonly hasUniqueFieldPath: boolean;
  readonly isPercentFeatureEnabled: boolean;
  private unknownUniqueFieldValue = 'unknownUniqueFieldValue';
  constructor(
    public readonly persistCollectionName: string,
    public readonly pkFieldPath: string = '',
    public readonly doValidateMinPercentDataMatch: boolean = false,
    public readonly validateMinPercentDataMatch: number = 0,
    public readonly doValidateUniqueField: boolean = false
  ) {
    this.hasUniqueFieldPath = !L.isEmpty(this.pkFieldPath);
    this.isPercentFeatureEnabled = !(
      this.doValidateMinPercentDataMatch === false || this.validateMinPercentDataMatch === 0
    );
  }

  getUniqueFieldValue(json: any): string {
    let ufValue: string = this.unknownUniqueFieldValue;
    if (this.hasUniqueFieldPath) {
      ufValue = L.get(json, this.pkFieldPath, this.unknownUniqueFieldValue);
    }
    return ufValue;
  }
}

export class CsvProcessResult {
  processEndedAt?: Date;
  processError?: Error;

  constructor(
    public readonly processStartedAt: Date = new Date(),
    public totalLinesInFile: number = 0, // this consists of records + 2 header lines
    public totalRecordCount: number = 0,
    public validRecordCount: number = 0,
    public insertedRecordCount: number = 0,
    public updatedRecordCount: number = 0,
    public inValidRecordCount: number = 0
  ) {}
}

export class CsvSrcRecord {
  srcValErrs: string[] = [];
  srcValErrCount: number = 0;
  srcJson?: any = {};
  srcJsonMd5Hash: string = '';

  constructor(public readonly srcLineIndex: number, public readonly srcLine: string) {}

  get hasSrcValidationErrors(): boolean {
    return this.srcValErrs.length > 0;
  }
}

export class CsvStageRecord extends CsvSrcRecord {
  recValErrs: string[] = [];
  recValErrCount: number = 0;
  recBatchId: string = '';
  recAction: number = 0;
  recPkValue: string = '';
  recCreatedAt = new Date();

  get hasStageValidationErrors(): boolean {
    return this.recValErrs.length > 0;
  }

  async parse(csvSchema: BaseCsvSchema): Promise<void> {
    if (this.srcLine.split(',').length < csvSchema.schemaFields.length) {
      this.srcValErrs.push(`expected [${csvSchema.schemaFields.length}] fields`);
    } else {
      this.srcJson = csvSchema.buildModel(this.srcLine.split(','));
      this.srcJsonMd5Hash = utils.calcHexMd5(this.srcJson);
      this.recPkValue = L.get(this.srcJson, csvSchema.uniqueFieldName, '');
      await this.validate(csvSchema);
    }
  }

  private async validate(csvSchema: BaseCsvSchema): Promise<void> {
    const errMsgs: string[] = [];
    try {
      const resultPromise = Joi.validate(this.srcJson, csvSchema.getValidationSchema(), {
        abortEarly: false,
        presence: 'optional'
      });
      if (resultPromise && resultPromise.error && resultPromise.error.details) {
        const valErrItems: Joi.ValidationErrorItem[] = resultPromise.error.details;
        for (const vmsg of valErrItems) {
          errMsgs.push(vmsg.message);
        }
      }
      // console.log(`errMsgs=`, errMsgs)
      this.srcValErrs.push(...errMsgs);
    } catch (err) {
      logger.error(`validate-method`, err);
    }
  }
}

// should see error `expected [${this.csvSchema.schemaFields.length}] fields`
// create index for recUniquFieldValue,recBatchId,srcLineIndex, srcValidationErrorCount, recValidationErrorCount
