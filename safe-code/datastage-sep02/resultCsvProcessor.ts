/* eslint-disable no-console */
/* eslint-disable @typescript-eslint/no-parameter-properties */
import {CsvProcessor} from './CsvProcessor';
import {mongo, logger} from '../../Application';
import {BulkWriteOpResultObject, AggregationCursor} from 'mongodb';
import * as utils from '../../utils';
import * as L from 'lodash';
import {siteCsvFieldSchemaArray} from '../../config';
import {deviceCsvFieldSchemaArray} from '../../config';
import {createWriteStream, WriteStream} from 'fs';

async function buildLine(mongoPaths: string[], rec: any): Promise<string> {
  const colData: string[] = [];
  mongoPaths.forEach((path, i) => {
    colData.push(L.get(rec, path, ''));
  });
  return colData.join(',');
}

async function buildErrorContent(rec: any): Promise<string> {
  const errData: string[] = [];
  let totalErrorCount = 0;

  if (rec.srcValErrCount > 0) {
    errData.push(rec.srcValErrs.join(';'));
    totalErrorCount += rec.srcValErrCount;
  }

  if (rec.recValErrCount > 0) {
    errData.push(rec.recValErrs.join(';'));
    totalErrorCount += rec.recValErrCount;
  }

  return `, ${totalErrorCount} , ${errData.join(';').replace(',', '|')} `;
}

export class ResultCsvProcessor {
  constructor(public readonly csvProc: CsvProcessor) {}
  private batchRecords: any[] = [];

  async process(): Promise<number> {
    let lineCount = 0;
    const fileWriteStream: WriteStream = createWriteStream(this.csvProc.csvProcessOptions.resultOutputFilepath);
    const mongoPaths: string[] = utils.parseFieldFromObjectArray(this.csvProc.csvSchema.schemaFields, 'mongoPath');

    /*
    let mongoPaths: string[] = [];
    if (this.csvProc.csvFeatureOptions.persistCollectionName === 'prp_site')
      mongoPaths = utils.parseFieldFromObjectArray(siteCsvFieldSchemaArray, 'mongoPath');
    if (this.csvProc.csvFeatureOptions.persistCollectionName === 'prp_device')
      mongoPaths = utils.parseFieldFromObjectArray(deviceCsvFieldSchemaArray, 'mongoPath');
*/
    logger.info(`mongoPaths=${mongoPaths}`);

    const cursor = await mongo
      .utilityDB('prp1')
      .collection('prp_data_stage')
      .find({}); //recBatchId: 'PRP_SITE_CSV_IMPORT-2f7a3910-cdb3-11e9-979c-ef4971b2d99c'});
    //.find({recBatchId: this.csvProc.csvProcessOptions.recBatchId});

    while (await cursor.hasNext()) {
      const rec = await cursor.next();
      const line = await buildLine(mongoPaths, rec.srcJson);
      const errorContent = await buildErrorContent(rec);
      // logger.info(`line=${line}`);
      fileWriteStream.write(line);
      fileWriteStream.write(errorContent);
      fileWriteStream.write('\n');
      lineCount++;
    }
    return lineCount;
  }
}
