/* eslint-disable @typescript-eslint/no-parameter-properties */

import {mongo, logger} from '../../Application';
import {DataStageInsertProcessor} from './dataStageInsertProcessor';
import {DuplicateCheckProcessor} from './duplicateCheckProcessor';
import {InsertUpdateProcessor} from './insertUpdateProcessor';
import {ResultCsvProcessor} from './resultCsvProcessor';
import {CsvStageRecord} from './model';
import {BaseCsvSchema} from '../csv';
import {CsvProcessOptions, CsvProcessResult, CsvFeatureOptions} from './model';

export class CsvProcessor {
  constructor(
    public readonly csvSchema: BaseCsvSchema,
    public readonly csvProcessOptions: CsvProcessOptions,
    public readonly csvFeatureOptions: CsvFeatureOptions,
    public readonly csvProcessResult: CsvProcessResult = new CsvProcessResult()
  ) {}

  async process(): Promise<void> {
    logger.info(`== starting data ingest csvProcessOptions=${JSON.stringify(this.csvProcessOptions)}`);
    try {
      const dataStageProc = new DataStageInsertProcessor(this);
      const totalLinesInFile = await dataStageProc.process();
      this.csvProcessResult.totalLinesInFile = totalLinesInFile;

      const dupeProc = new DuplicateCheckProcessor(this);
      await dupeProc.process();

      // insert into parent entity
      const insertUpdateProc = new InsertUpdateProcessor(this);
      this.csvProcessResult.insertedRecordCount = await insertUpdateProc.processInsert();
      this.csvProcessResult.updatedRecordCount = await insertUpdateProc.processUpdate();

      await this.processGetCounts();

      const resultCsvProc = new ResultCsvProcessor(this);
      const nbrLinesWritten = await resultCsvProc.process();
      logger.info(`nbrLinesWritten=${nbrLinesWritten}`);
    } catch (err) {
      this.csvProcessResult.processError = err;
      logger.error(err, err);
    } finally {
      this.csvProcessResult.processEndedAt = new Date();
      logger.info(`summary = ${JSON.stringify(this.csvProcessResult)}`);
    }
    return;
  }

  async processGetCounts(): Promise<void> {
    //total inValid records in batch
    this.csvProcessResult.inValidRecordCount = await mongo
      .utilityDB('prp1')
      .collection('prp_data_stage')
      .count({
        recBatchId: this.csvProcessOptions.recBatchId,
        srcValErrCount: {$gt: 0} /*,
        recValErrCount: {$gt: 0}*/
      });

    //valid is diff of above 2
    this.csvProcessResult.validRecordCount =
      this.csvProcessResult.totalRecordCount - this.csvProcessResult.inValidRecordCount;
  }
} //end-class
