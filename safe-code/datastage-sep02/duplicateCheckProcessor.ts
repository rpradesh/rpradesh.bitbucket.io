/* eslint-disable @typescript-eslint/no-parameter-properties */
import {CsvProcessor} from './csvProcessor';
import {mongo, logger} from '../../Application';

export interface IDupeCountRecord {
  dupeCount: number;
  recPkValue: string;
}

export async function updateDupeRecords(recBatchId: string, dupeRec: IDupeCountRecord): Promise<number> {
  const updateOps = await mongo
    .utilityDB('prp1')
    .collection('prp_data_stage')
    .updateMany(
      {
        recPkValue: dupeRec.recPkValue,
        recBatchId: recBatchId
      },
      {
        $push: {
          recValErrs: `record ${dupeRec.recPkValue} has ${dupeRec.dupeCount} duplicates`
        },
        $inc: {
          recValErrCount: 1,
          srcValErrCount: 1
        }
      }
    );

  return updateOps.result.n;
}
export async function fetchDupeRecords(recBatchId: string): Promise<IDupeCountRecord[]> {
  // count quey did not work, gives error so usign agg
  const aggCursor = await mongo
    .utilityDB('prp1')
    .collection('prp_data_stage')
    .aggregate([
      {
        $match: {
          recBatchId: recBatchId,
          srcLineIndex: {
            $ne: null
          }
        }
      },
      {
        $group: {
          _id: {
            recPkValue: '$recPkValue'
          },
          dupeCount: {
            $sum: 1
          }
        }
      },
      {
        $project: {
          recPkValue: '$_id.recPkValue',
          dupeCount: '$dupeCount'
        }
      },
      {
        $match: {
          dupeCount: {
            $gt: 1
          }
        }
      },
      {
        $project: {_id: 0, dupeCount: 1, recPkValue: 1}
      }
    ]);

  return await aggCursor.toArray();
}

export class DuplicateCheckProcessor {
  constructor(public readonly csvProc: CsvProcessor) {}

  async process(): Promise<void> {
    const dupeArray = await fetchDupeRecords(this.csvProc.csvProcessOptions.recBatchId);
    dupeArray.forEach(async (rec, i) => {
      await updateDupeRecords(this.csvProc.csvProcessOptions.recBatchId, rec);
    });
  }
}
