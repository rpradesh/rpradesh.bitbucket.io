/* eslint-disable no-console */
/* eslint-disable @typescript-eslint/no-parameter-properties */
import {CsvProcessor} from './CsvProcessor';
import {mongo, logger} from '../../Application';
import {BulkWriteOpResultObject, AggregationCursor} from 'mongodb';
import * as utils from '../../utils';

export async function getInsertOrUpdateRecords(
  givenRecBatchId: string,
  recActionValue: number
): Promise<AggregationCursor> {
  const aggCursor = await mongo
    .utilityDB('prp1')
    .collection('prp_data_stage')
    .aggregate([
      {
        $match: {
          recBatchId: givenRecBatchId,
          srcValErrCount: 0,
          recValErrCount: 0,
          recAction: recActionValue
        }
      },
      {$project: {_id: 0, srcJsonMd5Hash: 1, srcJson: 1, recPkValue: 1, recCreatedAt: 1}}
    ]);

  return aggCursor;
}

export class InsertUpdateProcessor {
  constructor(public readonly csvProc: CsvProcessor) {}
  private batchRecords: any[] = [];

  async processInsert(): Promise<number> {
    let insertCount = 0;
    const cursor: AggregationCursor = await getInsertOrUpdateRecords(this.csvProc.csvProcessOptions.recBatchId, 0);

    while (await cursor.hasNext()) {
      const rec = await cursor.next();
      this.batchRecords.push({
        insertOne: {
          document: {
            srcJsonMd5Hash: rec.srcJsonMd5Hash,
            recCreatedAt: rec.recCreatedAt,
            recUpdatedAt: rec.recCreatedAt,
            ...rec.srcJson
          }
        }
      });

      if (this.batchRecords.length === this.csvProc.csvProcessOptions.batchSize) {
        insertCount += await this.persistBatch();
      }
    } //end-while

    insertCount += await this.persistBatch();

    return insertCount;
  }

  async processUpdate(): Promise<number> {
    let updateCount = 0;
    const cursor: AggregationCursor = await getInsertOrUpdateRecords(this.csvProc.csvProcessOptions.recBatchId, 1);
    while (await cursor.hasNext()) {
      const rec = await cursor.next();

      this.batchRecords.push({
        updateOne: {
          filter: {[this.csvProc.csvFeatureOptions.pkFieldPath]: rec.recPkValue},
          update: {
            $set: {
              srcJsonMd5Hash: rec.srcJsonMd5Hash,
              recUpdatedAt: rec.recCreatedAt,
              ...rec.srcJson
            }
          }
        }
      });

      if (this.batchRecords.length === this.csvProc.csvProcessOptions.batchSize) {
        updateCount += await this.persistBatch();
      }
    } //end-while

    updateCount += await this.persistBatch();
    return updateCount;
  }

  private async persistBatch(): Promise<number> {
    const count = await utils.bulkWrite(
      'prp1',
      this.csvProc.csvFeatureOptions.persistCollectionName,
      this.batchRecords
    );
    this.batchRecords = [];
    logger.info(`persistBatch count=${count}`);
    return count;
  }
}
