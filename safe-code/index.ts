import {logger} from '../../Application';
import * as L from 'lodash';

export class TaskRunner {
  /**
   * mark running = true
   * loop through utilities
   * for each utility, loop though colectionName and create Index
   * mark running = flase
   */
  async execute(task: Task): Promise<void> {
    let taskResult: any = {};
    try {
      L.assign(taskResult, {startedAt: new Date()});
      taskResult = task.run(taskResult);
    } catch (err) {
      L.assign(taskResult, {errorDetails: err});
      logger.error(err);
    } finally {
      L.assign(taskResult, {completedAt: new Date()});
    }
    return taskResult;
  }
}

export interface Task {
  run(taskResult: any): any;
}

const taskRunner = new TaskRunner();
export {taskRunner};
