/* eslint-disable @typescript-eslint/no-parameter-properties */
import {CommonCsvProcessor} from './CommonCsvProcessor';
import * as utils from '../../utils';
import {CsvStageRecord} from './model';
import {logger} from '../../Application';

export class DataStageInsertProcessor {
  private batchRecords: any[] = [];
  private fileLineCounter: number = 0;
  private insertCount = 0;

  constructor(public readonly csvProc: CommonCsvProcessor) {}

  async process(): Promise<number> {
    for await (const line of utils.readLines(this.csvProc.csvProcessOptions.inputFilepath)) {
      if (this.fileLineCounter < this.csvProc.csvProcessOptions.skipHeaderLineCount) {
        this.fileLineCounter++;
      } else {
        await this.processLine(this.fileLineCounter, line);
        this.fileLineCounter++;
      }
    } //end-for

    // process last not-fully-filled batch records
    await this.persistBatch();
    this.batchRecords = [];

    // update total inserted records n data stage
    this.csvProc.csvProcessResult.totalRecordCount = this.insertCount;

    logger.info(`this.fileLineCounter=${this.fileLineCounter} this.insertCount=${this.insertCount}`);
    return this.fileLineCounter;
  }

  private async processLine(lineIndex: number, srcLine: string): Promise<void> {
    const csvStageRecord: CsvStageRecord = new CsvStageRecord(lineIndex, srcLine.replace(/\r?\n|\r/g, ''));
    await csvStageRecord.parse(this.csvProc.csvSchema);
    this.batchRecords.push(csvStageRecord);
    ///logger.info(`csvStageRecord=${JSON.stringify(csvStageRecord)}`);
    if (this.batchRecords.length === this.csvProc.csvProcessOptions.batchSize) {
      await this.persistBatch();
      this.batchRecords = [];
    }
  }

  private async persistBatch(): Promise<void> {
    const insertArray: any[] = this.batchRecords.map(rec => {
      rec.recBatchId = this.csvProc.csvProcessOptions.recBatchId;
      rec.srcValidationErrorCount = rec.srcValidationErrors.length;
      rec.recUniqueFieldValue = this.csvProc.csvFeatureOptions.getUniqueFieldValue(rec.srcJson);

      return {
        insertOne: {...rec}
      };
    });

    this.insertCount += await utils.bulkWrite('prp1', 'prp_data_stage', insertArray);
  }
}
