/* eslint-disable no-console */
/* eslint-disable @typescript-eslint/no-parameter-properties */
import {CommonCsvProcessor} from './CommonCsvProcessor';
import {mongo, logger} from '../../Application';
import {BulkWriteOpResultObject, AggregationCursor} from 'mongodb';
import * as utils from '../../utils';

export async function getUpdateRecords(givenRecBatchId: string): Promise<AggregationCursor> {
  const aggCursor = await mongo
    .utilityDB('prp1')
    .collection('prp_data_stage')
    .aggregate([
      {
        $match: {
          recBatchId: givenRecBatchId,
          srcValidationErrorCount: 0,
          recValidationErrorCount: 0,
          recAction: 1
        }
      },
      {$project: {_id: 0, srcJsonMd5Hash: 1, recUpdatedAt: '$recCreatedAt', srcJson: 1, recParentObjectId: 1}}
    ]);

  return aggCursor;
}

export class UpdateEntityProcessor {
  constructor(public readonly csvProc: CommonCsvProcessor) {}
  private counter = 0;
  private batchRecords: any[] = [];

  async process(): Promise<number> {
    let updateCount = 0;
    const cursor: AggregationCursor = await getUpdateRecords(this.csvProc.csvProcessOptions.recBatchId);
    while (await cursor.hasNext()) {
      const rec = await cursor.next();

      this.batchRecords.push({
        updateOne: {
          filter: {_id: rec.recParentObjectId},
          update: {
            $set: {
              srcJsonMd5Hash: rec.srcJsonMd5Hash,
              recUpdatedAt: rec.recUpdatedAt,
              ...rec.srcJson
            }
          }
        }
      });

      if (this.batchRecords.length === this.csvProc.csvProcessOptions.batchSize) {
        updateCount += await utils.bulkWrite(
          'prp1',
          this.csvProc.csvFeatureOptions.persistCollectionName,
          this.batchRecords
        );
        this.batchRecords = [];
      }
    } //end-while

    updateCount += await utils.bulkWrite(
      'prp1',
      this.csvProc.csvFeatureOptions.persistCollectionName,
      this.batchRecords
    );
    this.batchRecords = [];

    return updateCount;
  }
}
