/* eslint-disable @typescript-eslint/no-parameter-properties */
import {mongo, logger} from '../../Application';
import {BaseCsvSchema} from '../csv';
import {CsvProcessOptions, CsvProcessResult, CsvFeatureOptions} from './model';

export class BaseCommonCsvProcessor {
  constructor(
    public readonly csvSchema: BaseCsvSchema,
    public readonly csvProcessOptions: CsvProcessOptions,
    public readonly csvFeatureOptions: CsvFeatureOptions,
    public readonly csvProcessResult: CsvProcessResult = new CsvProcessResult()
  ) {}

  protected async processGetCounts(): Promise<void> {
    //total inValid records in batch
    this.csvProcessResult.inValidRecordCount = await mongo
      .utilityDB('prp1')
      .collection('prp_data_stage')
      .count({
        recBatchId: this.csvProcessOptions.recBatchId,
        srcValidationErrorCount: {$gt: 0},
        recValidationErrorCount: {$gt: 0}
      });

    //valid is diff of above 2
    this.csvProcessResult.validRecordCount =
      this.csvProcessResult.totalRecordCount - this.csvProcessResult.inValidRecordCount;
  }

  protected async fetchTotalRowCount(): Promise<void> {
    //first get total Rows for batch
    this.csvProcessResult.totalRecordCount = await mongo
      .utilityDB('prp1')
      .collection('prp_data_stage')
      .countDocuments({recBatchId: this.csvProcessOptions.recBatchId});
  }
} //end-class
