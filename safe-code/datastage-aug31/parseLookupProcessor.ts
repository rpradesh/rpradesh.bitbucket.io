/* eslint-disable @typescript-eslint/no-parameter-properties */
import {CommonCsvProcessor} from './CommonCsvProcessor';
import * as utils from '../../utils';
import * as L from 'lodash';
import {CsvStageRecord} from './model';
import {mongo, logger} from '../../Application';

function updateFilds(rec: CsvStageRecord, foundSiteHash: any): CsvStageRecord {
  rec.recAction = foundSiteHash.srcJsonMd5Hash === rec.srcJsonMd5Hash ? -1 : 1; //if same : not-same
  return rec;
}

export class ParseLookupProcessor {
  private batchRecords: any[] = [];
  private fileLineCounter: number = 0;
  private insertCount = 0;

  private lookupMap: Map<string, any> = new Map();
  private batchCounter = 0;

  constructor(public readonly csvProc: CommonCsvProcessor) {}

  async process(): Promise<number> {
    for await (const line of utils.readLines(this.csvProc.csvProcessOptions.inputFilepath)) {
      if (this.fileLineCounter < this.csvProc.csvProcessOptions.skipHeaderLineCount) {
        this.fileLineCounter++;
      } else {
        ///if (this.batchCounter > 0) return;
        await this.processLine(this.fileLineCounter, line);
        this.fileLineCounter++;
      }
    } //end-for

    // process last not-fully-filled batch records
    await this.persistBatch();
    this.batchRecords = [];

    // update total inserted records n data stage
    this.csvProc.csvProcessResult.totalRecordCount = this.insertCount;

    logger.info(`this.fileLineCounter=${this.fileLineCounter} this.insertCount=${this.insertCount}`);
    return this.fileLineCounter;
  }

  private async processLine(lineIndex: number, srcLine: string): Promise<void> {
    const csvStageRecord: CsvStageRecord = new CsvStageRecord(lineIndex, srcLine.replace(/\r?\n|\r/g, ''));
    await csvStageRecord.parse(this.csvProc.csvSchema);
    this.batchRecords.push(csvStageRecord);
    ///logger.info(`csvStageRecord=${JSON.stringify(csvStageRecord)}`);
    if (this.batchRecords.length === this.csvProc.csvProcessOptions.batchSize) {
      await this.persistBatch();
      this.batchRecords = [];
    }
  }

  private async _createLookupMap(): Promise<void> {
    // run lookup query, by passing the in query
    const pkValueArray: string[] = this.batchRecords.map(rec => rec.recPkValue);
    const arrayOfTupes = await mongo
      .utilityDB('prp1')
      .collection('prp_site')
      .find({siteId: {$in: [...pkValueArray]}})
      .project({_id: 0, siteId: 1, srcJsonMd5Hash: 1})
      .toArray();
    this.lookupMap.clear();
    this.lookupMap = await utils.buildMap(arrayOfTupes, 'siteId');
    ///logger.info(`pkValueArray=${JSON.stringify(pkValueArray)}, pkMap=${JSON.stringify(this.lookupMap)}`);
  }

  private async persistBatch(): Promise<void> {
    logger.info(`metrics persistBatch START=${JSON.stringify(utils.processMetrics())}==`);
    await this._createLookupMap();

    const insertArray: any[] = this.batchRecords.map((rec: CsvStageRecord) => {
      rec.recBatchId = this.csvProc.csvProcessOptions.recBatchId;
      rec.srcValidationErrorCount = rec.srcValidationErrors.length;
      rec.recUniqueFieldValue = this.csvProc.csvFeatureOptions.getUniqueFieldValue(rec.srcJson);

      if (this.lookupMap.has(rec.recUniqueFieldValue))
        rec = updateFilds(rec, this.lookupMap.get(rec.recUniqueFieldValue));
      else rec.recAction = 0;

      return {
        insertOne: {...rec}
      };
    });

    this.insertCount += await utils.bulkWrite('prp1', 'prp_data_stage', insertArray);
    this.batchCounter++;
    logger.info(`metrics persistBatch END=${JSON.stringify(utils.processMetrics())}==`);
  }
}
