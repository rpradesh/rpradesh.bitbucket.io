/* eslint-disable @typescript-eslint/no-parameter-properties */
import {CommonCsvProcessor} from './CommonCsvProcessor';
import {mongo, logger} from '../../Application';

export class PercentMatchProcessor {
  constructor(public readonly csvProc: CommonCsvProcessor) {}

  async process(): Promise<void> {
    if (this.csvProc.csvFeatureOptions.isPercentFeatureEnabled === false) {
      logger.info(`skipping % check for batchId=${this.csvProc.csvProcessOptions.recBatchId}`);
      return;
    }

    const aggCursor = await mongo
      .utilityDB('prp1')
      .collection(this.csvProc.csvFeatureOptions.persistCollectionName)
      .aggregate([
        {
          $lookup: {
            from: 'prp_data_stage',
            localField: this.csvProc.csvFeatureOptions.validateUniqeFieldPath,
            foreignField: 'recUniqueFieldValue',
            as: 'data_stage_docs'
          }
        },
        {$match: {'data_stage_docs.recBatchId': this.csvProc.csvProcessOptions.recBatchId}},
        {$project: {_id: 1}}
      ]);
    const matchingCount = (await aggCursor.toArray()).length;

    const toMeet =
      (this.csvProc.csvProcessResult.totalRecordCount * this.csvProc.csvFeatureOptions.validateMinPercentDataMatch) /
      100;
    logger.info(
      `matchingCount=${matchingCount} toMeet=${toMeet} % batchId=${this.csvProc.csvProcessOptions.recBatchId}`
    );

    if (matchingCount < toMeet)
      throw new Error(`matchingCount=${matchingCount} is less than given threashold to meet ${toMeet}`);
  }
}
