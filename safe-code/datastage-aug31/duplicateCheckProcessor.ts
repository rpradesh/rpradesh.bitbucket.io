/* eslint-disable @typescript-eslint/no-parameter-properties */
import {CommonCsvProcessor} from './CommonCsvProcessor';
import {mongo, logger} from '../../Application';

export interface IDupeCountRecord {
  dupeCount: number;
  recUniqueFieldValue: string;
}

export async function updateDupeRecords(recBatchId: string, dupeRec: IDupeCountRecord): Promise<number> {
  const updateOps = await mongo
    .utilityDB('prp1')
    .collection('prp_data_stage')
    .updateMany(
      {
        recUniqueFieldValue: dupeRec.recUniqueFieldValue,
        recBatchId: recBatchId
      },
      {
        $push: {
          recValidationErrors: `record ${dupeRec.recUniqueFieldValue} has ${dupeRec.dupeCount} duplicates`
        },
        $inc: {
          recValidationErrorCount: 1
        }
      }
    );

  return updateOps.result.n;
}
export async function fetchDupeRecords(recBatchId: string): Promise<IDupeCountRecord[]> {
  // count quey did not work, gives error so usign agg
  const aggCursor = await mongo
    .utilityDB('prp1')
    .collection('prp_data_stage')
    .aggregate([
      {
        $match: {
          recBatchId: recBatchId,
          srcLineIndex: {
            $ne: null
          }
        }
      },
      {
        $group: {
          _id: {
            recUniqueFieldValue: '$recUniqueFieldValue'
          },
          dupeCount: {
            $sum: 1
          }
        }
      },
      {
        $project: {
          recUniqueFieldValue: '$_id.recUniqueFieldValue',
          dupeCount: '$dupeCount'
        }
      },
      {
        $match: {
          dupeCount: {
            $gt: 1
          }
        }
      },
      {
        $project: {_id: 0, dupeCount: 1, recUniqueFieldValue: 1}
      }
    ]);

  return await aggCursor.toArray();
}

export class DuplicateCheckProcessor {
  constructor(public readonly csvProc: CommonCsvProcessor) {}

  async process(): Promise<void> {
    const dupeArray = await fetchDupeRecords(this.csvProc.csvProcessOptions.recBatchId);
    dupeArray.forEach(async (rec, i) => {
      await updateDupeRecords(this.csvProc.csvProcessOptions.recBatchId, rec);
    });
  }
}
