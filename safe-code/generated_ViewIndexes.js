function getCollectionIndexes(col){
    const indexStats = (() => {
        const dbVersion = parseFloat(db.version());
        if (dbVersion >= 3.2) {
            try{
                return db.getCollection(col).aggregate([{ $indexStats: {} }]).toArray()
            }catch(err){
                console.error(err)
                return [];
            }
        }
    })();

    const indexSizes=db.getCollection(col).stats().indexSizes;

    const indexes=db.getCollection(col).getIndexes();

    return indexes.map(it=>{
        const usageStats=(()=>{
            const stats=(indexStats || []).filter(stat=>stat.name===it.name);
            if (_.isEmpty(stats)) return {"usage stats": "not available"};

            const formatAccesses=(it)=>`${it.ops} since ${it.since.toLocaleString()}`
            if (stats.length>1){
                return {
                    "usage stats":stats.reduce((acc, cur, i)=> {
                        acc[i]={
                            host:cur.host,
                            accesses:formatAccesses(cur.accesses)
                        }

                        return acc;
                    }, {})
                }
            }else{
                return {
                    ...stats[0],
                    accesses: formatAccesses(stats[0].accesses),
                }
            }
        })();

        const size=indexSizes[it.name];
        const type=(_.find(_.values(it.key), v=>_.isString(v)) || "regular").toUpperCase();
        const info={
            ...it,
            size,
            type,
            ...usageStats,
        }

        const commonFields=["name","key","type","size", "ns","accesses","usage stats"];

        return _.omitBy({
            ..._.pick(info,commonFields),
            ...{properties: _.omit(info, [...commonFields,"v","host"])},
            ..._.pick(info,"v","host")
        },_.isEmpty)
    })
}

getCollectionIndexes("prp_site_1");