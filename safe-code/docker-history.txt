+ history
    1  whoami
    2  whoami; hostname; ls;
    3  zdump /etc/localtime
    4  zdump /usr/share/zoneinfo/US/Eastern
    5  groupadd -r mongodb && useradd -r -g mongodb mongodb
    6  MONGO_PACKAGE=mongodb-org
    7  MONGO_REPO=repo.mongodb.org
    8  MONGO_MAJOR 3.2
    9  MONGO_VERSION 3.2.22
   10  MONGO_PACKAGE=mongodb-org
   11  MONGO_REPO=repo.mongodb.org
   12  MONGO_MAJOR=3.2
   13  MONGO_VERSION=3.2.22
   14  echo "deb http://$MONGO_REPO/apt/ubuntu xenial/${MONGO_PACKAGE%-unstable}/$MONGO_MAJOR multiverse" | tee "/etc/apt/sources.list.d/${MONGO_PACKAGE%-unstable}.list"
   15  set -x && apt-get update && apt-get install -y --allow-unauthenticated ${MONGO_PACKAGE}=$MONGO_VERSION ${MONGO_PACKAGE}-server=$MONGO_VERSION ${MONGO_PACKAGE}-shell=$MONGO_VERSION ${MONGO_PACKAGE}-mongos=$MONGO_VERSION \
   16  set -x && apt-get update && apt-get install -y --allow-unauthenticated ${MONGO_PACKAGE}=$MONGO_VERSION ${MONGO_PACKAGE}-server=$MONGO_VERSION ${MONGO_PACKAGE}-shell=$MONGO_VERSION ${MONGO_PACKAGE}-mongos=$MONGO_VERSION ${MONGO_PACKAGE}-tools=$MONGO_VERSION
   17  ll /var/lib/mongodb
   18  cat /etc/mongod.conf
   19  which mongod
   20  ll /usr/bin/mon*
   21  echo $PATH
   22  ll /bin
   23  ll /binmkdir -p /data/db
   24  mkdir -p /var/log/mongodb
   25  mkdir -p /data/db
   26  mkdir -p /var/log/mongodb
   27  chown -R mongodb:mongodb /data/db /var/log/mongodb
   28  mongod --dbpath /data/db --logpath /var/log/mongodb/mongod.log --fork
   29  grep connections /var/log/mongodb/mongod.log
   30  mongo --host 127.0.0.1 --port 27017 --quiet
   31  mongo
   32  mongo --help
   33  mongo --host 127.0.0.1 --port 27017 --quiet --eval 'db.version'
   34  mongo --host 127.0.0.1 --port 27017 --quiet --eval 'db.version()'
   35  ps -a | grep mongo
   36  ps -aux | grep mongo
   37  ps -tulnp | grep 27017
   38  ps -tlnp | grep 27017
   39  ps -tlp | grep 27017
   40  ps -lp | grep 27017
   41  netstat -tulnp | grep 27017
   42  ps
   43  kill -9 361
   44  ps -aux | grep mongo
   45  history
