db.prp_site.deleteMany({});

db.prp_site
    .find({})
    //.find({siteId: /sid100000/})
    //.projection({_id:0, siteId:1, srcJsonMd5Hash:1})
    .projection({})
    .sort({ _id: -1 })
    .limit(100);

db.prp_device
    .find({})
    //.find({siteId: /sid100000/})
    //.projection({_id:0, siteId:1, srcJsonMd5Hash:1})
    .projection({})
    .sort({ _id: -1 })
    .limit(100);
    
db.prp_data_stage.find({})
    //.projection({_id:0, siteId:1, srcJsonMd5Hash:1})
    .projection({})
    .sort({ _id: -1 })
    .limit(100);
    
db.ingest_files.find({})
    //.projection({_id:0, siteId:1, srcJsonMd5Hash:1})
    .projection({})
    .sort({ _id: -1 })
    .limit(100);
    
db.prp_site.count([
    {
        $lookup: {
            from: "prp_data_stage",
            localField: "siteId",
            foreignField: "recUniqueFieldValue",
            as: "data_stage_docs"
        }
    },
    { $match: { "data_stage_docs.recBatchId": "SiteIngest-9d643620-c457-11e9-a35a-4169d3eb6f48" } },
    { $project: { _id: 1 } }
]);

// percent match
// db.prp_site.count([
db.prp_site.count([
    {
        $lookup: {
            from: "prp_data_stage",
            localField: "siteId",
            foreignField: "recUniqueFieldValue",
            as: "data_stage_docs"
        }
    },
    { $match: { "data_stage_docs.recBatchId": "SiteIngest-e54df640-c553-11e9-854b-efa9d6099ad0" } },
    { $project: { _id: 1 } }

]);

db.prp_data_stage.aggregate([
    {
        $lookup: {
            from: "prp_site",
            localField: "recUniqueFieldValue",
            foreignField: "siteId",
            as: "site_docs"
        }
    },
    { $match: { "recBatchId": "SiteIngest-18bcc600-c4ff-11e9-894a-e7bca349abd2" } },
    { $unwind: { path: "$site_docs", preserveNullAndEmptyArrays: false } },
    { $project: { _id: 1, "site_docs.srcJsonMd5Hash": 1, srcJsonMd5Hash: 1, "site_docs.siteId": 1, recUniqueFieldValue: 1 } }
]);

db.getCollection("prp_site").deleteMany({siteId:"sid4000009"});
db.getCollection("prp_site").drop();
/*
db.getCollection("prp_site").insert({
    "siteId": "sid1000008",
    "siteName": "siteName 1000008 ngme3gbl0b"
});
*/

db.getCollection("prp_site").insert({
    "siteId": "sid1000009",
    "siteName": "siteName 1000009 ngme3gbl0b"
});

db.getCollection("prp_site").insert({
    "siteId": "sid1000008",
    "siteName": "siteName 1000008 ngme3gbl0b",
    "srcJsonMd5Hash": "3f688a2c191d4002f4edb407a7d50b46"
});


db.getCollection("prp_site").deleteMany({});
db.getCollection("prp_site").count({});
// db.prp_site.renameCollection('prp_site_1000k')
// db.prp_site_1000k.renameCollection('prp_site')
// db.getCollection('prp_site').drop();

db.prp_data_stage.aggregate([
    {
        $lookup: {
            from: "prp_site",
            localField: "recUniqueFieldValue",
            foreignField: "siteId",
            as: "site_docs"
        }
    },
    { $match: { "recBatchId": "SiteIngest-18bcc600-c4ff-11e9-894a-e7bca349abd2" } },
    { $unwind: { path: "$site_docs", preserveNullAndEmptyArrays: false } },
    { $project: { _id: 1, "site_docs.srcJsonMd5Hash": 1, srcJsonMd5Hash: 1, "site_docs.siteId": 1, recUniqueFieldValue: 1 } }
]);

// select for insert
db.prp_data_stage.aggregate([
    { $match: { "recBatchId": "SiteIngest-18bcc600-c4ff-11e9-894a-e7bca349abd2", "srcValidationErrorCount": 0, "recValidationErrorCount": 0, "recAction": 0 } },
    { $project: { _id: 0, srcJsonMd5Hash: 1, recCreatedAt: 1, recUpdatedAt: "$recCreatedAt", siteId: "$srcJson.siteId" } }
]);

// run insert
db.prp_site.insertMany(
    db.prp_data_stage.aggregate([
        { $match: { "recBatchId": "SiteIngest-18bcc600-c4ff-11e9-894a-e7bca349abd2", "srcValidationErrorCount": 0, "recValidationErrorCount": 0, "recAction": 0 } },
        { $project: { _id: 0, srcJsonMd5Hash: 1, recCreatedAt: 1, recUpdatedAt: "$recCreatedAt", siteId: "$srcJson.siteId" } }
    ]).toArray(), { ordered: false }
);

// select for UPDATE
db.prp_data_stage.aggregate([
    { $match: { "recBatchId": "SiteIngest-18bcc600-c4ff-11e9-894a-e7bca349abd2", "srcValidationErrorCount": 0, "recValidationErrorCount": 0, "recAction": 1 } },
    { $project: { _id: "$recParentObjectId", srcJsonMd5Hash: 1, recUpdatedAt: "$recCreatedAt", siteId: "$srcJson.siteId" } }
]);

db.prp_data_stage.count(
    { "recBatchId": "SiteIngest-18bcc600-c4ff-11e9-894a-e7bca349abd2", "srcValidationErrorCount": 0, "recValidationErrorCount": 0, "recAction": 1 }
);

// run UPDATE
db.prp_site.bulkWrite(
    db.prp_data_stage.aggregate([
        { $match: { "recBatchId": "SiteIngest-18bcc600-c4ff-11e9-894a-e7bca349abd2", "srcValidationErrorCount": 0, "recValidationErrorCount": 0, "recAction": 1 } },
        { $project: { _id: "$recParentObjectId", srcJsonMd5Hash: 1, recUpdatedAt: "$recCreatedAt", siteId: "$srcJson.siteId" } }
    ]).toArray(), { ordered: false }
);

db.prp_site.bulkWrite(
    [{ "insertOne": { "document": { "srcJsonMd5Hash": "3e803f693afb475735992082d1b01c7b", "recCreatedAt": "2019-08-23T01:07:13.533Z", "recUpdatedAt": "2019-08-23T01:07:13.533Z", "siteId": "sid1000000", "siteName": "siteName 1000000 3p0z9l47fm", "siteType": "Valve", "insertDate": "2018-06-11", "lat": "36.71636", "lng": "-79.09958", "pressureZone": "prZone-low", "elevation": "927", "geoHash": "dnxbbh12q", "elevationOffset": "16", "address": { "line1": "8481 Deangelo Route", "line2": "Suite 635", "city": "Marisaview", "state": "South Dakota", "zip": "34573-9050" } } } },
    { "insertOne": { "document": { "srcJsonMd5Hash": "8289c5ecf8c69752c27e9e1df1b2fa6d", "recCreatedAt": "2019-08-23T01:07:13.537Z", "recUpdatedAt": "2019-08-23T01:07:13.537Z", "siteId": "sid1000001", "siteName": "siteName 1000001 mqusw3ht6v", "siteType": "PRV", "insertDate": "2016-08-30", "lat": "35.89699", "lng": "-81.67647", "pressureZone": "prZone-low", "elevation": "315", "geoHash": "dnmujqv50", "elevationOffset": "16", "address": { "line1": "0464 Kautzer Inlet", "line2": "Apt. 121", "city": "South Shayne", "state": "South Dakota", "zip": "04563-7045" } } } }]
    , { ordered: false });
