db.prp_site.createIndex({ siteId: 1 }, {
    name: 'prpsite_siteidA_unique',
    unique: true
});

db.prp_device.createIndex({ deviceId: 1 }, {
    name: 'prpsite_deviceidA_unique',
    unique: true
});

db.prp_process.createIndex({ processId: 1 }, {
    name: 'prpprocess_processidA_unique',
    unique: true
});

let dataStageFields = ['srcLineIndex','srcJsonMd5Hash','totalValErrCount','recBatchId' ,'recAction' ,'recPkValue','recCreatedAt'];

dataStageFields.forEach( (v,i) => {
    db.prp_data_stage.createIndex({ [v]: 1 }, {
        name: 'prpDataStage_' + v + '_index'
    })
});

let dataStageFields = ['srcLineIndex','srcJsonMd5Hash','totalValErrCount','recBatchId' ,'recAction' ,'recPkValue','recCreatedAt'];

// print index quiries
dataStageFields.forEach( (v,i) => {
    print( `{
    "collection": "prp_data_stage",
    "keys": {"${v}": 1},
    "options": {"name": "prpdatastage_${v.toLowerCase()}_A")}
  },`)
});


db.prp_site.getIndexes();
db.prp_site.dropIndexes();

db.prp_device.getIndexes();
db.prp_device.dropIndexes();

db.prp_data_stage.getIndexes();
db.prp_data_stage.dropIndexes();
