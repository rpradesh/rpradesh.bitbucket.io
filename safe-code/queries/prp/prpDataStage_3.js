db.prp_site.count({});
db.prp_data_stage.count({});

db.prp_site
    .find({})
    //.find({siteId: /sid100000/})
    //.projection({_id:0, siteId:1, srcJsonMd5Hash:1})
    .projection({})
    .sort({ _id: -1 })
    .limit(100);

db.prp_device
    .find({})
    //.find({siteId: /sid100000/})
    //.projection({_id:0, siteId:1, srcJsonMd5Hash:1})
    .projection({})
    .sort({ _id: -1 })
    .limit(100);
    
db.prp_data_stage.find({})
    //.projection({_id:0, siteId:1, srcJsonMd5Hash:1})
    .projection({})
    .sort({ _id: -1 })
    .limit(100);
    
db.ingest_files.find({})
    //.projection({_id:0, siteId:1, srcJsonMd5Hash:1})
    .projection({})
    .sort({ _id: -1 })
    .limit(100);

db.file_processing_details.find({})
    //.projection({_id:0, siteId:1, srcJsonMd5Hash:1})
    .projection({})
    .sort({ _id: -1 })
    .limit(100);

db.prp_site.find({
    "siteId": { "$in": ["sid4999999", "sid4999998", "sid4999997"] }
})
    .projection({ _id: 0, siteId: 1, srcJsonMd5Hash: 1 });

db.prp_site_10.aggregate([
    {
        $lookup: {
            from: "prp_site_10",
            localField: "siteId",
            foreignField: "siteId",
            as: "site_docs"
        }
    },
    { $match: { "siteId": { $in: ["sid2000005", "sid2000006", "sid2000007"] } } },
    { $unwind: { path: "$site_docs", preserveNullAndEmptyArrays: true } },
    { $project: { _id: 1, "site_docs.srcJsonMd5Hash": 1, srcJsonMd5Hash: 1, "site_docs.siteId": 1, recUniqueFieldValue: 1 } }
]);

db.inventory.aggregate(
    [
        {
            $project:
                {
                    item: 1,
                    discount:
                        {
                            $cond: [{ $gte: ["$qty", 250] }, 30, 20]
                        }
                }
        }
    ]
);

db.prp_data_stage.aggregate([
    {
        $match: { recBatchId: "PRP_SITE_CSV_IMPORT-ae9bd920-cdfd-11e9-97ea-bd771f17f457" }
    },
    {
        $project: {
            rowErrorCount: { $or: [{ $gt: ["$srcValErrCount", 0] }, { $gt: ["$recValErrCount", 0] }] }
        }
    },
    {
        $match: { rowErrorCount: true }
    }
]).count();

// WORKS for OR ///////////
db.prp_data_stage_v1.find(
    {
        "recBatchId": "PRP_SITE_CSV_IMPORT-705d7750-ce06-11e9-a190-8b87f4d3c097",
        $or: [
            { totalValErrCount: { $gt: 0 } }, 
            { srcLineIndex: { $gt: 3 } }
            ] 
        
    });
    
db.prp_data_stage_v1.insert({
	
	"srcLineIndex" : 9,
	"srcLine" : "0,sid4440005,siteName 1000005 rftp5554t1,Flushing,2015-07-11,36.50116,-77.22661,prZone-a566mb96l6,219,dq3pdsv69,40,08753 Kuphal Common,Suite 894,West Moshe,Illinois,54713-1579a",
	"srcValErrs" : [ ],
	"srcJson" : {
		"recordInActive" : 0,
		"siteId" : "sid4440005",
		"siteName" : "siteName 1000005 rftp5554t1",
		"siteType" : "Flushing",
		"insertDate" : "2015-07-11",
		"lat" : 36.50116,
		"lng" : -77.22661,
		"pressureZone" : "prZone-a566mb96l6",
		"elevation" : 219,
		"geoHash" : "dq3pdsv69",
		"elevationOffset" : 40,
		"address" : {
			"line1" : "08753 Kuphal Common",
			"line2" : "Suite 894",
			"city" : "West Moshe",
			"state" : "Illinois",
			"zip" : "54713-1579a"
		}
	},
	"srcJsonMd5Hash" : "53da9c1a4def1c7aecbe40b7084f0cbf",
	"recValErrs" : [ ],
	"totalValErrCount" : 0,
	"recBatchId" : "99PRP_SITE_CSV_IMPORT-705d7750-ce06-11e9-a190-8b87f4d3c097",
	"recAction" : 0,
	"recPkValue" : "sid4440005",
	"recCreatedAt" : ISODate("2019-09-03T00:51:06.193-04:00")
});
