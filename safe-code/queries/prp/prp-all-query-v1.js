//use verdeeco_prp1_db;
// use verdeeco_prp2_db;
/* db.cloneCollection('mongodb://devadmin:xxx@daz1e1-umongo0-mongos0.dev-dc1.sensus-sa.net:27017/verdeeco_prp1_db', 'zipcodes', {} ); */ 
// host can be different
db.cloneCollection('127.0.0.1:27017/verdeeco_prp1_db', 'prp_site', {} );

db.prp_site.find({}).projection({}).sort({_id:-1}).limit(100); 
db.prp_device.find({}).projection({}).sort({_id:-1}).limit(100); 
db.prp_process.find({}).projection({}).sort({_id:-1}).limit(100); 
db.prp_data_stage.find({}).projection({}).sort({_id:-1}).limit(100); 
db.prp_site_1.find({siteId: /^site_9.*$/i}); 

/*
db.prp_site.deleteMany({});
db.prp_device.deleteMany({});
*/
 
db.prp_site.count({});
db.prp_data_stage.count({});

db.prp_site_1.drop();

db.version();
