db.prp_process.count({});
db.prp_process.deleteMany({});
db.prp_process.drop({});

db.prp_process.aggregate([
    {
        $match: { processName: {$in: []}}
    }
]);

db.prp_process
    .find({})
    //.find({siteId: /sid100000/})
    //.projection({_id:0, siteId:1, srcJsonMd5Hash:1})
    .projection({})
    .sort({ _id: -1 })
    .limit(100);

db.prp_process
    .find({processId: /_id_100/});    

db.prp_process.insert({
	"processId" : 'progress_id_1001',
	"startedAt" : new Date(),
	"completedAt" : new Date(),
	"resultJson" :  {
		"recordInActive" : 0,
		"siteId" : "sid4440005"
	},
	"errorDetail" : "",
	"processName" : "SITE",
	"processAction" : "CSV_INGEST",
	"status" : "IN_PROGRESS"
});

db.prp_process.insert({
	"processId" : 'success_id_1002',
	"startedAt" : new Date(),
	"completedAt" : new Date(),
	"resultJson" :  {
		"recordInActive" : 0,
		"siteId" : "deviceId4440005"
	},
	"errorDetail" : "",
	"processName" : "DEVICE",
	"processAction" : "CSV_INGEST",
	"status" : "SUCCESS"
});

