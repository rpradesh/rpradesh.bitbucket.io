db.prp_site.count([
    {
        $lookup: {
            from: "prp_data_stage",
            localField: "siteId",
            foreignField: "recUniqueFieldValue",
            as: "data_stage_docs"
        }
    },
    { $match: { "data_stage_docs.recBatchId": "SiteIngest-9d643620-c457-11e9-a35a-4169d3eb6f48" } },
    { $project: { _id: 1 } }
]);

db.getCollection("prp_site").deleteMany({});

db.getCollection("prp_site").insert({
    "siteId": "sid1000008",
    "siteName": "siteName 1000008 ngme3gbl0b"
});

db.getCollection("prp_site").insert({
    "siteId": "sid1000009",
    "siteName": "siteName 1000009 ngme3gbl0b"
});

db.getCollection("prp_site").insert({
    "siteId": "sid1000008",
    "siteName": "siteName 1000008 ngme3gbl0b",
    "srcJsonMd5Hash":"3f688a2c191d4002f4edb407a7d50b46"
});



db.prp_data_stage.aggregate([
    {
        $match: {
            recBatchId: "SiteIngest-8b130140-c47f-11e9-84f1-4b6ce1a9c7c4",
            srcLineIndex: {
                $ne: null
            }
        }
    },
    {
        $group: {
            _id: {
                recUniqueFieldValue: "$recUniqueFieldValue"
            },
            dupeCount: {
                $sum: 1
            }
        }
    },
    {
        $project: {
            recUniqueFieldValue: "$_id.recUniqueFieldValue",
            dupeCount: "$dupeCount"
        }
    }, {
        $match: {
            dupeCount: {
                $gt: 1
            }
        }
    },
    {
        $project: { _id: 0, dupeCount:1, recUniqueFieldValue:1 }
    }
]);


db.prp_data_stage.updateMany(
    {
        recUniqueFieldValue: "sid1000012"
    },
    {
        $push: {
            recValidationErrors: "sid1000012 has 2 duplicates"
        },
        $inc: {
            recValidationErrorCount: +1
        }
    }
);

