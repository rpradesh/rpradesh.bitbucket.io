// https://docs.mongodb.com/manual/tutorial/write-scripts-for-the-mongo-shell/

///conn = new Mongo();
///db = conn.getDB('verdeeco_prp1_db');
// it takes 2.5 mins for 100K, so for 1Mill it takes 25 mins

var colName = 'prp_device';

//db.getCollection(colName).deleteMany({});
db.getCollection(colName).count({});


var start = 2000 * 1000;
var counter = 0;
var totalRecords = 1000 * 1000;//10;//0000;//100k
var indexId = '';
var batchSize=1000;//2; //1000
print('started totalRecords=' + totalRecords  + ' at=' + new Date());
var records = [];

while(counter < totalRecords) {
    counter++;
    indexId = '' + (start + counter);
    
    if (counter % batchSize === 0) {
        db.getCollection(colName).bulkWrite(
            records, 
            {ordered : false}
        );
        
        records = [];
    }
    
    var aRecord = {
                      "deviceId": "deviceId" + indexId,
                      "meterType": "WATER",
                      "deviceType": "ALLY_METER",
                      "siteId": "sid1000009",
                      "deviceName": "deviceName "  + indexId + " necvxzl44b",
                      "dma": "dma-mw7vol2yjj",
                      "lat": "35.44183",
                      "lng": "-79.43442",
                      "pressureZone": "prZone-6vgkfox40s",
                      "elevation": "273",
                      "geoHash": "dnr987vxe",
                      "elevationOffset": "40",
                      "address": {
                          "line1": "9735 Berge Junctions",
                          "line2": "Apt. 348",
                          "city": "Schummville",
                          "state": "Indiana",
                          "zip": "01357-0955"
                      },
                      "analogLabel1": "ang_AA",
                      "analogLabel2": "ang_AA",
                      "digLowLabel1": "DG_low_AA",
                      "digLowLabel2": "DG_low_AA",
                      "digHighLabel1": "dg_HGH_BB",
                      "digHighLabel2": "dg_HGH_BB"
                  };
        
    var aDoc = { insertOne : { "document" : aRecord } }
    records.push(aDoc);

}//end-while

// remaining out of batch flush them :)
db.getCollection(colName).bulkWrite(
        records, 
        {ordered : false}
    );

db.getCollection(colName).count({});

print('ended=' + new Date());

/*
1 MILLION SITE started totalRecords=1000000 at=Mon Aug 12 2019 15:41:22 GMT-0400 (EDT) ended=Mon Aug 12 2019 15:44:58 GMT-0400 (EDT)
1 MILLION DEVICE started totalRecords=1000000 at=Mon Aug 12 2019 17:37:36 GMT-0400 (EDT) ended=Mon Aug 12 2019 17:42:51 GMT-0400 (EDT)
*/
