// https://docs.mongodb.com/manual/tutorial/write-scripts-for-the-mongo-shell/

///conn = new Mongo();
///db = conn.getDB('verdeeco_prp1_db');
// it takes 2.5 mins for 100K, so for 1Mill it takes 25 mins

var start = 2000 * 1000;
var counter = 0;
var totalRecords = 100 * 1000;//10;//0000;//100k
var indexId = '';
print('started totalRecords=' + totalRecords  + ' at=' + new Date());

while(counter < totalRecords) {
counter++;
indexId = '' + (start + counter);

db.getCollection("prp_site1").insert(
    {
    "siteId": "sid" + indexId,
    "siteName": "siteName "  + indexId + " vx7icbdpcy",
    "siteType": "Flushing",
    "insertDate": "2015-10-14",
    "lat": "36.19839",
    "lng": "-78.42492",
    "pressureZone": "prZone-xm8wskcvhv",
    "elevation": "657",
    "geoHash": "dq2jzmswy",
    "elevationOffset": "40",
    "address": {
        "line1": "85988 "  + indexId + " Drive",
        "line2": "Apt. 078",
        "city": "Sanfordberg",
        "state": "Pennsylvania",
        "zip": "80136-3992"
    }
});

}//end-while


print('ended=' + new Date());
