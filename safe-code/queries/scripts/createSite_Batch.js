// https://docs.mongodb.com/manual/tutorial/write-scripts-for-the-mongo-shell/

///conn = new Mongo();
///db = conn.getDB('verdeeco_prp1_db');
// it takes 2.5 mins for 100K, so for 1Mill it takes 25 mins

var colName = 'prp_site';

//db.getCollection(colName).deleteMany({});
db.getCollection(colName).count({});


var start = 2000 * 1000;
var counter = 0;
var totalRecords = 1000 * 1000;//10;//0000;//100k
var indexId = '';
var batchSize=1000;//2; //1000
print('started totalRecords=' + totalRecords  + ' at=' + new Date());
var records = [];

while(counter < totalRecords) {
    counter++;
    indexId = '' + (start + counter);
    
    if (counter % batchSize === 0) {
        db.getCollection(colName).bulkWrite(
            records, 
            {ordered : false}
        );
        
        records = [];
    }
    
    var aRecord = {
        "siteId": "sid" + indexId,
        "siteName": "siteName "  + indexId + " vx7icbdpcy",
        "siteType": "Flushing",
        "insertDate": "2015-10-14",
        "lat": "36.19839",
        "lng": "-78.42492",
        "pressureZone": "prZone-xm8wskcvhv",
        "elevation": "657",
        "geoHash": "dq2jzmswy",
        "elevationOffset": "40",
        "address": {
            "line1": "85988 "  + indexId + " Drive",
            "line2": "Apt. 078",
            "city": "Sanfordberg",
            "state": "Pennsylvania",
            "zip": "80136-3992"
        } };
        
    var aDoc = { insertOne : { "document" : aRecord } }
    records.push(aDoc);

}//end-while

// remaining out of batch flush them :)
db.getCollection(colName).bulkWrite(
        records, 
        {ordered : false}
    );

db.getCollection(colName).count({});

print('ended=' + new Date());

/*
1 MILLION started totalRecords=1000000 at=Mon Aug 12 2019 15:41:22 GMT-0400 (EDT) ended=Mon Aug 12 2019 15:44:58 GMT-0400 (EDT)

*/
