db.ri_event_range.find({})
   .projection({})
   .sort({_id:-1})
   .limit(100);
   
   
db.ri_event_range.find({ "productType": 20, "deviceType": 47 });


db.ri_event_range.update({ "productType": 20, "deviceType": 47 },
    {
        "productType": 20,
        "device_identifier": {
            "customer_id": "p0-0001",
            "device_id": "M1599",
            "service_type": "ELECTRIC"
        },
        "description": "Electric",
        "deviceType": 47
    },
    { upsert: true }
);

db.ri_configuration.update({
  _id: ObjectId("5b5a13ffd202542c84871e83")
}, {
  _id: ObjectId("5b5a13ffd202542c84871e83"),
  _t: "com.sensus.analytics.rni.integration.data.dto.projection.CoordinatorConfiguration",
  created: ISODate("2018-07-26T00:00:00Z"),
  modified: ISODate("2018-07-26T00:00:00Z"),
  modified_by: "anonymous",
  description: "rni-integration-coordinator configuration",
  task_duration: NumberInt(10),
  utility_refresh_duration: NumberInt(10)
}, {
  upsert: true
});

db.ri_configuration.update({
  _id: ObjectId("5b5a1d61d202543fe481ab63")
}, {
  _id: ObjectId("5b5a1d61d202543fe481ab63"),
  _t: "com.sensus.analytics.rni.integration.data.dto.projection.ServiceConfiguration",
  created: ISODate("2018-07-26T00:00:00Z"),
  modified: ISODate("2018-07-26T00:00:00Z"),
  modified_by: "anonymous",
  description: "rni-integration-service configuration",
  coordinator_conn_task_delay: NumberInt(10),
  failed_conn_retry_delay: NumberInt(10)
}, {
  upsert: true
});
   