db.ri_device_information.aggregate([
    { "$group": { "_id": { device_type: "$device_type", product_type: "$product_type" } } },
    { "$project": { _id: 0, deviceType: "$_id.device_type", productType: "$_id.product_type" } }
]);

db.ri_device_information.aggregate([
    { "$group": { "_id": { device_type: "$device_type", product_type: "$product_type" , java_hash_value: "$java_hash_value" } } },
    { "$project": { _id: 0, device_type: "$_id.device_type", product_type: "$_id.product_type", java_hash_value: "$_id.java_hash_value" } }
]);


db.ri_device_information.find({}, {  _id: 0, device_type: 1, product_type: 1 } );

db.ri_device_information.aggregate([
     { "$group": { "_id": { product_type: "$product_type", device_type: "$device_type" } } },
    { "$project": { _id: 0, device_type: "$_id.device_type", product_type: "$_id.product_type" } }
]);

db.ri_device_information.find({ "device_key": "1599;ELECTRIC", "device_type": 49 })
    .projection({})
    .sort({ _id: -1 })
    .limit(100);

db.ri_event_range.update({ "productType": 20, "deviceType": 47 },
    {
        "productType": 20,
        "device_identifier": {
            "customer_id": "p0-0001",
            "device_id": "M1599",
            "service_type": "ELECTRIC"
        },
        "description": "Electric",
        "deviceType": 47
    },
    { upsert: true }
);

db.device_info_1.find();

db.device_info_1.drop();

x = ObjectId();