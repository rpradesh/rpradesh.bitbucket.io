// wget -q http://media.mongodb.org/zips.json -O /tmp/zips.json
// mongoimport --host 127.0.0.1 --port 27017 --db verdeeco_rzip_db  --collection src_zipcodes --file /tmp/zips.json

/*
db.collection.find( { "name" : { $regex : /Andrew/i } } );
To use the regex pattern from your thename variable, construct a new RegExp object:
var thename = "Andrew";
db.collection.find( { "name" : { $regex : new RegExp(thename, "i") } } );
*/

db.zipcodes.find({})
   .projection({})
   .sort({_id:1})
   .limit(100);
// works
db.zipcodes.aggregate( [
   { $group: { _id: "$state", totalPop: { $sum: "$pop" } } },
   { $match: { totalPop: { $gte: 10*1000*1000 } } },
   { $out: 'zip1' }
] );
// works
db.zip1.find({}).forEach(function(doc) {db.zip2.insert(doc)});

// works
db.src_zipcodes.aggregate( [
   { $group: { _id: "$state", totalPop: { $sum: "$pop" } } },
   { $match: { totalPop: { $gte: 10*1000*1000 } } }
] ).forEach(function(doc) {db.zip3.insert(doc)});

//works
db.zip33.insertMany(
db.zipcodes.aggregate( [
   { $group: { _id: "$state", totalPop: { $sum: "$pop" } } },
   { $match: { totalPop: { $gte: 10*1000*1000 } } }
] ).toArray()
);
// this returns result even
db.src_zipcodes_agg.insertMany(
db.src_zipcodes.aggregate( [
   { $group: { _id: "$state", totalPop: { $sum: "$pop" } } },
   { $match: { totalPop: { $gte: 10*1000*1000 } } }
] ).toArray(), {ordered:false}
);

db.zip34.update(
db.zipcodes.aggregate( [
   { $group: { _id: "$state", totalPop: { $sum: "$pop" } } },
   { $match: { totalPop: { $gte: 10*1000*1000 } } }
] ).toArray(), {upsert:true}
);


db.zip1.drop();
db.zipcodes_src.drop();


// this returns result even
db.zipcodes_src.insertMany(
db.zipcodes.aggregate( [
   { $match: { NumberInt(_id): { $gte: 0 } } },
   { $limit: 10 /* * 100 * 1000*/}
] ).toArray(), {ordered:false}
).forEach(function(doc) {print(doc)});

// nope
cols = db.runCommand( { listCollections: 1.0, nameOnly: true } )
.forEach(function(doc) {print(doc)});
//nope
var myCursor =  db.runCommand( { listCollections: 1.0, nameOnly: true } );
myCursor.forEach(printjson);

/// works
var cols = db.getCollectionNames();
// Iterate through each collection.
cols.forEach(function(col) {
    print(col);
    var colInfo = db.getCollection(col);
    print(colInfo);
});


db.getCollectionNames().forEach(function(col) {
    print(col);
    var colInfo = db.getCollection(col);
    print(colInfo);
});


db.adminCommand( { flushRouterConfig: "mydb.zipcodes" } );
db.getSiblingDB("mydb").myShardedCollection.getShardDistribution();

db.prp_site.find({"siteId" : "sid3000005"})
.projection({_id:0, siteId: 0})
.forEach( function(documentPass){
   documentPass.md5HashValue = hex_md5(JSON.stringify(documentPass));
   print(documentPass.md5HashValue);
   //db.addMd5HashValueDemo.save(documentPass);
});

