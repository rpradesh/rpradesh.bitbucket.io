import {Command, flags} from '@oclif/command'
import cli from 'cli-ux'
import * as Listr from 'listr'
import {MongoClient} from 'mongodb';
import { async } from 'rxjs/internal/scheduler/async';

export async function sleepSecs(secs: number) {
  await new Promise(done => setTimeout(done, secs * 1000));
}

async function _runNow(mongoUrl: string): Promise<void>{
  let connected = false;
  cli.action.start(`connecting to ${mongoUrl}`)
  try {
    const mongoClient: MongoClient = await MongoClient.connect(mongoUrl, {useUnifiedTopology: true, useNewUrlParser: true});
    connected = await mongoClient.isConnected();
    await mongoClient.close();
    }
  catch (err) {
    cli.error(err.message)
  }
  finally {
    cli.action.stop(connected ? 'Success': 'Failed')
  }
}


async function runTask1(mongoUrl: string): Promise<void>{  
  await sleepSecs(3);
  const mongoClient: MongoClient = await MongoClient.connect(mongoUrl, {useUnifiedTopology: true, useNewUrlParser: true});
  const connected = await mongoClient.isConnected();
  await mongoClient.close();
}

async function runTask2(mongoUrl: string): Promise<void>{  
  try {
  await sleepSecs(10);
  const mongoClient: MongoClient = await MongoClient.connect(mongoUrl, {useUnifiedTopology: true, useNewUrlParser: true});
  const connected = await mongoClient.isConnected();
  await mongoClient.close();
  }
  catch (err) {
    throw new Error(err)
  }
}

 function runTask3(mongoUrl: string):void {  
  (async () => {
  await sleepSecs(10);
  const mongoClient: MongoClient = await MongoClient.connect(mongoUrl, {useUnifiedTopology: true, useNewUrlParser: true});
  const connected = await mongoClient.isConnected();
  await mongoClient.close();
  })();
}

export default class Mongo extends Command {
  static description = 'Mongo Utilities'
  static args = [{name: 'mongo-check-connection'}]  
  static examples = [
    `$ saprp mongo-check-connection -u "mongodb://localhost:27017/"
    connecting to mongodb://localhost:27017/... Success
    `,
  ]

  static flags = {
    help: flags.help({char: 'h'}),
    url: flags.string({char: 'u', description: 'mongo url to connect', default: 'mongodb://localhost:27017/'}),
  }

  async run() {
    //try {
    const {args, flags} = this.parse(Mongo)
    const mongoUrl = flags.url;
    /// this.log(`args=${JSON.stringify(args)} flags=${JSON.stringify(flags) }`)
    // await _runNow(mongoUrl);

    const tasks = new Listr([
      {
        title: `Checking Mongo Connection url=${mongoUrl}`,
        //task: async () => await _runNow(mongoUrl)
        task: async () => await runTask1(mongoUrl)
        //task: async () => await runTask2(mongoUrl)
        //task: () => runTask3(mongoUrl)
      }
    ]);  
 
    await tasks.run();
    // }catch(erer){
    // this.log(`Error Details=${erer}`)
    // }
  }//end-run
}
