import {Command, flags} from '@oclif/command'

export default class Datagen extends Command {
  static description = 'Datagen Utilities'

  static examples = [
    `$ saprp mongo
Datagen world from ./src/Datagen.ts!
`,
  ]

  static flags = {
    help: flags.help({char: 'h'}),
    // flag with a value (-n, --action=VALUE)
    action: flags.string({char: 'a', description: 'action to print'}),
    //name: flags.string({char: 'n', description: 'action to print'}),
    // flag with no value (-f, --force)
    force: flags.boolean({char: 'f'}),
  }

  static args = [{action: 'file', name:'some'}]

  async run() {
    const {args, flags} = this.parse(Datagen)

    const action = flags.action || 'world'
    this.log(`Datagen ${action} from ./src/commands/Datagen.ts`)
    if (args.file && flags.force) {
      this.log(`you input --force and --file: ${args.file}`)
    }
  }
}
