/* eslint-disable no-console */
import {expect, test} from '@oclif/test';
import {TestUrl, TestUtility, executeMongoCmd} from '../test-helper';
import * as L from 'lodash';
import * as cmn from '../../src/common';

describe('mongo-delete-records', () => {
  before(async () => {
    // let us init a dummy table
    executeMongoCmd(`db.prp_site_1.deleteMany({})`);

    L.range(0, 4).forEach(i => {
      executeMongoCmd(`db.prp_site_1.insert({ theDate: "${new Date()}", index: ${i} })`);
    });
  });

  // test at 27017 "prp_site_1" should be 4
  test
    .stdout()
    .command(['mongo-delete-records', '-m', TestUrl, '-u', TestUtility, '-c', 'prp_site_1'])
    .it('should return 4, for deleting 4 existing records', ctx => {
      expect(ctx.stdout).to.contain('=4;');
    });

  //  test at 27017 "prp_site_ZZ" should be 0
  test
    .stdout()
    .command(['mongo-delete-records', '-m', TestUrl, '-u', TestUtility, '-c', 'prp_site_1'])
    .it('should return 0, for non-existing records', ctx => {
      expect(ctx.stdout).to.contain('=0;');
    });

  test
    .stdout()
    .command(['mongo-delete-records', '-m', TestUrl, '-u', TestUtility, '-c', 'prp_site_ZZ'])
    .it('should return 0, for non-existing collection', ctx => {
      expect(ctx.stdout).to.contain('=0;');
    });
});
