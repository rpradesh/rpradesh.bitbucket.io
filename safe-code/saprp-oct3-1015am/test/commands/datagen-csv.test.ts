/* eslint-disable no-console */
import {expect, test} from '@oclif/test';
import {TestUrl, TestUtility, executeMongoCmd, countWords, log, writeJsonArrayToFile, deleteFile} from '../test-helper';
import * as fs from 'fs';
import * as jp from 'jmespath';
import {DataGenTypeEnum as tE} from '../../src/common';

function fName(key: string): string {
  return `test--${key}--dategen.csv`;
}

function readFileToString(key: string): string {
  return fs.readFileSync(fName(key), 'utf8');
}

describe('datagen tests for all types', () => {
  before(async () => {
    for (const k in tE) {
      const filename = fName(k);
    }
  });

  after(async () => {
    for (const k in tE) {
      const filename = fName(k);
      deleteFile(filename);
    }
  });

  test
    .stdout()
    .command(['datagen', '-t', tE.prp_site, '-f', fName(tE.prp_site), '-n', '10', '-i', '2000000', '-o', 'csv'])
    .it(`should generate 10 records in file: ${fName(tE.prp_site)}`, ctx => {
      expect(ctx.stdout).contain('writeCount=10;');
      const data = readFileToString(tE.prp_site);
      expect(countWords(data, 'sid200000')).equal(10);
      expect(countWords(data, '\n')).equal(10 + 2); //including 2 header lines
    });

  test
    .stdout()
    .command(['datagen', '-t', tE.prp_device, '-f', fName(tE.prp_device), '-n', '10', '-i', '3000000', '-o', 'csv'])
    .it(`should generate 10 records in file: ${fName(tE.prp_device)}`, ctx => {
      expect(ctx.stdout).contain('writeCount=10;');
      const data = readFileToString(tE.prp_device);
      expect(countWords(data, 'deviceId300000')).equal(10);
      expect(countWords(data, '\n')).equal(10 + 2); //including 2 header lines
    });
});
