/* eslint-disable no-console */
import {expect, test} from '@oclif/test';
import {TestUrl, TestUtility, executeMongoCmd, log} from '../test-helper';

describe('mongo-check-connection', () => {
  // test at 27018 should give error
  test
    .stdout()
    .command(['mongo-check-connection', '-m', 'mongodb://localhost:27018'])
    .it('runs => mongo-check-connection -m mongodb://localhost:27018', ctx => {
      expect(ctx.stdout).to.contain('connected=false;');
    });

  // test at 27017 should pass
  test
    .stdout()
    .command(['mongo-check-connection', '-m', TestUrl])
    .it('runs => mongo-check-connection -m mongodb://localhost:27017', ctx => {
      expect(ctx.stdout).to.contain('connected=true;');
    });

  // test at 27017 using ENV variable, should pass
  // test
  //   .env({MONGO_URL: 'mongodb://localhost:27017'})
  //   .stdout()
  //   .command(['export MONGO_URL=mongodb://localhost:27017;', 'mongo-check-connection'])
  //   .it('runs => mongo-check-connection without args, using env', ctx => {
  //     expect(ctx.stdout).to.contain('connected=true;');
  //   });
});
