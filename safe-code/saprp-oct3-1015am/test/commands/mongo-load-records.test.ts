/* eslint-disable no-console */
import {expect, test} from '@oclif/test';
import {TestUrl, TestUtility, executeMongoCmd, countWords, log, writeJsonArrayToFile, deleteFile} from '../test-helper';
import * as L from 'lodash';
import {DataGenTypeEnum} from '../../src/common';

const testColName = 'prp_site_2';
const testFileName = 'test-records-15-load.json';

describe('mongo-load-records', () => {
  before(async () => {
    const records15 = L.range(0, 15).map(i => ({theDate: new Date(), index: i}));
    writeJsonArrayToFile(records15, testFileName);
  });

  after(async () => {
    deleteFile(testFileName);
  });

  test
    .stdout()
    .command(['mongo-load-records', '-m', TestUrl, '-u', TestUtility, '-c', testColName, '-f', testFileName])
    .it(`should load 15 records into col: ${testColName}`, ctx => {
      expect(ctx.stdout).contain('insertCount=15;');
    });
});
