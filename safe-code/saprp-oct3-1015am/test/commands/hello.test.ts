import {expect, test} from '@oclif/test';

describe('hello', () => {
  test
    .stdout()
    .command(['hello'])
    .it('runs hello', ctx => {
      expect(ctx.stdout).to.contain('hello WORLD');
    });

  test
    .stdout()
    .command(['hello', '-n', 'raj'])
    .it('runs hello -n raj', ctx => {
      expect(ctx.stdout).to.contain('hello RAJ');
    });
});
