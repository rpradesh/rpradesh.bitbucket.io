/* eslint-disable no-console */
import {flags} from '@oclif/command';
import {BaseCommand, flagsClass} from '../common/base-command1';
import {sleepSecs} from '../common';

import {checkConnectionUrl} from '../mongo';

export default class Hello extends BaseCommand {
  static description = 'describe the command here';
  static args = [{name: 'file'}];
  static examples = [
    `$ saprp hello raj
     hello RAJ
    `
  ];
  static flags = {
    help: flags.help({char: 'h'}),
    name: flags.string({char: 'n', description: 'name to print', required: true}),
    force: flags.boolean({char: 'f'})
  };

  static result: any = null;

  async execute() {
    const {args, flags} = this.parse(Hello);

    this.setStatusText('Running step 1');
    await sleepSecs(1);
    this.setStatusText('Running step 2');
    await sleepSecs(1);
    this.appendResult('connected', await checkConnectionUrl('mongodb://localhost:27017'));
    if (flags.name === 'tom') throw new Error('Tom error');

    const result = 'hello ' + (flags.name || 'world').toUpperCase();
    this.appendResult('pkvalue', result);

    Hello.result = 'hello ' + (flags.name || 'world').toUpperCase();
  }
  /*
  output(): void {
    // this.printResult();
    console.log(Hello.result);
  } */
}

// displays spinner
// able to change text
// logs error to /tmp/
