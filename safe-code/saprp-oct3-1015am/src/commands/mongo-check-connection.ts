import {BaseCommand} from '../common/base-command';
import {checkConnection} from '../mongo';
import {buildFlagsFor, buildMongoModel, sleepSecs} from '../common';

export default class Mongo extends BaseCommand {
  static description = 'Mongo: Check Connection';
  static args = [{name: 'mongo-check-connection'}];
  static definedFlags(): string[] {
    return ['help', 'mongos'];
  }
  // static flags = {...buildFlagsFor(['help', 'mongos'])};
  static buildflags = {...buildFlagsFor(['help', 'mongos'])};

  static examples = [
    `$ saprp mongo-check-connection -m "mongodb://localhost:27099"    
    or have MONGO_URL set as mongodb://localhost:27017
    saprp mongo-check-connection
    connected=true; 
    `
  ];

  async execute() {
    const {args, flags} = this.parse(Mongo);
    const mongoArgs = buildMongoModel(flags);
    ///this.setStatusText(`CChecking Mongo Connection at: ${this.mongoArgs.mongoUrl}`);
    await sleepSecs(1);
    ///this.appendResult('connected', await checkConnection(this.mongoArgs));
  } //end-execute
}
