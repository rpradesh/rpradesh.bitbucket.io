import {BaseCommand} from '../common/base-command';
import {getCollectionCount} from '../mongo';
import {buildFlagsFor, buildMongoModel} from '../common';

export default class Mongo extends BaseCommand {
  static description = 'Mongo: Get Count of Records in given Collection';
  static args = [{name: 'mongo-count-records'}];
  static flags = {...buildFlagsFor(['help', 'mongos', 'utility', 'collection'])};

  static examples = [
    `$ saprp mongo-count-records -u central -c utility
    or with mongo url
    saprp mongo-count-records -m "mongodb://localhost:27099" -u central -c utility
    `
  ];

  async run() {
    const {args, flags} = this.parse(Mongo);
    const mongoArgs = buildMongoModel(flags);

    try {
      this.setStatusText(`Count of: ${mongoArgs.displayCollectionName}`);
      this.appendResult('count', await getCollectionCount(mongoArgs));
    } catch (err) {
      this.logError(err);
    } finally {
      ///this.stopStatus();
      this.printResult();
    }
  } //end-run
}
