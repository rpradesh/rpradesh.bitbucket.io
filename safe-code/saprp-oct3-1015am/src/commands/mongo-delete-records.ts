import {BaseCommand} from '../common/base-command';
import {clearCollection} from '../mongo';
import {buildFlagsFor, buildMongoModel} from '../common';

export default class Mongo extends BaseCommand {
  static description = 'Mongo: Delete all Records in given Collection';
  static args = [{name: 'mongo-delete-records'}];
  static flags = {...buildFlagsFor(['help', 'mongos', 'utility', 'collection'])};

  static examples = [
    `$ saprp mongo-delete-records -u prp1 -c prp_process
    or with mongo url
    saprp mongo-delete-records -m "mongodb://localhost:27099" -u prp1 -c prp_process
    `
  ];

  async run() {
    const {args, flags} = this.parse(Mongo);
    const mongoArgs = buildMongoModel(flags);

    try {
      this.setStatusText(`Clear Collection: ${mongoArgs.displayCollectionName}`);
      this.appendResult('count', await clearCollection(mongoArgs));
    } catch (err) {
      this.logError(err);
    } finally {
      ///this.stopStatus();
      this.printResult();
    }
  } //end-run
}
