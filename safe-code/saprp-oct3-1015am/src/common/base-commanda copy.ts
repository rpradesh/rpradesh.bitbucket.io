/* eslint-disable no-console */
import Command, {flags} from '@oclif/command';
//import * as ora from 'ora';
import * as fs from 'fs';
import * as path from 'path';
import * as cmn from '../common';
import {buildFlagsFor, buildMongoModel} from '../common';

const enable = true;
const tmpFilePath = '/tmp/saprp-error.log';

export abstract class BaseCommandA extends Command {
  static usage = `${Command.name} [OPTIONS]`;
  ///static spinner = ora('').start();
  static parsedFlags: any = null;
  static flagsA = {...buildFlagsFor(BaseCommandA.definedFlags())};
  static flags = {...buildFlagsFor(['help', 'mongos'])};

  static definedFlags(): string[] {
    return [];
  }

  stopSpinner(): void {
    ///BaseCommandA.spinner.stop();
    ///BaseCommandA.spinner.clear();
  }

  static resultMap = new Map();

  async catch(err: Error) {
    await super.catch(err);
    this.logError(err);
    this.stopSpinner();
    process.exit(-1);
  }

  get mongoArgs(): cmn.MongoArgs {
    return BaseCommandA.parsedFlags as cmn.MongoArgs;
  }

  get datagenArgs(): cmn.DatagenArgs {
    return BaseCommandA.parsedFlags as cmn.DatagenArgs;
  }

  async init() {
    // if (fs.existsSync(tmpFilePath)) fs.unlinkSync(tmpFilePath);
    // do some initialization
    ///BaseCommandA.spinner.color = 'green';
  }

  async finally(err: Error) {
    if (err) this.logError(err);
    ///this.stopSpinner();
    this.output();
  }

  async run() {
    const {args, flags} = this.parse(BaseCommandA);
    //const {args, flags} = this.parse(...buildFlagsFor(['help', 'mongos']));
    //console.log('flags=', flags);
    BaseCommandA.parsedFlags = buildMongoModel(flags);
    await this.execute();
  }

  abstract execute(): Promise<void>;

  output(): void {
    this.printResult();
  }

  // custom methods
  setStatusText(txt: string) {
    ///BaseCommandA.spinner.text = txt;
  }

  appendResult(key: string, result: string | number | boolean) {
    BaseCommandA.resultMap.set(key, new String(result));
  }

  printResult() {
    if (BaseCommandA.resultMap.size === 0) return;
    let output = '';
    for (const pair of BaseCommandA.resultMap) output += `${pair[0]}=${pair[1]};`;
    this.log(output);
  }

  logError(msg: any) {
    fs.appendFileSync(tmpFilePath, '' + new Date() + ' ' + new String(msg) + '\n');
  }

  get resultMap(): Map<string, string> {
    return BaseCommandA.resultMap;
  }
}
