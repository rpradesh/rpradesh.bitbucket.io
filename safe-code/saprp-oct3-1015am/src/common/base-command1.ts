import Command, {flags} from '@oclif/command';
import * as ora from 'ora';
import * as fs from 'fs';
import * as path from 'path';
import * as cmn from '../common';
import {sleepSecs} from '../utils';

const enable = true;
const tmpFilePath = '/tmp/saprp-error.log';

export class Flags {
  pushFlags(): void {
    const a = 1;
  }

  getFlags(): any {
    return {
      help: flags.help({char: 'h'}),
      name: flags.string({char: 'n', description: 'name to print', required: true}),
      force: flags.boolean({char: 'f'})
    };
  }
}

export const flagsClass = new Flags();

export abstract class BaseCommand extends Command {
  static usage = `${Command.name} [OPTIONS]`;
  static spinner = ora('').start();

  stopSpinner(): void {
    BaseCommand.spinner.stop();
    BaseCommand.spinner.clear();
  }

  static resultMap = new Map();

  async catch(err: Error) {
    await super.catch(err);
    this.logError(err);
    this.stopSpinner();
    process.exit(-1);
  }

  async run() {
    try {
      await this.execute();
      // eslint-disable-next-line no-empty
    } catch (err) {
      this.logError(err);
      this.stopSpinner();
      process.exit(-1);
    }
    this.stopSpinner();
    this.output();
  }

  async execute(): Promise<void> {
    await sleepSecs(0);
  }

  async init() {
    /// if (fs.existsSync(tmpFilePath)) fs.unlinkSync(tmpFilePath);
    // do some initialization
    BaseCommand.spinner.color = 'green';
  }

  async finally(err: Error) {
    this.stopSpinner();
  }

  // custom methods
  setStatusText(txt: string) {
    BaseCommand.spinner.text = txt;
  }

  appendResult(key: string, result: string | number | boolean) {
    BaseCommand.resultMap.set(key, new String(result));
  }

  printResult() {
    let output = '';
    for (const pair of BaseCommand.resultMap) output += `${pair[0]}=${pair[1]};`;
    this.log(output);
  }

  logError(msg: any) {
    fs.appendFileSync(tmpFilePath, '' + new Date() + ' ' + new String(msg) + '\n');
  }

  output(): void {
    this.printResult();
  }
  get resultMap(): Map<string, string> {
    return BaseCommand.resultMap;
  }
}
