/* eslint-disable no-console */
import * as fs from 'fs';

const debug = process.env.DEBUG === 'true';

export async function sleepSecs(secs: number) {
  await new Promise(done => setTimeout(done, secs * 1000));
}

export async function isFile(absFilePath: string): Promise<boolean> {
  let exists = false;
  if (fs.existsSync(absFilePath)) {
    const stat = fs.statSync(absFilePath);
    exists = stat && stat.isFile();
  }

  if (debug) console.log(`exists=${exists} absFilePath=${absFilePath}`);
  return exists;
}

export async function isDirectory(absPath: string): Promise<boolean> {
  if (fs.existsSync(absPath)) {
    const stat = fs.statSync(absPath);
    return stat && stat.isDirectory();
  } else return false;
}

export function enumValueAsArray(anyEnum: any): string[] {
  const stringArray: string[] = [];
  for (const k in anyEnum) {
    stringArray.push(k as string);
  }
  return stringArray;
}
