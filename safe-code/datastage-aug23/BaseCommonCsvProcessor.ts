/* eslint-disable @typescript-eslint/no-parameter-properties */
import * as Joi from '@hapi/joi';
import {mongo, logger} from '../../Application';
import {BaseCsvSchema} from '../csv';

import * as moment from 'moment';
import {createWriteStream, WriteStream} from 'fs';
import {BulkWriteOpResultObject} from 'mongodb';
import * as utils from '../../utils';
import * as L from 'lodash';

import {
  /* Models */
  ICsvFieldSchema,
  CsvProcessOptions,
  CsvProcessResult,
  CsvSrcRecord,
  CsvStageRecord,
  CsvFeatureOptions
  /* processor */
} from './index';

export class BaseCommonCsvProcessor {
  fileWriteStream: WriteStream;
  /*batchRecords: CsvStageRecord[] = [];
  counter: number = 0;
*/
  constructor(
    public readonly csvSchema: BaseCsvSchema,
    public readonly csvProcessOptions: CsvProcessOptions,
    public readonly csvFeatureOptions: CsvFeatureOptions,
    public readonly csvProcessResult: CsvProcessResult = new CsvProcessResult()
  ) {
    this.fileWriteStream = createWriteStream(this.csvProcessOptions.errorOutputFilepath);
  }

  // protected async persistBatch(): Promise<number> {
  //   if (this.batchRecords.length === 0) return 0;

  //   const insertArray: any[] = this.batchRecords.map(rec => {
  //     rec.recBatchId = this.csvProcessOptions.recBatchId;
  //     rec.srcValidationErrorCount = rec.srcValidationErrors.length;

  //     if (!L.isEmpty(this.csvFeatureOptions.validateUniqeFieldPath)) {
  //       rec.recUniqueFieldValue = L.get(rec.srcJson, this.csvFeatureOptions.validateUniqeFieldPath, 'unknownField');
  //     }
  //     return {
  //       insertOne: {...rec}
  //     };
  //   });

  //   const writeOps: BulkWriteOpResultObject = await mongo
  //     .utilityDB('prp1')
  //     .collection('prp_data_stage')
  //     .bulkWrite(insertArray, {ordered: false});
  //   logger.debug(`writeOps=${JSON.stringify(writeOps)}`);

  //   return 0;
  // }

  protected async processGetCounts(): Promise<void> {
    //total inValid records in batch
    this.csvProcessResult.inValidRecordCount = await mongo
      .utilityDB('prp1')
      .collection('prp_data_stage')
      .count({
        recBatchId: this.csvProcessOptions.recBatchId,
        srcValidationErrorCount: {$gt: 0},
        recValidationErrorCount: {$gt: 0}
      });

    //valid is diff of above 2
    this.csvProcessResult.validRecordCount =
      this.csvProcessResult.totalRecordCount - this.csvProcessResult.inValidRecordCount;
  }

  protected async fetchTotalRowCount(): Promise<void> {
    //first get total Rows for batch
    this.csvProcessResult.totalRecordCount = await mongo
      .utilityDB('prp1')
      .collection('prp_data_stage')
      .countDocuments({recBatchId: this.csvProcessOptions.recBatchId});
  }

  protected async processTransferOfData(): Promise<void> {
    // insert from stage to site where unique != pkField and hash != jsonHash
  }
} //end-class
