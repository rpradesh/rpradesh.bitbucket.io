import {logger} from '../../Application';
import * as moment from 'moment';
import * as utils from '../../utils';
import {BaseCommonCsvProcessor} from './BaseCommonCsvProcessor';
import {DataStageInsertProcessor} from './dataStageInsertProcessor';
import {PercentMatchProcessor} from './percentMatchProcessor';
import {DuplicateCheckProcessor} from './duplicateCheckProcessor';
import {RecordActionProcessor} from './recordActionProcessor';
import {InsertEntityProcessor} from './insertEntityProcessor';
import {UpdateEntityProcessor} from './updateEntityProcessor';
import {CsvStageRecord} from './model';

export class CommonCsvProcessor extends BaseCommonCsvProcessor {
  async process(): Promise<void> {
    logger.info(`== starting data ingest csvProcessOptions=${JSON.stringify(this.csvProcessOptions)}`);
    try {
      const dataStageProc = new DataStageInsertProcessor(this);
      const totalLinesInFile = await dataStageProc.process();
      this.csvProcessResult.totalLinesInFile = totalLinesInFile;

      // const percentProc = new PercentMatchProcessor(this);
      // await percentProc.process();

      // const dupeProc = new DuplicateCheckProcessor(this);
      // await dupeProc.process();

      // // mark which needs to be inserted or updated
      const recActionProc = new RecordActionProcessor(this);
      await recActionProc.process();

      // // insert into parent entity
      const insertEntityProc = new InsertEntityProcessor(this);
      this.csvProcessResult.insertedRecordCount = await insertEntityProc.process();

      // // update into parent entity
      const updateEntityProc = new UpdateEntityProcessor(this);
      this.csvProcessResult.updatedRecordCount = await updateEntityProc.process();

      // await this.processGetCounts();
    } catch (err) {
      this.csvProcessResult.processError = err;
      logger.error(err, err);
    } finally {
      this.csvProcessResult.processEndedAt = new Date();
      logger.info(`summary = ${JSON.stringify(this.csvProcessResult)}`);
    }
    return;
  }
} //end-class
