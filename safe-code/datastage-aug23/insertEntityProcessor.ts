/* eslint-disable no-console */
/* eslint-disable @typescript-eslint/no-parameter-properties */
import {CommonCsvProcessor} from './CommonCsvProcessor';
import {mongo, logger} from '../../Application';
import {BulkWriteOpResultObject, AggregationCursor} from 'mongodb';
import * as utils from '../../utils';

export async function getInsertRecords(givenRecBatchId: string): Promise<AggregationCursor> {
  const aggCursor = await mongo
    .utilityDB('prp1')
    .collection('prp_data_stage')
    .aggregate([
      {
        $match: {
          recBatchId: givenRecBatchId,
          srcValidationErrorCount: 0,
          recValidationErrorCount: 0,
          recAction: 0
        }
      },
      {$project: {_id: 0, srcJsonMd5Hash: 1, recCreatedAt: 1, recUpdatedAt: '$recCreatedAt', srcJson: 1}}
    ]);

  return aggCursor;
}

export class InsertEntityProcessor {
  constructor(public readonly csvProc: CommonCsvProcessor) {}
  private batchRecords: any[] = [];

  async process(): Promise<number> {
    let insertCount = 0;
    const cursor: AggregationCursor = await getInsertRecords(this.csvProc.csvProcessOptions.recBatchId);
    while (await cursor.hasNext()) {
      const rec = await cursor.next();

      this.batchRecords.push({
        insertOne: {
          document: {
            srcJsonMd5Hash: rec.srcJsonMd5Hash,
            recCreatedAt: rec.recCreatedAt,
            recUpdatedAt: rec.recUpdatedAt,
            ...rec.srcJson
          }
        }
      });

      if (this.batchRecords.length === this.csvProc.csvProcessOptions.batchSize) {
        insertCount += await utils.bulkWrite(
          'prp1',
          this.csvProc.csvFeatureOptions.persistCollectionName,
          this.batchRecords
        );
        this.batchRecords = [];
      }
    } //end-while

    insertCount += await utils.bulkWrite(
      'prp1',
      this.csvProc.csvFeatureOptions.persistCollectionName,
      this.batchRecords
    );
    this.batchRecords = [];

    return insertCount;
  }
}
