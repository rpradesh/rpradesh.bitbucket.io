/* eslint-disable no-console */
/* eslint-disable @typescript-eslint/no-parameter-properties */
import {CommonCsvProcessor} from './CommonCsvProcessor';
import {mongo, logger} from '../../Application';
import {BulkWriteOpResultObject} from 'mongodb';
import * as L from 'lodash';

export async function getUpdateRecordActionRecords(recBatchId: string): Promise<any> {
  const colName = 'prp_site';
  const pkField = 'siteId';

  const aggCursor = await mongo
    .utilityDB('prp1')
    .collection('prp_data_stage')
    .aggregate([
      {
        $lookup: {
          from: colName,
          localField: 'recUniqueFieldValue',
          foreignField: pkField,
          as: 'site_docs'
        }
      },
      {$match: {recBatchId: recBatchId, srcValidationErrorCount: 0, recValidationErrorCount: 0}},
      {$unwind: {path: '$site_docs', preserveNullAndEmptyArrays: false}},
      {
        $project: {
          _id: 1,
          srcJsonMd5Hash: 1,
          recUniqueFieldValue: 1,
          'site_docs.siteId': 1,
          'site_docs.srcJsonMd5Hash': 1,
          'site_docs._id': 1
        }
      }
    ]);

  return aggCursor;
}

export class RecordActionProcessor {
  constructor(public readonly csvProc: CommonCsvProcessor) {}

  async process(): Promise<number> {
    const selectOps = await getUpdateRecordActionRecords(this.csvProc.csvProcessOptions.recBatchId);

    const updateArray: any[] = (await selectOps.toArray()).map(rec => {
      const hashMatched = L.isEqual(rec.srcJsonMd5Hash, L.get(rec, 'site_docs.srcJsonMd5Hash', 'unknownHash'));

      logger.info(`hashMatched=${hashMatched} rec=${JSON.stringify(rec)}`);
      return {
        updateOne: {
          filter: {_id: rec._id},
          update: {
            $set: {
              recAction: hashMatched === true ? -1 : 1,
              recParentObjectId: L.get(rec, 'site_docs._id', 'unknownParentId')
            }
          },
          upsert: false
        }
      };
    });

    console.log(`updateArray=`, JSON.stringify(updateArray));

    const writeOps: BulkWriteOpResultObject = await mongo
      .utilityDB('prp1')
      .collection('prp_data_stage')
      .bulkWrite(updateArray, {ordered: false});

    const matchedCount = writeOps.matchedCount;
    logger.info(`matchedCount=${matchedCount} writeOps=${JSON.stringify(writeOps)}`);
    return matchedCount;
  }
}
