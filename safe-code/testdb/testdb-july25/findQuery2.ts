/* eslint-disable @typescript-eslint/no-use-before-define */
/* eslint-disable no-console */
import {app, mongo} from '../Application';

export const insertGivenRecords = async (colName: string, dataAray: any[]): Promise<number> => {
  ///const insertWriteOps = await mongo.centralCollection(colName).insertMany(
  const insertWriteOps = await mongo
    .utilityDB('prp1')
    .collection(colName)
    .insertMany(dataAray, {ordered: false});
  return insertWriteOps.result.n;
};

// 1 million of insert from local to QA DB takes
//4 insert raw documents //return insertedCount: 2,
const insertRecords = async (): Promise<void> => {
  console.log('inside insertRecords 4');
  ///await mongo.centralCollection(`prp_site_5`).deleteMany({});
  await mongo
    .utilityDB('prp1')
    .collection(`prp_site_5`)
    .deleteMany({});
  let records4: any[] = [];

  const totalRecs = 100; //1000 * 1000;//1000;//100;
  const batchSize = 10; //100;//10;

  for (let i = 0; i < totalRecs; i++) {
    records4.push({siteId: `site_${i}`, siteName: `name ${i}`});

    if (records4.length === batchSize) {
      const insertCount = await insertGivenRecords('prp_site_5', records4);
      console.log(`batch insertCount=${insertCount}`);
      records4 = [];
    }
  }
};

export const findRecords = async (): Promise<void> => {
  console.log('inside findRecords 4');

  ///const siteIdArray = Array.from(Array(5).keys()).map(i => `site_${i}`)
  const siteIdArray = Array.from(Array(6).keys()).map(i => `site_${98 + i}`);

  const findIn = await mongo
    .utilityDB('prp1')
    .collection(`prp_site_5`)
    .aggregate([
      {
        $project: {siteId: 1, _id: 0}
      },
      {
        $match: {siteId: {$in: [...siteIdArray]}}
      }
    ])
    .toArray();

  //const oneSiteId = { siteId: 'site_1' };
  console.log('findIn=', findIn);
  //   console.log('findIn=', toStringArray(findIn, 'siteId'));

  /*
{
   let [large, small] = partition([12, 5, 8, 130, 44], x => x > 10 )
   console.log('large=', large, 'small=', small );
}
*/

  {
    const large: any[] = [98, 99, 100, 101, 102, 103];
    const small: any[] = Array.from(Array(100).keys());
    console.log('small=', small, 'large=', large);

    const [inc, exc] = partitionArray(small, large);

    console.log('inc=', inc, 'exc=', exc);
  }
};

export function toStringArray(srcArray: any[], fieldName: string): string[] {
  return srcArray.map(rec => rec[fieldName] as string);
}

export const partition = (ary: any[], filter: (a: any) => boolean) =>
  ary.reduce(
    (acc, e) => {
      acc[filter(e) ? 0 : 1].push(e);
      return acc;
    },
    [[], []]
  );

export const partitionArray = (arr1: any[], arr2: any[]) => {
  let small: any[];
  let large: any[];

  const int: any[] = [];
  const exc: any[] = [];

  if (arr1.length < arr2.length) [small, large] = [arr1, arr2];
  else [large, small] = [arr1, arr2];

  console.log('small=', small, 'large=', large);

  small.forEach(e => {
    console.log(`loop called`);
    if (large.includes(e)) int.push(e);
    else exc.push(e);
  });

  return [int, exc];
};

/*

sid1000000,siteName 1000000 3p0z9l47fm,Valve,2018-06-11,36.71636,-79.09958,prZone-low,927,dnxbbh12q,16,8481 Deangelo Route,Suite 635,Marisaview,South Dakota,34573-9050
sid1000001,siteName 1000001 mqusw3ht6v,PRV,2016-08-30,35.89699,-81.67647,prZone-low,315,dnmujqv50,16,0464 Kautzer Inlet,Apt. 121,South Shayne,South Dakota,04563-7045
sid1000002,siteName 1000002 bozbqbytyb,PRV,2015-08-09,35.90123,-76.65060,prZone-low,711,dq3kpz8ez,38,4011 Cruickshank Track,Suite 028,New Marjorystad,Colorado,38149-0875
sid1000003,siteName 1000003 u30xh0b7n7,Tank,2016-05-11,35.05085,-79.50634,prZone-high,771,dnprquc1z,26,323 Carli Shoals,Suite 441,South Ambrose,Pennsylvania,18941
sid1000004,siteName 1000004 2nt0hn2f30,Flushing,2015-12-18,35.88672,-81.77262,prZone-high,903,dnmu5hyzr,34,215 Schroeder Camp,Suite 089,Bonitaborough,Mississippi,19480
sid1000005,siteName 1000005 rftp5554t1,Flushing,2015-07-11,36.50116,-77.22661,prZone-high,219,dq3pdsv69,40,08753 Kuphal Common,Suite 894,West Moshe,Illinois,54713-1579
sid1000006,siteName 1000006 exvk68ydbo,Valve,2017-02-08,35.47313,-78.27402,prZone-medium,474,dq23fcdtg,20,02222 Ullrich Parkway,Suite 850,Port Skylarton,West Virginia,36111
sid1000007,siteName 1000007 2heayp2duc,PRV,2017-05-17,34.73305,-78.14394,prZone-medium,858,dq0mtfcqd,50,0255 Hickle Harbors,Suite 223,New Myrlstad,Idaho,54125
sid1000008,siteName 1000008 h2tlbj3sqo,Tank,2017-08-25,36.44498,-80.98930,prZone-medium,177,dnqrm497k,40,559 Kasey Harbors,Suite 546,Lake Dean,Oregon,78696
sid1000009,siteName 1000009 ngme3gbl0b,Valve,2017-12-08,36.11286,-76.09393,prZone-medium,510,dq3vkqnhd,14,25765 Conroy Avenue,Apt. 009,East Karlie,New Mexico,79518-2852

*/
