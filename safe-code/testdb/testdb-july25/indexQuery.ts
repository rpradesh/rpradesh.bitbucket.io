/* eslint-disable no-case-declarations */
/* eslint-disable no-console */
import {app, mongo, logger} from '../Application';
import * as utils from '../utils';
import {execFileSync} from 'child_process';
import * as indexQuery from './indexQuery';
import * as findQuery from './findQuery';
import * as findQuery2 from './findQuery2';
import * as bulkQuery from './bulkQuery';

export async function init() {
  console.info(`process title=[${process.title}] pid=[${process.pid}] env=${JSON.stringify(app.env)}`);
  // init and validate mongo connection using env
  await mongo.initConnection(app.env.mongoConnectionURL);
  console.info(`isConnected=${mongo.mongoClient.isConnected()}`);
}

// createIndex(), we need 3 args, colName, keys, options
// collectionIndexSummary() returns array, we need  dbname, collection Name and filter_id=true/false
// listCollNames() returns string[] for given dbname

export const createUniqueIndex = async (): Promise<void> => {
  console.log('inside createUniqueIndex');
  /*
  const count = await mongo
    .utilityDB(`prp1`)
    .collection('prp_site')
    .deleteMany({});
  console.log(`deleteMany prp_site count=${count}`);
  */
  const indexOps = await mongo
    .utilityDB(`prp1`)
    .collection('prp_site')
    .createIndex({item: 1}, {unique: true});
  console.log('>> createIndex indexOps=', JSON.stringify(indexOps));
};

export const createAndDisplayIndex = async (uName: string, colName: string): Promise<void> => {
  const indexOps = await mongo.createIndex(mongo.utilityDB(uName), colName, {item: 1}, {unique: true});
  console.log('>> createAndDisplayIndex indexOps=', JSON.stringify(indexOps));
};

export const displayIndexSummary = async (colName: string): Promise<void> => {
  console.log('inside displayIndexSummary');
  const indexes = await mongo
    .utilityDB(`prp1`)
    .collection(colName)
    .indexes();
  utils.printPretty(indexes);
  console.log('>> indexes() indexes=', JSON.stringify(indexes));
};

export const displayCollectionNames = async (): Promise<void> => {
  console.log('inside displayCollectionNames');
  const cols = await mongo.centralDB.listCollections({}).toArray();
  console.log('>> listCollections() cols=', cols);
};

export async function directQuery() {
  console.info(`isConnected=${mongo.mongoClient.isConnected()}`);
}

export async function testSleep() {
  execFileSync(`sleep 10`);
}

async function sleepNow(secs) {
  //return new Promise(resolve => setTimeout(resolve, millis));
  await new Promise(done => setTimeout(done, secs * 1000));
}

const isMongo = true;
// javasript IIFE
void (async function() {
  if (isMongo) await init();
  try {
    //parsePom();
    //console.log(`process.execArgv=`, process.execArgv);
    // --
    // await bulkQuery.insertManyOrderedFalse();
    ///await createUniqueIndex();
    const idxCreateSummary = await mongo.createIndex(mongo.utilityDB('prp1'), 'prp_site', {item: 1}, {unique: true});
    console.log(`>> idxCreateSummary=`, idxCreateSummary);
    //const _createAndDisplayIndex = await createAndDisplayIndex('prp1', 'prp_site');
    //console.log(`>> createAndDisplayIndex=`, _createAndDisplayIndex);
    // --
    ///await displayIndexSummary('prp_site');
    // const idxSummary = await mongo.getIndexSummary(mongo.centralDB, 'prp_site_1');
    // console.log(`>> idxSummary=`, idxSummary);
    // --
    // await displayCollectionNames();
    // const strArray = await mongo.listCollectionNames(mongo.centralDB);
    // console.log(`>> strArray=`, strArray);
    // --
    // await directQuery();
    // logger.info(`> 33task-${process.pid} start=${new Date()}`);
  } catch (err) {
    console.error('Application Start Error', err);
  } finally {
    if (isMongo) mongo.close();
  }
})();

/*
GetIndexs = https://www.guru99.com/working-mongodb-indexes.html#3

Create Index=
https://docs.mongodb.com/manual/reference/method/db.collection.createIndex/#db.collection.createIndex

collection: "prp_site", keys : { siteId: 1 }, options: {unique:true} 

https://bitbucket.org/rpradesh/fork-sa-wms-prp/src/wms150_sitecsv_v1/prp-service/src/
GOOD https://bitbucket.org/rpradesh/fork-sa-wms-prp/src/working_sitefilter_jun26/prp-service/src/server.2.ts

// https://docs.mongodb.com/manual/reference/method/db.collection.bulkWrite/
// https://docs.mongodb.com/manual/core/bulk-write-operations/

*/
