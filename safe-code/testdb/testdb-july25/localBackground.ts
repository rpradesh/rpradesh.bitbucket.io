/* eslint-disable no-console */
import * as testutil from '../spec/testutil';
import * as L from 'lodash';

process.on('message', msg => {
  const secs: number = L.isNumber(msg.secs) ? L.toNumber(msg.secs) : 5;
  L.assign(msg, {SubProcStart: new Date()});
  console.log(`starting-tb2 sleep secs=${secs}`);
  console.log(`> task-${process.pid} start:`, new Date());
  testutil.executeShell(`sleep ${secs}`);
  console.log(`> task-${process.pid}   end:`, new Date());
  L.assign(msg, {SubProcsEnd: new Date()});

  process.send(msg);
});
