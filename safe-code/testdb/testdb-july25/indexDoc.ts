/* eslint-disable no-console */
import * as fs from 'fs';
import {valid} from '@hapi/joi';
const filePath = '/home/rpradesh/projects/github/sense-workspace/tasks/wms/app-infra/npm_library_list_csv.csv';
const file = fs.readFileSync(filePath, 'utf-8');
const fileContents = file.toString();
import {createWriteStream, WriteStream} from 'fs';
const fileWriteStream: WriteStream = createWriteStream(`outfile.md`);

const options = {
  hasTemplate: true,
  hasHeader: true,
  columnCount: 11,
  pad: 2
};
// console.log(fileContents);

interface IRecord {
  colValue: string[];
  lineIndex: number;
  varValue: string;
}

const fileRecords: IRecord[] = [];
const outRecords: IRecord[] = [];

function readIntoFileRecords() {
  fileContents.split('\n').forEach((line, i) => {
    const rec: IRecord = {
      colValue: line.split('|'),
      lineIndex: i,
      varValue: line.split('|')[1]
    };
    fileRecords.push(rec);
  });
}

function populateOutRecords() {
  //using var replace value too
  const templateRec: IRecord = fileRecords[0];
  outRecords.push(fileRecords[1]); //header row

  // const headerRec: IRecord = fileContents[1];
  //for (let i = 2; i < 5 /*fileRecords.length*/; i++) {
  for (let i = 2; i < fileRecords.length; i++) {
    const fRec: IRecord = fileRecords[i];
    ///console.log(`fRec=`, fRec);
    const outRec: IRecord = fileRecords[i];

    templateRec.colValue.forEach((v, i) => {
      const val = v.replace(/PPP/g, fRec.colValue[i]);
      const val2 = val.replace(/QQQ/g, fRec.varValue);
      ///console.log(`v=${v} fRec.colValue[${i}]=${fRec.colValue[i]} val=${val}  val2=${val2}`)
      outRec.colValue[i] = val2;
    });

    outRecords.push(outRec); //data row replaced
  } //outer-for
}

function updateOutRecordsForPadding() {}

function writetoMdFile() {
  outRecords.forEach((rec, i) => {
    const aLine = `|  ` + rec.colValue.join(`  |  `) + `  | \n`;
    const newLine = i == 0 ? aLine.replace(/\|/g, '||') : aLine;

    //console.log(newLine);
    fileWriteStream.write(newLine);
  });
}

function run() {
  readIntoFileRecords();
  populateOutRecords();
  updateOutRecordsForPadding();
  writetoMdFile();
}

// IIFE
(() => {
  run();
})();
