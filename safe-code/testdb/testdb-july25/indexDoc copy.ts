import * as fs from 'fs'
const filePath = '/home/rpradesh/projects/bitbucket/master/sa-wms-prp/docs/_media/misc/list.txt'
const file = fs.readFileSync(filePath, 'utf-8');
const fileContents = file.toString();

// console.log(fileContents);

function pad(line: string, pad: number): string {
    const diff = pad - line.length
    const buildStr =  diff > 0 ? line + new Array(diff).fill(' ').join(''): line;
    return '|  ' + buildStr;
}

function run(){
    let counter = 1;
    for(const word of fileContents.split('\n')){
        const lib = word.trim();
        console.log(
            pad(counter + '', 2),
            pad(`https://www.npmjs.com/package/${lib}`, 60),
            pad(`ccc`, 3),
            pad(` `, 120),
            pad(`eee`, 3),
            pad(`fff`, 3),
            pad(`[![npm version](https://badge.fury.io/js/${lib}.svg)](https://badge.fury.io/js/${lib})`, 130),
            pad(`[![Known Vulnerabilities](https://snyk.io/test/npm/${lib}/badge.svg)](https://snyk.io/test/npm/${lib})`, 145),
            '|'
        );
        counter++;
    }
}
/* 1) 
https://www.npmjs.com/package/fantasy-land mongodb-memory-server-core
[![npm version](https://badge.fury.io/js/mongodb-memory-server-core.svg)](https://badge.fury.io/js/mongodb-memory-server-core)
[![Known Vulnerabilities](https://snyk.io/test/npm/mongodb-memory-server-core/badge.svg)](https://snyk.io/test/npm/mongodb-memory-server-core)

*/

// IIFE
( ()=>{

    run();
})();