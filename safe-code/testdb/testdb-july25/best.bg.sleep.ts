/* eslint-disable @typescript-eslint/no-use-before-define */
/* eslint-disable no-case-declarations */
/* eslint-disable no-console */
import {app, mongo, applicationInit, applicationClose} from '../Application';

import {executeInBackground} from '../common/task';
import {SleepTaskRequest, SleepTaskResponse, SleepTask} from '../common/task/sleepTask';
import * as utils from '../utils';
import * as L from 'lodash';

const sleepTaskConfig = app.properties.sleepTaskConfig;
const person: ITestPerson = {name: 'Jonny 15', age: 15};

interface ITestPerson {
  name: string;
  age: number;
}

const execCallback = (err: any, tskRes: SleepTaskResponse) => {
  console.log(`main-pid=${process.pid} msg=${tskRes.completedMessage} tskRes=${JSON.stringify(tskRes)}`);
};

function testTaskWithCallbackWithObject() {
  const tskReq = new SleepTaskRequest(10);
  const task = new SleepTask(tskReq);
  task.start(execCallback, person);
}

function testTaskWithCallback() {
  const tskReq = new SleepTaskRequest(10);
  const task = new SleepTask(tskReq);
}

// javasript IIFE
const isMongo = true;
void (async function() {
  try {
    console.log(`process.execPath=${process.execPath} |process.execArgv=${process.execArgv.join('|')}`);
    if (isMongo) await applicationInit();
    console.log(`main process mongo-connected=${mongo.mongoClient.isConnected()}`);

    console.info(` beforeStart() beforeStart() beforeStart()=${new Date()}`);
    const tskReq = new SleepTaskRequest(10);
    const task = new SleepTask(tskReq);
    task.start(); /// test 1 ///
    //task.start(execCallback);/// test 2 ///
    //task.start(execCallback, person);/// test 3 ///
    console.info(` afterStart() afterStart() afterStart()=${new Date()}`);

    await utils.sleepSecs(15);
    console.info(`after-15secs  after-15secs after-15secs after-15secs =${new Date()}`);
  } catch (err) {
    console.error('Application Start Error', err);
  } finally {
    if (isMongo) await applicationClose();
  }
})();

function isEq(left: any, right: any, msg?: string): void {
  if (!L.isEqual(left, right)) throw new Error(`left=${left}, right=${right} are NOT equal msg=${msg}`);
}

function log(msg: any) {
  console.log(msg);
}
