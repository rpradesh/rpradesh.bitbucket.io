import * as path from 'path';
import {fork} from 'child_process';
import * as L from 'lodash';
import {app, logger} from '../../Application';
import {TaskRequest, TaskResponse} from './model';

export abstract class BaseTask {
  public readonly taskName: string;

  constructor(taskName: string) {
    this.taskName = taskName;
  }

  async start(givenTaskRequest: TaskRequest, callback?: (err: any, data: TaskResponse) => any): Promise<void> {
    const taskId = `${new Date().toUTCString()}`;
    L.assign(givenTaskRequest, {taskId});
    app.taskInProcess.set(taskId, 'unknown-pid');

    // fork and send Request
    const task = fork(path.join(__dirname, './background.ts'));
    task.send({taskRequest: givenTaskRequest});

    // gets called after task is completed from background.ts
    task.on('message', result => {
      // update completedTS to result
      L.assign(result, {taskCompletedTS: new Date()});

      if (callback) {
        if (result) callback(null, result);
        else callback(`failed to get results from bg givenTaskRequest=${JSON.stringify(givenTaskRequest)}`, null);
      }
      task.disconnect();
    });
  }

  abstract run(taskRequest: TaskRequest, taskResponse: TaskResponse): Promise<TaskResponse>;

  supportsMultipleInstance(): boolean {
    return false;
  }

  get taskConfigName() {
    return `${this.taskName}TaskConfig`;
  }

  get taskStatusName() {
    return `${this.taskName}TaskStatus`;
  }
} //end-class
