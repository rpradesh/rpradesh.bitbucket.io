/* eslint-disable @typescript-eslint/no-use-before-define */
/* eslint-disable no-case-declarations */
/* eslint-disable no-console */
import {app, mongo, logger, applicationInit, applicationClose, IMongoIndexDef} from '../Application';
import {MongoIndexTaskRequest, MongoIndexTaskResponse} from '../common/task/mongoIndexTask';
import {DefaultUtilityIndexTaskRequest, DefaultUtilityIndexTaskResponse} from '../common/task/defaultUtilityIndexTask';

import {executeInBackground} from '../common/task';
import {SleepTaskRequest, SleepTaskResponse} from '../common/task/sleepTask';
import * as utils from '../utils';
import * as L from 'lodash';

interface ITestPerson {
  name: string;
  age: number;
}

const siteIndexDefn: IMongoIndexDef = {
  collection: 'prp_site',
  keys: {siteName: -1},
  options: {name: 'prpsite_sitenameD', background: false}
};

const execCallback = (err: any, taskResponse: MongoIndexTaskResponse) => {
  console.log(
    `defaultUtilityIndexTaskRequest DONE main-pid=${process.pid} completedMessage=${
      taskResponse.completedMessage
    } taskResponse=${JSON.stringify(taskResponse)}`
  );
};

async function insertMoreUtility(): Promise<void> {
  for (const i of L.range(11, 21)) {
    const j = {
      _id: `utility_id_test${i}`,
      name: `test${i}`,
      description: `test${i} Tenant - test${i} Pressure Profile on Local`,
      db: `verdeeco_test${i}_db`,
      timezone: `America/New_York`
    };

    const insertOps = await mongo.centralDB.collection('utility').insertOne(j);
    console.log(`insertOps=`, insertOps);
  } //end-for
}

async function dropIndexes(): Promise<void> {
  for (const i of L.range(11, 21)) {
    for (const idx of ['prp_site', 'prp_device']) {
      const dropOps = await mongo
        .utilityDB(`test${i}`)
        .collection(idx)
        .dropIndexes();
      console.log(`dropOps=`, dropOps);
    }
  } //end-for
}

const siteAddress = {
  siteName: 'site name 1',
  address: {
    addressStree1: ' ad str 11',
    city: 'city 1'
  }
};

/*
db.getCollection("prp_properties").find({ _id: "DefaultUtilityIndexTaskStatus" })

cd ~/projects/bitbucket/master/sa-wms-prp
grep 'firing one' 
ps -aux | grep node_prp

tail -f prp-service/log/prp-service.log | grep 'firing one'

*/

// javasript IIFE
const isMongo = true;
void (async function() {
  try {
    if (isMongo) await applicationInit();
    // 1 -----
    //console.log(`print 2=`, siteAddress);
    //console.log(`city=`, L.get(siteAddress, 'address.city'));
    // 2 -----
    //const givenUtility = 'prp1';
    //const mongoIndexDefn = L.assign(siteIndexDefn, {utility: givenUtility});
    //const person: ITestPerson = {name: 'Jonny 20', age: 20};
    //const mongoIndexTaskRequest = new MongoIndexTaskRequest(mongoIndexDefn);
    //await executeInBackground(mongoIndexTaskRequest, execCallback, person);

    // 3 ----
    //await insertMoreUtility();
    //await dropIndexes();

    // 4 ----
    const defaultUtilityIndexTaskRequest = new DefaultUtilityIndexTaskRequest('ALL', 2);
    await executeInBackground(defaultUtilityIndexTaskRequest, execCallback, {});

    // end -------
    await utils.sleepSecs(15 * 15);
    console.info(`44 isConnected=${mongo.mongoClient.isConnected()}`);
  } catch (err) {
    console.error('Application Start Error', err);
  } finally {
    if (isMongo) await applicationClose();
  }
})();

function isEq(left: any, right: any, msg?: string): void {
  if (!L.isEqual(left, right)) throw new Error(`left=${left},  right=${right} are NOT equal msg=${msg}`);
}

function log(msg: any) {
  console.log(msg);
}
