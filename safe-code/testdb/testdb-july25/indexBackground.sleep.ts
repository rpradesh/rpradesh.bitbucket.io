/* eslint-disable @typescript-eslint/no-use-before-define */
/* eslint-disable no-case-declarations */
/* eslint-disable no-console */
import {app, mongo, logger, applicationInit, applicationClose} from '../Application';

import {executeInBackground} from '../common/task';
import {SleepTaskRequest, SleepTaskResponse} from '../common/task/sleepTask';
import * as utils from '../utils';
import * as L from 'lodash';

interface ITestPerson {
  name: string;
  age: number;
}

const execCallback = (err: any, taskResponse: SleepTaskResponse) => {
  console.log(
    `main-pid=${process.pid} sleepCompletedMessage=${taskResponse.sleepCompletedMessage} taskResponse=${JSON.stringify(
      taskResponse
    )}`
  );
};

// javasript IIFE
const isMongo = true;
void (async function() {
  try {
    console.log(`process.execPath=${process.execPath} |process.execArgv=${process.execArgv.join('|')}`);
    if (isMongo) await applicationInit();

    const sleepTaskConfig = app.properties.sleepTaskConfig;
    const person: ITestPerson = {name: 'Jonny 15', age: 15};
    const sleepTaskRequest = new SleepTaskRequest(sleepTaskConfig.defaultSecs);
    await executeInBackground(sleepTaskRequest, execCallback, person);

    await utils.sleepSecs(15);
    console.info(`37 isConnected=${mongo.mongoClient.isConnected()}`);
  } catch (err) {
    console.error('Application Start Error', err);
  } finally {
    if (isMongo) await applicationClose();
  }
})();

function isEq(left: any, right: any, msg?: string): void {
  if (!L.isEqual(left, right)) throw new Error(`left=${left}, right=${right} are NOT equal msg=${msg}`);
}

function log(msg: any) {
  console.log(msg);
}
