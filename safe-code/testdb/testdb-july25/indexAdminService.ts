/* eslint-disable no-case-declarations */
/* eslint-disable no-console */
import {AdminService} from '../components/Admin/service';
import {app, mongo, logger, applicationInit, applicationClose} from '../Application';

const json = {
  keys: {siteId: 1},
  options: {
    name: 'prpsite_siteidA_unique',
    unique: true
  }
};

const isMongo = true;
// javasript IIFE
void (async function() {
  if (isMongo) await applicationInit();
  try {
    const getMongoIndexes: any = await AdminService.getMongoIndexes('prp1');
    console.log('getMongoIndexes response==', getMongoIndexes);
    /*
    console.log(
      'omitProperties getMongoIndexes response==',
      utils.omitPropertiesForMapWithArray(getMongoIndexes, ['key'])
    );
    */
    //--
    // const refreshMongoIndexes: any = await AppService.getMongoIndexes('prp1');
    // console.log('refreshMongoIndexes response==', refreshMongoIndexes);
  } catch (err) {
    console.error('Application Start Error', err);
  } finally {
    if (isMongo) await applicationClose();
  }
})();
