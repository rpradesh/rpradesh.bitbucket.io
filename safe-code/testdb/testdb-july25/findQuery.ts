/* eslint-disable @typescript-eslint/no-use-before-define */
/* eslint-disable no-console */
import {mongo} from '../Application';
import * as utils from '../utils';
import * as L from 'lodash';

export const insertGivenRecords = async (colName: string, dataAray: any[]): Promise<number> => {
  ///const insertWriteOps = await mongo.centralCollection(colName).insertMany(
  const insertWriteOps = await mongo
    .utilityDB('prp1')
    .collection(colName)
    .insertMany(dataAray, {ordered: false});
  return insertWriteOps.result.n;
};
// 1 million of insert from local to QA DB takes
//4 insert raw documents //return insertedCount: 2,
export const insertRecords = async (): Promise<void> => {
  console.log('inside insertRecords 4');
  ///await mongo.centralCollection(`prp_site_5`).deleteMany({});
  await mongo
    .utilityDB('prp1')
    .collection(`prp_site_1`)
    .deleteMany({});
  let records4: any[] = [];

  const totalRecs = 100; //1000 * 1000;//1000;//100;
  const batchSize = 10; //100;//10;

  for (let i = 0; i < totalRecs; i++) {
    records4.push({siteId: `site_${i}`, siteName: `name ${i}`});

    if (records4.length === batchSize) {
      const insertCount = await insertGivenRecords('prp_site_1', records4);
      console.log(`batch insertCount=${insertCount}`);
      records4 = [];
    }
  }
};

export const findRecords = async (): Promise<void> => {
  console.log('inside findRecords 4');

  ///const siteIdArray = Array.from(Array(5).keys()).map(i => `site_${i}`)
  const siteIdArray = Array.from(Array(6).keys()).map(i => `site_${98 + i}`);

  const findIn = await mongo
    .utilityDB('prp1')
    .collection(`prp_site_1`)
    .aggregate([{$project: {siteId: 1, _id: 0}}, {$match: {siteId: {$in: [...siteIdArray]}}}])
    .toArray();

  const oneSiteId = {siteId: 'site_1'};
  console.log('findIn=', findIn);
};

export const findRecordsCursor = async (): Promise<void> => {
  console.log('inside findRecordsCursor 1.0');

  const cursor = await mongo
    .utilityDB('prp1')
    .collection(`prp_site_1`)
    .find({siteId: /^site_9.*$/i});

  const oneSiteId = {siteId: 'site_1'};
  //console.log('findIn=', findIn);
};

/*
export const persistBatchIntoMongo = (recs: IRecord[], csvOptions: ICsvOptions): number => {
  // build given csv siteId Array
  const srcSiteIdArray = recs.map( r => r.json['siteId']);
  // pass this siteIdArray to make findArray to know if they exist
  const existSiteIdArray =
  // using both of above array's, make a split for insert/update id array

  // execute insert's

  // execute updates


  return 0;
}

const findRecords = async (siteIdArray: string[]): Promise<string[]> => {
   const foundObjArray = await mongo.utilityDB('prp1').collection(`prp_site`).aggregate([
     {   $project: { siteId: 1, _id: 0}  },
     {   $match:   { siteId: {$in: [...siteIdArray]} }   }
 ]).toArray();

 return utils.parseFieldFromObjectArray(foundObjArray,'siteId' );

}
*/

// javasript IIFE
const isMongo = true;
void (async function() {
  try {
    if (isMongo) await mongo.initConnection(`mongodb://127.0.0.1:27017/`);
    console.info(`44 isConnected=${mongo.mongoClient.isConnected()}`);

    await insertRecords();
  } catch (err) {
    console.error('Application Start Error', err);
  } finally {
    if (isMongo) await mongo.close();
  }
})();

function isEq(left: any, right: any, msg?: string): void {
  if (!L.isEqual(left, right)) throw new Error(`left=${left},  right=${right} are NOT equal msg=${msg}`);
}

function log(msg: any) {
  console.log(msg);
}
