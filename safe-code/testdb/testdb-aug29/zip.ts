/* eslint-disable @typescript-eslint/camelcase */
/* eslint-disable @typescript-eslint/no-object-literal-type-assertion */
/* eslint-disable @typescript-eslint/no-use-before-define */
/* eslint-disable no-case-declarations */
/* eslint-disable no-console */
import {app, logger, mongo, applicationInit, applicationClose} from '../Application';
import * as utils from '../utils';
import * as L from 'lodash';
import * as fs from 'fs';
import * as moment from 'moment';
import * as md5File from 'md5-file';
import * as crypto from 'crypto';
import {SiteCsvSchema} from '../components/Site/siteCsvSchema';
import {CommonCsvProcessor} from '../common/datastage/CommonCsvProcessor';
import {BulkWriteOpResultObject} from 'mongodb';

import {
  /* Models */
  ICsvFieldSchema,
  CsvProcessOptions,
  CsvProcessResult,
  CsvSrcRecord,
  CsvStageRecord,
  CsvFeatureOptions
  /* processor */
} from '../common/datastage';

const siteRecord = {
  siteName: 'siteName 1000005 rftp5554t1',
  siteType: 'Flushing',
  insertDate: '2015-07-11',
  lng: '-77.22661',
  lat: '36.50116',
  pressureZone: 'prZone-a566mb96l6',
  elevation: '219',
  geoHash: 'dq3pdsv69',
  elevationOffset: '40',
  address: {
    line1: '08753 Kuphal Common',
    line2: 'Suite 894',
    city: 'West Moshe',
    state: 'Illinois',
    zip: '54713-1579a'
  }
};

async function testHexMd5(): Promise<void> {
  const expectedString = 'fbd8acb9d912bc8e7be1c7e2b26f1e08';
  //crypto.Hash;//ash(alg).update(data).digest(digest)
  const actualString = crypto
    .createHash('md5')
    .update(JSON.stringify(siteRecord))
    .digest('hex');

  console.log(`expectedString=${expectedString} are equal=${expectedString === actualString}`);
}
const DIR_NAME = '/Users/rajeshpradeshik/data/site-ingest';
const inputFilepath = DIR_NAME + '/1000k-sitedata.csv';
//10-sitedata.csv'; //1.7KB
//10k-sitedata-edit-10.csv'; //1.7KB
//10k-sitedata.csv'; //1.6MB
//100k-sitedata.csv'; //16MB
//1000k-sitedata.csv';//163MB //takes 4.5 mins to push to stage
//1000k-sitedata-edit-10.csv';//163MB //takes 4.5 mins to push to stage
// db.prp_site.renameCollection('prp_site_10k')

async function testLookupAggQuery(): Promise<void> {
  const insertOps = await mongo
    .utilityDB('prp1')
    .collection('prp_site')
    .aggregate([
      {
        $lookup: {
          from: 'prp_data_stage',
          localField: 'siteId',
          foreignField: 'recUniqueFieldValue',
          as: 'data_stage_docs'
        }
      },
      {$match: {'data_stage_docs.recBatchId': 'SiteIngest-b068fc90-c47c-11e9-a3cb-ad6737b71c01'}},
      {$project: {_id: 1}}
    ]);
  // .toArray();

  const aggCount = (await insertOps.toArray()).length;
  //console.log(`testLookupQuery`, insertOps);
  console.log(`aggCount`, aggCount);
}
async function testLookupCountQuery(): Promise<void> {
  const insertOps = await mongo
    .utilityDB('prp1')
    .collection('prp_site')
    .countDocuments([
      {
        $lookup: {
          from: 'prp_data_stage',
          localField: 'siteId',
          foreignField: 'recUniqueFieldValue',
          as: 'data_stage_docs'
        }
      },
      {$match: {$expr: {'data_stage_docs.recBatchId': 'SiteIngest-b79ce000-c475-11e9-b3e0-8f4781c06518'}}},
      {$project: {_id: 1}}
    ]);

  console.log(`testLookupCountQuery`, insertOps);
}

async function testPercentCountQuery(): Promise<void> {
  const aggCursor = await mongo
    .utilityDB('prp1')
    .collection('prp_site')
    .aggregate([
      {
        $lookup: {
          from: 'prp_data_stage',
          localField: 'siteId',
          foreignField: 'recUniqueFieldValue',
          as: 'data_stage_docs'
        }
      },
      {$match: {'data_stage_docs.recBatchId': 'SiteIngest-e54df640-c553-11e9-854b-efa9d6099ad0'}},
      {$project: {_id: 1}}
    ]);
  const foundCount = (await aggCursor.toArray()).length;
  console.log(`testPercentCountQuery=${JSON.stringify(foundCount)}`);
}

async function testAggInsert() {
  const insertOps = await mongo
    .utilityDB('prp1')
    .collection('src_zipcodes_agg')
    .insertMany(
      await mongo
        .utilityDB('rzip')
        .collection('src_zipcodes')
        .aggregate([
          {$group: {_id: '$state', totalPop: {$sum: '$pop'}}},
          {$match: {totalPop: {$gte: 10 * 1000 * 1000}}}
        ])
        .toArray(),
      {ordered: false}
    );
  console.log(`insertOps=`, insertOps);
}

import * as bascCsv from '../common/datastage/duplicateCheckProcessor';

async function testDupeQuery() {
  const dupeArray = await bascCsv.fetchDupeRecords('SiteIngest-8b130140-c47f-11e9-84f1-4b6ce1a9c7c4');
  const batchId = 'SiteIngest-8b130140-c47f-11e9-84f1-4b6ce1a9c7c4';

  dupeArray.forEach(async (v, i) => {
    await bascCsv.updateDupeRecords(batchId, v);
  });

  console.log(`dupeArray=`, dupeArray);
}

interface IPerson {
  fname: string;
  lname: string;
}

interface IAgePerson {
  fname: string;
  lname: string;
  age: number;
}
async function testDestruct(): Promise<void> {
  const agePerson: IAgePerson = {fname: 'F1', lname: 'L1', age: 21};
  // https://lodash.com/docs/#pick

  const persons: IPerson[] = [];
  //persons.push(([fname, lname] = agePerson));
  persons.push(L.pick(agePerson, 'fname', 'lname')); //works

  const {fname, lname} = agePerson;
  persons.push({fname, lname}); //works

  //persons.push(({fname, lname} = agePerson)); //works
  console.log(persons);
}
//https://www.sitepoint.com/lodash-features-replace-es6/
/*
element.sensors.forEach(s => {
          const sensor: ISensorMetaData = {
            id: s.id,
            UoM: s.UoM,
            label: s.label
          };
          item.sensors.push(sensor);
        });
   element.sensors.forEach(s => {
      //item.sensors.push( L.pick(s, ['id', 'UoM', 'label']) );//ISensorMetaData
      //item.sensors.push( {id, UoM, label} = s) );//ISensorMetaData
   });     
*/

async function testCsvProcess(): Promise<void> {
  await mongo
    .utilityDB('prp1')
    .collection('prp_data_stage')
    .deleteMany({});

  const csvSchema = new SiteCsvSchema();
  const csvProcOptions: CsvProcessOptions = new CsvProcessOptions('SiteIngest', inputFilepath);
  const csvFeatureOptions = new CsvFeatureOptions('prp_site', false, 0, true, 'siteId');

  const csvProcessor = new CommonCsvProcessor(csvSchema, csvProcOptions, csvFeatureOptions);
  const processSummary = await csvProcessor.process();
}
// javasript IIFE
const isMongo = true;
void (async function() {
  try {
    if (isMongo) await applicationInit();
    await testCsvProcess();
    //await testDestruct();
    //await testPercentCountQuery();
    //await testAggInsert();
    //await testHexMd5();
    //await testLookupAggQuery();
    //await testLookupCountQuery();
    //await testDupeQuery();
    //await testUpdateFlagQuery();
  } catch (err) {
    console.error('Application Start Error', err);
  } finally {
    if (isMongo) await applicationClose();
  }
})();

/*
void (async function() {
  await testDate();
})();

CON_OPTS=' localhost:27017/verdeeco_prp1_db --quiet --eval '
mongo $CON_OPTS 'db.prp_site.deleteMany({})'

*/
