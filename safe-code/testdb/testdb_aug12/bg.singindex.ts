/* eslint-disable @typescript-eslint/no-use-before-define */
/* eslint-disable no-case-declarations */
/* eslint-disable no-console */
import {mongo, applicationInit, applicationClose, IMongoIndexDef} from '../Application';
import {
  SingleUtilityIndexTask,
  SingleUtilityIndexTaskRequest,
  SingleUtilityIndexTaskResponse
} from '../common/task/singleUtilityIndexTask';
import * as utils from '../utils';
import * as L from 'lodash';

const siteIndexDefn: IMongoIndexDef = {
  collection: 'prp_site',
  keys: {siteName: -1},
  options: {name: 'prpsite_sitenameD', background: false}
};

const execCallback = (err: any, taskResponse: SingleUtilityIndexTaskResponse) => {
  console.log(
    `defaultUtilityIndexTaskRequest DONE main-pid=${process.pid} completedMessage=${
      taskResponse.completedMessage
    } taskResponse=${JSON.stringify(taskResponse)}`
  );
};

// javasript IIFE
const isMongo = true;
void (async function() {
  try {
    if (isMongo) await applicationInit();

    // 1 --------
    const utilityName = 'ALL'; // 'ALL'; 'prp1';

    // 2 ----------
    const tskReq = new SingleUtilityIndexTaskRequest();
    tskReq.utility = utilityName;
    tskReq.mongoIndexDefinition = siteIndexDefn;
    const task = new SingleUtilityIndexTask(tskReq);
    task.start(execCallback);

    await utils.sleepSecs(35);
    console.info(`after-15secs  after-15secs after-15secs after-15secs =${new Date()}`);
  } catch (err) {
    console.error('Application Start Error', err);
  } finally {
    if (isMongo) await applicationClose();
  }
})();
