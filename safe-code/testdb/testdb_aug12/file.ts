/* eslint-disable @typescript-eslint/no-use-before-define */
/* eslint-disable no-case-declarations */
/* eslint-disable no-console */
import {app, logger, mongo, applicationInit, applicationClose} from '../Application';
import * as utils from '../utils';
import * as L from 'lodash';
import * as fs from 'fs';
import * as moment from 'moment';

import {siteCsvFieldSchemaArray} from '../config';

// load local json
import * as parseObjectArray from './parseObjectArray.json';
export const parseObjectArrayAny: any[] = (parseObjectArray as unknown) as any[];

async function testDate() {
  const dateTimeStr = moment(new Date()).format('MMDDYYYY_HHmmss');
  console.log(`dateTimeStr=${dateTimeStr}`);
}

async function buildLine(mongoPaths: string[], rec: any): Promise<string> {
  const colData: string[] = [];
  mongoPaths.forEach((path, i) => {
    colData.push(L.get(rec, path, ''));
  });
  return colData.join(',');
}

async function testCursorPipe() {
  const mongoPaths: string[] = utils.parseFieldFromObjectArray(siteCsvFieldSchemaArray, 'mongoPath');
  console.log(`mongoPaths=${mongoPaths}`);
  const cursor = await mongo
    .utilityDB('prp1')
    .collection('prp_site')
    .find({});

  const fsWriter = await fs.createWriteStream(`/tmp/file1.csv`);
  while (await cursor.hasNext()) {
    const rec = await cursor.next();
    const line = await buildLine(mongoPaths, rec);
    console.log(`line=${line}`);
    await fsWriter.write(line + '\n');
  }
  await fsWriter.close();

  //cat /tmp/file1.csv
}

// javasript IIFE
const isMongo = true;
void (async function() {
  try {
    if (isMongo) await applicationInit();
    await testCursorPipe();
  } catch (err) {
    console.error('Application Start Error', err);
  } finally {
    if (isMongo) await applicationClose();
  }
})();

/*
void (async function() {
  await testDate();
})();
*/
