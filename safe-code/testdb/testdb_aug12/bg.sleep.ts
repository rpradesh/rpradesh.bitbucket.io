/* eslint-disable @typescript-eslint/no-use-before-define */
/* eslint-disable no-case-declarations */
/* eslint-disable no-console */
import {app, logger, mongo, applicationInit, applicationClose} from '../Application';
import * as utils from '../utils';
import * as slTask from '../common/task/sleepTask';
import * as L from 'lodash';

function testInitWithArg() {
  const taskRequest = new slTask.SleepTaskRequest();
  taskRequest.sleepSecs = 15;
  ///taskRequest.msg = 'request msg';

  const task = new slTask.SleepTask(taskRequest);
  console.log(`sleep task.taskConfiguration 1.1)`, task.taskConfiguration);
  console.log(`sleep task.taskRequest 2.1)`, task.taskRequest);
}

function testBasicInit() {
  const task = new slTask.SleepTask();
  console.log(`sleep task.taskConfiguration 1)`, task.taskConfiguration);
  console.log(`sleep task.taskRequest 2)`, task.taskRequest);
}

const person: ITestPerson = {name: 'Jonny 15', age: 15};

interface ITestPerson {
  name: string;
  age: number;
}

const execCallback = (err: any, tskRes: slTask.SleepTaskResponse) => {
  console.log(`main-pid=${process.pid} msg=${tskRes.completedMessage} tskRes=${JSON.stringify(tskRes)}`);
  console.log(`delegateObject=${JSON.stringify(tskRes.delegateObject)}`);
};

async function testSleepTask(): Promise<void> {
  console.info(`1111111111111 beforeStart()=${new Date()}`);
  const tskReq = new slTask.SleepTaskRequest();
  tskReq.sleepSecs = 16;
  ///tskReq.msg = 'req msg 1';
  /// const task = new slTask.SleepTask(tskReq);
  const task = new slTask.SleepTask();
  task.start(); /// test 1 ///
  //task.start(execCallback); /// test 2 ///
  //task.start(execCallback, person); /// test 3 ///
  console.info(`22222222222 afterStart()=${new Date()}`);
}

// javasript IIFE
const isMongo = true;
void (async function() {
  try {
    if (isMongo) await applicationInit();
    // testBasicInit();
    // testInitWithArg();
    testSleepTask();

    await utils.sleepSecs(20);
  } catch (err) {
    console.error('Application Start Error', err);
  } finally {
    if (isMongo) await applicationClose();
  }
})();
