/* eslint-disable @typescript-eslint/no-use-before-define */
/* eslint-disable no-case-declarations */
/* eslint-disable no-console */
import {app, logger, mongo, applicationInit, applicationClose} from '../Application';
import * as utils from '../utils';
import * as L from 'lodash';

// load local json
import * as parseObjectArray from './parseObjectArray.json';
export const parseObjectArrayAny: any[] = (parseObjectArray as unknown) as any[];

async function testParseArrayField() {
  const resultArray: any[] = utils.parseFieldFromObjectArray(parseObjectArrayAny, 'Sleep.taskPid');
  console.log(`resultArray`, resultArray);

  for (const taskPid of resultArray) {
    try {
      await process.kill(taskPid, -9);
    } catch (err) {
      console.log(err.message);
    }
  }
}

import * as tskModel from '../common/task/model';

async function testEnumValueArray() {
  /*let newArray = [];

  // L.each(k in tskModel.TaskTypeEnum)
  (k in tskModel.TaskTypeEnum).forEach(item => {
    newArray.push(item + '+');
  });
  const array = L.map(k in tskModel.TaskTypeEnum, function(item) {
    return item + '+';
  });

  L.each(k in tskModel.TaskTypeEnum, function(item) {
    newArray.push(item + '+');
  });
*/

  console.log('k1111111111', utils.enumValueAsArray(tskModel.TaskNameEnum));
  for (const k in tskModel.TaskNameEnum) {
    console.log(`kkkkkk`, k);
  }
}

/*
// javasript IIFE
const isMongo = true;
void (async function() {
  try {
    if (isMongo) await applicationInit();

  } catch (err) {
    console.error('Application Start Error', err);
  } finally {
    if (isMongo) await applicationClose();
  }
})();
*/

void (async function() {
  //await testParseArrayField();
  await testEnumValueArray();
})();
