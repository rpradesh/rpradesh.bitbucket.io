/* eslint-disable @typescript-eslint/no-use-before-define */
/* eslint-disable no-case-declarations */
/* eslint-disable no-console */
import {mongo, applicationInit, applicationClose} from '../Application';
import {
  DefaultUtilityIndexTask,
  DefaultUtilityIndexTaskRequest,
  DefaultUtilityIndexTaskResponse
} from '../common/task/defaultUtilityIndexTask';
import * as utils from '../utils';
import * as L from 'lodash';

async function insertMoreUtility(): Promise<void> {
  for (const i of L.range(11, 21)) {
    const j = {
      _id: `utility_id_test${i}`,
      name: `test${i}`,
      description: `test${i} Tenant - test${i} Pressure Profile on Local`,
      db: `verdeeco_test${i}_db`,
      timezone: `America/New_York`
    };

    const insertOps = await mongo.centralDB.collection('utility').insertOne(j);
    console.log(`insertOps=`, insertOps);
  } //end-for
}

async function dropIndexes(): Promise<void> {
  for (const i of L.range(11, 21)) {
    for (const idx of ['prp_site', 'prp_device']) {
      const dropOps = await mongo
        .utilityDB(`test${i}`)
        .collection(idx)
        .dropIndexes();
      console.log(`dropOps=`, dropOps);
    }
  } //end-for
}

const execCallback = (err: any, taskResponse: DefaultUtilityIndexTaskResponse) => {
  console.log(
    `defaultUtilityIndexTaskRequest DONE main-pid=${process.pid} completedMessage=${
      taskResponse.completedMessage
    } taskResponse=${JSON.stringify(taskResponse)}`
  );
};

// javasript IIFE
const isMongo = true;
void (async function() {
  try {
    if (isMongo) await applicationInit();

    // 0 init ----
    //await insertMoreUtility();
    //await dropIndexes();

    // 1 ----------
    const utilityName = 'prp1'; // 'ALL'; 'prp1';

    // 2 ----------
    const tskReq = new DefaultUtilityIndexTaskRequest();
    tskReq.utility = utilityName;
    const task = new DefaultUtilityIndexTask(tskReq);
    task.start(execCallback);

    await utils.sleepSecs(15);
    console.info(`after-15secs  after-15secs after-15secs after-15secs =${new Date()}`);
  } catch (err) {
    console.error('Application Start Error', err);
  } finally {
    if (isMongo) await applicationClose();
  }
})();
