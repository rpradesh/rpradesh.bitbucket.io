/* eslint-disable @typescript-eslint/no-object-literal-type-assertion */
/* eslint-disable @typescript-eslint/no-use-before-define */
/* eslint-disable no-case-declarations */
/* eslint-disable no-console */
import {app, logger, mongo, applicationInit, applicationClose} from '../Application';
import * as utils from '../utils';
import * as L from 'lodash';
import * as fs from 'fs';
import * as moment from 'moment';
import * as md5File from 'md5-file';

import {siteCsvFieldSchemaArray} from '../config';

export interface IIngestFileDetail {
  // Collection Name: ingest_files
  fileName: string; // DB Column Name  'n'
  fileProcessingDate: Date; // DB Column Name  'dP'
  fileUploadDate: Date; // DB Column Name  'd'
  fileProcessingTime: number; // DB Column Name  'pT'
  fileSize: number; // DB Column Name  's'
  fileProcessingStatus: string; // DB Column Name  'st'
  fileType: string; // DB Column Name  'type'
  fileChecksum: string; // DB Column Name  'md5'
  fileNumberOfLines: number; // DB Column Name  'nL'
  fileNumberOfLinesProcessed: number; // DB Column Name  'nLP'
  fileNumberOfLinesFailed: number; // DB Column Name  'nLF'
  fileNumberDataValues: number; // DB Column Name  'nDV'
  fileGUID: string;
}
export interface IFileProcessingDetail {
  // Collection Name: file_processing_details
  fileName: string;
  fileType: string;
  processingDate: Date;
  success: boolean;
  errorCode: string;
  errorDescription: string;
  fileUploadDate: Date;
  ingest_files_id?: string;
}

// load local json
import * as parseObjectArray from './parseObjectArray.json';
export const parseObjectArrayAny: any[] = (parseObjectArray as unknown) as any[];

async function testDate() {
  const dateTimeStr = moment(new Date()).format('MMDDYYYY_HHmmss');
  console.log(`dateTimeStr=${dateTimeStr}`);
}

async function buildLine(mongoPaths: string[], rec: any): Promise<string> {
  const colData: string[] = [];
  mongoPaths.forEach((path, i) => {
    colData.push(L.get(rec, path, ''));
  });
  return colData.join(',');
}

async function testCursorPipe() {
  const mongoPaths: string[] = utils.parseFieldFromObjectArray(siteCsvFieldSchemaArray, 'mongoPath');
  console.log(`mongoPaths=${mongoPaths}`);
  const cursor = await mongo
    .utilityDB('prp1')
    .collection('prp_site')
    .find({});

  const fsWriter = await fs.createWriteStream(`/tmp/file1.csv`);
  while (await cursor.hasNext()) {
    const rec = await cursor.next();
    const line = await buildLine(mongoPaths, rec);
    console.log(`line=${line}`);
    await fsWriter.write(line + '\n');
  }
  await fsWriter.close();

  //cat /tmp/file1.csv
}

import {IFilestoreModel} from '../components/Filestore/model';
import {IProcessResult} from '../common/csv/model';

// function populateFileObjects(fsModel: IFilestoreModel, procResult: IProcessResult) : [ingestFile: IIngestFileDetail, procDetail: IFileProcessingDetail] {
async function populateFileObjects(
  fsModel: IFilestoreModel,
  procResult: IProcessResult,
  dataColumnCount: number
): Promise<[IIngestFileDetail, IFileProcessingDetail]> {
  const hasError = procResult.inValidRecordCount > 0;
  const procStart = moment(procResult.processStartedAt);
  const procEnd = moment(procResult.processEndedAt);
  const resultFileStat = await fs.promises.stat(fsModel.resultAbsolutePath);

  console.log(`procStart=`, procStart, `procEnd=`, procEnd);

  const ingestFile: IIngestFileDetail = {
    fileName: fsModel.originalFilename,
    fileProcessingDate: procResult.processStartedAt,
    fileUploadDate: moment(procStart)
      .startOf('day')
      .toDate(),
    fileProcessingTime: moment.duration(procEnd.diff(procStart)).asMilliseconds(),
    fileSize: L.toNumber((resultFileStat.size / 1024).toFixed(2)) /* in kb */,
    fileProcessingStatus: hasError === true ? 'HAS_ERRORS' : 'HAS_NO_ERROS',
    fileType: 'CSV_UPLOAD',
    fileChecksum: md5File.sync(fsModel.resultAbsolutePath),
    fileNumberOfLines: procResult.totalRecordCount,
    fileNumberOfLinesProcessed: procResult.validRecordCount,
    fileNumberOfLinesFailed: procResult.inValidRecordCount,
    fileNumberDataValues: dataColumnCount,
    fileGUID: fsModel.fileId
  };
  const procDetail: IFileProcessingDetail = {
    fileName: fsModel.originalFilename,
    fileType: 'CSV_UPLOAD',
    processingDate: procResult.processStartedAt,
    success: !(hasError === true),
    errorCode: hasError === true ? 'HAS_ERRORS' : 'HAS_NO_ERROS',
    errorDescription:
      hasError === true
        ? `File was processed successfully, with ${procResult.inValidRecordCount} records had validation errors`
        : 'File was processed successfully with no exceptions',
    fileUploadDate: moment(procStart)
      .startOf('day')
      .toDate()
  };

  return [ingestFile, procDetail];
}
async function testFileProperties() {
  const fsModel: IFilestoreModel = {
    fileId: 'fileid-string',
    originalFilename: 'parseObjectArray.json',
    resultFilename: 'string',
    sourceAbsolutePath: 'string',
    resultAbsolutePath:
      '/Users/rajeshpradeshik/projects/bb-master/sa-wms-prp/prp-service/src/testdb/parseObjectArray.json',
    exportFilename: 'string',
    exportAbsolutePath: 'string'
  };

  const procResult: IProcessResult = {
    totalRecordCount: 100,
    validRecordCount: 80,
    inValidRecordCount: 20,
    processStartedAt: new Date(),
    processEndedAt: moment(new Date())
      .add(10, 'minutes')
      .toDate(),
    processError: null
  };

  const [ingestFile, procDetail] = await populateFileObjects(fsModel, procResult, 10);
  console.log(`ingestFile=`, ingestFile);
  console.log(`procDetail=`, procDetail);
}

// javasript IIFE
const isMongo = true;
void (async function() {
  try {
    if (isMongo) await applicationInit();
    // await testCursorPipe();
    await testFileProperties();
  } catch (err) {
    console.error('Application Start Error', err);
  } finally {
    if (isMongo) await applicationClose();
  }
})();

/*
void (async function() {
  await testDate();
})();
*/
