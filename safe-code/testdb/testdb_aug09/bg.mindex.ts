/* eslint-disable @typescript-eslint/no-use-before-define */
/* eslint-disable no-case-declarations */
/* eslint-disable no-console */
import {mongo, applicationInit, applicationClose, IMongoIndexDef} from '../Application';
import {MongoIndexTask, MongoIndexTaskRequest, MongoIndexTaskResponse} from '../common/task/mongoIndexTask';
import * as utils from '../utils';
import * as L from 'lodash';

const siteIndexDefn: IMongoIndexDef = {
  collection: 'prp_site',
  keys: {siteName: -1},
  options: {name: 'prpsite_sitenameD', background: false}
};

const execCallback = (err: any, taskResponse: MongoIndexTaskResponse) => {
  console.log(
    `mongoIndexTaskRequest DONE main-pid=${process.pid} completedMessage=${
      taskResponse.completedMessage
    } taskResponse=${JSON.stringify(taskResponse)}`
  );
};

// javasript IIFE
const isMongo = true;
void (async function() {
  try {
    if (isMongo) await applicationInit();
    // db.prp_site.getIndexes()
    // db.prp_site.dropIndexes()

    const options: any = {
      utility: 'prp1',
      mongoIndexDefinition: siteIndexDefn
    };
    const tskReq: MongoIndexTaskRequest = L.assign(new MongoIndexTaskRequest(), options);
    tskReq.mongoIndexDefinition.utility = options.utility;

    const task = new MongoIndexTask(tskReq);
    task.start(execCallback);

    await utils.sleepSecs(15);
    console.info(`after-15secs  after-15secs after-15secs after-15secs =${new Date()}`);
  } catch (err) {
    console.error('Application Start Error', err);
  } finally {
    if (isMongo) await applicationClose();
  }
})();
