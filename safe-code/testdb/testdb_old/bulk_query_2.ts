/* eslint-disable prettier/prettier */
/* eslint-disable no-console */

import {CsvProcessor} from './csv';
import {app, mongo} from './config';

// https://docs.mongodb.com/manual/reference/method/db.collection.bulkWrite/
// https://docs.mongodb.com/manual/core/bulk-write-operations/

async function init() {
  console.info(`1.0 loaded env=${JSON.stringify(app.env)}`);
  // init and validate mongo connection using env
  await mongo.initConnection(app.env.mongoConnectionURL);
}

const processFile1 = async (filePath: string): Promise<void> => {
  console.log('inside processFile1');

  await mongo.centralCollection(`prp_site_1`).deleteMany({});

  const insertWriteOps = await mongo
    .centralCollection(`prp_site_1`)
    .insertMany(
      [
        {_id: 10, item: 'large box', qty: 20},
        {_id: 11, item: 'small box', qty: 55},
        {_id: 11, item: 'medium box', qty: 30},
        {_id: 12, item: 'envelope', qty: 100},
        {_id: 13, item: 'stamps', qty: 125},
        {_id: 13, item: 'tape', qty: 20},
        {_id: 14, item: 'bubble wrap', qty: 30}
      ],
      {ordered: false}
    );

  console.log('insertWriteOps=', insertWriteOps);
};

const processFile2 = async (filePath: string): Promise<void> => {
  console.log('inside processFile2');
  await mongo.centralCollection(`prp_site_2`).deleteMany({});

  const insertWriteOps = await mongo.centralCollection(`prp_site_2`).bulkWrite(
    [
      {
        insertOne: {
          document: {
            _id: 4,
            char: 'Dithras',
            class: 'barbarian',
            lvl: 4
          }
        }
      },
      {
        insertOne: {
          document: {
            _id: 5,
            char: 'Taeln',
            class: 'fighter',
            lvl: 3
          }
        }
      }
    ],
    {ordered: false}
  );

  console.log('insertWriteOps=', insertWriteOps);
};

const processFile3 = async (filePath: string): Promise<void> => {
  console.log('inside processFile 3');

  //await mongo.centralCollection(`prp_site_3`).deleteMany({});

  const insertWriteOps = await mongo.centralCollection(`prp_site_3`).bulkWrite(
    [
      {
        updateOne: {
          filter: {_id: 4},
          update: {
            _id: 4,
            char: 'Dithras',
            class: 'barbarian',
            lvl: 4
          },
          upsert: true
        }
      },
      {
        updateOne: {
          filter: {_id: 5},
          update: {
            _id: 5,
            char: 'Taeln',
            class: 'fighter',
            lvl: 3
          },
          upsert: true
        }
      }
    ],
    {ordered: false}
  );

  console.log('insertWriteOps=', insertWriteOps);
};

//4 works great
const processFile4 = async (filePath: string): Promise<void> => {
  console.log('inside processFile 4');

  //await mongo.centralCollection(`prp_site_4`).deleteMany({});

  const insertWriteOps = await mongo.centralCollection(`prp_site_4`).bulkWrite(
    [
      {
        updateOne: {
          filter: {siteId: 'site_1001'},
          update: {
            siteId: 'site_1001',
            class: 'barbarian',
            lvl: 4
          },
          upsert: true
        }
      },
      {
        updateOne: {
          filter: {siteId: 'site_1002'},
          update: {
            siteId: 'site_1002',
            class: 'fighter',
            lvl: 3
          },
          upsert: true
        }
      }
    ],
    {ordered: false}
  );

  console.log('insertWriteOps=', insertWriteOps);
};

//4.1 test with single quote
const processFile41 = async (filePath: string): Promise<void> => {
  console.log('inside processFile 4');

  /* await mongo.centralCollection(`prp_site_4`).deleteMany({});
   */
  const insertWriteOps = await mongo.centralCollection(`prp_site_4`).bulkWrite(
    [
      {
        updateOne: {
          filter: {siteId: 'site_1001'},
          update: {
            siteId: 'site_1001',
            class: 'barbarian',
            lvl: 4
          },
          $setOnInsert: {defaultQty: 100},
          upsert: true
        }
      },
      {
        updateOne: {
          filter: {siteId: 'site_1002'},
          update: {
            siteId: 'site_1002',
            class: 'fighter',
            lvl: 3
          },
          $setOnInsert: {defaultQty: 100},
          upsert: true
        }
      }
    ],
    {ordered: false}
  );

  console.log('insertWriteOps=', insertWriteOps);
};

void (async function() {
  try {
    await init();
    //await processFile1('../prp-tools/test-data/10-sitedata.csv');
    //await processFile2('../prp-tools/test-data/10-sitedata.csv');
    await processFile41('../prp-tools/test-data/10-sitedata.csv');
  } catch (err) {
    console.error('Application Start Error', err);
  }
})();