import { app, mongo } from './config'
import * as utils from './utils'

// https://docs.mongodb.com/manual/reference/method/db.collection.bulkWrite/
// https://docs.mongodb.com/manual/core/bulk-write-operations/

async function init() {
  console.info(`1.0 loaded env=${JSON.stringify(app.env)}`);
  // init and validate mongo connection using env
  await mongo.initConnection(app.env.mongoConnectionURL);
}


void async function () {
  try {
    console.time('dbjob');

    await init();
    //await insertRecords();
    await findRecords();

    console.timeEnd('dbjob');
  } catch (err) {
    console.error('Application Start Error', err);
  }
}();

const insertGivenRecords = async (colName: string, dataAray: any[]): Promise<number> => {
  ///const insertWriteOps = await mongo.centralCollection(colName).insertMany(
  const insertWriteOps = await mongo.utilityDB('test1').collection(colName).insertMany(
    dataAray, { ordered: false }
    );
  return insertWriteOps.result.n
}
// 1 million of insert from local to QA DB takes
//4 insert raw documents //return insertedCount: 2,
const insertRecords = async (): Promise<void> => {
  console.log('inside insertRecords 4');
  ///await mongo.centralCollection(`prp_site_5`).deleteMany({});
  await mongo.utilityDB('test1').collection(`prp_site_5`).deleteMany({});
  let records4:any[] = [];

  const totalRecs = 100;//1000 * 1000;//1000;//100;
  const batchSize  = 10; //100;//10;

  for(let i=0; i < totalRecs; i++){
    records4.push({siteId:`site_${i}`, siteName: `name ${i}`})

    if (records4.length === batchSize) {
      const insertCount = await insertGivenRecords('prp_site_5', records4);
      console.log(`batch insertCount=${insertCount}`);
      records4 = [];
    }
  }


}

const findRecords = async (): Promise<void> => {
  console.log('inside findRecords 4');

  ///const siteIdArray = Array.from(Array(5).keys()).map(i => `site_${i}`)
  const siteIdArray = Array.from(Array(6).keys()).map(i => `site_${ (98 + i)}`)

   const findIn = await mongo.utilityDB('test1').collection(`prp_site_5`).aggregate([
     {   $project: { siteId: 1, _id: 0}  },
     {   $match:   { siteId: {$in: [...siteIdArray]} }   }
 ]).toArray();

  const oneSiteId = { siteId: 'site_1' };
  console.log('findIn=', findIn);

}

/*
export const persistBatchIntoMongo = (recs: IRecord[], csvOptions: ICsvOptions): number => {
  // build given csv siteId Array
  const srcSiteIdArray = recs.map( r => r.json['siteId']);
  // pass this siteIdArray to make findArray to know if they exist
  const existSiteIdArray =
  // using both of above array's, make a split for insert/update id array

  // execute insert's

  // execute updates


  return 0;
}

const findRecords = async (siteIdArray: string[]): Promise<string[]> => {
   const foundObjArray = await mongo.utilityDB('test1').collection(`prp_site`).aggregate([
     {   $project: { siteId: 1, _id: 0}  },
     {   $match:   { siteId: {$in: [...siteIdArray]} }   }
 ]).toArray();

 return utils.parseFieldFromObjectArray(foundObjArray,'siteId' );

}
*/