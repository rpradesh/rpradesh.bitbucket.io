import {MongoClient, Db, Collection, MongoClientOptions} from 'mongodb';
import {app, mongo, logger, IMongoIndexDef, IMongoIndexSummary} from '../index';
import * as L from 'lodash';
import * as createMongoIndexDefinitions from '../../config/createMongoIndexDefinitions.json';

const mongoIndexDefinitions: IMongoIndexDef[] = createMongoIndexDefinitions;

class BackgroundTask {
  private running = false;

  get isRunning() {
    return this.running;
  }

  /**
   * mark running = true
   * loop through utilities
   * for each utility, loop though colectionName and create Index
   * mark running = flase
   */
  async start(): Promise<void> {
    return;
  }
}

const backgroundTask = new BackgroundTask();
export {backgroundTask};
