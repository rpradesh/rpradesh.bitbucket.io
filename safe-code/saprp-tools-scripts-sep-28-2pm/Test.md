- Build and Local Test steps
- https://www.npmjs.com/package/oclif#-usage
- https://github.com/oclif/oclif
- https://oclif.io/docs/base_class

## Build

```bash
cd sa-wms-prp/prp_tools/saprp
npm run clean
npm install
npm run compile

# to generate Readme
npm run version

# to installas binary
npm install -g
```

## Mongo - Test Examples

```bash
cd sa-wms-prp/prp_tools/saprp

#1
./bin/run mongo-check-connection -m mongodb://localhost:27017
export MONGO_URL="mongodb://localhost:27017"
./bin/run mongo-check-connection

#2
./bin/run mongo-count-records -u prp1 -c prp_site

#3
./bin/run mongo-display-records -u prp1 -c prp_site -n 12

#4
./bin/run mongo-delete-records -u prp1 -c prp_site

#5
./bin/run mongo-load-records -u prp1 -c device_1 --file ~/data/gen-data/device.json
```

## DataGen - Test Examples

- To Add new Model for Datagen Generator

  - Add new file under datagen/ example of datagen-prpsite.ts
  - Then add the entry in datagen/index.ts
  - Add typeName entry in enum DataGenTypeEnum at common/model.ts

```bash
cd sa-wms-prp/prp_tools/saprp

# options like
  # i,index=>StartIndex,
  # n,size=>no of Records to generate,
  # o,output=json/csv

#1
./bin/run datagen -t prp_site -f ~/data/gen-data/test_prpsite_v2.json
./bin/run datagen -t prp_site -f ~/data/gen-data/test_prpsite_v2.csv --output csv

cat ~/data/gen-data/test_prpsite_v2.json

#2
./bin/run datagen -t prp_device -f ~/data/gen-data/test_prpdevice_v2.json
./bin/run datagen -t prp_device -f ~/data/gen-data/test_prpdevice_v2.csv --output csv

cat  ~/data/gen-data/test_prpdevice_v2.csv

#3
./bin/run datagen -t daily_record -f ~/data/gen-data/test_dailyrecord_30_v2.json -n 30
cat ~/data/gen-data/test_dailyrecord_30_v2.json

#4
./bin/run datagen -t device -f ~/data/gen-data/test_justdevice_10_v2.json -n 10
cat ~/data/gen-data/test_justdevice_10_v2.json

#5
./bin/run datagen -t prp_reads -f ~/data/gen-data/test_sensor_read_10_v2_1.json -n 10 --constant 1
cat ~/data/gen-data/test_sensor_read_10_v2_1.json

./bin/run datagen -t prp_reads -f ~/data/gen-data/test_sensor_read_10_v2_2.json -n 10 --constant 2
cat ~/data/gen-data/test_sensor_read_10_v2_2.json

#6
./bin/run datagen -t prp_deviceData -f ~/data/gen-data/test_prp_devicedata_10_v2.json -n 10
cat ~/data/gen-data/test_prp_devicedata_10_v2.json

#7
./bin/run datagen -t prp_alarm -f ~/data/gen-data/test_prp_alarm_10_v2.json -n 10
cat ~/data/gen-data/test_prp_alarm_10_v2.json

```

## DataGen - using binary

```bash
mkdir -p ~/data/gen-data/test-saprp
cd ~/data/gen-data/test-saprp

#1
saprp mongo-check-connection -m mongodb://localhost:27017
export MONGO_URL="mongodb://localhost:27017"
saprp mongo-check-connection
#2
saprp mongo-count-records -u prp1 -c prp_site
#3
saprp mongo-display-records -u prp1 -c prp_site -n 12
#4
saprp mongo-delete-records -u prp1 -c prp_site
#5
saprp mongo-load-records -u prp1 -c device_1 --file ~/data/gen-data/device.json

# ################ DataGen ################

#1
saprp datagen -t prp_site -f test_prpsite_v2.json
saprp datagen -t prp_site -f test_prpsite_v2.csv --output csv
cat test_prpsite_v2.json
cat test_prpsite_v2.csv
#2
saprp datagen -t prp_device -f test_prpdevice_v2.json
saprp datagen -t prp_device -f test_prpdevice_v2.csv --output csv
cat  test_prpdevice_v2.json
cat  test_prpdevice_v2.csv
#3
saprp datagen -t daily_record -f test_dailyrecord_30_v2.json -n 30
cat test_dailyrecord_30_v2.json
#4
saprp datagen -t device -f test_justdevice_10_v2.json -n 10
cat test_justdevice_10_v2.json
#5
saprp datagen -t prp_reads -f test_sensor_read_10_v2_1.json -n 10 --constant 1
cat test_sensor_read_10_v2_1.json
saprp datagen -t prp_reads -f test_sensor_read_10_v2_2.json -n 10 --constant 2
cat test_sensor_read_10_v2_2.json
#6
saprp datagen -t prp_deviceData -f test_prp_devicedata_10_v2.json -n 10
cat test_prp_devicedata_10_v2.json
#7
saprp datagen -t prp_alarm -f test_prp_alarm_10_v2.json -n 10
cat test_prp_alarm_10_v2.json
```

## Test Old Script

```bash
cd ~/projects/bb-branch-master/sa-wms-prp/prp-tools/scripts

npm i


node cmd.js --cmd=generateDailyRecordData --records=20 --startindex=1000000 --filepath='../gen-data/v1_dailyrecord.json' --numberOfDays=30 --fileType='json'
node cmd.js --cmd=generateDeviceData --records=20 --startindex=1000000 --filepath='../gen-data/v1_device.json' --fileType='json'
node cmd.js --cmd=generateReadData --records=20 --startindex=1000000 --filepath='../gen-data/v1_prp_reads.json' --fileType='json'
node cmd.js --cmd=generateAlarmData --records=20 --startindex=1000000 --filepath='../gen-data/v1_prp_alarms.json' --fileType='json'
node cmd.js --cmd=generateDeviceDataData --records=20 --startindex=1000000 --filepath='../gen-data/v1_prp_devicedata.json' --fileType='json'

# node cmd.js --cmd=loadIntoCollection --db=verdeeco_central_db --collection=utility             --jsonFile=seed-data/dev/utility.json
# node cmd.js --cmd=loadIntoCollection --db=verdeeco_central_db --collection=prp_properties      --jsonFile=seed-data/dev/prp-properties.json

node cmd.js --cmd=displayCollection  --db=verdeeco_central_db --collection=utility
node cmd.js --cmd=displayCollection  --db=verdeeco_central_db --collection=prp_properties

node cmd.js --cmd=clearCollection    --db=verdeeco_prp1_db    --collection=daily_record
node cmd.js --cmd=clearCollection    --db=verdeeco_prp1_db    --collection=device
node cmd.js --cmd=clearCollection    --db=verdeeco_prp1_db    --collection=prp_reads
node cmd.js --cmd=clearCollection    --db=verdeeco_prp1_db    --collection=prp_alarms
node cmd.js --cmd=clearCollection    --db=verdeeco_prp1_db    --collection=prp_devicedata

node cmd.js --cmd=loadIntoCollection --db=verdeeco_prp1_db    --collection=daily_record    --jsonFile=../gen-data/v1_dailyrecord.json
node cmd.js --cmd=loadIntoCollection --db=verdeeco_prp1_db    --collection=device          --jsonFile=../gen-data/v1_device.json
node cmd.js --cmd=loadIntoCollection --db=verdeeco_prp1_db    --collection=prp_reads       --jsonFile=../gen-data/v1_prp_reads.json
node cmd.js --cmd=loadIntoCollection --db=verdeeco_prp1_db    --collection=prp_alarms      --jsonFile=../gen-data/v1_prp_alarms.json
node cmd.js --cmd=loadIntoCollection --db=verdeeco_prp1_db    --collection=prp_devicedata  --jsonFile=../gen-data/v1_prp_devicedata.json

```

## Run Data-Script

```bash
cd sa-wms-prp/prp-tools
./data_script.sh INIT_MONGO_DB
```
