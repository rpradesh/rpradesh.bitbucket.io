import {BaseDatagen} from './base-datagen';
import * as faker from 'faker';
import * as geohash from 'ngeohash';
import * as geoutil from '../common/geoutil';

const commodity = 'WATER';
const caryLatLong = {lat: 35.79154, lng: -78.7811169};
const siteTypes = ['Tank', 'Pump Station', 'PRV', 'Reservoir', 'Sensor', 'Flushing', 'Valve'];

export class Datagen extends BaseDatagen {
  generateRecordsForIndex(idx: number): void {
    const latLong = geoutil.generateRandomPoint(caryLatLong, 30);
    const index = this.datagenArgs.startIndex + idx;

    const record = {
      /*   0,  true, */ recordInActive: '0',
      /*   1,  true, */ siteId: `sid${index}`,
      /*   2,  true, */ siteName: `siteName ${index} ${faker.random.alphaNumeric(10)}`,
      // /*   3,  true, */ siteType: siteTypes[faker.random.number({min: 0, max: 6, precision: 1})],
      /*   3,  true, */ siteType: faker.random.arrayElement(siteTypes),
      /*   4,  true, */ insertDate: faker.date
        .between('2015-01-01', '2019-01-01')
        .toISOString()
        .split('T')[0],
      /*   5,  true, */ lat: latLong.lat.toFixed(5),
      /*   6,  true, */ lng: latLong.lng.toFixed(5),
      /*   7,  true, */ pressureZone: `prZone-${faker.random.alphaNumeric(10)}`,
      /*   8,  true, */ elevation: faker.random.number({min: 50, max: 999, precision: 3}),
      /*   9, false, */ geoHash: geohash.encode(latLong.lat, latLong.lng),
      /*  10, false, */ elevationOffset: faker.random.number({min: 5, max: 50, precision: 2}),
      address: {
        /*  11, false, */ line1: faker.address.streetAddress().replace(',', ''),
        /*  12, false, */ line2: faker.address.secondaryAddress().replace(',', ''),
        /*  13, false, */ city: faker.address.city().replace(',', ''),
        /*  14, false, */ state: faker.address.state(),
        /*  15, false, */ zip: faker.address.zipCode()
      }
    };

    this.records.push(record);
  }

  generateSingleRecord(idx: number): any {
    const latLong = geoutil.generateRandomPoint(caryLatLong, 30);
    const index = this.datagenArgs.startIndex + idx;

    return {
      /*   0,  true, */ recordInActive: '0',
      /*   1,  true, */ siteId: `sid${index}`,
      /*   2,  true, */ siteName: `siteName ${index} ${faker.random.alphaNumeric(10)}`,
      // /*   3,  true, */ siteType: siteTypes[faker.random.number({min: 0, max: 6, precision: 1})],
      /*   3,  true, */ siteType: faker.random.arrayElement(siteTypes),
      /*   4,  true, */ insertDate: faker.date
        .between('2015-01-01', '2019-01-01')
        .toISOString()
        .split('T')[0],
      /*   5,  true, */ lat: latLong.lat.toFixed(5),
      /*   6,  true, */ lng: latLong.lng.toFixed(5),
      /*   7,  true, */ pressureZone: `prZone-${faker.random.alphaNumeric(10)}`,
      /*   8,  true, */ elevation: faker.random.number({min: 50, max: 999, precision: 3}),
      /*   9, false, */ geoHash: geohash.encode(latLong.lat, latLong.lng),
      /*  10, false, */ elevationOffset: faker.random.number({min: 5, max: 50, precision: 2}),
      address: {
        /*  11, false, */ line1: faker.address.streetAddress().replace(',', ''),
        /*  12, false, */ line2: faker.address.secondaryAddress().replace(',', ''),
        /*  13, false, */ city: faker.address.city().replace(',', ''),
        /*  14, false, */ state: faker.address.state(),
        /*  15, false, */ zip: faker.address.zipCode()
      }
    };
  }

  getJsonPathToGenerateCsv(): string[] {
    return [
      /*   0,  true, */ 'recordInActive',
      /*   1,  true, */ 'siteId',
      /*   2,  true, */ 'siteName',
      /*   3,  true, */ 'siteType',
      /*   4,  true, */ 'insertDate',
      /*   5,  true, */ 'lat',
      /*   6,  true, */ 'lng',
      /*   7,  true, */ 'pressureZone',
      /*   8,  true, */ 'elevation',
      /*   9, false, */ 'geoHash',
      /*  10, false, */ 'elevationOffset',
      /*  11, false, */ 'address.line1',
      /*  12, false, */ 'address.line2',
      /*  13, false, */ 'address.city',
      /*  14, false, */ 'address.state',
      /*  15, false, */ 'address.zip'
    ];
  }
}
