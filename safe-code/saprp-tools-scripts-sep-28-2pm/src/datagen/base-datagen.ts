/* eslint-disable no-console */
import * as fs from 'fs';
import * as L from 'lodash';
import {DatagenArgs} from '../common';
import * as notifier from 'node-notifier';

const WriteBatchSize = 100;

export abstract class BaseDatagen {
  readonly datagenArgs: DatagenArgs;
  records: any[] = [];
  private comma = '';

  constructor(datagenArgs: DatagenArgs) {
    this.datagenArgs = datagenArgs;
  }

  generate(): void {
    // remove if file exists
    if (fs.existsSync(this.datagenArgs.file)) {
      fs.unlinkSync(this.datagenArgs.file);
    }

    // to prevent memory hog, let us write at batch size of 100
    this.printStart();

    for (let i = 0; i < this.datagenArgs.size; i++) {
      ///this.records.push(this.generateSingleRecord(i));
      this.generateRecordsForIndex(i);
      if (this.records.length > WriteBatchSize) this.flushRecords();
    }

    // flush any remaining
    this.flushRecords();

    this.printEnd();

    if (this.datagenArgs.notify === true) {
      notifier.notify({
        title: `Data generator: completed for type=${this.datagenArgs.type}`,
        message: `output type: "${this.datagenArgs.json ? 'json' : 'csv'}" at file: ${this.datagenArgs.file}`
      });
    }
  }

  private flushRecords(): void {
    if (this.records.length <= 0) return;

    const lines: string[] = this.datagenArgs.json
      ? this.records.map(rec => JSON.stringify(rec))
      : this.buildStringLinesFromJson();

    const text = this.datagenArgs.json ? lines.join(',\n') : lines.join('\n');
    fs.appendFileSync(this.datagenArgs.file, this.comma);
    fs.appendFileSync(this.datagenArgs.file, text);
    // init comma for next cycle
    if (this.comma === '') this.comma = this.datagenArgs.json ? ',\n' : '\n';
    // clear it after writing to file
    this.records = [];
  }

  private printStart(): void {
    if (this.datagenArgs.json) fs.appendFileSync(this.datagenArgs.file, '[');
    else if (this.doGenerateHeadersForCsv() && this.getJsonPathToGenerateCsv().length > 0) {
      fs.appendFileSync(
        this.datagenArgs.file,
        `The data for "${this.datagenArgs.type}" was generated on ${new Date()} \n`
      );
      fs.appendFileSync(this.datagenArgs.file, `${this.getJsonPathToGenerateCsv().join(',')} \n`);
    }
  }

  private buildSingleStringLine(rec: any): string {
    const fields: string[] = [];

    this.getJsonPathToGenerateCsv().forEach(path => {
      const fValue: string = '' + L.get(rec, path, '');
      fields.push(fValue.replace(/,/g, '|'));
    });
    return fields.join(',');
  }

  private buildStringLinesFromJson(): string[] {
    const stringLines: string[] = [];

    this.records.forEach(rec => {
      stringLines.push(this.buildSingleStringLine(rec));
    });

    return stringLines;
  }

  private printEnd(): void {
    fs.appendFileSync(this.datagenArgs.file, this.datagenArgs.json ? '\n]' : '\n');
  }

  generateRecordsForIndex(currentIndex: number): void {
    this.records.length;
  }
  abstract generateSingleRecord(currentIndex: number): any;

  getJsonPathToGenerateCsv(): string[] {
    return [];
  }

  doGenerateHeadersForCsv(): boolean {
    return true;
  }
}
