import {BaseDatagen} from './base-datagen';
import * as L from 'lodash';
import * as faker from 'faker';
import * as geoutil from '../common/geoutil';

const commodity = 'WATER';
const caryLatLong = {lat: 35.79154, lng: -78.7811169};
const deviceTypes = ['METER', 'ALLY_METER', 'SGW'];

export class Datagen extends BaseDatagen {
  generateRecordsForIndex(idx: number): void {
    const latLong = geoutil.generateRandomPoint(caryLatLong, 30);
    const index = this.datagenArgs.startIndex + idx;

    L.range(0, this.datagenArgs.days).forEach(i => {
      const d = new Date();
      const hourOffset = d.getTimezoneOffset() / 60;
      d.setDate(d.getDate() - i);
      d.setMilliseconds(0);
      d.setSeconds(0);
      d.setMinutes(0);
      d.setHours(0 - hourOffset);

      const record = {
        device: index,
        date: d.toISOString(),
        alarms: []
      };

      L.range(0, 12).forEach(j => {
        // randomize time data
        const hour = faker.random.number({min: 0, max: 23, precision: 1});
        d.setHours(hour - hourOffset);
        record.alarms.push({
          id: `CIM_CODE${i}${j}`,
          observed: d.toISOString(),
          label: `label-${faker.random.alphaNumeric(5)}`,
          source: faker.random.arrayElement([
            'SMART_POINT',
            'DEVICE',
            'DEVICE_MANAGER',
            'READ_ENGINE',
            'METER',
            'METROLOGY'
          ]),
          severity: faker.random.arrayElement(['CRITICAL', 'MAJOR', 'MINOR']),
          state: faker.random.number({min: 1, max: 3, precision: 1}) === 1 ? 'INACTIVE' : 'ACTIVE',
          input: {
            type: 'SENSOR',
            id: 1,
            label: 'Pressure'
          },
          readings: [
            {
              readingType: 'PRESSURE',
              label: `readlabel-${faker.random.alphaNumeric(5)}`,
              endTime: d.toISOString(),
              value: faker.random.number({min: 1, max: 1000, precision: 1})
            },
            {
              readingType: 'PRESSURE',
              label: `readlabel-${faker.random.alphaNumeric(5)}`,
              endTime: d.toISOString(),
              value: faker.random.number({min: 1, max: 1000, precision: 1})
            }
          ],
          trigger: {
            readingType: 'PRESSURE',
            label: `triglabel-${faker.random.alphaNumeric(5)}`,
            triggerType: 'numericGreaterThanEquals',
            value: faker.random.number({min: 1, max: 150, precision: 1})
          }
        } as never);
      });

      this.records.push(record);
    });
  }

  generateSingleRecord(i: number): any {
    const latLong = geoutil.generateRandomPoint(caryLatLong, 30);
    const index = this.datagenArgs.startIndex + i;

    // date obj
    const d = new Date();
    const hourOffset = d.getTimezoneOffset() / 60;
    d.setDate(d.getDate() - i);
    d.setMilliseconds(0);
    d.setSeconds(0);
    d.setMinutes(0);
    d.setHours(0 - hourOffset);

    const record = {
      device: index,
      date: d.toISOString(),
      alarms: []
    };

    L.range(0, 12).forEach(j => {
      // randomize time data
      const hour = faker.random.number({min: 0, max: 23, precision: 1});
      d.setHours(hour - hourOffset);
      record.alarms.push({
        id: `CIM_CODE${i}${j}`,
        observed: d.toISOString(),
        label: `label-${faker.random.alphaNumeric(5)}`,
        source: faker.random.arrayElement([
          'SMART_POINT',
          'DEVICE',
          'DEVICE_MANAGER',
          'READ_ENGINE',
          'METER',
          'METROLOGY'
        ]),
        severity: faker.random.arrayElement(['CRITICAL', 'MAJOR', 'MINOR']),
        state: faker.random.number({min: 1, max: 3, precision: 1}) === 1 ? 'INACTIVE' : 'ACTIVE',

        input: {
          type: 'SENSOR',
          id: 1,
          label: 'Pressure'
        },

        readings: [
          {
            readingType: 'PRESSURE',
            label: `readlabel-${faker.random.alphaNumeric(5)}`,
            endTime: d.toISOString(),
            value: faker.random.number({min: 1, max: 1000, precision: 1})
          },
          {
            readingType: 'PRESSURE',
            label: `readlabel-${faker.random.alphaNumeric(5)}`,
            endTime: d.toISOString(),
            value: faker.random.number({min: 1, max: 1000, precision: 1})
          }
        ],

        trigger: {
          readingType: 'PRESSURE',
          label: `triglabel-${faker.random.alphaNumeric(5)}`,
          triggerType: 'numericGreaterThanEquals',
          value: faker.random.number({min: 1, max: 150, precision: 1})
        }
      } as never);
    });

    return record;
  }
}
