import {DatagenArgs, DataGenTypeEnum} from '../common';

// add new datagen files below
import {Datagen as DatagenPrpSite} from './datagen-prpsite';
import {Datagen as DatagenPrpDevice} from './datagen-prpdevice';
import {Datagen as DatagenDailyRecord} from './datagen-daily-record';
import {Datagen as DatagenJustDevice} from './datagen-just-device';
import {Datagen as DatagenSensorRead} from './datagen-sensor-read';
import {Datagen as DatagenPrpDeviceData} from './datagen-prp-devicedata';
import {Datagen as DatagenPrpAlarm} from './datagen-prp-alarm';

export function doGenerate(datagenArgs: DatagenArgs): void {
  if (DataGenTypeEnum.prp_site == datagenArgs.type) new DatagenPrpSite(datagenArgs).generate();
  else if (DataGenTypeEnum.prp_device == datagenArgs.type) new DatagenPrpDevice(datagenArgs).generate();
  else if (DataGenTypeEnum.daily_record == datagenArgs.type) new DatagenDailyRecord(datagenArgs).generate();
  else if (DataGenTypeEnum.device == datagenArgs.type) new DatagenJustDevice(datagenArgs).generate();
  else if (DataGenTypeEnum.prp_reads == datagenArgs.type) new DatagenSensorRead(datagenArgs).generate();
  else if (DataGenTypeEnum.prp_devicedata == datagenArgs.type) new DatagenPrpDeviceData(datagenArgs).generate();
  else if (DataGenTypeEnum.prp_alarm == datagenArgs.type) new DatagenPrpAlarm(datagenArgs).generate();
  else throw new Error(`Unknwon datagen type value=[${datagenArgs.type}]`);
}

export function buildDatagenModel(flagMap: any): DatagenArgs {
  const datagenArgs = new DatagenArgs(flagMap.type);
  datagenArgs.file = flagMap.file;
  datagenArgs.size = flagMap.size;
  datagenArgs.startIndex = flagMap.index;
  datagenArgs.constant = flagMap.constant;
  datagenArgs.json = flagMap.output === 'json';
  datagenArgs.notify = flagMap.notify === true;

  return datagenArgs;
}
