import {BaseDatagen} from './base-datagen';
import * as faker from 'faker';
import * as L from 'lodash';
import * as geoutil from '../common/geoutil';

const commodity = 'WATER';
const caryLatLong = {lat: 35.79154, lng: -78.7811169};
const deviceTypes = ['METER', 'ALLY_METER', 'SGW'];

export class Datagen extends BaseDatagen {
  generateRecordsForIndex(idx: number): void {
    const latLong = geoutil.generateRandomPoint(caryLatLong, 30);
    const index = this.datagenArgs.startIndex + idx;

    L.range(0, this.datagenArgs.days).forEach(i => {
      const d = new Date();
      d.setDate(d.getDate() - i);

      const record = {
        _id: index + ';' + index + ';' + d.getFullYear() + d.getMonth() + d.getDate(),
        device: index,
        radio: index,
        commodity: 'WATER',
        date: d.toISOString(),
        lastUpdate: d.toISOString(),
        registers: []
      };

      for (let j = 0; j < 24; j++) {
        d.setHours(j);
        const subObj = {
          dS: 'gen',
          eT: d.toISOString(),
          iT: d.toISOString(),
          iL: 0, // what is this?
          mul: 1.0,
          rQ: 'R',
          m: 'CCF',
          v: faker.random.number({min: j * 5, max: j * 5 + 5, precision: 1})
        };

        record.registers.push(subObj as never);
      } // loop 24-hours

      this.records.push(record);
    });
  }

  generateSingleRecord(i: number): any {
    const latLong = geoutil.generateRandomPoint(caryLatLong, 30);
    const index = this.datagenArgs.startIndex + i;

    const d = new Date();
    d.setDate(d.getDate() - i);
    const record = {
      _id: index + ';' + index + ';' + d.getFullYear() + d.getMonth() + d.getDate(),
      device: index,
      radio: index,
      commodity: 'WATER',
      date: d.toISOString(),
      lastUpdate: d.toISOString(),
      registers: []
    };

    for (let j = 0; j < 24; j++) {
      d.setHours(j);
      const subObj = {
        dS: 'gen',
        eT: d.toISOString(),
        iT: d.toISOString(),
        iL: 0, // what is this?
        mul: 1.0,
        rQ: 'R',
        m: 'CCF',
        v: faker.random.number({min: j * 5, max: j * 5 + 5, precision: 1})
      };

      record.registers.push(subObj as never);
    } // loop 24-hours

    return record;
  }
}
