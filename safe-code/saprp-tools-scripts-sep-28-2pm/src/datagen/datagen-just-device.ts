import {BaseDatagen} from './base-datagen';
import * as faker from 'faker';
import * as geohash from 'ngeohash';
import * as geoutil from '../common/geoutil';

const commodity = 'WATER';
const caryLatLong = {lat: 35.79154, lng: -78.7811169};
const deviceTypes = ['METER', 'ALLY_METER', 'SGW'];

export class Datagen extends BaseDatagen {
  generateRecordsForIndex(idx: number): void {
    const latLong = geoutil.generateRandomPoint(caryLatLong, 30);
    const index = this.datagenArgs.startIndex + idx;

    const record = {
      _id: {deviceNumber: index, meterType: commodity, deviceType: 'METER'},
      account: {
        _id: faker.random.number({min: 1, max: 1000, precision: 1}),
        name: 'acct' + faker.random.alphaNumeric(6),
        bs: 1,
        s: 'A',
        st: 'commercial',
        address: {
          line1: faker.address.streetAddress().replace(/,/g, '|'),
          zip: faker.address.zipCode(),
          city: faker.address.city().replace(/,/g, '|'),
          state: faker.address.state(),
          lat: latLong.lat.toFixed(5),
          lon: latLong.lng.toFixed(5),
          geohash: geohash.encode(latLong.lat, latLong.lng)
        }
      },
      amrIdentifier: faker.random.number({min: 10000000, max: 99999999, precision: 1}),
      meterManufacturer: '900GM',
      serviceRoute: '1',
      sdp: {
        _id: index,
        address: {
          line1: faker.address.streetAddress().replace(/,/g, '|'),
          zip: faker.address.zipCode(),
          city: faker.address.city().replace(/,/g, '|'),
          state: faker.address.state(),
          lat: latLong.lat.toFixed(5),
          lon: latLong.lng.toFixed(5),
          geohash: geohash.encode(latLong.lat, latLong.lng)
        }
      },
      status: 'ACTIVE',
      address: {
        line1: faker.address.streetAddress().replace(/,/g, '|'),
        zip: faker.address.zipCode(),
        city: faker.address.city().replace(/,/g, '|'),
        state: faker.address.state(),
        lat: latLong.lat.toFixed(5),
        lon: latLong.lng.toFixed(5),
        geohash: geohash.encode(latLong.lat, latLong.lng)
      }
    };

    this.records.push(record);
  }

  generateSingleRecord(i: number): any {
    const latLong = geoutil.generateRandomPoint(caryLatLong, 30);
    const index = this.datagenArgs.startIndex + i;

    const record = {
      _id: {deviceNumber: index, meterType: commodity, deviceType: 'METER'},
      account: {
        _id: faker.random.number({min: 1, max: 1000, precision: 1}),
        name: 'acct' + faker.random.alphaNumeric(6),
        bs: 1,
        s: 'A',
        st: 'commercial',
        address: {
          line1: faker.address.streetAddress().replace(/,/g, '|'),
          zip: faker.address.zipCode(),
          city: faker.address.city().replace(/,/g, '|'),
          state: faker.address.state(),
          lat: latLong.lat.toFixed(5),
          lon: latLong.lng.toFixed(5),
          geohash: geohash.encode(latLong.lat, latLong.lng)
        }
      },
      amrIdentifier: faker.random.number({min: 10000000, max: 99999999, precision: 1}),
      meterManufacturer: '900GM',
      serviceRoute: '1',
      sdp: {
        _id: index,
        address: {
          line1: faker.address.streetAddress().replace(/,/g, '|'),
          zip: faker.address.zipCode(),
          city: faker.address.city().replace(/,/g, '|'),
          state: faker.address.state(),
          lat: latLong.lat.toFixed(5),
          lon: latLong.lng.toFixed(5),
          geohash: geohash.encode(latLong.lat, latLong.lng)
        }
      },
      status: 'ACTIVE',
      address: {
        line1: faker.address.streetAddress().replace(/,/g, '|'),
        zip: faker.address.zipCode(),
        city: faker.address.city().replace(/,/g, '|'),
        state: faker.address.state(),
        lat: latLong.lat.toFixed(5),
        lon: latLong.lng.toFixed(5),
        geohash: geohash.encode(latLong.lat, latLong.lng)
      }
    };

    return record;
  }
}
