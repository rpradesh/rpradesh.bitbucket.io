import {BaseDatagen} from './base-datagen';
import * as L from 'lodash';
import * as faker from 'faker';
import * as geoutil from '../common/geoutil';

const commodity = 'WATER';
const caryLatLong = {lat: 35.79154, lng: -78.7811169};
const deviceTypes = ['METER', 'ALLY_METER', 'SGW'];

export class Datagen extends BaseDatagen {
  generateRecordsForIndex(idx: number): void {
    const latLong = geoutil.generateRandomPoint(caryLatLong, 30);
    const index = this.datagenArgs.startIndex + idx;
    const d = new Date();
    const hourOffset = d.getTimezoneOffset() / 60;

    L.range(0, this.datagenArgs.days).forEach(i => {
      d.setDate(d.getDate() - i);
      d.setMilliseconds(0);
      d.setSeconds(0);
      d.setMinutes(0);
      d.setHours(0 - hourOffset);

      const record = {
        device: index.toString(),
        date: d.toISOString(),
        serviceType: 'WATER',
        sensorId: '1',
        readingType: 'PRESSURE',
        reads: []
      };

      const recordSensor2 = {
        device: index.toString(),
        date: d.toISOString(),
        serviceType: 'WATER',
        sensorId: '2',
        readingType: 'PRESSURE',
        reads: []
      };

      L.range(0, 24).forEach(j => {
        if (j !== 0) d.setTime(d.getTime() + 60 * 60 * 1000);
        record.reads.push({
          readDate: d.toISOString(),
          value: faker.random.number({min: 1, max: 100, precision: 1})
        } as never);
        recordSensor2.reads.push({
          readDate: d.toISOString(),
          value: faker.random.number({min: 1, max: 100, precision: 1})
        } as never);
      });

      this.records.push(record);
      this.records.push(recordSensor2);
    });
  }

  generateSingleRecord(i: number): any {
    const latLong = geoutil.generateRandomPoint(caryLatLong, 30);
    const index = this.datagenArgs.startIndex + i;

    // date obj
    const d = new Date();
    const hourOffset = d.getTimezoneOffset() / 60;
    d.setDate(d.getDate() - i);
    d.setMilliseconds(0);
    d.setSeconds(0);
    d.setMinutes(0);
    d.setHours(0 - hourOffset);

    //main record obj
    const record = {
      device: index.toString(),
      date: d.toISOString(),
      serviceType: 'WATER',
      sensorId: '' + this.datagenArgs.constant,
      readingType: 'PRESSURE',
      reads: []
    };

    L.range(0, 24).forEach(j => {
      if (j !== 0) d.setTime(d.getTime() + 60 * 60 * 1000);
      record.reads.push({
        readDate: d.toISOString(),
        value: faker.random.number({min: 1, max: 100, precision: 1})
      } as never);
    });

    return record;
  }
}
