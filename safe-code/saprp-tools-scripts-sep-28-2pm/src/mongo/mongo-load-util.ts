/* eslint-disable no-console */
import * as fs from 'fs';
import {MongoClient} from 'mongodb';
import * as mu from './index';
import {MongoArgs, debug} from '../common';

function parseSeedData(filename: string): any[] {
  const dataBuffer = fs.readFileSync(filename);
  const dataJSON = dataBuffer.toString();
  const list = JSON.parse(dataJSON);
  if (debug) console.log(`list length=${list.length}`);
  return list;
}

function setDatesAsDates(records: any[]) {
  const firstRecord = records[0];
  const dateNames = [];
  const arrayNames = [];
  const keyNames = Object.keys(firstRecord);

  for (let i = 0; i < keyNames.length; i++) {
    if (keyNames[i].indexOf('date') >= 0 || keyNames[i].indexOf('Date') >= 0) {
      dateNames.push(keyNames[i]);
    }
    if (Array.isArray(firstRecord[keyNames[i]])) {
      arrayNames.push(keyNames[i]);
    }
  }

  if (dateNames.length > 0 || arrayNames.length > 0) {
    for (let i = 0; i < records.length; i++) {
      const record = records[i];
      for (let j = 0; j < dateNames.length; j++) {
        record[dateNames[j]] = new Date(record[dateNames[j]]);
      }
      for (let j = 0; j < arrayNames.length; j++) {
        setDatesAsDates(record[arrayNames[j]]);
      }
    }
  }
}

export async function loadIntoCollection(mongoArgs: MongoArgs): Promise<MongoArgs> {
  let upsertCount = 0;
  let insertCount = 0;

  const records = parseSeedData(mongoArgs.file);
  setDatesAsDates(records);
  const client: MongoClient = await mu.getConnection(mongoArgs.mongoUrl);
  const col = client.db(mongoArgs.dbName).collection(mongoArgs.collectionName);

  for (let i = 0; i < records.length; i++) {
    const rec = records[i];
    if (rec.hasOwnProperty(mongoArgs.pkfield)) {
      const ops = await col.updateOne({[mongoArgs.pkfield]: rec[mongoArgs.pkfield]}, {$set: rec}, {upsert: true});
      upsertCount += ops.result.n;
      if (debug) console.log('update=', upsertCount);
    } else {
      const ops = await col.insertOne(rec);
      insertCount += ops.insertedCount;
      if (debug) console.log('insert=', insertCount);
    }
  }

  await mu.closeConnection(client);

  mongoArgs.upsertCount = upsertCount;
  mongoArgs.insertCount = insertCount;
  return mongoArgs;
}
