/* eslint-disable no-console */
import Command from '../common/base';
import * as mu from '../mongo';
import {mongoFlags} from '../common';
import * as Listr from 'listr';

export async function sleepSecs(secs: number) {
  await new Promise(done => setTimeout(done, secs * 1000));
}

export default class Mongo extends Command {
  static description = 'Mongo: Get Count of Records in given Collection';
  static args = [{name: 'mongo-count-records'}];
  static flags = {...mongoFlags(['help', 'mongos', 'utility', 'collection'])};

  static examples = [
    `$ saprp mongo-count-records -u central -c utility
    or with mongo url
    saprp mongo-count-records -m "mongodb://localhost:27099" -u central -c utility
    `
  ];

  async run() {
    const {args, flags} = this.parse(Mongo);
    const mongoArgs = mu.buildModel(flags);
    let countOfRecords = 0;
    const tasks = new Listr([
      {
        title: `Count of: ${mongoArgs.dbName}.${mongoArgs.collectionName}`,
        task: async () => {
          countOfRecords = await mu.getCollectionCount(mongoArgs);
        }
      }
    ]);
    await tasks.run();

    console.log(countOfRecords);
  } //end-run
}
