/* eslint-disable no-console */
import Command from '../common/base';
import * as Listr from 'listr';
import * as mu from '../mongo';
import {mongoFlags} from '../common';

export default class Mongo extends Command {
  static description = 'Mongo: Check Connection';
  static args = [{name: 'mongo-check-connection'}];
  static flags = {...mongoFlags(['help', 'mongos'])};

  static examples = [
    `$ saprp mongo-check-connection -m "mongodb://localhost:27099"    
    or have MONGO_URL set as mongodb://localhost:27017
    saprp mongo-check-connection 
    `
  ];

  async run() {
    const {args, flags} = this.parse(Mongo);
    const mongoArgs = mu.buildModel(flags);

    const tasks = new Listr([
      {
        title: `Checking Mongo Connection at: ${mongoArgs.mongoUrl}`,
        task: async () => await mu.checkConnection(mongoArgs)
      }
    ]);

    await tasks.run();
  } //end-run
}
