//copied shamelessly from https://gist.github.com/mkhatib/5641004

export interface LatLng {
  lat: number;
  lng: number;
}
/**
 * Generates number of random geolocation points given a center and a radius.
 * Reference URL: http://goo.gl/KWcPE.
 * @param  {Object} center A JS object with lat and lng attributes.
 * @param  {number} radius Radius in meters.
 * @return {Object} The generated random points as JS object with lat and lng attributes.
 */
export function generateRandomPoint(center: LatLng, radius: number): any {
  const x0 = center.lng;
  const y0 = center.lat;
  // Convert Radius from meters (by miles) to degrees.
  const rd = (radius * 1.60934 * 1000) / 111300;

  const u = Math.random();
  const v = Math.random();

  const w = rd * Math.sqrt(u);
  const t = 2 * Math.PI * v;
  const x = w * Math.cos(t);
  const y = w * Math.sin(t);

  const xp = x / Math.cos(y0);

  // Resulting point.
  return {lat: y + y0, lng: xp + x0};
}

/**
 * Generates number of random geolocation points given a center and a radius.
 * @param  {Object} center A JS object with lat and lng attributes.
 * @param  {number} radius Radius in miles.
 * @param {number} count Number of points to generate.
 * @return {array} Array of Objects with lat and lng attributes.
 */
export function generateRandomPoints(center: LatLng, radius: number, count: number) {
  const points = [];
  for (let i = 0; i < count; i++) {
    points.push(generateRandomPoint(center, radius));
  }
  return points;
}

// Usage Example.
// Generates 100 points that is in a 1 mile radius from the given lat and lng point.
const randomGeoPoints = generateRandomPoints({lat: 24.23, lng: 23.12}, 1, 100);
