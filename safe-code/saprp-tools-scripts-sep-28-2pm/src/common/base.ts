import Command, {flags} from '@oclif/command';

export default abstract class extends Command {
  static usage = `${Command.name} [OPTIONS]`;

  async catch(err: Error) {
    await super.catch(err);
    process.exit(-1);
  }
}
