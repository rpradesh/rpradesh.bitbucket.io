/* eslint-disable @typescript-eslint/camelcase */
export enum DataGenTypeEnum {
  prp_site = 'prp_site',
  prp_device = 'prp_device',
  daily_record = 'daily_record',
  device = 'device',
  prp_alarm = 'prp_alarm',
  prp_devicedata = 'prp_devicedata',
  prp_reads = 'prp_reads'
}

export class DatagenArgs {
  public readonly type: string;
  public file = '';
  public constant = '';
  public setEmptyForNotRequiredFields = false;
  public startIndex = 1000 * 1000;
  public size = 10;
  public json = true;
  public notify = false;
  public days = 30;

  constructor(type: string) {
    this.type = type;
  }
}

export const mongoConnectionOpts = {
  useUnifiedTopology: true,
  useNewUrlParser: true
};

export class MongoArgs {
  public readonly mongoUrl: string;
  public utility?: string;
  public col?: string;
  public file = '';
  public pkfield = '_id';
  public recordCount = 10;
  public notify = false;

  upsertCount = 0;
  insertCount = 0;

  constructor(url: string) {
    this.mongoUrl = url;
  }

  get collectionName(): string {
    return '' + this.col;
  }

  get dbName() {
    return this.utility ? `verdeeco_${this.utility}_db` : '';
  }
}
