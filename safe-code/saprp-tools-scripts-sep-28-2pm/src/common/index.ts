/* eslint-disable no-console */
import * as fs from 'fs';
import {MongoArgs, DatagenArgs, mongoConnectionOpts, DataGenTypeEnum} from './model';
import {mongoFlags, datagenFlags} from './flag-options';
import {generateRandomPoint} from './geoutil';

export const debug = process.env.DEBUG === 'true';

export async function sleepSecs(secs: number) {
  await new Promise(done => setTimeout(done, secs * 1000));
}

export {
  /* Model */
  MongoArgs,
  DatagenArgs,
  mongoConnectionOpts,
  DataGenTypeEnum,
  /* Options */
  mongoFlags,
  datagenFlags,
  /* functions */
  generateRandomPoint
};
