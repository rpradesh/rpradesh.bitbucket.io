saprp
=====

SA cmd line tool for data generator and mongo utilities

[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![Version](https://img.shields.io/npm/v/saprp.svg)](https://npmjs.org/package/saprp)
[![Downloads/week](https://img.shields.io/npm/dw/saprp.svg)](https://npmjs.org/package/saprp)
[![License](https://img.shields.io/npm/l/saprp.svg)](https://github.com/rajeshpv/saprp/blob/master/package.json)

<!-- toc -->
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->
# Usage
<!-- usage -->
```sh-session
$ npm install -g saprp
$ saprp COMMAND
running command...
$ saprp (-v|--version|version)
saprp/1.0.3 darwin-x64 node-v10.16.3
$ saprp --help [COMMAND]
USAGE
  $ saprp COMMAND
...
```
<!-- usagestop -->
# Commands
<!-- commands -->
* [`saprp Command [OPTIONS]`](#saprp-command-options)
* [`saprp Command [OPTIONS]`](#saprp-command-options-1)
* [`saprp help [COMMAND]`](#saprp-help-command)
* [`saprp Command [OPTIONS]`](#saprp-command-options-2)
* [`saprp Command [OPTIONS]`](#saprp-command-options-3)
* [`saprp Command [OPTIONS]`](#saprp-command-options-4)
* [`saprp Command [OPTIONS]`](#saprp-command-options-5)
* [`saprp Command [OPTIONS]`](#saprp-command-options-6)

## `saprp Command [OPTIONS]`

Generate data for following types: prp_site,prp_device,daily_record,device,prp_alarm,prp_devicedata,prp_reads

```
USAGE
  $ saprp Command [OPTIONS]

OPTIONS
  -d, --days=days                                                                        [default: 30] number of days,
                                                                                         used by some generator

  -f, --file=file                                                                        (required) path of json file

  -h, --help                                                                             show CLI help

  -i, --startindex=startindex                                                            [default: 1000000] start index,
                                                                                         used for _id while generating
                                                                                         data

  -n, --recordcount=recordcount                                                          [default: 10] number of records
                                                                                         to be generated

  -o, --output=json|csv                                                                  [default: json] The format of
                                                                                         output: [json, csv]

  -t, --type=prp_site|prp_device|daily_record|device|prp_alarm|prp_devicedata|prp_reads  (required) The model of
                                                                                         collection:
                                                                                         prp_site,prp_device,daily_recor
                                                                                         d,device,prp_alarm,prp_deviceda
                                                                                         ta,prp_reads

  -z, --notify                                                                           pass true to display desktop
                                                                                         notifier, when completed

EXAMPLE
  $ saprp datagen -t PrpSite -i 1000 -n 15 -f ~/data/gen-data/test_prpsite_v1.json
```

_See code: [src/commands/datagen.ts](https://github.com/rajeshpv/saprp/blob/v1.0.3/src/commands/datagen.ts)_

## `saprp Command [OPTIONS]`

Mongo: Init  Mongo Collection Directly without generating Json file into given Collection Name

```
USAGE
  $ saprp Command [OPTIONS]

OPTIONS
  -d, --days=days                                                                        [default: 30] number of days,
                                                                                         used by some generator

  -h, --help                                                                             show CLI help

  -i, --startindex=startindex                                                            [default: 1000000] start index,
                                                                                         used for _id while generating
                                                                                         data

  -m, --mongos=mongos                                                                    (required) [default:
                                                                                         mongodb://localhost:27017]
                                                                                         mongo url to connect or export
                                                                                         MONGO_URL

  -n, --recordcount=recordcount                                                          [default: 10] number of records
                                                                                         to be generated

  -o, --output=json|csv                                                                  [default: json] The format of
                                                                                         output: [json, csv]

  -t, --type=prp_site|prp_device|daily_record|device|prp_alarm|prp_devicedata|prp_reads  (required) The model of
                                                                                         collection:
                                                                                         prp_site,prp_device,daily_recor
                                                                                         d,device,prp_alarm,prp_deviceda
                                                                                         ta,prp_reads

  -u, --utility=utility                                                                  (required) name of utility,
                                                                                         example central or nicor to
                                                                                         deduce db name

  -z, --notify                                                                           pass true to display desktop
                                                                                         notifier, when completed

EXAMPLE
  $ saprp datagen-init-mongo-collection -t prp_site -i 1000000 -n 10 -u prp1 --days 30
```

_See code: [src/commands/datagen-init-mongo-collection.ts](https://github.com/rajeshpv/saprp/blob/v1.0.3/src/commands/datagen-init-mongo-collection.ts)_

## `saprp help [COMMAND]`

display help for saprp

```
USAGE
  $ saprp help [COMMAND]

ARGUMENTS
  COMMAND  command to show help for

OPTIONS
  --all  see all commands in CLI
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v2.2.1/src/commands/help.ts)_

## `saprp Command [OPTIONS]`

Mongo: Check Connection

```
USAGE
  $ saprp Command [OPTIONS]

OPTIONS
  -h, --help           show CLI help
  -m, --mongos=mongos  (required) [default: mongodb://localhost:27017] mongo url to connect or export MONGO_URL

EXAMPLE
  $ saprp mongo-check-connection -m "mongodb://localhost:27099"    
       or have MONGO_URL set as mongodb://localhost:27017
       saprp mongo-check-connection
```

_See code: [src/commands/mongo-check-connection.ts](https://github.com/rajeshpv/saprp/blob/v1.0.3/src/commands/mongo-check-connection.ts)_

## `saprp Command [OPTIONS]`

Mongo: Get Count of Records in given Collection

```
USAGE
  $ saprp Command [OPTIONS]

OPTIONS
  -c, --collection=collection  (required) name of collection in given db
  -h, --help                   show CLI help
  -m, --mongos=mongos          (required) [default: mongodb://localhost:27017] mongo url to connect or export MONGO_URL
  -u, --utility=utility        (required) name of utility, example central or nicor to deduce db name

EXAMPLE
  $ saprp mongo-count-records -u central -c utility
       or with mongo url
       saprp mongo-count-records -m "mongodb://localhost:27099" -u central -c utility
```

_See code: [src/commands/mongo-count-records.ts](https://github.com/rajeshpv/saprp/blob/v1.0.3/src/commands/mongo-count-records.ts)_

## `saprp Command [OPTIONS]`

Mongo: Delete all Records in given Collection

```
USAGE
  $ saprp Command [OPTIONS]

OPTIONS
  -c, --collection=collection  (required) name of collection in given db
  -h, --help                   show CLI help
  -m, --mongos=mongos          (required) [default: mongodb://localhost:27017] mongo url to connect or export MONGO_URL
  -u, --utility=utility        (required) name of utility, example central or nicor to deduce db name

EXAMPLE
  $ saprp mongo-delete-records -u prp1 -c prp_process
       or with mongo url
       saprp mongo-delete-records -m "mongodb://localhost:27099" -u prp1 -c prp_process
```

_See code: [src/commands/mongo-delete-records.ts](https://github.com/rajeshpv/saprp/blob/v1.0.3/src/commands/mongo-delete-records.ts)_

## `saprp Command [OPTIONS]`

Mongo: Disaply Records in given Collection

```
USAGE
  $ saprp Command [OPTIONS]

OPTIONS
  -c, --collection=collection    (required) name of collection in given db
  -h, --help                     show CLI help

  -m, --mongos=mongos            (required) [default: mongodb://localhost:27017] mongo url to connect or export
                                 MONGO_URL

  -n, --recordcount=recordcount  [default: 10] number of records to be generated

  -u, --utility=utility          (required) name of utility, example central or nicor to deduce db name

EXAMPLE
  $ saprp mongo-display-records -u prp1 -c prp_site 
       or default display size is 10, can overwrite using -n 20
```

_See code: [src/commands/mongo-display-records.ts](https://github.com/rajeshpv/saprp/blob/v1.0.3/src/commands/mongo-display-records.ts)_

## `saprp Command [OPTIONS]`

Mongo: Load Records from JSON file into given Collection Name

```
USAGE
  $ saprp Command [OPTIONS]

OPTIONS
  -c, --collection=collection  (required) name of collection in given db
  -f, --file=file              (required) path of json file
  -h, --help                   show CLI help
  -m, --mongos=mongos          (required) [default: mongodb://localhost:27017] mongo url to connect or export MONGO_URL
  -u, --utility=utility        (required) name of utility, example central or nicor to deduce db name

EXAMPLE
  $ saprp mongo-load-records -u prp1 -c prp_device -f someDir/prp_device.json
```

_See code: [src/commands/mongo-load-records.ts](https://github.com/rajeshpv/saprp/blob/v1.0.3/src/commands/mongo-load-records.ts)_
<!-- commandsstop -->
