/* eslint-disable no-console */
import {expect, test} from '@oclif/test';
import {TestUrl, TestUtility, executeMongoCmd, log} from '../test-helper';
import * as L from 'lodash';

describe('mongo-count-records', () => {
  before(async () => {
    // let us init a dummy table
    executeMongoCmd(`db.prp_site_1.deleteMany({})`);

    L.range(0, 4).forEach(i => {
      executeMongoCmd(`db.prp_site_1.insert({ theDate: "${new Date()}", index: ${i} })`);
    });
  });

  // test at 27017 "prp_site_1" should be 4
  test
    .stdout()
    .command(['mongo-count-records', '-m', TestUrl, '-u', TestUtility, '-c', 'prp_site_1'])
    .it('should return 4, for above inserted 4 records', ctx => {
      expect(ctx.stdout).to.contain('=4;');
    });

  // test at 27017 "prp_site_ZZ" should be 0
  test
    .stdout()
    .command(['mongo-count-records', '-m', TestUrl, '-u', TestUtility, '-c', 'prp_site_ZZ'])
    .it('should return 0, when counted on non-existent collection', ctx => {
      expect(ctx.stdout).to.contain('=0;');
    });
});
