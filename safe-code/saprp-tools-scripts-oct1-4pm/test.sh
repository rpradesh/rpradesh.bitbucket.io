#!/usr/bin/env bash
export MONGO_URL=mongodb://localhost:27017

COL_NAME=prp_site
# ./bin/run datagen -t PrpSite -f ~/data/gen-data/test_prpsite_v2.json
#./bin/run datagen -t $COL_NAME -f ~/data/gen-data/test_dailyrecord_30_v3.json -n 30 --days 30 --notify=true

if [[ "x$1" == "x1" ]]; then

  ./bin/run datagen               -t $COL_NAME -f ~/data/gen-data/test_dailyrecord_30_v3.json -n 30 --days 30
  ./bin/run mongo-delete-records  -u prp2 -c $COL_NAME
  ./bin/run mongo-load-records    -u prp2 -c $COL_NAME --file ~/data/gen-data/test_dailyrecord_30_v3.json
  ./bin/run mongo-count-records   -u prp2 -c $COL_NAME

elif [[ "x$1" == "x2" ]]; then
  
  ./bin/run datagen-init-mongo-collection  --utility=prp2 --type=$COL_NAME --startIndex=1000 --recordCount=40 --days=30 --empty=false --pkfield=_id

fi

