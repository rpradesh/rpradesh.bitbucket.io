- Build and Local Test steps
- https://www.npmjs.com/package/oclif#-usage
- https://github.com/oclif/oclif
- https://oclif.io/docs/base_class

## Build

```bash
cd sa-wms-prp/prp_tools/saprp
npm run clean
npm install
npm run compile

# execute Tests
npm run test

# to generate Readme.d
npm run version

# to install as binary
npm install -g
```
## Following colleaction takes --days flag
* daily-records
* prp_alarm
* prp_reads

## Following collections have different uniqueId's
* prp_site=siteId
* prp_device=deviceId

## Flags/Options used in Cmd
 * m, mongos, string=env[MONGO_URL]
 * u, utility, string
 * c, collection, string
 * p, pkfield, string
 * *** common ***
 * f, file, string
 * i, startindex, number=1000000
 * n, recordcount, number=10
 * z, notify, boolean=false
 * e, empty, boolean=false
 * *** datagen ***
 * t, type, string
 * d, days, number
 * o, output, string=json of(json,csv)


  
## Mongo - DEV Test Examples 
* Note: below commands are run with flags (meaning single char arg names)
* Note: the "=" sign after option name (either single char or --word) is totally optional, but can be used for brevity  
```bash
cd sa-wms-prp/prp_tools/saprp

#1
./bin/run mongo-check-connection -m mongodb://localhost:27017
export MONGO_URL="mongodb://localhost:27017"
./bin/run mongo-check-connection
#2
./bin/run mongo-count-records -u prp1 -c prp_site
#3
./bin/run mongo-display-records -u prp1 -c prp_site -n 5
#4
./bin/run mongo-delete-records -u prp1 -c prp_site
#5
./bin/run mongo-load-records -u prp1 -c device_1 --file ~/data/gen-data/device.json
```

## DataGen - DEV Test Examples
* Note: these commands are run as flags (meaning single char arg names)

- To Add new Model for Datagen Generator

  - Add new file under datagen/ see example of datagen-prpsite.ts
  - Then add the entry in datagen/index.ts at fn#getDatagenInstance and at import
  - Add typeName entry in enum DataGenTypeEnum at common/model.ts

```bash
cd sa-wms-prp/prp_tools/saprp

# options like
  # i,index=>StartIndex,
  # n,size=>no of Records to generate,
  # o,output=json/csv

#1
./bin/run datagen -t prp_site -f ~/data/gen-data/test_prpsite_v2.json -i 2000000
./bin/run datagen -t prp_site -f ~/data/gen-data/test_prpsite_v2.csv -o csv
cat ~/data/gen-data/test_prpsite_v2.json
cat ~/data/gen-data/test_prpsite_v2.csv
#2
./bin/run datagen -t prp_device -f ~/data/gen-data/test_prpdevice_v2.json -i 2000000
./bin/run datagen -t prp_device -f ~/data/gen-data/test_prpdevice_v2.csv -o csv
cat  ~/data/gen-data/test_prpdevice_v2.json 
cat  ~/data/gen-data/test_prpdevice_v2.csv
#3
./bin/run datagen -t daily_record -f ~/data/gen-data/test_dailyrecord_30_v2.json -n 30
cat ~/data/gen-data/test_dailyrecord_30_v2.json
#4
./bin/run datagen -t device -f ~/data/gen-data/test_justdevice_10_v2.json -n 10
cat ~/data/gen-data/test_justdevice_10_v2.json
#5
./bin/run datagen -t prp_reads -f ~/data/gen-data/test_sensor_read_10_v2_1.json -n 10 -d 30
cat ~/data/gen-data/test_sensor_read_10_v2_1.json
#6
./bin/run datagen -t prp_devicedata -f ~/data/gen-data/test_prp_devicedata_10_v2.json -n 10
cat ~/data/gen-data/test_prp_devicedata_10_v2.json
#7
./bin/run datagen -t prp_alarm -f ~/data/gen-data/test_prp_alarm_10_v2.json -n 10
cat ~/data/gen-data/test_prp_alarm_10_v2.json
```

## DataGen - using binary
* Note: these commands are run as options (not as flags) (meaning with 2 dash --options as arg names)
* Note: the "=" sign after option name (either single char or --word) is totally optional, but can be used for brevity  

```bash
mkdir -p ~/data/gen-data/test-saprp
cd ~/data/gen-data/test-saprp

#1
saprp mongo-check-connection --mongos=mongodb://localhost:27017
export MONGO_URL="mongodb://localhost:27017"
saprp mongo-check-connection
#2
saprp mongo-count-records   --utility=prp1 --collection=prp_site
#3
saprp mongo-display-records --utility=prp1 --collection=prp_site --recordcount=12
#4
saprp mongo-delete-records  --utility=prp1 --collection=prp_site
#5
saprp mongo-load-records    --utility=prp1 --collection=device_1 --file ~/data/gen-data/device.json

# ################ DataGen ################

#1
saprp datagen --recordcount=20 --type=prp_site --file=test_prpsite_v2.json --startindex=2000000
saprp datagen --recordcount=20 --type=prp_site --file=test_prpsite_v2.csv  --output=csv
cat test_prpsite_v2.json
cat test_prpsite_v2.csv
#2
saprp datagen --recordcount=20 --type=prp_device --file=test_prpdevice_v2.json --startindex=2000000
saprp datagen --recordcount=20 --type=prp_device --file=test_prpdevice_v2.csv --output=csv
cat  test_prpdevice_v2.json
cat  test_prpdevice_v2.csv
#3
saprp datagen --recordcount=20 --type=daily_record   --file=test_dailyrecord_30_v2.json
cat test_dailyrecord_30_v2.json
#4
saprp datagen --recordcount=20 --type=device         --file=test_justdevice_10_v2.json
cat test_justdevice_10_v2.json
#5
saprp datagen --recordcount=20 --type=prp_reads      --file=test_sensor_read_10_v2_1.json
cat test_sensor_read_10_v2_1.json
#6
saprp datagen --recordcount=20 --type=prp_devicedata --file=test_prp_devicedata_10_v2.json
cat test_prp_devicedata_10_v2.json
#7
saprp datagen --recordcount=20 --type=prp_alarm      --file test_prp_alarm_10_v2.json
cat test_prp_alarm_10_v2.json
```

## Test *OLD* Data-Script

```bash
cd ~/projects/bb-branch-master/sa-wms-prp/prp-tools/scripts
npm i

COMN_OPTIONS_CMD="  --records=20 --startindex=1000000 --fileType='json' "

node cmd.js --cmd=generateDailyRecordData $COMN_OPTIONS_CMD --filepath='../gen-data/v1_dailyrecord.json' --numberOfDays=30 
node cmd.js --cmd=generateDeviceData      $COMN_OPTIONS_CMD --filepath='../gen-data/v1_device.json'         
node cmd.js --cmd=generateReadData        $COMN_OPTIONS_CMD --filepath='../gen-data/v1_prp_reads.json'      
node cmd.js --cmd=generateAlarmData       $COMN_OPTIONS_CMD --filepath='../gen-data/v1_prp_alarms.json'     
node cmd.js --cmd=generateDeviceDataData  $COMN_OPTIONS_CMD --filepath='../gen-data/v1_prp_devicedata.json' 

# node cmd.js --cmd=loadIntoCollection --db=verdeeco_central_db --collection=utility             --jsonFile=seed-data/dev/utility.json
# node cmd.js --cmd=loadIntoCollection --db=verdeeco_central_db --collection=prp_properties      --jsonFile=seed-data/dev/prp-properties.json

node cmd.js --cmd=displayCollection  --db=verdeeco_central_db --collection=utility
node cmd.js --cmd=displayCollection  --db=verdeeco_central_db --collection=prp_properties

node cmd.js --cmd=clearCollection    --db=verdeeco_prp1_db    --collection=daily_record
node cmd.js --cmd=clearCollection    --db=verdeeco_prp1_db    --collection=device
node cmd.js --cmd=clearCollection    --db=verdeeco_prp1_db    --collection=prp_reads
node cmd.js --cmd=clearCollection    --db=verdeeco_prp1_db    --collection=prp_alarms
node cmd.js --cmd=clearCollection    --db=verdeeco_prp1_db    --collection=prp_devicedata

node cmd.js --cmd=loadIntoCollection --db=verdeeco_prp1_db    --collection=daily_record    --jsonFile=../gen-data/v1_dailyrecord.json
node cmd.js --cmd=loadIntoCollection --db=verdeeco_prp1_db    --collection=device          --jsonFile=../gen-data/v1_device.json
node cmd.js --cmd=loadIntoCollection --db=verdeeco_prp1_db    --collection=prp_reads       --jsonFile=../gen-data/v1_prp_reads.json
node cmd.js --cmd=loadIntoCollection --db=verdeeco_prp1_db    --collection=prp_alarms      --jsonFile=../gen-data/v1_prp_alarms.json
node cmd.js --cmd=loadIntoCollection --db=verdeeco_prp1_db    --collection=prp_devicedata  --jsonFile=../gen-data/v1_prp_devicedata.json

```

## Run *NEW* Data-Script

```bash
cd sa-wms-prp/prp-tools
./data_script.sh INIT_MONGO_DB
```
