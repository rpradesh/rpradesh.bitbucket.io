import {Command, flags} from '@oclif/command';
import {DatagenArgs, DataGenTypeEnum} from '../common';
import * as utils from '../utils';

const GenTypesArray = utils.enumValueAsArray(DataGenTypeEnum);
/**
 * m, mongos, string
 * u, utility, string
 * c, collection, string
 * p, pkfield, string
 * *** common ***
 * f, file, string
 * i, startindex, number=1000000
 * n, recordcount, number=10
 * z, notify, boolean=false
 * *** datagen ***
 * t, type, string
 * d, days, number
 * o, output, string=json of(json,csv)
 * e, empty, boolean=false
 */
export function buildFlagsFor(names: string[]): any {
  let retFlags = {};
  names.forEach(name => {
    if (name === 'help') retFlags = Object.assign(retFlags, {help: flags.help({char: 'h'})});
    else if (name === 'mongos')
      retFlags = Object.assign(retFlags, {
        mongos: flags.string({
          char: 'm',
          description: 'mongo url to connect or export MONGO_URL',
          default: process.env.MONGO_URL,
          required: true
        })
      });
    else if (name === 'utility')
      retFlags = Object.assign(retFlags, {
        utility: flags.string({
          char: 'u',
          description: 'name of utility, example central or nicor to deduce db name',
          required: true,
          dependsOn: ['mongos']
        })
      });
    else if (name === 'collection')
      retFlags = Object.assign(retFlags, {
        collection: flags.string({
          char: 'c',
          description: 'name of collection in given db',
          required: true,
          dependsOn: ['mongos', 'utility']
        })
      });
    else if (name === 'pkfield')
      retFlags = Object.assign(retFlags, {
        pkfield: flags.string({
          char: 'p',
          description: 'PrimaryKey field Name to be used duringLoadIntoMongo',
          default: '_id'
        })
      });
    /*  ==========  Common Flags ============ */ else if (name === 'file')
      retFlags = Object.assign(retFlags, {
        file: flags.string({
          char: 'f',
          description: 'path of json file',
          required: true
        })
      });
    else if (name === 'startindex')
      retFlags = Object.assign(retFlags, {
        startindex: flags.integer({
          char: 'i',
          description: `start index, used for _id while generating data`,
          default: 1000 * 1000
        })
      });
    else if (name === 'recordcount')
      retFlags = Object.assign(retFlags, {
        recordcount: flags.integer({
          char: 'n',
          description: `number of records to be generated`,
          default: 10
        })
      });
    else if (name === 'notify')
      retFlags = Object.assign(retFlags, {
        notify: flags.string({
          char: 'z',
          description: `pass true to display desktop notifier, when completed`,
          default: 'false'
        })
      });
    /*  ==========  DataGen Flags ============ */ else if (name === 'type')
      retFlags = Object.assign(retFlags, {
        type: flags.string({
          char: 't',
          description: `The model of collection: ${GenTypesArray.join(',')}`,
          required: true,
          options: GenTypesArray
        })
      });
    else if (name === 'days')
      retFlags = Object.assign(retFlags, {
        days: flags.integer({
          char: 'd',
          description: `number of days, used by some generator`,
          default: 30
        })
      });
    else if (name === 'output')
      retFlags = Object.assign(retFlags, {
        output: flags.string({
          char: 'o',
          description: `The format of output: [json, csv]`,
          default: 'json',
          options: ['json', 'csv']
        })
      });
    else if (name === 'empty')
      retFlags = Object.assign(retFlags, {
        empty: flags.string({
          char: 'e',
          description: `Empty Collection before Ingest, based on argument`,
          default: 'false'
        })
      });
    else throw new Error(`No flag option found for name=${name}`);
  });
  return retFlags;
}
