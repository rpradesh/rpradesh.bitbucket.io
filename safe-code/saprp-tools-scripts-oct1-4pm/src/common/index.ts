/* eslint-disable no-console */
import * as L from 'lodash';
import {MongoArgs, DatagenArgs, mongoConnectionOpts, DataGenTypeEnum} from './model';
import {buildFlagsFor} from './flag-options';
import {generateRandomPoint} from './geoutil';

export const debug = process.env.DEBUG === 'true';

export async function sleepSecs(secs: number) {
  await new Promise(done => setTimeout(done, secs * 1000));
}

function updateMongoFlags(flagMap: any, mongoArgs: any): any {
  // mongo
  mongoArgs.mongoUrl = flagMap.mongos;
  mongoArgs.utility = flagMap.utility;
  mongoArgs.col = flagMap.collection;
  // reset _id only if flag has a value
  if (flagMap.pkfield) mongoArgs.pkfield = flagMap.pkfield;
  // common
  mongoArgs.file = flagMap.file;
  if (flagMap.startindex) mongoArgs.startIndex = flagMap.startindex;
  if (flagMap.recordcount) mongoArgs.recordCount = flagMap.recordcount;
  if (flagMap.notify) mongoArgs.doNotify = flagMap.notify;
  if (flagMap.empty) mongoArgs.doEmpty = flagMap.empty;

  /// console.log(`flagMap=${JSON.stringify(flagMap)} mongoArgs=${JSON.stringify(mongoArgs)}`);
  return mongoArgs;
}

export function buildDatagenModel(flagMap: any): DatagenArgs {
  let datagenArgs = new DatagenArgs(flagMap.type);
  if (flagMap.days) datagenArgs.days = flagMap.days;
  datagenArgs.json = flagMap.output === 'json';
  // update extra for DataGen from MongoGen too
  datagenArgs = updateMongoFlags(flagMap, datagenArgs);

  return datagenArgs;
}

export function buildMongoModel(flagMap: any): MongoArgs {
  let mongoArgs = new MongoArgs();
  mongoArgs = updateMongoFlags(flagMap, mongoArgs);
  /// console.log(`mongoArgs=`, mongoArgs);
  return mongoArgs;
}

export {
  /* Model */
  MongoArgs,
  DatagenArgs,
  mongoConnectionOpts,
  DataGenTypeEnum,
  /* Options */
  buildFlagsFor,
  /* functions */
  generateRandomPoint
};
