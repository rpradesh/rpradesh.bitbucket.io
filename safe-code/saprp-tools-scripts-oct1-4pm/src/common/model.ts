/* eslint-disable @typescript-eslint/camelcase */
export enum DataGenTypeEnum {
  prp_site = 'prp_site',
  prp_device = 'prp_device',
  daily_record = 'daily_record',
  device = 'device',
  prp_alarm = 'prp_alarm',
  prp_devicedata = 'prp_devicedata',
  prp_reads = 'prp_reads'
}

export const mongoConnectionOpts = {
  useUnifiedTopology: true,
  useNewUrlParser: true
};

class CommonArgs {
  public file = '';
  public startIndex = 1000 * 1000;
  public recordCount = 10;
  public doNotify = 'false';
  public doEmpty = 'false';
}

export class MongoArgs extends CommonArgs {
  public mongoUrl = '';
  public utility?: string;
  public col?: string;
  public pkfield = '_id';

  upsertCount = 0;
  insertCount = 0;

  get collectionName(): string {
    return '' + this.col;
  }
  get displayCollectionName(): string {
    return '' + this.dbName + '.' + this.collectionName;
  }

  get dbName() {
    return this.utility ? `verdeeco_${this.utility}_db` : '';
  }
}

export class DatagenArgs extends MongoArgs {
  public readonly type: string;
  public days = 30;
  public json = true; //from output

  // extra props
  public setEmptyForNotRequiredFields = false;
  public doDirectPublish = false;

  constructor(type: string) {
    super();
    this.type = type;
  }
}
