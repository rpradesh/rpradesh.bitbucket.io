/* eslint-disable no-console */
import Command from '../common/base';
import * as mu from '../mongo';
import {buildFlagsFor, MongoArgs, buildMongoModel} from '../common';
import * as utils from '../utils';
import * as Listr from 'listr';

export default class Mongo extends Command {
  static description = 'Mongo: Load Records from JSON file into given Collection Name';
  static args = [{name: 'mongo-load-records'}];
  static flags = {...buildFlagsFor(['help', 'mongos', 'utility', 'collection', 'file'])};
  static examples = [
    `$ saprp mongo-load-records -u prp1 -c prp_device -f someDir/prp_device.json    
    `
  ];

  async run() {
    const {args, flags} = this.parse(Mongo);
    const mongoArgs = buildMongoModel(flags);
    let outMongoArgs: MongoArgs = mongoArgs;
    const tasks = new Listr([
      {
        title: `Checking File Exists: ${mongoArgs.file}`,
        task: async () => await utils.isFile(mongoArgs.file)
      },
      {
        title: `Loading into Collection: ${mongoArgs.displayCollectionName}`,
        task: async () => {
          outMongoArgs = await mu.loadIntoCollection(mongoArgs);
        }
      }
    ]);

    await tasks.run();
    this.log(
      `Loaded into Collection: ${mongoArgs.displayCollectionName} upsertCount=${outMongoArgs.upsertCount}; insertCount=${outMongoArgs.insertCount};`
    );
  } //end-run
}
