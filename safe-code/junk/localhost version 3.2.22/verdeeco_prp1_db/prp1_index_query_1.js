db.prp_site.createIndex({ siteId: 1 }, {
    name: 'prpsite_siteidA_unique',
    unique: true
});

db.prp_site.createIndex({ siteId: 1 }, {
    name: 'prpsite_siteidA_unique',
    unique: true
});

db.prp_site.getIndexes();

db.prp_site.dropIndexes();


db.prp_properties.update({ _id: 'site' }, 
    { $set: { 'site.csvUploadSizeLimitBytes': 1000 } }
);

db.prp_properties.update({ _id: 'allDefaultMongoIndexDefinitions' }, 
    { $set: { 'allDefaultMongoIndexDefinitions.executeAllDefaultMongoIndexDefinitions': true } }
);

db.prp_properties.update({ _id: 'test1' }, 
    { $set: { one: 1, two: 'two'} }, 
    {upsert:true}
);

db.prp_properties.update({ _id: 'singleMongoIndexDefinition' }, 
    { $set: { 'singleMongoIndexDefinition.executeSingleMongoIndexDefinition': true } }
);


db.prp_properties.findOne({ _id: 'allDefaultMongoIndexDefinitions' });
// enable the Job before it can be triggered by api
db.prp_properties.update({ _id: 'allDefaultMongoIndexDefinitions' }, 
    { $set: { 'allDefaultMongoIndexDefinitions.jobEnabled': true } }
);
