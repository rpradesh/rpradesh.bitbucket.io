db.prp_device.find({})
   .projection({})
   .sort({_id:-1})
   .limit(100);
   
db.prp_site.find({})
   .projection({})
   .sort({_id:-1})
   .limit(100);   
   

db.prp_site.createIndex({siteId: 1},{
    name: 'prpsite_siteidA_uniqueB',
    unique: true
  });
  
db.prp_site.getIndexes();
db.prp_site.dropIndexes();