db.version(); //3.2.21

db.prp_site.find({})
   .projection({})
   .sort({_id:-1})
   .limit(100);
   
db.zipcodes.find({})
   .projection({})
   .sort({_id:-1})
   .limit(100);
// works
db.zipcodes.aggregate( [
   { $group: { _id: "$state", totalPop: { $sum: "$pop" } } },
   { $match: { totalPop: { $gte: 10*1000*1000 } } },
   { $out: 'zip1' }
] );
// works
db.zip1.find({}).forEach(function(doc) {db.zip2.insert(doc)});

// works
db.zipcodes.aggregate( [
   { $group: { _id: "$state", totalPop: { $sum: "$pop" } } },
   { $match: { totalPop: { $gte: 10*1000*1000 } } }
] ).forEach(function(doc) {db.zip3.insert(doc)});

//works
db.zip33.insertMany(
db.zipcodes.aggregate( [
   { $group: { _id: "$state", totalPop: { $sum: "$pop" } } },
   { $match: { totalPop: { $gte: 10*1000*1000 } } }
] ).toArray()
);
// this returns result even
db.zip34.insertMany(
db.zipcodes.aggregate( [
   { $group: { _id: "$state", totalPop: { $sum: "$pop" } } },
   { $match: { totalPop: { $gte: 10*1000*1000 } } }
] ).toArray(), {ordered:false}
);

db.zip34.update(
db.zipcodes.aggregate( [
   { $group: { _id: "$state", totalPop: { $sum: "$pop" } } },
   { $match: { totalPop: { $gte: 10*1000*1000 } } }
] ).toArray(), {upsert:true}
);


db.zip34.drop();   

db.myShardedCollection.getShardDistribution();

//++/ works
var cols = db.getCollectionNames();
// Iterate through each collection.
cols.forEach(function(col) {
    print(col);
        print(db.getCollection(col).getShardDistribution());
});