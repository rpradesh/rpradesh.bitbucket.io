const chalk = require('chalk');

const infoMsgAndReturn = (msg, returnCode=0) => {
    console.log(chalk.default.green(msg));
    //process.exit(returnCode); 
    //always return zero since bash scripts error for non-zero returns based on set -e
    process.exit(0);
}

const errorMsgAndReturn = (msg, errMsg = '') => {
    console.log(chalk.default.red(msg) , ' ' , chalk.default.red.inverse(errMsg));
    process.exit(-1);
}

module.exports = {
    infoMsgAndReturn,
    errorMsgAndReturn
}