const faker = require('faker');
const L = require('lodash');
const commonDatagen = require('./commonDatagen');

let numberOfDays = 30;

function getDataForOneDevice(index) {
  const records = [];
  const d = new Date();
  const hourOffset = d.getTimezoneOffset() / 60;

  L.range(0, numberOfDays).forEach((i) => {
    d.setDate(d.getDate() - i);
    d.setMilliseconds(0);
    d.setSeconds(0);
    d.setMinutes(0);
    d.setHours(0 - hourOffset);

    const record = {
      device: index.toString(),
      date: d.toISOString(),
      serviceType: 'WATER',
      sensorId: '1',
      readingType: 'PRESSURE',
      reads: []
    };

    const recordSensor2 = {
      device: index.toString(),
      date: d.toISOString(),
      serviceType: 'WATER',
      sensorId: '2',
      readingType: 'PRESSURE',
      reads: []
    };

    L.range(0, 24).forEach((j) => {
      if (j !== 0) d.setTime(d.getTime() + (60 * 60 * 1000));
      record.reads.push({
        readDate: d.toISOString(),
        value: faker.random.number({ min: 1, max: 100, precision: 1 })
      });
      recordSensor2.reads.push({
        readDate: d.toISOString(),
        value: faker.random.number({ min: 1, max: 100, precision: 1 })
      });
    });

    records.push(record);
    records.push(recordSensor2);
  });

  return records;
}

const generate = async (argv) => {
  // eslint-disable-next-line prefer-destructuring
  if (argv.numberOfDays) { numberOfDays = argv.numberOfDays; }
  commonDatagen.generate(argv, getDataForOneDevice);
};

module.exports = {
  generate
};
