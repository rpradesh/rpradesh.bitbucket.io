const faker = require('faker');
const geohash = require('ngeohash');
const geoutil = require('./geoutil');
const commonDatagen = require('./commonDatagen');

const caryLatLong = { lat: 35.79154, lng: -78.7811169 };
const nullablesColIndex = [ 9, 10, 11, 12, 13, 14, 15 ];

const siteTypes = [ 'Tank', 'Pump Station', 'PRV', 'Reservoir', 'Sensor', 'Flushing', 'Valve' ];

let hideNullable = 0;
const generate = async (argv) => {
  hideNullable = argv.setEmptyForNotRequiredFields && argv.setEmptyForNotRequiredFields === 'true' ? 1 : 0;
  commonDatagen.generate(argv, getColumns);
};

function getColumns(index) {
  const latLong = geoutil.generateRandomPoint(caryLatLong, 30);
  const cols = [
    /*   0, 'recordInActive'  ,  true, */ '0',
    /*   0, 'siteId'          ,  true, */ `sid${index}`,
    /*   1, 'siteName'        ,  true, */ `siteName ${index} ${faker.random.alphaNumeric(10)}`,
    /*   2, 'siteType'        ,  true, */ siteTypes[faker.random.number({ min: 0, max: 6, precision: 1 })],
    /*   3, 'insertDate'      ,  true, */ faker.date
      .between('2015-01-01', '2019-01-01')
      .toISOString()
      .split('T')[0],
    /*   4, 'lat'             ,  true, */ latLong.lat.toFixed(5),
    /*   5, 'lng'             ,  true, */ latLong.lng.toFixed(5),
    /*   6, 'pressureZone'    ,  true, */ `prZone-${faker.random.alphaNumeric(10)}`,
    /*   7, 'elevation'       ,  true, */ faker.random.number({ min: 50, max: 999, precision: 3 }),
    /*   8, 'geoHash'         , false, */ geohash.encode(latLong.lat, latLong.lng),
    /*   9, 'elevationOffset' , false, */ faker.random.number({ min: 5, max: 50, precision: 2 }),
    /*  10, 'address.line1'   , false, */ faker.address.streetAddress().replace(',', ''),
    /*  11, 'address.line2'   , false, */ faker.address.secondaryAddress().replace(',', ''),
    /*  12, 'address.city'    , false, */ faker.address.city().replace(',', ''),
    /*  13, 'address.state'   , false, */ faker.address.state(),
    /*  14, 'address.zip'     , false, */ faker.address.zipCode()
  ];

  return hideNullable > 0 ? emptyForNotRequired(cols) : cols;
}

function emptyForNotRequired(cols) {
  for (const i of nullablesColIndex) cols[i] = '';
  return cols;
}

module.exports = {
  generate
};
