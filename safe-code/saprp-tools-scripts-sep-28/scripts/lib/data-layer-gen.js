const faker = require('faker');

const geoutil = require('./geoutil');
const commonDatagen = require('./commonDatagen');

const caryLatLong = { lat: -33.8582, lng: 151.401842 };

const timestamp = [ '1-1', '1-2', '1-3' ];
const sizes = [ '20', '40', '60', '80', '100' ];

function getColumns(index) {
  const latLong = geoutil.generateRandomPoint(caryLatLong, 30);

  const cols = [
    `{
      "type": "Feature",
      "properties": {
        "metaData": "info ${index} ${faker.random.alphaNumeric(10)}",
        "percentFilled": "${sizes[faker.random.number({ min: 0, max: 4, precision: 1 })]}",
        "timestamp": "${timestamp[0]}"        
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
       ${latLong.lng.toFixed(5)},
       ${latLong.lat.toFixed(5)}
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "metaData": "info ${index} ${faker.random.alphaNumeric(10)}",
        "percentFilled": "${sizes[faker.random.number({ min: 0, max: 4, precision: 1 })]}",
        "timestamp": "${timestamp[1]}"        
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
       ${latLong.lng.toFixed(5)},
       ${latLong.lat.toFixed(5)}
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "metaData": "info ${index} ${faker.random.alphaNumeric(10)}",
        "percentFilled": "${sizes[faker.random.number({ min: 0, max: 4, precision: 1 })]}",
        "timestamp": "${timestamp[2]}"        
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
       ${latLong.lng.toFixed(5)},
       ${latLong.lat.toFixed(5)}
        ]
      }
    },        
    `
  ];

  return cols;
}

// eslint-disable-next-line arrow-parens
const generate = async argv => {
  commonDatagen.generate(argv, getColumns);
};

module.exports = {
  generate
};
