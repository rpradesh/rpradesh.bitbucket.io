const faker = require('faker');
const geohash = require('ngeohash');
const L = require('lodash');
const geoutil = require('./geoutil');
const commonDatagen = require('./commonDatagen');

const caryLatLong = { lat: 35.79154, lng: -78.7811169 };
const nullablesColIndex = L.range(5, 24);
// console.log(`nullablesColIndex=${nullablesColIndex}`)

const deviceTypes = [ 'METER', 'ALLY_METER', 'SGW' ];

let hideNullable = 0;
const generate = async (argv) => {
  hideNullable = argv.setEmptyForNotRequiredFields && argv.setEmptyForNotRequiredFields === 'true' ? 1 : 0;
  commonDatagen.generate(argv, getColumns);
};

function getColumns(index) {
  const latLong = geoutil.generateRandomPoint(caryLatLong, 30);
  const cols = [
    /* 0		recordInActive  true   */ '0',
    /* 0		deviceId        true   */ `deviceId${index}`,
    /* 1		meterType       true   */ 'WATER',
    /* 2		deviceType      true   */ deviceTypes[faker.random.number({ min: 0, max: 2, precision: 1 })],
    /* 3		siteId          true   */ `sid${index}`,
    /* 4		deviceName      false  */ `deviceName ${index} ${faker.random.alphaNumeric(10)}`,
    /* 5		dma             false  */ `dma-${faker.random.alphaNumeric(10)}`,
    /* 6		lat             false  */ latLong.lat.toFixed(5),
    /* 7		lng             false  */ latLong.lng.toFixed(5),
    /* 8		pressureZone    false  */ `prZone-${faker.random.alphaNumeric(10)}`,
    /* 9		elevation       false  */ faker.random.number({ min: 50, max: 999, precision: 3 }),
    /* 10		geoHash         false  */ geohash.encode(latLong.lat, latLong.lng),
    /* 11		elevationOffset false  */ faker.random.number({ min: 5, max: 50, precision: 2 }),
    /* 12		address.line1   false  */ faker.address.streetAddress().replace(',', ''),
    /* 13		address.line2   false  */ faker.address.secondaryAddress().replace(',', ''),
    /* 14		address.city    false  */ faker.address.city().replace(',', ''),
    /* 15		address.state   false  */ faker.address.state(),
    /* 16		address.zip     false  */ faker.address.zipCode(),
    /* 17		analogLabel1    false  */ Array.from([ 'ang_AA', 'ang_BB' ])[
      faker.random.number({ min: 0, max: 1, precision: 1 })
    ],
    /* 18		analogLabel2    false  */ Array.from([ 'ang_AA', 'ang_BB' ])[
      faker.random.number({ min: 0, max: 1, precision: 1 })
    ],
    /* 19		digLowLabel1    false  */ Array.from([ 'DG_low_AA', 'DG_high_BB' ])[
      faker.random.number({ min: 0, max: 1, precision: 1 })
    ],
    /* 20		digLowLabel2    false  */ Array.from([ 'DG_low_AA', 'DG_high_BB' ])[
      faker.random.number({ min: 0, max: 1, precision: 1 })
    ],
    /* 21		digHighLabel1   false  */ Array.from([ 'dg_HGH_AA', 'dg_HGH_BB' ])[
      faker.random.number({ min: 0, max: 1, precision: 1 })
    ],
    /* 22		digHighLabel2   false  */ Array.from([ 'dg_HGH_AA', 'dg_HGH_BB' ])[
      faker.random.number({ min: 0, max: 1, precision: 1 })
    ]
  ];

  return hideNullable > 0 ? emptyForNotRequired(cols) : cols;
}

function emptyForNotRequired(cols) {
  for (const i of nullablesColIndex) cols[i] = '';
  return cols;
}

module.exports = {
  generate
};
