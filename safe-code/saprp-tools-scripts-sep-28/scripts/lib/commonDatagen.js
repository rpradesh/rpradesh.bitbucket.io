const fs = require('fs')
const faker = require('faker')
const geoutil = require('./geoutil')
const common = require('./common')
const geohash = require('ngeohash');

const debug = process.env.DEBUG_SCRIPT === 'false';
const WriteBatchSize = 100;

function log(...args) {
    if (debug === true) console.log(...args);
}

const generate = async (argv, callback) => {
    try {
        const startedAt = new Date().getTime();
        const hideNullable = argv.setEmptyForNotRequiredFields && argv.setEmptyForNotRequiredFields === 'true' ? 1 : 0;
        if (argv.fileType === 'json') {
            doGenerateJson(callback, argv.filepath, argv.startindex, argv.records);
        }
        else {
            // Assume CSV if not of a called out type
            doGenerateCsv(callback, argv.filepath, argv.startindex, argv.records, hideNullable);
        }
        const diffMins = (new Date().getTime() - startedAt) / (1000 * 60)
        common.infoMsgAndReturn(`generated [${argv.records}] lines of csv at filepath=${argv.filepath} in [${diffMins.toFixed(2)}] mins.`, argv.records);
    }
    catch (err) {
        console.error(err)
        common.errorMsgAndReturn(`Failed to generate Site-Csv data`, err.message);
    }
}

function doGenerateCsv(callback, filepath = '../test-data/10-sitedata.csv', startindex = 1000000, records = 10, hideNullable) {
    for (const arg of arguments) log(arg);
    //     return; // remove if file exists
    if (fs.existsSync(filepath)) {
        fs.unlinkSync(filepath);
    }

    let lines = [];

    for (let i = 0; i < records; i++) {
        const cols = callback(startindex + i);
        let line = cols.join(',');

        log(`line=${line}`);
        lines.push(line);

        if (i % WriteBatchSize === 0) {
            log(`inside batch i=${i}`);
            flushLines(filepath, lines);
            lines = [];
        }
    }

    //if any left outside batch
    flushLines(filepath, lines);
}

function doGenerateJson(callback, filepath = '../gen-data/dailyrecord.json', startindex = 1000000, records = 10) {
    for (const arg of arguments) log(arg);

    //     return; // remove if file exists
    if (fs.existsSync(filepath)) {
        fs.unlinkSync(filepath);
    }

    let lines = [];

    for (let i = 0; i < records; i++) {
        const cols = callback(startindex + i);
        cols.forEach((item, index) => { lines.push(item); });
    }

    flushLinesText(filepath, JSON.stringify(lines));
}

function emptyForNotRequired(cols, hideNullable) {
    let newcols = cols;
    if (hideNullable > 0) {
        for (const i of nullablesColIndex) {
            newcols[i] = '';
        }
    }

    return newcols;
}

function flushLines(filepath, lines) {
    if (!lines || lines.length === 0) return;
    log(`inside flush lines.length=${lines.length}`);

    const wStream = fs.appendFileSync(filepath, lines.join('\n').concat('\n'));
}

function flushLinesText(filepath, lines) {
    if (!lines || lines.length === 0) return;
    log(`inside flush lines.length=${lines.length}`);

    const wStream = fs.appendFileSync(filepath, lines);
}

function testFaker(index) {
    let latLong = geoutil.generateRandomPoint(caryLatLong, 30);
    console.log('siteId=', `sid${index}`)
    console.log('siteName=', `siteName${index}`)
    console.log('siteType=', siteTypes[faker.random.number({ min: 0, max: 7, precision: 1 })])
    console.log('installDate=', faker.date.between('2015-01-01', '2019-01-01').toISOString().split('T')[0]);
    console.log('lat+lng=', latLong)
    console.log('geohash=', geohash.encode(latLong.lat, latLong.lng))
    console.log('elev=', faker.random.number({ min: 50, max: 999, precision: 3 }))
    console.log('elevOffset=', faker.random.number({ min: 5, max: 50, precision: 2 }))
    console.log('pressureZone=', `prZone-${faker.random.alphaNumeric(10)}`)
    console.log('ad line1=', faker.address.streetAddress().replace(',', ''));
    console.log('ad line2=', faker.address.secondaryAddress().replace(',', ''));
    console.log('ad city=', faker.address.city().replace(',', ''));
    console.log('ad state=', faker.address.state());
    console.log('ad zipCode=', faker.address.zipCode());
}

module.exports = {
    generate,
    testFaker
}