const argv = require('yargs').argv;
const MongoClient = require('mongodb').MongoClient;
const fs = require('fs')

const common = require('./common')

const debug = process.env.DEBUG === 'true';
const mongoURL = process.env.MONGO_URL || 'mongodb://localhost:27017/';
const client = new MongoClient(mongoURL, { useNewUrlParser: true });
if (debug) console.log(mongoURL);

const checkMongoConnection = async () => {
    try {
        await client.connect();
        const isConnected = client.isConnected();
        if (debug) console.log(`isConnected=${isConnected}`);
        client.close();
        common.infoMsgAndReturn(`Mongo at ${mongoURL} is Running`);
    }
    catch (err) {
        common.errorMsgAndReturn(`Mongo at ${mongoURL} is NOT Running`, err.message);
    }
}

const getCollectionCount = async () => {
    try {
        await client.connect();
        const count = await client.db(argv.db).collection(argv.collection).countDocuments();
        client.close();
        common.infoMsgAndReturn(`${argv.collection} count=${count}`, count);
    }
    catch (err) {
        common.errorMsgAndReturn(`Mongo at ${mongoURL} is NOT Running`, err.message);
    }
}

const clearCollection = async () => {
    try {
        await client.connect();
        const ops = await client.db(argv.db).collection(argv.collection).deleteMany();
        client.close();
        common.infoMsgAndReturn(`deletedCount=${ops.deletedCount}`, ops.deletedCount);
    } catch (err) {
        common.errorMsgAndReturn(`failed to clear collection=${argv.collection} mongo db`, err.message);
    }
}

const displayCollection = async () => {
    try {
        await client.connect();
        const propsArray = await client.db(argv.db).collection(argv.collection).find({}).toArray();
        client.close();
        const myModel = {};
        propsArray.forEach((rec, idx) => {
            console.log(`rec[${idx}] = ${JSON.stringify(rec)}`);
        });

        common.infoMsgAndReturn(`propsArray length=${JSON.stringify(propsArray)}`);
    } catch (err) {
        common.errorMsgAndReturn(`failed to display collection details=${argv.collection} mongo db`, err.message);
    }
}

const loadIntoCollection = async () => {
    try {
        const records = parseSeedData(argv.jsonFile);
        setDatesAsDates(records);
        await client.connect();
        const col = client.db(argv.db).collection(argv.collection);

        let upsertCount = 0;
        let insertCount = 0;
        for (let i = 0; i < records.length; i++) {
            let rec = records[i];
            if (rec.hasOwnProperty('_id')) {
                let ops = await col.updateOne({ _id: rec._id }, { $set: rec }, { upsert: true });
                upsertCount += ops.result.n;
                if (debug) console.log('update=', upsertCount)
            } else {
                let ops = await col.insertOne(rec);
                insertCount += ops.insertedCount;
                if (debug) console.log('insert=', insertCount)
            }
        }

        client.close();
        common.infoMsgAndReturn(`upsertCount=${upsertCount} insertCount=${insertCount}`, upsertCount > insertCount ? upsertCount : insertCount);
    } catch (err) {
        common.errorMsgAndReturn(`failed to load json=${argv.jsonFile} into mongo db`, err.message);
    }
}

function setDatesAsDates(records) {
    const firstRecord = records[0];
    const dateNames = [];
    const arrayNames = [];
    const keyNames = Object.keys(firstRecord);

    for (let i = 0; i < keyNames.length; i++) {
        if (keyNames[i].indexOf('date') >= 0 || keyNames[i].indexOf('Date') >= 0) { dateNames.push(keyNames[i]) }
        if (Array.isArray(firstRecord[keyNames[i]])) { arrayNames.push(keyNames[i]) }
    }

    if (dateNames.length > 0 || arrayNames.length > 0) {
        for (let i = 0; i < records.length; i++) {
            let record = records[i];
            for (let j = 0; j < dateNames.length; j++) {
                record[dateNames[j]] = new Date(record[dateNames[j]]);
            }
            for (let j = 0; j < arrayNames.length; j++) {
                setDatesAsDates(record[arrayNames[j]]);
            }
        }
    }
}

const parseSeedData = (filename) => {
    const dataBuffer = fs.readFileSync(filename);
    const dataJSON = dataBuffer.toString();
    const list = JSON.parse(dataJSON);
    if (debug) console.log(`list length=${list.length}`);
    return list;
}

module.exports = {
    checkMongoConnection,
    getCollectionCount,
    clearCollection,
    displayCollection,
    loadIntoCollection
}