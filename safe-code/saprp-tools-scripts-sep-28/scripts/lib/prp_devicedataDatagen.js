const faker = require('faker');
const geohash = require('ngeohash');
const commonDatagen = require('./commonDatagen');
const geoutil = require('./geoutil');

const caryLatLong = { lat: 35.79154, lng: -78.7811169 };

function getDataForOneDevice(index) {
  const records = [];

  const latLong = geoutil.generateRandomPoint(caryLatLong, 30);

  const record = {
    _id: index,
    deviceId: index.toString(),
    flexnetId: index.toString(),
    meterType: 'WATER',
    deviceType: 'SGW',
    siteId: `sid${index}`,
    deviceName: `deviceName ${index} ${faker.random.alphaNumeric(10)}`,
    dma: `dma-${faker.random.alphaNumeric(10)}`,
    productType: '17',
    model: 'Smart Gateway',
    manufacturer: 'Sensus',
    installDate: '2019-08-15T02:34:56Z',
    smartpointFirmware: '1.19',
    location: {
      lat: latLong.lat.toFixed(5),
      lng: latLong.lng.toFixed(5),
      geohash: geohash.encode(latLong.lat, latLong.lng),
      elevation: faker.random.number({ min: 50, max: 999, precision: 3 }),
      elevationOffset: faker.random.number({ min: 5, max: 50, precision: 2 })
    },
    pressureZone: `prZone-${faker.random.alphaNumeric(10)}`,
    adminState: 'Active',
    sensorState: 'Active',
    messagingState: 'true',
    address: {
      line1: faker.address.streetAddress().replace(',', ''),
      line2: faker.address.secondaryAddress().replace(',', ''),
      city: faker.address.city().replace(',', ''),
      state: faker.address.state(),
      zip: faker.address.zipCode()
    },
    sensors: [ {
      type: 'SENSOR',
      id: '1',
      label: `pressure${faker.random.alphaNumeric(4)}`,
      readingType: 'Pressure',
      uom: 'psi',
      elevationOffset: faker.random.number({ min: 5, max: 50, precision: 2 }),
      eventConfigurations: [
        {
          eventid: 'CIMCODE1',
          uom: 'psi',
          high: faker.random.number({ min: 5, max: 100, precision: 2 }),
          durationUom: 'min',
          duration: faker.random.number({ min: 1, max: 30, precision: 2 })
        },
        {
          eventid: 'CIMCODE2',
          uom: 'psi',
          low: faker.random.number({ min: 5, max: 25, precision: 2 }),
          high: faker.random.number({ min: 30, max: 100, precision: 2 }),
          durationUom: 'min',
          duration: faker.random.number({ min: 1, max: 30, precision: 2 })
        }
      ]
    },
    {
      type: 'SENSOR',
      id: '2',
      label: `pressure${faker.random.alphaNumeric(4)}`,
      readingType: 'Pressure',
      uom: 'psi',
      elevationOffset: faker.random.number({ min: 5, max: 50, precision: 2 }),
      eventConfigurations: [
        {
          eventid: 'CIMCODE1',
          uom: 'psi',
          high: faker.random.number({ min: 5, max: 100, precision: 2 }),
          durationUom: 'min',
          duration: faker.random.number({ min: 1, max: 30, precision: 2 })
        },
        {
          eventid: 'CIMCODE2',
          uom: 'psi',
          low: faker.random.number({ min: 5, max: 25, precision: 2 }),
          high: faker.random.number({ min: 30, max: 100, precision: 2 }),
          durationUom: 'min',
          duration: faker.random.number({ min: 1, max: 30, precision: 2 })
        }
      ]
    } ]
  };

  records.push(record);

  return records;
}
const generate = async (argv) => {
  commonDatagen.generate(argv, getDataForOneDevice);
};

module.exports = {
  generate
};
