//copied shamelessly from https://gist.github.com/mkhatib/5641004

/**
* Generates number of random geolocation points given a center and a radius.
* @param  {Object} center A JS object with lat and lng attributes.
* @param  {number} radius Radius in miles.
* @param {number} count Number of points to generate.
* @return {array} Array of Objects with lat and lng attributes.
*/
function generateRandomPoints(center, radius, count) {
    let points = [];
    for (let i=0; i<count; i++) {
      points.push(generateRandomPoint(center, radius));
    }
    return points;
  }
  
  
  /**
  * Generates number of random geolocation points given a center and a radius.
  * Reference URL: http://goo.gl/KWcPE.
  * @param  {Object} center A JS object with lat and lng attributes.
  * @param  {number} radius Radius in meters.
  * @return {Object} The generated random points as JS object with lat and lng attributes.
  */
  function generateRandomPoint(center, radius) {
    let x0 = center.lng;
    let y0 = center.lat;
    // Convert Radius from meters (by miles) to degrees.
    let rd = radius * 1.60934 * 1000/111300;
  
    let u = Math.random();
    let v = Math.random();
  
    let w = rd * Math.sqrt(u);
    let t = 2 * Math.PI * v;
    let x = w * Math.cos(t);
    let y = w * Math.sin(t);
  
    let xp = x/Math.cos(y0);
  
    // Resulting point.
    return {'lat': y+y0, 'lng': xp+x0};
  }
  
  
  // Usage Example.
  // Generates 100 points that is in a 1 mile radius from the given lat and lng point.
  let randomGeoPoints = generateRandomPoints({'lat':24.23, 'lng':23.12}, 1, 100);

  module.exports = {
    generateRandomPoint,
    generateRandomPoints
  }