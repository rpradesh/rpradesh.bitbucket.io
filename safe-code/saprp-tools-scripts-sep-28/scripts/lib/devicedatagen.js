const faker = require('faker')
const commonDatagen = require('./commonDatagen')
const geoutil = require('./geoutil')
const geohash = require('ngeohash');

const commodity = 'WATER';
const caryLatLong = { 'lat': 35.79154, 'lng': -78.7811169 };

const generate = async (argv) => {
    commonDatagen.generate(argv, getDataForOneDevice);
}

function getDataForOneDevice(index) {
    let records = [];
    let latLong = geoutil.generateRandomPoint(caryLatLong, 30);

    let record = {
        _id: { deviceNumber: index, meterType: commodity, deviceType: 'METER' },
        account: {
            _id: faker.random.number({ min: 1, max: 1000, precision: 1 }),
            name: 'acct' + faker.random.alphaNumeric(6),
            bs: 1,
            s: 'A',
            st: 'commercial',
            address: {
                line1: faker.address.streetAddress().replace(',', ''),
                zip: faker.address.zipCode(),
                city: faker.address.city().replace(',', ''),
                state: faker.address.state(),
                lat: latLong.lat.toFixed(5),
                lon: latLong.lng.toFixed(5),
                geohash: geohash.encode(latLong.lat, latLong.lng)
            }
        },
        amrIdentifier: faker.random.number({ min: 10000000, max: 99999999, precision: 1 }),
        meterManufacturer: '900GM',
        serviceRoute: '1',
        sdp: {
            _id: index,
            address: {
                line1: faker.address.streetAddress().replace(',', ''),
                zip: faker.address.zipCode(),
                city: faker.address.city().replace(',', ''),
                state: faker.address.state(),
                lat: latLong.lat.toFixed(5),
                lon: latLong.lng.toFixed(5),
                geohash: geohash.encode(latLong.lat, latLong.lng)
            }
        },
        status: 'ACTIVE',
        address: {
            line1: faker.address.streetAddress().replace(',', ''),
            zip: faker.address.zipCode(),
            city: faker.address.city().replace(',', ''),
            state: faker.address.state(),
            lat: latLong.lat.toFixed(5),
            lon: latLong.lng.toFixed(5),
            geohash: geohash.encode(latLong.lat, latLong.lng)
        }
    }

    records.push(record);

    return records;
}

module.exports = {
    generate
}