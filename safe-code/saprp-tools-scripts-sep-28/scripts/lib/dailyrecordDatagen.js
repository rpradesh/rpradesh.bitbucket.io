const faker = require('faker')
const commonDatagen = require('./commonDatagen')
let numberOfDays = 30;

const generate = async (argv) => {
    commonDatagen.generate(argv, getDataForOneDevice);
    numberOfDays = argv.numberOfDays;
}

function getDataForOneDevice(index) {
    let records = [];
    for (i = 0; i < numberOfDays; i++) {
        let d = new Date();
        d.setDate(d.getDate() - i);
        let record = {
            _id: index + ';' + index + ';' + d.getFullYear() + d.getMonth() + d.getDate(),
            device: index,
            radio: index,
            commodity: 'WATER',
            date: d.toISOString(),
            lastUpdate: d.toISOString(),
            registers: []
        }

        for (j = 0; j < 24; j++) {
            d.setHours(j);
            record.registers.push({
                dS: 'gen',
                eT: d.toISOString(),
                iT: d.toISOString(),
                iL: 0, // what is this?
                mul: 1.0,
                rQ: 'R',
                m: 'CCF',
                v: faker.random.number({ min: j * 5, max: (j * 5) + 5, precision: 1 })
            });
        }

        records.push(record);
    }

    return records;
}

module.exports = {
    generate
}