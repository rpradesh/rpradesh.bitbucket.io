/* eslint-disable no-console */
const { argv } = require('yargs');

const common = require('./lib/common');
const mongo = require('./lib/mongo');
const sitedatagen = require('./lib/sitedatagen');
const deviceCsvDatagen = require('./lib/deviceCsvDatagen');
const dailyrecorddatagen = require('./lib/dailyrecordDatagen');
const deviceDatagen = require('./lib/devicedatagen');
const prpReadsDatagen = require('./lib/prp_readsDatagen');
const prpDeviceDataDatagen = require('./lib/prp_devicedataDatagen');
const prpAlarmsDatagen = require('./lib/prp_alarmsDatagen');
const dataLayerGen = require('./lib/data-layer-gen');

switch (argv.cmd) {
  case 'generateLayerData':
    if (!argv.records || !argv.startindex || !argv.filepath) {
      common.errorMsgAndReturn(
        "--records=10 --startindex=1000000 --filepath='../test-data/data-layer.csv",
        'arguments are required'
      );
    }

    dataLayerGen.generate(argv);
    break;
  case 'generateDeviceCsvData':
    if (!argv.records || !argv.startindex || !argv.filepath) {
      common.errorMsgAndReturn(
        "--records=10 --startindex=1000000 --filepath='../test-data/10-devicedata.csv",
        'arguments are required'
      );
    }

    deviceCsvDatagen.generate(argv);
    break;

  case 'generateSiteData':
    if (!argv.records || !argv.startindex || !argv.filepath) {
      common.errorMsgAndReturn(
        "--records=10 --startindex=1000000 --filepath='../test-data/10-sitedata.csv",
        'arguments are required'
      );
    }

    sitedatagen.generate(argv);
    break;

  case 'generateDailyRecordData':
    if (!argv.records || !argv.startindex || !argv.filepath) {
      common.errorMsgAndReturn(
        "--records=10 --startindex=1000000 --filepath='../gen-data/dailyrecord.json",
        'arguments are required'
      );
    }

    dailyrecorddatagen.generate(argv);
    break;

  case 'generateDeviceData':
    if (!argv.records || !argv.startindex || !argv.filepath) {
      common.errorMsgAndReturn(
        "--records=10 --startindex=1000000 --filepath='../gen-data/device.json",
        'arguments are required'
      );
    }

    deviceDatagen.generate(argv);
    break;

  case 'generateReadData':
    if (!argv.records || !argv.startindex || !argv.filepath) {
      common.errorMsgAndReturn(
        "--records=10 --startindex=1000000 --filepath='../gen-data/prp_reads.json",
        'arguments are required'
      );
    }

    prpReadsDatagen.generate(argv);
    break;

  case 'generateDeviceDataData':
    if (!argv.records || !argv.startindex || !argv.filepath) {
      common.errorMsgAndReturn(
        "--records=10 --startindex=1000000 --filepath='../gen-data/prp_devicedata.json",
        'arguments are required'
      );
    }

    prpDeviceDataDatagen.generate(argv);
    break;

  case 'generateAlarmData':
    if (!argv.records || !argv.startindex || !argv.filepath) {
      common.errorMsgAndReturn(
        "--records=10 --startindex=1000000 --filepath='../gen-data/prp_alarms.json",
        'arguments are required'
      );
    }

    prpAlarmsDatagen.generate(argv);
    break;

  case 'checkMongoConnection':
    mongo.checkMongoConnection();
    break;

  case 'getCollectionCount':
    if (!argv.db || !argv.collection) {
      common.errorMsgAndReturn('--db=someDbName --collection=someCollection', 'arguments are required');
    }

    mongo.getCollectionCount();
    break;

  case 'clearCollection':
    if (!argv.db || !argv.collection) {
      common.errorMsgAndReturn('--db=verdeeco_central_db --collection=utility', 'arguments are required');
    }

    mongo.clearCollection();
    break;

  case 'displayCollection':
    if (!argv.db || !argv.collection) {
      common.errorMsgAndReturn('--db=verdeeco_central_db --collection=utility', 'arguments are required');
    }

    mongo.displayCollection();
    break;

  case 'loadIntoCollection':
    if (!argv.db || !argv.collection || !argv.jsonFile) {
      common.errorMsgAndReturn(
        '--db=verdeeco_central_db --collection=utility --jsonFile=seed-utility.json',
        'arguments are required'
      );
    }

    mongo.loadIntoCollection();
    break;

  default:
    console.log('Usage: please look at scripts/DevReadmd.md');
}
