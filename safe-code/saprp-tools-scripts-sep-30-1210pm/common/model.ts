/* eslint-disable @typescript-eslint/camelcase */
export enum DataGenTypeEnum {
  prp_site = 'prp_site',
  prp_device = 'prp_device',
  daily_record = 'daily_record',
  device = 'device',
  prp_alarm = 'prp_alarm',
  prp_devicedata = 'prp_devicedata',
  prp_reads = 'prp_reads'
}

export const mongoConnectionOpts = {
  useUnifiedTopology: true,
  useNewUrlParser: true
};

export class MongoArgs {
  public mongoUrl = '';
  public utility?: string;
  public col?: string;
  public file = '';
  public pkfield = '_id';
  public recordCount = 10;
  public notify = false;

  upsertCount = 0;
  insertCount = 0;

  get collectionName(): string {
    return '' + this.col;
  }
  get displayCollectionName(): string {
    return '' + this.dbName + '.' + this.collectionName;
  }

  get dbName() {
    return this.utility ? `verdeeco_${this.utility}_db` : '';
  }
}

export class DatagenArgs extends MongoArgs {
  public readonly type: string;
  public setEmptyForNotRequiredFields = false;
  public startIndex = 1000 * 1000;
  public size = 10;
  public json = true;
  public notify = false;
  public doDirectPublish = false;
  public days = 30;
  public doEmpty = false;

  constructor(type: string) {
    super();
    this.type = type;
  }
}
