import {Command, flags} from '@oclif/command';
import {DatagenArgs, DataGenTypeEnum} from '../common';
import * as utils from '../utils';

const GenTypesArray = utils.enumValueAsArray(DataGenTypeEnum);

export const flagsMap = new Map([
  [
    'help',
    {
      flagType: 'help',
      char: 'h'
    }
  ],
  [
    'type',
    {
      flagType: 'string',
      char: 't',
      description: `The model of collection: ${GenTypesArray.join(',')}`,
      required: true,
      options: GenTypesArray
    }
  ],
  [
    'startIndex',
    {
      flagType: 'integer',
      char: 'i',
      description: 'start index, used for _id while generating data',
      default: 1000000
    }
  ],
  [
    'recordCount',
    {
      flagType: 'integer',
      char: 'n',
      description: 'number of records to be generated',
      default: 10
    }
  ],
  [
    'days',
    {
      flagType: 'integer',
      char: 'd',
      description: 'number of days, used by some generator',
      default: 30
    }
  ],
  [
    'output',
    {
      flagType: 'string',
      char: 'o',
      description: 'The format of output: [json, csv]',
      default: 'json',
      options: ['json', 'csv']
    }
  ],
  [
    'empty',
    {
      flagType: 'boolean',
      char: 'e',
      description: 'Empty Collection before Ingest, based on argument',
      default: false
    }
  ],
  [
    'notify',
    {
      flagType: 'boolean',
      char: 'z',
      description: 'pass true to display desktop notifier, when completed',
      default: false
    }
  ],
  /*********  Mongo Flags  **************/
  [
    'mongos',
    {
      flagType: 'string',
      char: 'm',
      description: 'mongo url to connect or export MONGO_URL',
      default: process.env.MONGO_URL,
      required: true
    }
  ],
  [
    'utility',
    {
      flagType: 'string',
      char: 'u',
      description: 'name of utility, example central or nicor to deduce db name',
      required: true,
      dependsOn: ['mongos']
    }
  ],
  [
    'collection',
    {
      flagType: 'string',
      char: 'c',
      description: 'name of collection in given db',
      required: true,
      dependsOn: ['mongos', 'utility']
    }
  ],
  [
    'file',
    {
      flagType: 'string',
      char: 'f',
      description: 'path of json file',
      required: true
    }
  ],
  [
    'pkfield',
    {
      flagType: 'string',
      char: 'p',
      description: 'PrimaryKey field Name to be used duringLoadIntoMongo',
      default: '_id'
    }
  ]
]);

export function allFlags(names: string[]): any {
  let retFlags = {};
  names.forEach(name => {
    if (name === 'help') retFlags = Object.assign(retFlags, {help: flags.help({char: 'h'})});
    else if (name === 'mongos')
      retFlags = Object.assign(retFlags, {
        mongos: flags.string({
          char: 'm',
          description: 'mongo url to connect or export MONGO_URL',
          default: process.env.MONGO_URL,
          required: true
        })
      });
    else if (name === 'utility')
      retFlags = Object.assign(retFlags, {
        utility: flags.string({
          char: 'u',
          description: 'name of utility, example central or nicor to deduce db name',
          required: true,
          dependsOn: ['mongos']
        })
      });
    else if (name === 'collection')
      retFlags = Object.assign(retFlags, {
        collection: flags.string({
          char: 'c',
          description: 'name of collection in given db',
          required: true,
          dependsOn: ['mongos', 'utility']
        })
      });
    else if (name === 'count')
      retFlags = Object.assign(retFlags, {
        count: flags.integer({
          char: 'n',
          description: 'name of collection in given db',
          default: 5,
          dependsOn: ['mongos', 'utility', 'collection']
        })
      });
    else if (name === 'file')
      retFlags = Object.assign(retFlags, {
        file: flags.string({
          char: 'f',
          description: 'path of json file',
          required: true
        })
      });
    else if (name === 'pkfield')
      retFlags = Object.assign(retFlags, {
        pkfield: flags.string({
          char: 'p',
          description: 'PrimaryKey field Name to be used duringLoadIntoMongo',
          default: '_id'
        })
      });
  });
  return retFlags;
}

export function mongoFlagsOld(names: string[]): any {
  let retFlags = {};
  names.forEach(name => {
    if (name === 'help') retFlags = Object.assign(retFlags, {help: flags.help({char: 'h'})});
    else if (name === 'mongos')
      retFlags = Object.assign(retFlags, {
        mongos: flags.string({
          char: 'm',
          description: 'mongo url to connect or export MONGO_URL',
          default: process.env.MONGO_URL,
          required: true
        })
      });
    else if (name === 'utility')
      retFlags = Object.assign(retFlags, {
        utility: flags.string({
          char: 'u',
          description: 'name of utility, example central or nicor to deduce db name',
          required: true,
          dependsOn: ['mongos']
        })
      });
    else if (name === 'collection')
      retFlags = Object.assign(retFlags, {
        collection: flags.string({
          char: 'c',
          description: 'name of collection in given db',
          required: true,
          dependsOn: ['mongos', 'utility']
        })
      });
    else if (name === 'count')
      retFlags = Object.assign(retFlags, {
        count: flags.integer({
          char: 'n',
          description: 'name of collection in given db',
          default: 5,
          dependsOn: ['mongos', 'utility', 'collection']
        })
      });
    else if (name === 'file')
      retFlags = Object.assign(retFlags, {
        file: flags.string({
          char: 'f',
          description: 'path of json file',
          required: true
        })
      });
    else if (name === 'pkfield')
      retFlags = Object.assign(retFlags, {
        pkfield: flags.string({
          char: 'p',
          description: 'PrimaryKey field Name to be used duringLoadIntoMongo',
          default: '_id'
        })
      });
  });
  return retFlags;
}
