/* eslint-disable no-console */
import * as L from 'lodash';
import {MongoArgs, DatagenArgs, mongoConnectionOpts, DataGenTypeEnum} from './model';
import {allFlags} from './flag-options';
import {flagsMap} from './flag-options';
import {generateRandomPoint} from './geoutil';
import {flags} from '@oclif/command';

export const debug = process.env.DEBUG === 'true';
const genFlagsMap = new Map();

export async function sleepSecs(secs: number) {
  await new Promise(done => setTimeout(done, secs * 1000));
}

function updateMongoFlags(flagMap: any, mongoArgs: any): any {
  mongoArgs.mongoUrl = flagMap.mongos;
  mongoArgs.utility = flagMap.utility;
  mongoArgs.col = flagMap.collection;
  mongoArgs.recordCount = flagMap.count;
  mongoArgs.file = flagMap.file;
  mongoArgs.notify = flagMap.notify === true;

  return mongoArgs;
}

export function buildDatagenModel(flagMap: any): DatagenArgs {
  let datagenArgs = new DatagenArgs(flagMap.type);
  datagenArgs.file = flagMap.file;
  datagenArgs.size = flagMap.recordCount;
  datagenArgs.startIndex = flagMap.startIndex;
  datagenArgs.json = flagMap.output === 'json';
  datagenArgs.notify = flagMap.notify === true;

  // update extra for DataGen from MongoGen too
  datagenArgs = updateMongoFlags(flagMap, datagenArgs);

  return datagenArgs;
}

export function buildMongoModel(flagMap: any): MongoArgs {
  let mongoArgs = new MongoArgs();
  mongoArgs = updateMongoFlags(flagMap, mongoArgs);

  console.log(`mongoArgs=`, mongoArgs);

  return mongoArgs;
}

function buildFlagsMap() {
  if (genFlagsMap.size > 0) return;
  // build flag map
  for (const [k, fEntry] of flagsMap) {
    const flagType = L.get(fEntry, 'flagType');
    const newFlagEntry = L.omit(fEntry, 'flagType');
    /*
    const strFuntion = flags.string;
    const newFlagEntryObject = strFuntion.apply(null, newFlagEntry as any);
    */
    let newFlagEntryWithType = null;

    switch (flagType) {
      case 'string':
        newFlagEntryWithType = flags.string(newFlagEntry as any);
        break;
      case 'help':
        newFlagEntryWithType = flags.help(newFlagEntry as any);
        break;
    }

    if (newFlagEntryWithType) {
      genFlagsMap.set(k, {
        [k]: newFlagEntryWithType
      });
    }
  } // end-for
}

export function getAllFlags(names: string[]): any {
  buildFlagsMap();
  let retFlags = {};
  names.forEach(name => {
    retFlags = Object.assign(retFlags, genFlagsMap.get(name));
  });

  return retFlags;
}

export {
  /* Model */
  MongoArgs,
  DatagenArgs,
  mongoConnectionOpts,
  DataGenTypeEnum,
  /* Options */
  allFlags,
  /* functions */
  generateRandomPoint
};
