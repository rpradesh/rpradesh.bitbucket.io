/* eslint-disable no-console */
import {MongoClient} from 'mongodb';
import {MongoArgs, mongoConnectionOpts, debug} from '../common';
import {loadIntoCollection} from './mongo-load-util';
import * as utils from '../utils';
import * as L from 'lodash';
import cli from 'cli-ux';

export async function getConnection(mongos: string): Promise<MongoClient> {
  const mongoClient: MongoClient = await MongoClient.connect(mongos, mongoConnectionOpts);
  return mongoClient;
}

export async function closeConnection(mongoClient: MongoClient): Promise<void> {
  if (mongoClient) await mongoClient.close();
}

export async function checkConnection(mongoArgs: MongoArgs): Promise<void> {
  await utils.sleepSecs(1);
  const mongoClient: MongoClient = await getConnection(mongoArgs.mongoUrl);
  await closeConnection(mongoClient);
}

export async function getCollectionCount(mongoArgs: MongoArgs): Promise<number> {
  const client: MongoClient = await getConnection(mongoArgs.mongoUrl);
  const count = await client
    .db(mongoArgs.dbName)
    .collection(mongoArgs.collectionName)
    .countDocuments();
  await closeConnection(client);

  return count;
}

export async function clearCollection(mongoArgs: MongoArgs): Promise<number> {
  const client: MongoClient = await getConnection(mongoArgs.mongoUrl);
  const ops = await client
    .db(mongoArgs.dbName)
    .collection(mongoArgs.collectionName)
    .deleteMany({});
  await closeConnection(client);

  return ops.deletedCount ? Number(ops.deletedCount) : 0;
}

function buildColumns(record: any): any {
  let columns: any = {};

  Object.keys(record).forEach((key, idx) => {
    if (debug) console.log('key', key);
    columns = Object.assign(columns, {[key]: {minWidth: 10}});
  });

  return columns;
}

export async function displayCollection(mongoArgs: MongoArgs): Promise<void> {
  const client: MongoClient = await getConnection(mongoArgs.mongoUrl);
  const cursor = await client
    .db(mongoArgs.dbName)
    .collection(mongoArgs.collectionName)
    .aggregate([{$sort: {_id: -1}}, {$limit: mongoArgs.recordCount}]);

  const dataArray: any[] = [];

  let index = 0;
  while (await cursor.hasNext()) {
    const rec = await cursor.next();
    dataArray.push(rec);
    if (debug) console.log(`rec[${index}] = ${JSON.stringify(rec)}`);
    index++;
  }
  await closeConnection(client);

  if (L.isEmpty(dataArray)) return;

  const columns: any = buildColumns(dataArray[0]);
  cli.table(dataArray, columns);
}

export {loadIntoCollection};
