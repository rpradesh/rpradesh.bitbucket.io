import {BaseDatagen} from './base-datagen';
import * as faker from 'faker';
import * as geohash from 'ngeohash';
import * as geoutil from '../common/geoutil';

const commodity = 'WATER';
const caryLatLong = {lat: 35.79154, lng: -78.7811169};
const deviceTypes = ['METER', 'ALLY_METER', 'SGW'];

export class Datagen extends BaseDatagen {
  appendNewRecordsForIndex(idx: number): void {
    const latLong = geoutil.generateRandomPoint(caryLatLong, 30);
    const index = this.datagenArgs.startIndex + idx;

    const record = {
      /* 0	, true   */ recordInActive: '0',
      /* 1	, true   */ deviceId: `deviceId${index}`,
      /* 2	, true   */ meterType: 'WATER',
      /* 3	, true   */ deviceType: faker.random.arrayElement(deviceTypes),
      /* 4	, true   */ siteId: `sid${index}`,
      /* 5	, false  */ deviceName: `deviceName ${index} ${faker.random.alphaNumeric(10)}`,
      /* 6	, false  */ dma: `dma-${faker.random.alphaNumeric(10)}`,
      /* 7	, false  */ lat: latLong.lat.toFixed(5),
      /* 8	, false  */ lng: latLong.lng.toFixed(5),
      /* 9	, false  */ pressureZone: `prZone-${faker.random.alphaNumeric(10)}`,
      /* 10	, false  */ elevation: faker.random.number({min: 50, max: 999, precision: 3}),
      /* 11	, false  */ geoHash: geohash.encode(latLong.lat, latLong.lng),
      /* 12	, false  */ elevationOffset: faker.random.number({min: 5, max: 50, precision: 2}),
      address: {
        /* 13	, false  */ line1: faker.address.streetAddress().replace(/,/g, '|'),
        /* 14	, false  */ line2: faker.address.secondaryAddress().replace(/,/g, '|'),
        /* 15	, false  */ city: faker.address.city().replace(/,/g, '|'),
        /* 16	, false  */ state: faker.address.state(),
        /* 17	, false  */ zip: faker.address.zipCode()
      },
      /* 18	, false  */ analogLabel1: faker.random.arrayElement(['ang_AA', 'ang_BB']),
      /* 19	, false  */ analogLabel2: faker.random.arrayElement(['ang_AA', 'ang_BB']),
      /* 20	, false  */ digLowLabel1: faker.random.arrayElement(['DG_low_AA', 'DG_high_BB']),
      /* 21	, false  */ digLowLabel2: faker.random.arrayElement(['DG_low_AA', 'DG_high_BB']),
      /* 22	, false  */ digHighLabel1: faker.random.arrayElement(['dg_HGH_AA', 'dg_HGH_BB']),
      /* 23	, false  */ digHighLabel2: faker.random.arrayElement(['dg_HGH_AA', 'dg_HGH_BB'])
    };

    this.records.push(record);
  }

  getJsonPathToGenerateCsv(): string[] {
    return [
      /* 0	, true   */ 'recordInActive',
      /* 1	, true   */ 'deviceId',
      /* 2	, true   */ 'meterType',
      /* 3	, true   */ 'deviceType',
      /* 4	, true   */ 'siteId',
      /* 5	, false  */ 'deviceName',
      /* 6	, false  */ 'dma',
      /* 7	, false  */ 'lat',
      /* 8	, false  */ 'lng',
      /* 9	, false  */ 'pressureZone',
      /* 10	, false  */ 'elevation',
      /* 11	, false  */ 'geoHash',
      /* 12	, false  */ 'elevationOffset',
      /* 13	, false  */ 'address.line1',
      /* 14	, false  */ 'address.line2',
      /* 15	, false  */ 'address.city',
      /* 16	, false  */ 'address.state',
      /* 17	, false  */ 'address.zip',
      /* 18	, false  */ 'analogLabel1',
      /* 19	, false  */ 'analogLabel2',
      /* 20	, false  */ 'digLowLabel1',
      /* 21	, false  */ 'digLowLabel2',
      /* 22	, false  */ 'digHighLabel1',
      /* 23	, false  */ 'digHighLabel2'
    ];
  }
}
