import {DatagenArgs, MongoArgs, DataGenTypeEnum} from '../common';
import {BaseDatagen} from '../datagen/base-datagen';

// add new datagen files below
import {Datagen as DatagenPrpSite} from './datagen-prpsite';
import {Datagen as DatagenPrpDevice} from './datagen-prpdevice';
import {Datagen as DatagenDailyRecord} from './datagen-daily-record';
import {Datagen as DatagenJustDevice} from './datagen-just-device';
import {Datagen as DatagenSensorRead} from './datagen-sensor-read';
import {Datagen as DatagenPrpDeviceData} from './datagen-prp-devicedata';
import {Datagen as DatagenPrpAlarm} from './datagen-prp-alarm';

export function getDatagenInstance(datagenArgs: DatagenArgs): BaseDatagen {
  if (DataGenTypeEnum.prp_site == datagenArgs.type) return new DatagenPrpSite(datagenArgs);
  else if (DataGenTypeEnum.prp_device == datagenArgs.type) return new DatagenPrpDevice(datagenArgs);
  else if (DataGenTypeEnum.daily_record == datagenArgs.type) return new DatagenDailyRecord(datagenArgs);
  else if (DataGenTypeEnum.device == datagenArgs.type) return new DatagenJustDevice(datagenArgs);
  else if (DataGenTypeEnum.prp_reads == datagenArgs.type) return new DatagenSensorRead(datagenArgs);
  else if (DataGenTypeEnum.prp_devicedata == datagenArgs.type) return new DatagenPrpDeviceData(datagenArgs);
  else if (DataGenTypeEnum.prp_alarm == datagenArgs.type) return new DatagenPrpAlarm(datagenArgs);
  else throw new Error(`Unknwon datagen type value=[${datagenArgs.type}]`);
}

export function doGenerateOLD(datagenArgs: DatagenArgs): void {
  if (DataGenTypeEnum.prp_site == datagenArgs.type) new DatagenPrpSite(datagenArgs).generate();
  else if (DataGenTypeEnum.prp_device == datagenArgs.type) new DatagenPrpDevice(datagenArgs).generate();
  else if (DataGenTypeEnum.daily_record == datagenArgs.type) new DatagenDailyRecord(datagenArgs).generate();
  else if (DataGenTypeEnum.device == datagenArgs.type) new DatagenJustDevice(datagenArgs).generate();
  else if (DataGenTypeEnum.prp_reads == datagenArgs.type) new DatagenSensorRead(datagenArgs).generate();
  else if (DataGenTypeEnum.prp_devicedata == datagenArgs.type) new DatagenPrpDeviceData(datagenArgs).generate();
  else if (DataGenTypeEnum.prp_alarm == datagenArgs.type) new DatagenPrpAlarm(datagenArgs).generate();
  else throw new Error(`Unknwon datagen type value=[${datagenArgs.type}]`);
}

export function doGenerate(datagenArgs: DatagenArgs): void {
  const dgInstance = getDatagenInstance(datagenArgs);
  dgInstance.generate();
}

export async function doPersistIntoMongo(datagenArgs: DatagenArgs): Promise<DatagenArgs> {
  const dgInstance = getDatagenInstance(datagenArgs);
  return await dgInstance.persistIntoMongo();
}
