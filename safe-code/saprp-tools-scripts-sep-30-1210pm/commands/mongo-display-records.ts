/* eslint-disable no-console */
import Command from '../common/base';
import * as mu from '../mongo';
import {allFlags, buildMongoModel} from '../common';

export default class Mongo extends Command {
  static description = 'Mongo: Disaply Records in given Collection';
  static args = [{name: 'mongo-display-records'}];
  static flags = {...allFlags(['help', 'mongos', 'utility', 'collection', 'count'])};

  static examples = [
    `$ saprp mongo-display-records -u prp1 -c prp_site 
    or default display size is 10, can overwrite using -n 20
    `
  ];

  async run() {
    const {args, flags} = this.parse(Mongo);
    const mongoArgs = buildMongoModel(flags);
    await mu.displayCollection(mongoArgs);
  } //end-run
}
