#!/bin/bash

set -euo pipefail
IFS=$'\n\t'

set -x

skip_new_mongo_start=true

if [ $(uname) = "Darwin" ]; then
  if ! [[ -x $(command -v ggrep) ]] || ! [[ -x $(command -v greadlink) ]]; then
    echo "Install coreutils and GNU grep with \`brew install coreutils grep\`"
    exit 1
  fi
  shopt -s expand_aliases # https://unix.stackexchange.com/a/158040
  alias readlink='greadlink'
  alias grep='ggrep'
fi

script_file_path=$(readlink -f ${BASH_SOURCE[0]})
script_directory_path=$(dirname ${script_file_path})
project_artifact_id=$(basename $script_directory_path)

stop_script="$script_directory_path/stop_action_guide_local.sh"
config_file_path="$script_directory_path/config-action-guide/configuration.json"

pushd $script_directory_path

if [ -f "$stop_script" ]
then
    eval $stop_script || true
    rm $stop_script
fi

action_guide_local_working_path="$script_directory_path/action_guide_local_work"
rm -rf "$action_guide_local_working_path"
mkdir "$action_guide_local_working_path"

mongod_db_path=/tmp/mongod_ag
rm -rf $mongod_db_path
mkdir $mongod_db_path

if [[ "x$skip_new_mongo_start" == "xfalse" ]]; then
  mongod --dbpath=$mongod_db_path > $action_guide_local_working_path/mongod.log 2>&1 &
  mongod_pid=$!
  echo "mongod pid=$mongod_pid"
  sleep 5 
fi

# collect pgid by using sleep 
sleep 5 2>&1 &
sleep_pid=$!

if [[ "${OS:-Linux}" =~ ^Win ]]
then
    pgid=$(ps -p $sleep_pid | grep -v 'PGID' | awk '{print $3}')
else
    pgid=$(ps -p $sleep_pid -o pgid | grep -v PGID | awk '{print $1}')
fi

# RNI service userid:  agTestUser
# RNI service password:  agTestPassword
mongo verdeeco_central_db --eval 'db.utility.update({name: "mayberry"},{_id: ObjectId("598c88c495f96bbb6c6d4c3b"), name:"mayberry",description:"Mayberry, NC",db:"mayberry_db",applications:["Action Guide"],homeUrl: "http://localhost:4200","rni_rest_service":{"service_base_url":"https://localhost:9999/rest-service","userid":"DLMhjs1jlE2xhW3Em1tU+g==","password":"rEPPbl8wcVxnCWQz28t5HQ=="}, timezone:"America/New_York"},{upsert:true})'
mongo verdeeco_central_db --eval 'db.user.update({email: "anonymous@mayberry.com"},{email: "anonymous@mayberry.com",first_name: "anony",last_name: "mous"},{upsert:true})'
mongo mayberry_db --eval 'db.ag_config.drop()'
mongo mayberry_db --eval 'db.ag_config.insert({_t: "com.sensus.analytics.actionguide.data.common.dto.configuration.UtilityConfiguration", supervisory_transmit_rate: 5, average_uiaf_completion_hours: 45, batch_flash_size: 1000, created: new Date(), modified: new Date(), modified_by: "dbuser"})'

java -Dglobal.configuration=$config_file_path -jar service-action-guide/target/service-action-guide-*-jar-with-dependencies.jar > $action_guide_local_working_path/service-action-guide.log 2>&1 &
service_pid=$!
echo "service-action-guide pid=$service_pid"

java -Dglobal.configuration=$config_file_path -jar tools-action-guide/testdata-action-guide/target/testdata-action-guide-*-jar-with-dependencies.jar > $action_guide_local_working_path/testdata-action-guide.log 2>&1 &
testdata_pid=$!
echo "testdata-action-guide pid=$testdata_pid"

java -Dglobal.configuration=$config_file_path -jar tools-action-guide/test-drive-action-guide/target/test-drive-action-guide-*-jar-with-dependencies.jar > $action_guide_local_working_path/test-drive-action-guide.log 2>&1 &
test_drive_pid=$!
echo "test-drive-action-guide pid=$test_drive_pid"

sleep 3 # give test drive time to update central.ag_config before launching the coordinator

java -Dglobal.configuration=$config_file_path -jar coordinator-action-guide/target/coordinator-action-guide-*-jar-with-dependencies.jar > $action_guide_local_working_path/coordinator-action-guide.log 2>&1 &
coordinator_pid=$!
echo "coordinator-action-guide pid=$coordinator_pid"

cd ./webapp-action-guide/
npm start > $action_guide_local_working_path/webapp-action-guide.log 2>&1 &
node_pid=$!
echo "node_pid=$node_pid"

echo "#!/bin/bash" > $stop_script
echo "set -euo pipefail" >> $stop_script
echo "IFS=$'\n\t'" >> $stop_script
echo "set -x" >> $stop_script
if [[ "${OS:-Linux}" =~ ^Win ]]
then
	echo 'node_windows_pid=$(ps -p '"$node_pid"' | grep -v "WINPID" | awk '"'"'BEGIN { FS=" " } { print $4 }'"'"')' >> $stop_script
	echo 'env taskkill -T -PID $node_windows_pid -F' >> $stop_script
fi
echo "kill -9 -- -$pgid || true" >> $stop_script
echo "sleep 2" >> $stop_script
echo "rm -rf $mongod_db_path" >> $stop_script
echo "" >> $stop_script
chmod 755 $stop_script

popd
