const testFolder = './';
const fs = require('fs');
const path = require('path');

const staticHtml = `

<h3>IO Pages</h3>

<li> <a href=""> </a> </li>

`

const lines = [];
const fout = fs.createWriteStream('index.html');
const html = 1; // 0
function scroll(prefixPath, dir) {
fs.readdir(dir, {withFileTypes:true}, (err, files) => {
  let dirName = '';
  files.forEach(file => {    
    if (file.isDirectory()){
      dirName = path.join(dir, file.name);
      if (dirName !== '') {
        if (html == 1) fout.write(`<h3>${dirName}</h3>`)
        else console.log(`### ${dirName}`)
      }  
      scroll(prefixPath, dirName)
    }  else {
      let fileName = file.name;
      //console.log(`dirName=${dirName} dir=${dir} fileName=${fileName}`);
      let newPrefix = fileName.endsWith('md') ? `${prefixPath}/${dir}/${fileName}`: `${dir}/${fileName}`;

      if (html == 1) fout.write(`<li> <a href="${newPrefix}"> ${fileName.replace(/_|-/gi, ' ')} </a> </li> `)
      else console.log(`* [${fileName.replace(/_|-/gi, ' ')}](${dir}/${fileName}) `)
    }
  });
});

}

function callNow(){
scroll('https://bitbucket.org/rpradesh/rpradesh.bitbucket.io/src/master', 'design')
//console.log('lines 1=', lines.join('\n'))
}

callNow();

 