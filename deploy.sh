#!/usr/bin/env bash

# copy wms docs into project folder
# PRJ_FROM_DIR=/home/rpradesh/projects/bitbucket/rpradesh/fork-sa-wms-prp

# old commented on Sep 13
# PRJ_FROM_DIR=/home/rpradesh/projects/bitbucket/master/sa-wms-prp
# PRJ_TO_DIR=/home/rpradesh/projects/bitbucket/rpradesh/rpradesh.bitbucket.io/sa-docs

PRJ_FROM_DIR=/Users/rajeshpradeshik/projects/bb-master/sa-wms-prp
PRJ_TO_DIR=/Users/rajeshpradeshik/projects/bb-rpradesh/rpradesh.bitbucket.io/sa-docs

# -----
cp -R ${PRJ_FROM_DIR}/docs ${PRJ_TO_DIR}/sa-wms-prp
# open /sa-docs/sa-wms-prp/coverage/lcov-report/index.html
cp -R ${PRJ_FROM_DIR}/prp-service/coverage ${PRJ_TO_DIR}/sa-wms-prp
# open /sa-docs/sa-wms-prp/spec-report/spec.html
cp -R ${PRJ_FROM_DIR}/prp-service/spec-report ${PRJ_TO_DIR}/sa-wms-prp

node ./populateIndex.js

git add .

git commit -am "Commit at `date +'%Y-%m-%d %H:%M:%S'` "

git push