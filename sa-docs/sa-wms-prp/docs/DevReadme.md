## Dev Scripts and Utilities

### Node install/update - one time
* Install latest Node 10.x LTS as follows
    - To install nvm follow [here(https://gist.github.com/d2s/372b5943bce17b964a79)
    - Note: when there is .nvmrc file present in the folder, it can be used to init the node version by executing "nvm use"
    - If Issue installing npm using nvm tool in *windows* follow [here](https://gist.github.com/rajeshpv/1e5ad771532e272ae058a2e228dd4666)
```bash
nvm install 10.16.0
nvm use 10.16.0
#check the install
node -v
npm -v
# if want to make it default
nvm alias default 10.16.0

# required global packages/bin
npm install -g npm@latest 
npm install -g @angular/cli
# used for file watch and launcher - dev friendly
npm install --global nodemon
# used for rm -rf the node way  
npm install --global rimraf 
# used to execute postman tests 
npm install --global newman 
# used to execute test report like sonar 
npm install --global nyc tape tap-nyc riteway
```

### Eslint and Prettier
* Install "eslint and prettier" VSCODE marketplace plugins
* The .vscode /settings.json as setings to format and change on saveoptions

### Build using maven
* following does npm clean+install+compile+testWithCoverage
```bash
cd prp-service
mvn clean install
```

### To clean project build/fetch dependencies
```bash
cd prp-service
npm run clean
# notice node_modules,dist,coverage and log folders are gone
npm install
# notice node_modules is with dependencies now
```

### To launch node app in *Dev Watch* mode
* Browse Node app at http://localhost:3000/health
* in logger.ts remove meta, if required
```bash
cd prp-service
npm run dev:watch
# notice ./dist appears which has compiled ts into js
```

### To run test 
```bash
# runs all tests and generate coverage and report it 
npm run test
# open Generated code-coverage page in browser at
# ./prp-service/coverage/lcov-report/index.html

# to run tests in watch mode, execute as follows
npm run test:watch
# problem is it executes all tests instead of recent most edited

# to watch single test ts file, you can execute as follows
nodemon --watch src --ext ts --exec nyc riteway src/test/utils/index.test.ts
```

#### Execute Application api
* Install HttPIE [from](https://httpie.org/doc#examples)
* Execute api calls and validate as follows:
```bash
# http request/error logs
http -jb http://localhost:3000/test/testHttpRequestLog
tail -f log/prp-http-access.log

http -jb http://localhost:3000/test/testHttpErrorLog
tail -f log/prp-http-error.log

# node application log
http -jb http://localhost:3000/test/testApplicationLog a==1 b==2
http -b http://localhost:3000/test/testApplicationErrorLog
tail -f log/prp-service.log
```

### To build - Mongo scripts and utils
* Has Node based command line utilities for Mongo, etc.,
```bash
cd prp-tools/scripts
npm install
```

### To launch/use - Mongo scripts and utils
* Default Mongo url "mongodb://localhost:27017/" can be overriden by export of "MONGO_URL"
* Return code is 0 if sucessfull or -1 if any error
```bash
node cmd.js --cmd=checkMongoConnection 
# returns 0 if running, else -1

node cmd.js --cmd=getCollectionCount --db=verdeeco_central_db --collection=utility
# display count as "count=2" 

node cmd.js --cmd=clearCollection   --db=verdeeco_central_db --collection=utility
# display deleted count as "deletedCount=2"

node cmd.js --cmd=loadIntoCollection --db=verdeeco_central_db --collection=utility    --jsonFile=../seed-dev/utility.json
node cmd.js --cmd=loadIntoCollection --db=verdeeco_central_db --collection=prp_config --jsonFile=../seed-dev/prp-config.json
# display upsert/insert count as "upsertCount=2 insertCount=0" 
# Note: if record has "_id" field it uses to do "upsert" else it inserts it

node cmd.js --cmd=displayCollection   --db=verdeeco_central_db --collection=prp_config
# displays records
```
