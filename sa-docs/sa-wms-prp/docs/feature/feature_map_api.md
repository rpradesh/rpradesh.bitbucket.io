# Map Data

- Following doc describes the feature of create/access "Map" resource using REST API.

## API - Map Data

- Checkout the Rest API for "Map" resource at "/api/v1/map" http://localhost:3000/api-docs

## Generate and Ingest - Map Data

- Since Data for Map api involves mnay-collections, the following steps can be used to init/reset the data
  - prp_site
  - prp_devicedata
  - prp_reads
  
```bash
cd prp-tools/scripts
# one time
npm install

# generate data for required collections
node cmd.js --cmd=generateSiteData --records=10 --startindex=1000000 --filepath='../gen-data/10-sitedata.csv'

node cmd.js --cmd=generateDailyRecordData --records=10 --startindex=1000000 --filepath='../gen-data/dailyrecord.json' --numberOfDays=30 --fileType='json'
node cmd.js --cmd=generateDeviceData --records=10 --startindex=1000000 --filepath='../gen-data/device.json' --fileType='json'
node cmd.js --cmd=generateReadData --records=10 --startindex=1000000 --filepath='../gen-data/prp_reads.json' --fileType='json'
node cmd.js --cmd=generateAlarmData --records=10 --startindex=1000000 --filepath='../gen-data/prp_alarms.json' --fileType='json'
node cmd.js --cmd=generateDeviceDataData --records=10 --startindex=1000000 --filepath='../gen-data/prp_devicedata.json' --fileType='json'


# Clear collections
node cmd.js --cmd=clearCollection    --db=verdeeco_prp1_db    --collection=prp_site
node cmd.js --cmd=clearCollection    --db=verdeeco_prp1_db    --collection=daily_record
node cmd.js --cmd=clearCollection    --db=verdeeco_prp1_db    --collection=device
node cmd.js --cmd=clearCollection    --db=verdeeco_prp1_db    --collection=prp_reads
node cmd.js --cmd=clearCollection    --db=verdeeco_prp1_db    --collection=prp_alarms
node cmd.js --cmd=clearCollection    --db=verdeeco_prp1_db    --collection=prp_devicedata

# Insert into collections
# ingest Site data using csv
http -bf POST :3000/api/v1/site/ingest upload@../gen-data/10-sitedata.csv

node cmd.js --cmd=loadIntoCollection --db=verdeeco_prp1_db    --collection=daily_record    --jsonFile=../gen-data/dailyrecord.json #300
node cmd.js --cmd=loadIntoCollection --db=verdeeco_prp1_db    --collection=device          --jsonFile=../gen-data/device.json  #10
node cmd.js --cmd=loadIntoCollection --db=verdeeco_prp1_db    --collection=prp_reads       --jsonFile=../gen-data/prp_reads.json #600
node cmd.js --cmd=loadIntoCollection --db=verdeeco_prp1_db    --collection=prp_alarms      --jsonFile=../gen-data/prp_alarms.json #300
node cmd.js --cmd=loadIntoCollection --db=verdeeco_prp1_db    --collection=prp_devicedata  --jsonFile=../gen-data/prp_devicedata.json #10

# Check inserted data count
CON_OPTS=' localhost:27017/verdeeco_prp1_db --quiet --eval '
mongo $CON_OPTS 'db.prp_site.countDocuments({})'
mongo $CON_OPTS 'db.daily_record.countDocuments({})'
mongo $CON_OPTS 'db.device.countDocuments({})'
mongo $CON_OPTS 'db.prp_reads.countDocuments({})'
mongo $CON_OPTS 'db.prp_alarms.countDocuments({})'
mongo $CON_OPTS 'db.prp_devicedata.countDocuments({})'

http -jb GET :3000/api/v1/map/filter
```

## API - Map Data

- Use http tool for API testing and mongo shell for query db

```bash
cd sa-wms-prp

# run following query, for endDate=2019-09-22, with subtract of 2 hours
CON_OPTS=' localhost:27017/verdeeco_prp1_db --quiet --eval '

db.prp_reads.aggregate([
      {$unwind: '$reads'},
        {$match: {'reads.readDate': {$gte: ISODate("2019-09-21T22:00:00Z")}}},
        {$match: {'reads.readDate': {$lte: ISODate("2019-09-22T00:00:00Z")}}},
      {$sort: {device: 1, date: -1, 'reads.readDate': -1}},
      {
        $project: {_id: 0, device: 1, date: 1, sensorId: 1, 'reads.readDate': 1, 'reads.value': 1}
      }
    ]);

http -jb GET :3000/api/v1/map/filter singleSlice==true endDate==2019-09-22 > ../gen-data/endDate_9_22.json

```

