# Site Data

- Following doc describes the feature of create/access "Site" resource using REST API.

## API - Site Data

- Checkout the Rest API for "Site" resource at "/api/v1/site" http://localhost:3000/api-docs

## Generate - Site Data

### Profiling:

- for: 2 Million records, size 400MB (takes 1 min to generate)
- for: 100k records, size 18MB
- for: 7k records, size 1MB

* Make sure "saprp" tool is already installed

```bash
cd prp-tools
# check saprp tool
saprp -v

# to generate site upload data in csv format
colName=prp_site
saprp datagen -t $colName -f gen-data/${colName}_100.csv --output csv --recordcount=100 --startindex=2000000

cat gen-data/${colName}_100.csv
cat gen-data/${colName}_100.csv | wc -l
```

## Ingest - Site Data

- Use http tool for API testing and mongo shell for query db

```bash
CON_OPTS=' localhost:27017/verdeeco_test1_db --quiet --eval '
mongo $CON_OPTS 'db.prp_site.countDocuments({})'
mongo $CON_OPTS 'db.prp_site.deleteMany({})'
mongo $CON_OPTS 'db.prp_site.find({}).projection({}).sort({_id:-1}).limit(20)'

# check upload folder anytime to validate file exits
cd prp-tools
ls -al gen-data/*.csv

http -bf POST :3000/api/v1/site/ingest upload@gen-data/prp_site_100.csv
# above returns a tracking TxnId, usign that make a GET
http -bf GET :3000/api/v1/process/7f2d1010-42cd-11ea-b5ba-bd1794e7b60f

http -jb GET :3000/api/v1/site/sid2000001

http --download :3000/api/v1/site/download/7f2d1010-42cd-11ea-b5ba-bd1794e7b60f/source
http --download :3000/api/v1/site/download/7f2d1010-42cd-11ea-b5ba-bd1794e7b60f/result

# Note: use "==" to append name+value to Query URL
# https://httpie.org/doc#request-items

# all 10
http -jb GET :3000/api/v1/site/filter | jq '. | length'

http -jb GET :3000/api/v1/site/filter siteId==sid2000001
http -jb GET :3000/api/v1/site/filter siteId==sid2000001 siteId==sid2000003

# count =3
http -jb GET :3000/api/v1/site/filter siteType==PRV
# count =6
http -jb GET :3000/api/v1/site/filter siteType==PRV siteType==Valve

# count =3
http -jb GET :3000/api/v1/site/filter pressureZone==prZone-low
# count =6
http -jb GET :3000/api/v1/site/filter pressureZone==prZone-low pressureZone==prZone-high

# with all 3 params, count =1
http -jb GET :3000/api/v1/site/filter siteId==sid1000001 siteType==PRV pressureZone==prZone-low
```

### Sample Data Ingest - profiling

- from local office network to mongo QA in AWS
- file 1000k-sitedata.csv of size 164M took 7 mins
- file 100k-sitedata.csv of size 17M took 45 secs
- file 10k-sitedata.csv of size 1.7M took 3 sec
