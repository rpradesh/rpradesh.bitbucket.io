# Process Data

- Following doc describes the feature of access "Process/Task" resource using REST API.

## API - Process/Task Data

- Checkout the Rest API for "Map" resource at "/api/v1/process" http://localhost:3000/api-docs

## Ingest - Process Data

- Since Create Data is done by execution of an process, you can manually insert as follows
  
```text
db.prp_process.insert({
	"processId" : 'progress_id_1001',
	"startedAt" : new Date(),
	"completedAt" : new Date(),
	"resultJson" :  {
		"recordInActive" : 0,
		"siteId" : "sid4440005"
	},
	"errorDetail" : "",
	"processName" : "SITE",
	"processAction" : "CSV_INGEST",
	"status" : "IN_PROGRESS"
});

db.prp_process.insert({
	"processId" : 'success_id_1002',
	"startedAt" : new Date(),
	"completedAt" : new Date(),
	"resultJson" :  {
		"recordInActive" : 0,
		"siteId" : "deviceId4440005"
	},
	"errorDetail" : "",
	"processName" : "DEVICE",
	"processAction" : "CSV_INGEST",
	"status" : "SUCCESS"
});
```

## API - Process Data

- Use http tool for API testing and mongo shell for query db

```bash
cd sa-wms-prp

# run following query, for endDate=2019-09-22, with subtract of 2 hours
CON_OPTS=' localhost:27017/verdeeco_prp1_db --quiet --eval '

http -jb GET :3000/api/v1/process/filter processAction==CSV_INGEST
http -jb GET :3000/api/v1/process/filter processId==progress_id_1001 processName==SITE   processAction==CSV_INGEST status=IN_PROGRESS
http -jb GET :3000/api/v1/process/filter processId==success_id_1002     processName==DEVICE processAction==CSV_INGEST status=SUCCESS
http -jb GET :3000/api/v1/process/filter processName==DEVICE 
http -jb GET :3000/api/v1/process/filter processId==success_id_1002 

http -jb GET :3000/api/v1/process/success_id_1002
http -j GET :3000/api/v1/process/xxxx_1

http -jb GET :3000/api/v1/process/progress_id_1001
http -jb GET :3000/api/v1/process/progress_id_xxxxx

# search by processId array
http -jb GET :3000/api/v1/process/filter processId==progress_id_1001 processId==success_id_1002 | jp [].processId
# search by status array
http -jb GET :3000/api/v1/process/filter status==IN_PROGRESS status==SUCCESS | jp [].processId
# search by processAction 
http -jb GET :3000/api/v1/process/filter processAction==CSV_INGEST | jp [].processId
# search by processName 
http -jb GET :3000/api/v1/process/filter processName==SITE processName==DEVICE | jp [].processId

## Validation of Search Filter Arguments


# if we search by wrong enum/const value it should give 400
http -j GET :3000/api/v1/process/filter status==XX_IN_PROGRESS
# search by processAction 
http -j GET :3000/api/v1/process/filter processAction==XX_CSV_INGEST
# search by processName 
http -j GET :3000/api/v1/process/filter processName==XX_SITE

```

