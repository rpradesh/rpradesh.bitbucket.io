# TestLogger
* Following doc describes the feature of testing request+application with trace logging using REST API.

## API - TestLogger 
* Checkout the Rest API for "Site" resource at "/test" http://localhost:3000/api-docs

## Ingest - TestLogger 
* Use http tool for API testing 

### Execute Application api
* Install HttPIE [from](https://httpie.org/doc#examples)
* Execute api calls and validate as follows:

```bash
# http request/error logs
http -jb http://localhost:3000/test/testHttpRequestLog
tail -f log/prp-http-access.log

http -jb http://localhost:3000/test/testHttpErrorLog
tail -f log/prp-http-error.log

# node application log
http -jb http://localhost:3000/test/testApplicationLog a==1 b==2
http -b http://localhost:3000/test/testApplicationErrorLog
tail -f log/prp-service.log
```
