# Health Check API
* Using http://localhost:3000/health REST API.

## API - HealthCheck
* Use http tool for API testing 

```bash
http -jb GET :3000/health
```

* The response contains the Process details and Memory Metrics, as follows

```javascript
{
  "name": "PRP Service",
  "version": "1.0.2",
  "serverTS": "2019-06-28T05:44:39.788Z",
  "processMetrics": {
    "processTitle": "node_prp",
    "processPID": 20601,
    "rss": "1.69 MB",
    "heapTotal": "1.05 MB",
    "heapUsed": "0.96 MB",
    "external": "0.17 MB"
  }
}
```

