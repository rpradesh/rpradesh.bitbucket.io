# TestUser Data
* Following doc describes the feature of create/access "Site" resource using REST API.

## API - TestUser Data 
* Checkout the Rest API for "Site" resource at "/api/v1/site" http://localhost:3000/api-docs

## Ingest - TestUser Data
* Use http tool for API testing and mongo shell for query db

```bash
mongo localhost:27017/verdeeco_central_db --quiet --eval 'db.test_users.find({})'


http -jbf POST :3000/test/users name=raj2002 email==raj2002@example.com
# should get 400 'already taken' for same email
http -jbf POST :3000/test/users name=raj2002 email==raj2002@example.com

http -jb GET :3000/test/users

http -jb GET :3000/test/users/5d056fd06fb22f0e174a8223

http -jb DELETE :3000/test/users/5d056fd06fb22f0e174a8223
# should get 404 'not found' fo rsecond time delete
http -jb DELETE :3000/test/users/5d056fd06fb22f0e174a8223
```
