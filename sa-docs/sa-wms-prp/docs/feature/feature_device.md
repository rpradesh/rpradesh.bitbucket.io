# Device Data

- Following doc describes the feature of create/access "Device" resource using REST API.

## API - Device Data

- Checkout the Rest API for "Device" resource at "/api/v1/device" http://localhost:3000/api-docs

## Generate - Device Data

- Note: setEmptyForNotRequiredFields is optional,
- if set to true, all not-required fields in csv will not be generated
- Profiling:
  - for: 2 Million records, size 400MB (takes 1 min to generate)
  - for: 100k records, size 18MB
  - for: 7k records, size 1MB

```bash
cd prp-tools/scripts
# one time
npm install

# to generate device upload data in csv format
node cmd.js --cmd=generateDeviceCsvData --records=10 --startindex=1000000 --filepath='../test-data/10-devicedata.csv'

# to generate device data with empty data for nullable fields
node cmd.js --cmd=generateDeviceCsvData --records=20 --startindex=2000000 --filepath='../test-data/20-devicedata-with-null.csv' --setEmptyForNotRequiredFields=true
```

## Ingest - Device Data

- Use http tool for API testing and mongo shell for query db

```bash
cd sa-wms-prp

CON_OPTS=' localhost:27017/verdeeco_test1_db --quiet --eval '

# check upload folder anytime to validate file exits
ls -al /tmp/prp-site-upload
ls -al /tmp/prp-device-upload

mongo $CON_OPTS 'db.prp_device.countDocuments({})'
mongo $CON_OPTS 'db.prp_device.deleteMany({})'
mongo $CON_OPTS 'db.prp_device.find({}).projection({}).sort({_id:-1}).limit(10)'

http -bf POST :3000/api/v1/device/ingest upload@prp-tools/test-data/10-devicedata.csv
http -bf POST :3000/api/v1/device/ingest upload@prp-tools/test-data/5-invalid-devicedata.csv

http -jb GET :3000/api/v1/device/deviceId1000009
http -jb GET :3000/api/v1/device/deviceId2000000

http --download :3000/api/v1/device/download/6e74b4fee454ea01fd7621c0c1f8b33e/source
http --download :3000/api/v1/device/download/6e74b4fee454ea01fd7621c0c1f8b33e/result

# check for upload file type/size error
http -f POST :3000/api/v1/device/ingest upload@/home/rpradesh/data/50k-devicedata.csv
http -f POST :3000/api/v1/device/ingest upload@/home/rpradesh/data/some-textfile.txt
```

### Sample Data Ingest - profiling

- from local office network to mongo QA in AWS
- file 1000k-devicedata.csv of size 164M took 7 mins
- file 100k-devicedata.csv of size 17M took 45 secs
- file 10k-devicedata.csv of size 1.7M took 3 sec
