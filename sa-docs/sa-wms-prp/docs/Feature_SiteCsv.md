## Site Data API
* Following doc describes the feature of create/access "Site" resource using REST API.

### Implemented API
* Ingest Site Data - POST /api/v1/site/upload 
* Get Site record by ID - GET /api/v1/site/{:siteId}
* Get/download source csv file - GET /api/v1/site/download/{:fileId}/source
* Get/download result csv file - GET /api/v1/site/download/{:fileId}/result - contains source data with validation msgs
* Csv Upload should validate for file type CSV and Size with an error message 

### Csv Data Generator
* Note: setEmptyForNotRequiredFields is optional, 
* if set to true, all not-required fields in csv will not be generated
* Profiling:
    - for: 2 Million records, size 400MB (takes 1 min to generate)
    - for: 100k records, size 18MB
    - for: 7k  records, size 1MB
    
```bash
cd prp-tools/scripts
# one time
npm install

# to generate site upload data in csv format
node cmd.js --cmd=generateSiteData --records=10 --startindex=1000000 --filepath='../test-data/10-sitedata.csv'

# to generate site data with empty data for nullable fields
node cmd.js --cmd=generateSiteData --records=20 --startindex=2000000 --filepath='../test-data/20-sitedata-with-null.csv' --setEmptyForNotRequiredFields=true
```

## Ingest Site Data
* Using httpie cmd line 

### TestUser API - Testing
* Use http tool for API testing and mongo shell for query db
```bash
CON_OPTS=' localhost:27017/verdeeco_test1_db --quiet --eval '

mongo $CON_OPTS 'db.prp_site.countDocuments({})'
mongo $CON_OPTS 'db.prp_site.deleteMany({})'
mongo $CON_OPTS 'db.prp_site.find({}).projection({}).sort({_id:-1}).limit(20)'

http -bf POST :3000/api/v1/site/upload upload@prp-tools/test-data/10-sitedata.csv
http -bf POST :3000/api/v1/site/upload upload@prp-tools/test-data/5-invalid-sitedata.csv

http -jb GET :3000/api/v1/site/sid1000009
http -jb GET :3000/api/v1/site/sid2000001

http --download :3000/api/v1/site/download/ac8ce539b235b3f1242e3d7fe3e023c7/source
http --download :3000/api/v1/site/download/082c88ba1a717ac16bef7984996b86ea/result

# check for upload file type/size error
http -f POST :3000/api/v1/site/upload upload@/home/rpradesh/data/50k-sitedata.csv
http -f POST :3000/api/v1/site/upload upload@/home/rpradesh/data/some-textfile.txt

# Note: use "==" to append name+value to Query URL 
# https://httpie.org/doc#request-items
http -jb GET :3000/api/v1/site/filter siteId==1234  siteType==one siteType==two pressureZone==low

```

#### Sample Data Ingest - profiling 
* from local office network to mongo QA in AWS
* file 1000k-sitedata.csv  of size  164M  took 7 mins
* file 100k-sitedata.csv   of size   17M  took 45 secs
* file 10k-sitedata.csv    of size  1.7M  took 3 sec
