# Generate - OpenApi/Swagger doc
* The generated swagger,json and yml files will be published in sa-wms-prp/prp-service/swagger.json
* The live OpenApi swagger instance can be used at http://127.0.0.1:3000/api-docs/
* http://localhost:3000/api-docs  - with schema types - Show the swagger UI to allow interaction with the swagger file
* http://localhost:3000/api-docs/json  - Return the swagger.json file
* http://localhost:3000/api-docs/yaml  - Return the swagger.yaml file

```bash
cd sa-wms-prp/prp-tools/swagger-gen
npm run swagger
# or
npm run swagger:watch
```

## To Open swagger.json in Swagger Editor
* goto https://editor.swagger.io/
* File -> Import from file, choose 'sa-wms-prp/prp-service/swagger.json`
* You can generate client app, choose "Generate Client" and "Typescript Angular"
* It generates a zip file with api and model typescript files in it, all good to go