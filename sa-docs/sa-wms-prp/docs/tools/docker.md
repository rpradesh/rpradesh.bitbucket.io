# Docker - Images and Containers for PRP
* The process to launch application in production is by means of building docker images and getting them launched in kubernates service in AWS, through NgInx load balancer/proxy.
* The CI/CD build process generates the docker images using the `Dockerfile` present in respective modules.

## Xylem Docker Repository
* To pull docker images from xylem docker repo, Check your account at https://xylem.jfrog.io/xylem/webapp/#/login 
* Check for available images by browsing repo at https://xylem.jfrog.io/xylem/webapp/#/artifacts/browse/tree/General/xylem-docker-base 
* The available repositories are :
  - xylem-sa-docker-repo.jfrog.io
  - xylem-sa-docker-prod-releases.jfrog.io
  - xylem-xylem-docker-base.jfrog.io

* Create auth entries in `~/.docker/config.json` by logging into docker repo as follows

```bash
docker login xylem-xylem-docker-base.jfrog.io --username rpradeshik
# check auth entry using
cat ~/.docker/config.json

# pull an image and test it out
docker pull xylem-xylem-docker-base.jfrog.io/xylem:10-nodejs
docker tag xylem-xylem-docker-base.jfrog.io/xylem:10-nodejs xylem:10-nodejs

# launch container and check node version installed
docker run -it --rm xylem:10-nodejs node -v
```

## MongoDB Image
* The public image of mongodb pushed to public Docker hub in "rajeshpv"  repository 
* Which has versions of 3.2.22 or 4.0.11, and can be pulled and launched as follows

### MongoDB Image - Pull and Launch instance

```bash
cd ~
docker pull rajeshpv/mongodb:3.2.22
# one-time
docker run --name=localmongo32 -p 27017:27017 -d rajeshpv/mongodb:3.2.22

# start/stop
docker stop localmongo32
docker start localmongo32

# if want to mount local volumes
mkdir -p ~/data/mongodb-32/db
mkdir -p ~/data/mongodb-32/log
docker run --name=localmongo32 -v ~/data/mongodb-32/db:/data/db -v ~/data/mongodb-32/log:/var/log/mongodb -p 27017:27017 -d rajeshpv/mongodb:3.2.22
# you can check container log
docker logs localmongo32
```

## PRP Webapp Image
* Make sure you have access to xylem docker repo as described above  
  
```bash
cd ../prp-webapp
ng build --prod
docker build -t local/prp-webapp .
# test launch the container
docker run -it --rm -p 4280:80 --name prpWebapp  local/prp-webapp:latest
# open browser at http://localhost:4280
```

## PRP Service Image

```bash
cd ../prp-service
# run clean package 
npm run-script clean && npm install && npm run-script compile && npm run-script package
# launch and test server.js, before building image
node dist/server.js

# build docker image locally
docker build -t local/prp-service .
# test the new image by pointing to local Mongo
docker run -it --rm -p 3080:80 --name prpService \
    -e "NODE_ENV=production" -e "MONGODB_HOST=`hostname`" -e "MONGODB_PORT=27017" -e "MONGODB_USERNAME=" -e "MONGODB_PASSWORD=" \
    local/prp-service:latest
# check ports used and test app
netstat -vatn| grep LISTEN | grep 80
curl http://localhost:3080/health

# to run in daemon mode remove --rm and add -d 
# check logs of running container
docker logs prpService
```