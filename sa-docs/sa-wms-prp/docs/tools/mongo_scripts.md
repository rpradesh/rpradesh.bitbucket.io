## Build - Mongo scripts and utils
* Has Node based command line utilities for Mongo, etc.,

```bash
cd prp-tools/scripts
npm install
```

## Launch - Mongo scripts and utils
* Default Mongo url "mongodb://localhost:27017/" can be overriden by export of "MONGO_URL"
* Return code is 0 if sucessfull or -1 if any error

```bash
cd prp-tools/scripts

node cmd.js --cmd=checkMongoConnection 
# returns 0 if running, else -1

node cmd.js --cmd=getCollectionCount --db=verdeeco_central_db --collection=utility
# display count as "count=2" 

node cmd.js --cmd=clearCollection   --db=verdeeco_central_db --collection=utility
# display deleted count as "deletedCount=2"

node cmd.js --cmd=loadIntoCollection --db=verdeeco_central_db --collection=utility    --jsonFile=../seed-data/dev/utility.json
node cmd.js --cmd=loadIntoCollection --db=verdeeco_central_db --collection=prp_config --jsonFile=../seed-data/dev/prp-config.json
# display upsert/insert count as "upsertCount=2 insertCount=0" 
# Note: if record has "_id" field it uses to do "upsert" else it inserts it

node cmd.js --cmd=displayCollection   --db=verdeeco_central_db --collection=prp_config
# displays records
```
