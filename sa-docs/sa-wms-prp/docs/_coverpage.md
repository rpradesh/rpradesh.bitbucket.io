![logo](_media/icon.svg)

# WMS - PressureProfile Application <small>version 1.0.0</small>

> Application Components
* Introduction
* Frontend (Angular UI)
* Backend (Application tier - Node with Typescript)
* Database (Mongo)
* Integration (RNI)

[BitBucket Code Repo](https://bitbucket.org/sensusanalytics-dev/sa-wms-prp/src/master/)
[Get Started](README.md)

