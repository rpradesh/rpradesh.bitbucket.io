- Application Envs/Instances
    - [prp-localhost-ui](http://localhost:4200/) [prp-localhost-node](http://localhost:3000/health)  [prp-localhost-swagger](http://localhost:3000/api-docs)
    - [prp-dev](https://daz1e1-utilityportal.dev1-sensus-analytics.com/)
    - [prp-qa](https://daz1e1-utilityportal.dev1-sensus-analytics.com/)
    - [prp-prod](https://daz1e1-utilityportal.dev1-sensus-analytics.com/)

- External Links
    - [SA wiki](https://xyleminc.atlassian.net/wiki/spaces/SA/pages/193560637/Water+Management+System+Pressure+Profile)
    - [RNI Prp Data Definitions](https://xyleminc.atlassian.net/wiki/spaces/SA/pages/202014721/Reading+definition)
