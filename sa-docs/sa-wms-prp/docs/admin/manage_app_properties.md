# Manage Application Properties
* At application start following steps are executed:
  * Load of `Default` properties from `prp-service/src/config/defaultProperties.json`
  * `Merge` of properties from central db `prp_properties` collection to application properties.
  * For manual update of `seed-data` of dev/qa/prod `prp_properties` is stored in following location `prp-tools/seed-data/dev/prp-properties.json`

## Application Properties - List
* To confirm `prp_properties` collection will be the cached properties

```bash
# notice '.device.csvUploadSizeLimitMB' shows value (A)
cat ../prp-service/src/config/defaultProperties.json | jq '.site'
# notice '.device.csvUploadSizeLimitMB' shows value (B)
cat ../prp-tools/seed-data/dev/prp-properties.json | jq '.[] | .site'
# which is same in DB if uploaded by db script
CON_OPTS=' localhost:27017/verdeeco_central_db --quiet --eval '
mongo $CON_OPTS 'db.prp_properties.find({ _id: "site" })'

# running API to get application cache properties we notice value (B):
# Merge of prp_properties into defaultProperties.json
http -jb GET :3000/api/v1/admin/properties
http -jb GET :3000/api/v1/admin/properties | jq '.site'
```

## Application Properties - Reload
```bash
# update db-manually for property '.device.csvUploadSizeLimitMB' as value (C)
CON_OPTS=' localhost:27017/verdeeco_central_db --quiet --eval '
mongo $CON_OPTS 'db.prp_properties.update({ _id: "site" }, { $set: { "site.csvUploadSizeLimitMB": 1024 } })'

# notice doing a GET after db update won't refresh in application cache
http -jb GET :3000/api/v1/admin/properties | jq '.site'

# running POST API will refresh the application cache
http -jb POST :3000/api/v1/admin/properties/refresh
# You can confirm by doing a GET now and should see value of 1024 Bytes
http -jb GET :3000/api/v1/admin/properties | jq '.site'
```