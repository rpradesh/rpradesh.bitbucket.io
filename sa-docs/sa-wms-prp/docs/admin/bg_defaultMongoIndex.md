# Create Mongo "Default" Indexes - Background Task

## Task execution process:
* The `DefaultUtilityIndex` task is implemented to execute "defined" `prp-service/src/config/mongoIndexDefinitions.json` mongo indexes for single/ALL utilities in a background task.
* By design, only `single` instance of "DefaultMongoIndex" can be in a running state, trigger of second instance will return an error.
* The job is triggered by "/api/v1/admin/task/`{DefaultUtilityIndex}`/`{utility}`/start" where
  -  DefaultUtilityIndex is the name of task
  -  utility be value of 'ALL' or 'nicor' for single utility
  -  The `parallelExecutionUtilityCount` property of this Job can be set in `prp_properties` collection manually, which controls how many of same "Create" Index statements can be executed simultaneously for "different" utilities in parallel.
* Note: 
 - The Task ends after all indexes are created.
 - And error of single index creation does not break the whole task Job. The prp-service.log should log the details of error if any.
    
## Task Config - Set Properties
* DefaultUtilityIndex uses "parallelExecutionUtilityCount" property which can be modified at run time before trigger of task as follows:
  
```bash
# if required, update parallel Execution Count property in db-manually 
CON_OPTS=' localhost:27017/verdeeco_central_db --quiet --eval '
mongo $CON_OPTS 'db.prp_properties.update({ _id: "defaultUtilityIndexTaskConfig" }, { $set: { "defaultUtilityIndexTaskConfig.parallelExecutionUtilityCount": 2 } } , {upsert:true})'

# Check property is available in app
http -jb GET :3000/api/v1/admin/properties | jq '.defaultUtilityIndexTaskConfig'
# if not refresh
http -jb POST :3000/api/v1/admin/properties/refresh
# After refresh, the updated property is available in app, it will show value of 2
http -jb GET :3000/api/v1/admin/properties | jq '.defaultUtilityIndexTaskConfig'

```

## Start Task
```bash
# start task using Start API, notice taskName in url 'DefaultUtilityIndex' which is case sensitive
http -jb POST :3000/api/v1/admin/task/DefaultUtilityIndex/ALL/start
http -jb POST :3000/api/v1/admin/task/DefaultUtilityIndex/prp1/start

# Check log to show Parent and Child Process
tail -f ../prp-service/log/prp-service.log | grep taskPid
# list all node_prp Process with its started time
watch -n 5 'ps -eo pid,lstart,cmd | grep node_prp | grep -v watch | grep -v grep'

# list the source of default indexes
cat ../prp-service/src/config/mongoIndexDefinitions.json | jq '.[] | .options.name'

# after job is completed list indexes in 'prp1' db
http -jb GET :3000/api/v1/admin/mongo/index/prp1/summary | grep 'name'
```

## Get Task Status
```bash
# start task using Start API, notice taskName in url 'Sleep' which is case sensitive
http -jb POST :3000/api/v1/admin/task/DefaultUtilityIndex/ALL/start
http -jb POST :3000/api/v1/admin/task/DefaultUtilityIndex/prp1/start

# GET status API should return array of 1 task details as "Running"=true
http -jb GET :3000/api/v1/admin/task/DefaultUtilityIndex/status
```

## Kill Task
```bash
# start task using Start API, say for 60 secs
http -jb POST :3000/api/v1/admin/task/DefaultUtilityIndex/ALL/start
http -jb POST :3000/api/v1/admin/task/DefaultUtilityIndex/prp1/start

# GET status API should return array of 1 task details as "Running"=true
http -jb GET :3000/api/v1/admin/task/DefaultUtilityIndex/status

# run the kill status API within 60 secs
http -jb DELETE :3000/api/v1/admin/task/DefaultUtilityIndex/kill
# wil show the task Pid's getting killed
```
