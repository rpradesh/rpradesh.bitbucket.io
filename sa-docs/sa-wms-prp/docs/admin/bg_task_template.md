# Background Task - Design / Template

## Preface 
* Following describes the design and api to execute or implement new task as background child process.
* Following example: `prp-service/src/common/task/sleepTask.ts` has all the steps necassary to create a "run" function be executed as background process.
* Note: "Worker Threads" is still an experimental feature in Node 12.x+ , Hence for any resource intensive or background task execution the "Child Process" built-in library is used.
* The `start` of Task (trigger) and `stop` (abrupt end) of Task and `view` of status of Task can be managed by following 3 api:
  - POST ​/api​/v1​/admin​/task​/{taskName}​/{utility}​/start
  - GET ​/api​/v1​/admin​/task​/{taskName}​/status
  - DELETE ​/api​/v1​/admin​/task​/{taskName}​/kill
* Where `taskName` can be any value of "Sleep/DefaultUtilityIndex/SingleUtilityIndex", which maps to the functonality of respective task implementation.

## Example Task - Sleep
* Below api's describes how managment of a basic task like "Sleep" is implemented by means of 3 api's
  - POST ​/api​/v1​/admin​/task​/`Sleep​`/{utility}​/start
  - GET ​/api​/v1​/admin​/task​/`Sleep​​`/status
  - DELETE ​/api​/v1​/admin​/task​/`Sleep​`/kill
  
## Task Config - Set Properties
* Every task is backed by its own "Config" property in `defaultProperties.json` and values can be updated at runtime manually in `prp_properties` collection with an "post" of properties api to reload and merge the application properties cache.
* In below step we are modify the sleep duration from 10 secs (in defaultProperties.json)  to 16 secs in (prp_properties) collection and executing a reload.
  
```bash
# update the sleep duration property in db-manually 
CON_OPTS=' localhost:27017/verdeeco_central_db --quiet --eval '
mongo $CON_OPTS 'db.prp_properties.update({ _id: "sleepTaskConfig" }, { $set: { "sleepTaskConfig.sleepSecs": 16 } } , {upsert:true})'

# Check property is available in app, it will show value of 10
http -jb GET :3000/api/v1/admin/properties | jq '.sleepTaskConfig'
# if not refresh
http -jb POST :3000/api/v1/admin/properties/refresh
# After refresh, the updated property is available in app, it will show value of 16
http -jb GET :3000/api/v1/admin/properties | jq '.sleepTaskConfig'

```

## Start Task API
* The following "Post" api will start the Sleep task in background, basically it is doing is "sleep for 16 secs" an important task !!
* We should be able to monitor new process with name "node_prp" appear/disappear for life of Sleep task.
  
```bash
# start task using Start API, notice taskName in url 'Sleep' which is case sensitive
http -jb POST :3000/api/v1/admin/task/Sleep/prp1/start

# Check log to show Parent and Child Process
tail -f ../prp-service/log/prp-service.log | grep taskPid
# list all node_prp Process with its started time, 
watch -n 5 'ps -eo pid,lstart,cmd | grep node_prp | grep -v watch | grep -v grep'
```

## Get Task Status API
* The following "GET" api will show the details of "Running" task in progress.
* It means, the below "GET" api when executed in window of 16 secs will show details of Task.
  
```bash
# start task using Start API, notice taskName in url 'Sleep' which is case sensitive
http -jb POST :3000/api/v1/admin/task/Sleep/prp1/start

# GET status API should return array of 1 task details as "Running"=true
http -jb GET :3000/api/v1/admin/task/Sleep/status
```

## Kill Task API
* Using the following "DELETE" api, the live "Running" task can be ended (process kill)
* Meaning when "kill" of task is triggered before end of task, the api will kill the child-process and display the child PIDs being killed.
  
```bash
# start task using Start API, say for 60 secs
http -jb POST :3000/api/v1/admin/task/Sleep/prp1/start

# GET status API should return array of 1 task details as "Running"=true
http -jb GET :3000/api/v1/admin/task/Sleep/status

# run the kill status API within 60 secs
http -jb DELETE :3000/api/v1/admin/task/Sleep/kill
# wil show the task Pid's getting killed
```
