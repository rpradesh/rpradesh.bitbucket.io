# Mongo Utility API

## Mongo Indexes - Get Summary API

```bash
# Creating index manually
CON_OPTS=' localhost:27017/verdeeco_prp1_db --quiet --eval '
mongo $CON_OPTS 'db.prp_site.dropIndexes()' 
mongo $CON_OPTS "db.prp_site.createIndex({ siteId: 1 }, { name: 'prpsite_siteidA_unique', unique: true });"

mongo $CON_OPTS 'db.prp_site.getIndexes()'

# Confirming all index's are returned in summary - api, for "prp1" utility
http -jb GET :3000/api/v1/admin/mongo/index/prp1/summary | grep 'prp'
```
