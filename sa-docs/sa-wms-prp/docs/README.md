# Preface  
* WMS-PressureProfile Application provides analytics/visualization/alerts for non-volume related measures like Pressure/Temperature/Turbidity of water flow/management.
* The application uses customer submitted source data augmented with real-time hourly/daily data from downstream application like RNI.

# Feature Highlights - planned
* Upload seed/source Site and Device data by means of UI/FTP process
* View and analyze pressure data with inseractive dashboards, tables,graphs and maps.
* Create and act on groups
* Configure alarm threasholds and notifications - sms, email
* Initiate on-demand pressure read (how long does it take to get response from device)
* Adjust lat/long - on all supporeted devices
* Adjust elevation/elevation offset - Ally and SGW
* Obtain geographic data from external systems - GIS/Esri
* Generate Pressure related reports
* Obtain asset information from external systems - SCADACon
* Initiate valvle shutoff/control
