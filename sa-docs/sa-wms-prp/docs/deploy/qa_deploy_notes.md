# Mongo scripts and utils - QA
* Has Node based command line utilities for Mongo, etc.,

```bash
cd prp-tools/scripts
npm install
MONGO_URL="mongodb://daz1e1-umongo0-mongos0.dev-dc1.sensus-sa.net:27017:27017/"
# check connection
node scripts/cmd.js --cmd=checkMongoConnection 

# todo june-10-2019
node cmd.js --cmd=loadIntoCollection --db=verdeeco_central_db --collection=prp_config --jsonFile=../seed-data/qa/prp-config.json
```
