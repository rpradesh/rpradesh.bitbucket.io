# Launch Application - Developer mode

- To check available npm cli commands refer to https://docs.npmjs.com/misc/scripts

## Checkout branch

```bash
cd ~/projects
# git clone from https://bitbucket.org/sensusanalytics-dev/sa-wms-prp/src/master/ to sa-wms-prp

cd sa-wms-prp
git branch develop_wms_xxx
git checkout develop_wms_xxx
```

## 1) Install SAPRP tool

- "saprp" is nodejs based cmd-line tool to generate mongo/csv/json data
- For other needs take a look at prp-tools/data_script.sh

```bash
cd ../prp-tools/saprp
npm install
```

## 2) Init Data - Mongo

- below script will create following Db's
- verdeeco_central_db
- verdeeco_mayberry_db

```bash
# validate saprp tool
cd sa-wms-prp
saprp -v

# confirm mongo is running at localhost:27017, then execute
# if your intention is to seed to different DB server , export MONGO_URL='mongodb://someHost:someMongoPort/'
./mongo_local_init.sh

# validate mongo_init did upsert data propertly uisng mongo cli
CON_OPTS=' localhost:27017/verdeeco_central_db --quiet --eval '
mongo $CON_OPTS 'db.utility.countDocuments({})'
mongo $CON_OPTS 'db.prp_properties.countDocuments({})'
```

## 3) Build and Launch - Node Service

- Build and Launch NodeJS Service in `new terminal 2`
- Validate Node-Service at http://localhost:3000/health
- Validate Swagger API at http://localhost:3000/api-docs

```bash
cd ../prp-service
# required global packages - install once
npm install -g npm @angular/cli eslint mocha nodemon nyc prettier rimraf riteway ts-node typescript

npm run clean
npm install
npm run compile
npm run dev
# to run node service in watch mode, execute as follows
npm run dev:watch

# check express http request and error logger
tail -f log/prp-http-access.log
tail -f log/prp-http-error.log
# The console logger is the only logger avaialble for "Service" app
find . -iname '*prp*.log'
```

## 4) Build and Launch - Frontend

- Build and Launch Anugular webapp in `new terminal 1`
- Validate Angular UI at http://localhost:4200

```bash
cd sa-wms-prp/prp-webapp
npm install
npm start
```

## 5) Execute - Unit Test

- Execute Unit Tests in `new terminal 3`
- Validate Test and Code Coverage report at `./prp-service/coverage/lcov-report/index.html`

```bash
cd ../prp-service

npm install
# runs all tests and generate coverage and report it
npm run test
# open Generated code-coverage page in browser at
google-chrome ./coverage/lcov-report/index.html

# to run tests in watch mode, execute as follows
npm run test:watch
# problem is, it executes all tests instead of recent most edited

# to watch "single test" ts file, you can execute as follows
nodemon --watch src --ext ts --exec nyc riteway src/test/utils/index.test.ts
```

## 6) Execute - Integration Test

- Execute Integration-Tests in `new terminal 3`
- Validate Integration-Test and Code Coverage report at `./prp-service/spec-report/spec.html`

```bash
cd ../prp-service

# make sure node app is running at 3000, if not start as follows:
npm run dev
# in another terminal, run Specs as
npm run spec
# open Generated integration-test report in browser at
google-chrome ./spec-report/spec.html

# to run integration-test in watch mode, execute as follows
npm run spec:watch
# problem is, it executes all tests instead of recent most edited

# to watch "single spec" ts file, you can execute as follows
mocha --watch-extensions ts --watch --require ts-node/register src/spec/SiteRouter.ingest.spec.ts
```

## 7) Generate - Swagger Docs

- Execute Swagger-Gen in `new terminal`
- Validate Genearted swagger docs at http://localhost:3000/api-docs
- more details on [swagger gen](../tools/swagger_gen.md)
- Can open swagger.json in https://editor.swagger.io/

```bash
cd ../prp-tools/swagger-gen
npm run clean
npm install
npm run compile

# run following to generate swagger.json into prp-service
npm run swagger

# to launch browser at http://localhost:3100/api-docs
npm run start
```

## 8) Build using - Maven

- You need jdk/jre and maven installed on your local env
- This will run "clean, compile and install or package" scripts in package.json

```bash
cd ../prp-service
mvn clean install
# or
mvn clean package
```

## 9) Execute - ESLint

- eslint and prettier vscode plugins take care at format and save
- one can execute to make sure lint is executed as follows

```bash
cd ../prp-service
npm run lint
npm run lint:fix
```

## Rebuild Steps

- Whenever git pull/merge is made, to have clean packages/node-modules
- One can execute following steps:

```bash
cd ../prp-service
npm run clean
git status

npm install
npm run compile
npm run dev
# terminal 2
npm run test
npm run spec
```

## Rebuild Steps - watch Mode

```bash
cd ../prp-service

# terminal 1
npm run clean
git status
npm install
npm run compile:watch

# terminal 2
npm run dev:watch

# terminal 3
npm run test:watch
npm run spec:watch
```

## Ports/Processes/Logs

```bash
cd sa-wms-prp/prp-service
# Check ports being used
netstat -tulnp | grep LISTEN | grep -e 3000 -e 27017 -e 4200

tail -f log/prp-http-access.log
tail -f log/prp-http-error.log
# node application log
tail -f log/prp-service.log
```
