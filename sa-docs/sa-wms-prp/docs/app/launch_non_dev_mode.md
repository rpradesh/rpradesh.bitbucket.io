# Launch Application - NON-Developer mode

## Build and Start App Instance

```bash
cd ~/projects
# git clone from https://bitbucket.org/sensusanalytics-dev/sa-wms-prp/src/master/
cd sa-wms-prp/prp-tools

# required global packages - install once 
npm install -g npm @angular/cli eslint mocha nodemon nyc prettier rimraf riteway ts-node typescript

# build it one time
./build_local.sh

# start/restart many times
# this script kill's previous launch processes (if any) and starts a new app instance
./start_local.sh
```

## Validate the execution
* Frontend UI using Angular at http://localhost:4200
* Backend Service using Node at http://localhost:3000/health
* Swagger/OpenApi at http://localhost:3000/api-docs
* Data at mongodb://127.0.0.1:27017/
    - atleast 2 records in verdeeco_central_db/utility collection
    - atleast 4 records in verdeeco_central_db/prp_config collection
* Ports/Processes/Logs

```bash
# Check ports being used
netstat -tulnp | grep LISTEN | grep -e 3000 -e 27017 -e 4200

# confirm console redirected logs are in here
tail -f ./tmp/local_work/prp-service-console.log
tail -f ./tmp/local_work/prp-webapp-console.log

# check init data
CON_OPTS=' localhost:27017/verdeeco_central_db --quiet --eval '
mongo $CON_OPTS 'db.utility.countDocuments({})'
mongo $CON_OPTS 'db.prp_config.countDocuments({})'

# if you need to stop app without restart
./tmp/stop_local.sh
```
