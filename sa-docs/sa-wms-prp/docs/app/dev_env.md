# Development Environment 

## Install Node/Npm/global packages
* Install `git` and `GitBash` terminal from [here](https://git-scm.com/downloads)
* Install latest Node 10.x LTS as follows:
    - To install nvm [follow steps here](https://gist.github.com/d2s/372b5943bce17b964a79)
    - Note: when there is .nvmrc file present in the folder, it can be used to init the node version by executing "nvm use"
    - On *Windows* [if issue installing npm, follow here](https://gist.github.com/rajeshpv/1e5ad771532e272ae058a2e228dd4666) 

```bash
nvm install 10.16.0
nvm use 10.16.0

#check the install
node -v
npm -v
# if npm gives error on windows follow above gist link as described

# if want to make it default
nvm alias default 10.16.0

# required global packages/bin
npm install -g npm @angular/cli eslint mocha nodemon nyc prettier rimraf riteway ts-node typescript
```

## If you get an  error
```text
# checkPermissions Missing write access to /Users/rajeshpradeshik/.nvm/versions/node/v12.10.0/lib/node_modules/saprp
# if it was built from other versions of node, so the fix is, do
rm -rf /Users/rajeshpradeshik/.nvm/versions/node/v12.10.0/lib/node_modules/saprp
# and execute ./build_local.sh
```

## Install VSCode IDE and plugins
* Install `VSCode` IDE from [here](https://code.visualstudio.com/docs/setup/setup-overview) 
* Get familiar [using typescript](https://code.visualstudio.com/docs/languages/typescript)

### Eslint and Prettier
* Copy plugin install cmd from these:
    - Install "eslint" plugin following [here](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
    - Install "Pretty" plugin following [here](https://marketplace.visualstudio.com/items?itemName=mblode.pretty-formatter)
    - Jetbrains keybindings (if used to java) [here](https://marketplace.visualstudio.com/items?itemName=isudox.vscode-jetbrains-keybindings)
* Then install by clicking last (square shape icon) towards left of Navbar in vscode explorer
    - Paste in install bar and hit enter, the plugin will get installed
* The project's .vscode /settings.json has settings to format on save options, automatically.
* Restart vscode by opening project as "code ~/prp-service". You notice life is so better :) where format/lint'ing of code is done as we type and save the file.

## HttPIE
* Better than curl or wget to test API
* Install `httpie` from https://httpie.org/doc#installation
* format of commands https://httpie.org/doc#request-items

## JMESPath tool
* read more at http://jmespath.org/  download from https://github.com/jmespath/jp/releases
  
```bash
# steps for Mac
cd ~/Downloads
wget https://github.com/jmespath/jp/releases/download/0.1.2/jp-darwin-amd64
mv jp-darwin-amd64 jp
chmod u+x jp
cp ./jp /usr/local/bin/

# example usage: will list procesId fields in the object array
http -jb GET :3000/api/v1/process/filter processId==progress_id_1001 processId==success_id_1002 | jp [].processId
```

## Mongo Server and Mongo Shell

### Install/Launch Mongo using docker
* As I did not find 3.2.22 mongodb docker image, I have pushed one into public docker hub
* You can pull and launch locally as follows:

```bash
docker pull rajeshpv/mongodb:3.2.22
# one-time
docker run --name=localmongo32 -p 27017:27017 -d rajeshpv/mongodb:3.2.22
docker logs localmongo32

# many start/stop's later
docker stop localmongo32
docker start localmongo32

# If using volumes, the exposed volumes of data,logs are /data/db and /var/log/mongodb respectively
# example launcher is:
mkdir -p ~/data/mongodb-32/db
mkdir -p ~/data/mongodb-32/log
docker run --name=localmongo32 -v ~/data/mongodb-32/db:/data/db -v ~/data/mongodb-32/log:/var/log/mongodb -p 27017:27017 -d rajeshpv/mongodb:3.2.22
```

### Install/Launch using Binary Install
- Download 3.2.22 version from https://www.mongodb.com/download-center/community [download](https://fastdl.mongodb.org/win32/mongodb-win32-x86_64-2008plus-ssl-3.2.22-signed.msi)
- Launch installer and install in default location as C:\Program Files\MongoDB\Server\3.2

```bash
# put env variables as
MONGO_HOME=C:\Program Files\MongoDB\Server\3.2
MONGO_DATA=%USERPROFILE%\programs\mongo-data
# updated PATH as
PATH=%MONGO_HOME%\bin;rest_of_Path.,
``` 

## Registry - npm, docker
* email [Dan Finucane](mailto:dan.finucane@xyleminc.com) to get you account added to http://xylem.jfrog.io
* Once added use given username+password to login in browser at https://xylem.jfrog.io/xylem/webapp/#/login
* To generate the npm registry connection details follow steps: 

```bash
# confirm ~/.npmrc does NOT exist
cat ~/.npmrc

# let us create auth entry for "sa-npm"
npm login --registry https://xylem.jfrog.io/xylem/api/npm/sa-npm/
# it will promt for username/pwd/email; key-in as they prompt for you
npm config set registry https://xylem.jfrog.io/xylem/api/npm/sa-npm/
# cat to confirm entry is made
cat ~/.npmrc
# you notice line starts soemthing like this "//xylem.jfrog.io/xylem/api/npm/sa-npm/:_authToken=eyJ2....."
# and another line with "registry=.../sa-npm"

### Let us import an sa-module as dependency
# cd into a project where there is pkg.json
cd sa-wms-prp/prp-service
# we can import as follows
npm install --save @sa-server/core --registry https://xylem.jfrog.io/xylem/api/npm/sa-npm/
# or if registry url is already present in ~/.npmrc like said above, you can just run as 
npm install --save @sa-server/core 
# if sucessfull, you will notice new dependency "@sa-server/core": "^1.0.34" in pkg.json yaay! 
```