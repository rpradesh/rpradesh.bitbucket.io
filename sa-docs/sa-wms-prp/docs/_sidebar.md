* Getting started
    * [Home](README.md) 
    * [Dev Env - Setup and Tools](app/dev_env.md)
    * [App Launch - Developer Mode](app/launch_dev_mode.md)
    * [App Launch - **Non-Developer** Mode](app/launch_non_dev_mode.md)

* Tools and Scripts
    * [Mongo and Cmd Scripts](tools/mongo_scripts.md)
    * [Swagger Gen](tools/swagger_gen.md)
    * [Docker](tools/docker.md)    

* Admin Features
    * [Mongo Index Summary](admin/mongo_index_api.md)
    * [Application Properties](admin/manage_app_properties.md)
    * [Background Task Design](admin/bg_task_template.md)
    * [Create Default Mongo Indexes](admin/bg_defaultMongoIndex.md)
    * [Create OnDemand Mongo Index](admin/bg_singleMongoIndex.md)

* Application Features
    * [Site](feature/feature_site.md)
    * [Device](feature/feature_device.md)
    * [Map](feature/feature_map_api.md)
    * [Process](feature/feature_process_api.md)
    * [Health Check](feature/feature_health_check.md)
    * [Test Logger](feature/feature_test_log.md)
    * [Test Users](feature/feature_test_user.md)

* Release/Deployment Journal
    * [Dev/Local Deploy](deploy/dev_deploy_notes.md)
    * [QA Deploy](deploy/qa_deploy_notes.md)
    * [PROD Deploy](deploy/prod_deploy_notes.md)

* Sprint Planning
    * [SubTasks and Implementation](plan/story_planning.md)
    * [Definition of Done](plan/definition_done.md)
