# Definition of Done
* Story Planning and Definition of Done, steps to complete the story and some pointers

## Story SubTasks and Execution
* The breakdown of story/work as implementation units can be called as "Sub Tasks" in Jira.
    - Make sure grooming of story is done before "Story Planning", for clear technical requirements.
* Well broken down SubTasks, helps us to get insight of progress of Story
    - It gives current status of these sub-tasks and % of completion on Task/Story level
    - Easy to block a Story/Task by blocking particular sub-task (ex: RNI deviceInfo/v2 is not ready for "Integration Tests")
### Typical Story breakdown into Subtasks:
    - 1) Analyze/design api, and/or create test/seed data
    - 2) Main dev/service implementation - (this is where majority of dev work goes)
    - 3) Write Unit Tests and Integration Tests
    - 4) Execute test/spec and compare code coverage reports and validate build
### SubTasks Implementation - Example Story 
* Consider [Filter "Site" api](https://xyleminc.atlassian.net/browse/WMS-172) story where "prp_site" collection has existing data, and the requirement is to implement GET /site/filter?siteidArray&siteTypes&pressureZoneArray
* `The implementation details can be broken down as follows`:
* SubTask 1) - Analyze/design api, and/or create test/seed data:
    - Validate the required seed data fields/attributes match `input of story/ask`
        - example: prp-tools/test-data/site/10-sitedata.csv has records for attributes for (siteid,siteType,pressureZone)
    - Implement function and Rest api signature with types in swagger
        - example: prp-tools/swagger-gen/src/routes/SiteRouter.ts#filter
* SubTask 2) - Main dev/service implementation, example: components/Site
    - Add routes/SiteRouter.ts#filter route and map to Site/index.ts#filter function
    - Add Site/index.ts#filter, if request parameters are more than few create its own Request-Model "SiteFilterRequest"
    - Add Site/Model.ts#SiteFilterRequest model interface
    - Pass the "request: SiteFilterRequest" object (as single Param to Service layer) in Site/Service.ts#filter
    - Add Site/Validation.ts#filter, and "Service" implementation does validation before any db query is executed.
    - Mongo query is executed as pipeline using given request parameters and returns response data as list/object.
* SubTask 3) - Write Unit Tests and Integration Tests:
    - unit test: src/test/components/SIte/service.test.ts
    - spec or integration test: src/spec/SiteRouter.filter.spec.ts
* SubTask 4) - Execute test/spec and compare code coverage reports and validate build:

```bash
cd prp-service

npm run test 
google-chrome ./coverage/lcov-report/index.html

npm run spec
google-chrome ./spec-coverage/lcov-report/index.html

# or validate dist folder has js and unit tests are executed by
mvn clean install

# or validate integration test
mvn failsafe:integration-test

```

## DOD - Definition of Done

### Typical Story writing in Jira:
* 1) Title: Should describe the business value it is providing, not what/how to implement
    - example: Provide Site Filter api
* 2) Description: Should be brief about the context where/how the new feature/issue provides the value. As well the before and after components working together with this new feature/change.
    - example: As a end user (UI/client app) can use this api to filter across site entities
* 3) Acceptance Criteria: (difficult one) choosing right balance between being too/little detailed. Consider enough of details where Dev as well as QA or Acceptance test personal can validate the work by going though Jira, instead of reading through implementation code.
    - example-input: given 3 search attibutes, either optional/repeated values and validation of input fields as optional/data-type validations.
    - example-output: returns empty or object array in following format.
* 4) Additional Description, can be about SLA and Size of data expected/processed
    - example: api should reply in 1 sec, expecting "Device" collection having atleast 2 million records
