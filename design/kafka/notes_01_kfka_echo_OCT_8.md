## Start Local Kafka

```text

9092 : Kafka Broker
9581 : JMX

8081 : Schema Registry
9582 : JMX

8082 : Kafka REST Proxy
9583 : JMX

8083 : Kafka Connect Distributed
9584 : JMX

2181 : ZooKeeper
9585 : JMX

3030 : Web Server

```
## Start Kafka Services
```bash
# docker run -d --name=kafka_1 -it -p 9092:9092 -p 8081:8081 -p 8082:8082 -p 8083:8083 -p 2181:2181 -p 3030:3030 landoop/fast-data-dev
docker run -d --name=kafka_1 -it -p 9092:9092 -p 8081:8081 -p 8082:8082 -p 8083:8083 -p 2181:2181 -p 3030:3030 \
 -v /Users/rajeshpradeshik/data/kafka-connect-http:/opt/landoop/connectors/third-party/kafka-connect-myhttp \
landoop/fast-data-dev 

docker logs -f kafka_1
# current version is 2.2.1
```

* After 1 minute of start browse connector details
* http://localhost:3030/kafka-connect-ui/#/cluster/fast-data-dev/connector/logs-broker, see "Curl" tab

## Build Connector
```bash
# following mvn build worked
SETT_DIR=/Users/rajeshpradeshik/projects/gh-master/sense-workspace/install/mac-august2019/files     
cp $SETT_DIR/joe_h_settings.xml   ~/.m2/settings.xml 


cd ~/projects/bb-poc/kafka-all/kafka-connect-http
mvn clean install -DskipTests
find . -iname *.jar -exec ls -lh {} \;
# new terminal to execute producer consumer directly
docker exec -it kafka_1 bash
ls -al /opt/landoop/connectors/third-party/
mkdir /opt/landoop/connectors/third-party/kafka-connect-http
docker cp ./target/kafka-connect-http-5.2.1.jar kafka_1:/opt/landoop/connectors/third-party/kafka-connect-http/

```
## Http Sink Connector
* Read more steps https://thomaskwscott.github.io/kafka-connect-http/
* 5/5 https://github.com/simplesteph/kafka-connect-github-source
* Exampel Landoop connector https://medium.com/@randhirkumars/building-a-custom-kafka-connect-connector-7c9f73915977
* Example https://dzone.com/articles/building-a-custom-kafka-connect-connector
* https://www.baeldung.com/kafka-connectors-guide
* https://enfuse.io/a-diy-guide-to-kafka-connectors/

```bash
## docker run -it  --name=localHostKafka_http1 --rm --net=host -v /home/rpradesh/projects/my-http-connector:/opt/landoop/connectors/third-party/kafka-connect-myhttp landoop/fast-data-dev

# try 2 nope
curl -X POST -H "Content-Type: application/json" --data @/Users/rajeshpradeshik/projects/bb-rpradesh/rpradesh.bitbucket.io/design/kafka/post-connector-http.json http://localhost:8083/connectors

# try 1 nope , failed due to Avro
curl -X POST \
  http://localhost:3030/api/kafka-connect/connectors \
  -H 'Content-Type: application/json' \
  -H 'Accept: application/json' \
  -d '{
  "name": "HttpSinkConnector",
  "config": {
    "connector.class": "uk.co.threefi.connect.http.HttpSinkConnector",
    "topics": "jsontest_source_1",
    "tasks.max": 1,
    "http.api.url":"http://localhost:3000/test/topics/jsontest_source_1",
    "request.method":"POST"
  }
}'

# try 1 Data

kafka-topics --zookeeper localhost:2181 --topic jsontest_source_1 --create --replication-factor 1 --partitions 1

kafka-console-producer --broker-list localhost:9092 --topic jsontest_source_1
{"foo1":"bar1"}
```

## Other try  text
```text
name=HttpSinkConnector
connector.class=uk.co.threefi.connect.http.HttpSinkConnector 
tasks.max=1 
http.api.url=http://localhost:3000/test/topics/jsontest_source_1
topics=jsontest_source_1 
request.method=POST 
value.converter=org.apache.kafka.connect.storage.StringConverter 
batch.max.size=2 

headers=Content-Type=application/vnd.kafka.json.v2+json|Accept=application/vnd.kafka.v2+json 
batch.prefix={"records"=[ 
batch.suffix=]} 
batch.max.size=5 
regex.patterns=^~$ 
regex.replacements={"value"=~} 
regex.separator=~

```


### Add error 3rd time

errors.tolerance=all
errors.deadletterqueue.topic.name=dlq_file_sink_02
errors.deadletterqueue.topic.replication.factor=1
   
```bash

docker exec -it kafka_1 bash

kafka-topics --zookeeper localhost:2181 --topic jsontest.source --create --replication-factor 1 --partitions 1
kafka-topics --zookeeper localhost:2181 --topic jsontest.replica --create --replication-factor 1 --partitions 1

kafka-console-producer --broker-list localhost:9092 --topic jsontest.source
{"foo1":"bar1"}
{"foo2":"bar2"}
{"foo3":"bar3"}
{"foo4":"bar4"}
{"foo5":"bar5"}
{"foo6":"bar6"}


kafka-console-consumer --bootstrap-server localhost:9092 --topic jsontest.replica --from-beginning

wget http://localhost:3000/api/v1/test
wget http://192.168.1.158:3000/api/v1/test

```                

* https://medium.com/@xfor/express-with-typescript-setup-8d4863e4317e
