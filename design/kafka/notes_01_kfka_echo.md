https://docs.confluent.io/current/connect/kafka-connect-http/index.html
Kafka BenchMarks https://gist.github.com/jkreps/c7ddb4041ef62a900e6c


SEARCH "http wrapper for kafka"
https://github.com/mailgun/kafka-pixy seems good
Or use Kafka Connectors
https://www.baeldung.com/kafka-connectors-guide
https://thomaskwscott.github.io/kafka-connect-http/sink_config_options.html
https://developer.here.com/olp/documentation/data-client-library/dev_guide/client/direct-kafka.html
https://www.rittmanmead.com/blog/2019/02/kafka/
https://www.confluent.io/connector/kafka-connect-http/

"kafka http sink example" GOOGLE Images

https://docs.confluent.io/current/connect/references/restapi.html#connectors
https://medium.com/@kavimaluskam/start-your-real-time-pipeline-with-apache-kafka-39e30129892a


https://github.com/edenhill/kafkacat
https://github.com/infoslack/awesome-kafka 215
https://github.com/monksy/awesome-kafka good sublists
https://github.com/dharmeshkakadia/awesome-kafka

### Running Example https://docs.confluent.io/current/connect/kafka-connect-http/index.html

docker run --rm -it -p 2181:2181 -p 3030:3030 -p 8081:8081 -p 8082:8082 -p 8083:8083 -p 9092:9092 -p 9581:9581 -e ADV_HOST=127.0.0.1 --name kafka_1 landoop/fast-data-dev

docker run -d -p 2181:2181 -p 3030:3030 -p 8081:8081 -p 8082:8082 -p 8083:8083 -p 9092:9092 -p 9581:9581 -e ADV_HOST=127.0.0.1 --name kafka_2 landoop/fast-data-dev


docker exec -it kafka_1 bash

mvn spring-boot:run -Dspring.profiles.active=basic-auth


### Follow to install Downloaded plugin
https://docs.confluent.io/current/connect/managing/install.html#connect-install-connectors

https://www.confluent.io/download/

https://thomaskwscott.github.io/kafka-connect-http/sink_connector.html

https://github.com/confluentinc/cp-demo
https://docs.confluent.io/current/tutorials/cp-demo/docs/index.html

install compose https://docs.docker.com/compose/install/

BEST Kafka BenchMarks https://developer.rackspace.com/blog/Apache-Kafka-Client-Benchmarks/
KAFKA SEC = https://banzaicloud.com/blog/kafka-security-k8s/


### Try kafka-pixy
5/5 https://gist.github.com/kaustavha/4c020dae6517b963fd05
Read at https://github.com/mailgun/kafka-pixy
Install at https://github.com/mailgun/kafka-pixy/blob/master/howto-install.md

cd ~/projects/poc/kafka-all
docker pull mailgun/kafka-pixy
wget https://raw.githubusercontent.com/mailgun/kafka-pixy/master/default.yaml
https://hub.docker.com/r/alexproca/kafka-pixy/

docker run -d -p 19091:19091 -p 19092:19092 -v $CONFIG_PATH:/etc/kafka-pixy.yaml mailgun/kafka-pixy --config /etc/kafka-pixy.yaml
docker run -it --rm -p 19091:19091 -p 19092:19092 -v /home/rpradesh/projects/poc/kafka-all/default.yaml:/etc/kafka-pixy.yaml mailgun/kafka-pixy --config /etc/kafka-pixy.yaml

docker run -it --rm --net=host -v /home/rpradesh/projects/poc/kafka-all/default.yaml:/etc/kafka-pixy.yaml mailgun/kafka-pixy


docker run -d  -e --net=host --name=localHostKafka_3 landoop/fast-data-dev
docker logs -f localHostKafka_3
docker exec -it localHostKafka_3 bash
docker stop localHostKafka_3

WORKED
docker run -it --rm -e ADV_HOST=10.30.162.62 --net=host --name=localHostKafka_4 landoop/fast-data-dev

# try net host for pixy
WORKED
docker run -it --rm --net=host --name=localPixy_3 -v /home/rpradesh/projects/poc/kafka-all/default.yaml:/etc/kafka-pixy.yaml mailgun/kafka-pixy --config /etc/kafka-pixy.yaml


https://examples.javacodegeeks.com/enterprise-java/maven/maven-settings-xml-example/

### Confluent Way Testing
 
>> wget http://client.hub.confluent.io/confluent-hub-client-latest.tar.gz
>> cd /home/rpradesh/projects/poc/kafka-all/confluent-hub-client-latest/bin

follow to start https://github.com/confluentinc/cp-demo
Read steps at https://docs.confluent.io/current/tutorials/cp-demo/docs/index.html

>> cd /home/rpradesh/projects/poc/kafka-all/cp-demo

./scripts/start.sh

### Http Sink Connector
Follow steps https://www.confluent.io/connector/kafka-connect-http/
Read more steps https://thomaskwscott.github.io/kafka-connect-http/

HOW Install Connectors Read https://docs.confluent.io/current/connect/userguide.html#connect-installing-plugins
https://docs.confluent.io/current/connect/managing/install.html

Ladoop Archive https://archive.landoop.com/lkd/
http://www.devinline.com/2018/10/setup-kafka-connect-and-filestream-standalone.html

cp /home/rpradesh/projects/poc/kafka-all/kafka-connect-http/kafka-connect-http-5.2.1.jar localHostKafka_3:/opt/landoop/connectors/third-party/kafka-connect-twitter/

curl -X PUT \
  /api/kafka-connect/connectors/logs-broker/config \
  -H 'Content-Type: application/json' \
  -H 'Accept: application/json' \
  -d '{
  "connector.class": "org.apache.kafka.connect.file.FileStreamSourceConnector",
  "topic": "logs_broker",
  "file": "/var/log/broker.log",
  "tasks.max": "1"
}'

Exampel Landoop connector https://medium.com/@randhirkumars/building-a-custom-kafka-connect-connector-7c9f73915977

docker run -it  --name=localHostKafka_http1 --rm --net=host -v /home/rpradesh/projects/my-http-connector:/opt/landoop/connectors/third-party/kafka-connect-myhttp landoop/fast-data-dev


curl -X POST -H "Content-Type: application/json" --data '{ \
"name": "http-sink", \
"config": { \
        "connector.class":"uk.co.threefi.connect.http.HttpSinkConnector", \
        "tasks.max":"1", \
        "http.api.url":"https://localhost:3000/test/topics/jsontest.replica", \
        "topics":"jsontest.source", \
        "request.method":"POST", \
        "headers":"Content-Type:application/vnd.kafka.json.v2+json|Accept:application/vnd.kafka.v2+json", \
        "value.converter":"org.apache.kafka.connect.storage.StringConverter", \
        "batch.prefix":"{\"records\":[", \
        "batch.suffix":"]}", \
        "batch.max.size":"5", \
        "regex.patterns":"^~$", \
        "regex.replacements":"{\"value\":~}", \
        "regex.separator":"~" }}' \
http://localhost:8083/connectors


curl -X POST \
  http://localhost:3030/api/kafka-connect/connectors \
  -H 'Content-Type: application/json' \
  -H 'Accept: application/json' \
  -d '{
  "name": "HttpSinkConnector",
  "config": {
    "connector.class": "uk.co.threefi.connect.http.HttpSinkConnector",
    "topics": "TopicName_HttpSinkConnector_t1",
    "tasks.max": 1,
    "http.api.url":"https://localhost:3000/test/topics/jsontest.replica",
    "request.method":"POST",
  }
}'

name=HttpSinkConnector
connector.class=uk.co.threefi.connect.http.HttpSinkConnector 
tasks.max=1 
http.api.url=http://localhost:3000/test/topics/jsontest.replica 
topics=jsontest.source 
request.method=POST 
value.converter=org.apache.kafka.connect.storage.StringConverter 
batch.max.size=2 

headers=Content-Type=application/vnd.kafka.json.v2+json|Accept=application/vnd.kafka.v2+json 
batch.prefix={"records"=[ 
batch.suffix=]} 
batch.max.size=5 
regex.patterns=^~$ 
regex.replacements={"value"=~} 
regex.separator=~

### Add error 3rd time

errors.tolerance=all
errors.deadletterqueue.topic.name=dlq_file_sink_02
errors.deadletterqueue.topic.replication.factor=1
                
docker exec -it localHostKafka_http1 bash

kafka-topics --zookeeper localhost:2181 --topic jsontest.source --create --replication-factor 1 --partitions 1
kafka-topics --zookeeper localhost:2181 --topic jsontest.replica --create --replication-factor 1 --partitions 1

kafka-console-producer --broker-list localhost:9092 --topic jsontest.source
{"foo1":"bar1"}
{"foo2":"bar2"}
{"foo3":"bar3"}
{"foo4":"bar4"}
{"foo5":"bar5"}
{"foo6":"bar6"}


kafka-console-consumer --bootstrap-server localhost:9092 --topic jsontest.replica --from-beginning