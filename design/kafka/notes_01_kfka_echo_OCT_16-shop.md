## Start Kafka Services
* CODE from https://github.com/shoppilot/kafka-connect-http
## Build Connector
```bash
# following mvn build worked
SETT_DIR=/Users/rajeshpradeshik/projects/gh-master/sense-workspace/install/mac-august2019/files     
cp $SETT_DIR/joe_h_settings.xml   ~/.m2/settings.xml 


cd ~/projects/bb-poc/kafka-all/kafka-connect-http-shop
mvn clean install -DskipTests
find . -iname *.jar -exec ls -lh {} \;
```

```bash
# docker run -d --name=kafka_2 -it -p 9092:9092 -p 8081:8081 -p 8082:8082 -p 8083:8083 -p 2181:2181 -p 3030:3030 landoop/fast-data-dev
mkdir /Users/rajeshpradeshik/data/kafka-connect-http-shop
cp ~/projects/bb-poc/kafka-all/kafka-connect-http-shop/target/kafka-connect-http-shop-*.jar /Users/rajeshpradeshik/data/kafka-connect-http-shop

docker run -d --name=kafka_2 -it -p 9092:9092 -p 8081:8081 -p 8082:8082 -p 8083:8083 -p 2181:2181 -p 3030:3030 \
 -v /Users/rajeshpradeshik/data/kafka-connect-http-shop:/opt/landoop/connectors/third-party/kafka-connect-http-shop \
landoop/fast-data-dev 
# or start kafka_2
docker start kafka_2

docker logs -f kafka_2
# current version is 2.2.1
```

* After 1 minute of start browse connector details
* http://localhost:3030/kafka-connect-ui/#/cluster/fast-data-dev/connector/logs-broker, see "Curl" tab

## Check you can post to Node Test App
```bash
# new terminal to execute producer consumer directly
docker exec -it kafka_2 bash
ls -al /opt/landoop/connectors/third-party/kafka-connect-http-shop
wget http://localhost:3000/api/v1/test

# run following to capture Host IP
ifconfig | grep 'inet '
LHOST_IP=192.168.1.158 home
LHOST_IP=10.1.166.145

wget -S -O - http://${LHOST_IP}:3000/api/v1/test
wget --header "Content-Type: application/json" --post-data '{"fooA":"barA"}' http://${LHOST_IP}:3000/topics/jsontest.replica
```

## Create Kafka things
```bash
kafka-topics --zookeeper localhost:2181 --topic jsontest.source --create --replication-factor 1 --partitions 1
kafka-topics --zookeeper localhost:2181 --topic jsontest.replica --create --replication-factor 1 --partitions 1

# start monitoring consumer
kafka-console-consumer --bootstrap-server localhost:9092 --topic jsontest.replica --from-beginning

# start producer
kafka-console-producer --broker-list localhost:9092 --topic jsontest.source
>{"foo1":"bar1"}
>{"foo2":"bar2"}
>{"foo3":"bar3"}
>{"foo4":"bar4"}
>{"foo5":"bar5"}

```

## Configure/Start Sink Connector
```bash

# try 1 - Config
cd /Users/rajeshpradeshik/projects/bb-rpradesh/rpradesh.bitbucket.io/design/kafka
# MANUALLY edit ip to LHOST_IP in post_3 file
curl -X POST -H "Content-Type: application/json" --data @./post_3-connector-http-SHOP.json http://localhost:8083/connectors
# refresh connector to see "http-sink-3" in http://localhost:3030/kafka-connect-ui/#/cluster/fast-data-dev

```

## Node Http Server

```bash
cd /Users/rajeshpradeshik/projects/bb-poc/kafka-all/express-boilerplate
code .
npm run dev:watch


# http --form POST api.example.org/person/1 name='John Smith' email=john@example.org cv=@~/Documents/cv.txt
http -v GET http://localhost:3000/api/v1/test
http -v --form POST http://localhost:3000/topics/jsontest.replica name='John Smith' email=john@example.org

# in wireshark enter
tcp.port == 3000 || udp.port == 3000
```

## This is the BEST worked
```text
connector.class=uk.co.threefi.connect.http.HttpSinkConnector
headers=Content-Type:application/json
batch.max.size=5
tasks.max=1
http.api.url=http://10.1.166.145:3000/topics/jsontest.replica
topics=jsontest.source
request.method=POST
value.converter=org.apache.kafka.connect.storage.StringConverter
```


Hi Sky/Dan, Thought Frank would be available, but that is fine.

This demo is not to show – let us do this way, 
but , show why/how subscribe/tools/msg flow constructs are used
All I am trying to show is the paradigm of data flow – can be learnt from industry, than we build on basic backpressure 
Not make a subscribe in to our own steps of execute get call and we poll for data

The only point trying to be clear is, 
-	WE can come with our own data flow/pipleline implmenetation
-	But trying to follow/see industry standard pub/sub, messaging, oe event based where
As I heard conversations heard about subscribing to data/change, instead of pull, push based with back-pressure is available by means of kafka
