## Start Kafka Services
## Build Connector
```bash
# following mvn build worked
SETT_DIR=/Users/rajeshpradeshik/projects/gh-master/sense-workspace/install/mac-august2019/files     
cp $SETT_DIR/joe_h_settings.xml   ~/.m2/settings.xml 


cd ~/projects/bb-poc/kafka-all/kafka-connect-http-shop
mvn clean install -DskipTests
find . -iname *.jar -exec ls -lh {} \;
```

```bash
# docker run -d --name=kafka_2 -it -p 9092:9092 -p 8081:8081 -p 8082:8082 -p 8083:8083 -p 2181:2181 -p 3030:3030 landoop/fast-data-dev
mkdir /Users/rajeshpradeshik/data/kafka-connect-http-shop
cp ~/projects/bb-poc/kafka-all/kafka-connect-http-shop/target/kafka-connect-http-shop-*.jar /Users/rajeshpradeshik/data/kafka-connect-http-shop

docker run -d --name=kafka_2 -it -p 9092:9092 -p 8081:8081 -p 8082:8082 -p 8083:8083 -p 2181:2181 -p 3030:3030 \
 -v /Users/rajeshpradeshik/data/kafka-connect-http-shop:/opt/landoop/connectors/third-party/kafka-connect-http-shop \
landoop/fast-data-dev 
# or start kafka_2
docker start kafka_2

docker logs -f kafka_2
# current version is 2.2.1
```

* After 1 minute of start browse connector details
* http://localhost:3030/kafka-connect-ui/#/cluster/fast-data-dev/connector/logs-broker, see "Curl" tab

```bash
# new terminal to execute producer consumer directly
docker exec -it kafka_2 bash
ls -al /opt/landoop/connectors/third-party/kafka-connect-http-shop
wget http://localhost:3000/api/v1/test

# run following to capture Host IP
ifconfig | grep 'inet '
LHOST_IP=192.168.1.158 home
LHOST_IP=10.1.166.145

wget -S -O - http://${LHOST_IP}:3000/api/v1/test
wget --header "Content-Type: application/json" --post-data '{"fooA":"barA"}' http://${LHOST_IP}:3000/topics/jsontest.replica

# try 1 - Config
cd /Users/rajeshpradeshik/projects/bb-rpradesh/rpradesh.bitbucket.io/design/kafka
# MANUALLY edit ip to LHOST_IP in post_3 file
curl -X POST -H "Content-Type: application/json" --data @./post_3-connector-http.json http://localhost:8083/connectors
# refresh connector to see "http-sink-3" in http://localhost:3030/kafka-connect-ui/#/cluster/fast-data-dev

# try 1 - Data
kafka-topics --zookeeper localhost:2181 --topic jsontest_source_12 --create --replication-factor 1 --partitions 1
kafka-console-producer --broker-list localhost:9092 --topic jsontest.source
{"foo1":"bar1"}




kafka-console-producer --broker-list localhost:9092 --topic jsontest_source_1  --property "parse.key=true" --property "key.separator=:"
key11:value11
key21:value21
key31:value31
key41:value41
key51:value51
key61:value61
key71:value71

kafka-avro-console-producer --broker-list localhost:9092 --topic jsontest_source_1 --property value.schema='{"type":"record","name":"myrecord","fields":[{"name":"f1","type":"string"}]}' --property schema.registry.url=http://localhost:8081

{"f1": "value11"}
{"f1": "value12"}
{"f1": "value13"}

kafka-console-producer --broker-list localhost:9092 --topic jsontest.source
{"foo11":"bar11"}
{"foo21":"bar21"}
{"foo31":"bar31"}
{"foo41":"bar41"}
{"foo51":"bar51"}
{"foo61":"bar61"}


kafka-console-consumer --bootstrap-server localhost:9092 --topic jsontest.replica --from-beginning


add delete.topic.enable=true to server.properites
bin/kafka-topics.sh --zookeeper localhost:2181 --delete --topic test


kafka-topics.sh --zookeeper localhost:2181 --alter --topic jsontest_source_1 --config retention.ms=10
604800000
# 0000000

kafka-topics --zookeeper localhost:2181 --topic jsontest.source --create --replication-factor 1 --partitions 1
kafka-topics --zookeeper localhost:2181 --topic jsontest.replica --create --replication-factor 1 --partitions 1


kafka-console-producer --broker-list localhost:9092 --topic jsontest.source
>{"foo1":"bar1"}
>{"foo2":"bar2"}
>{"foo3":"bar3"}
>{"foo4":"bar4"}
>{"foo5":"bar5"}

docker exec -it kafka_2 bash
kafka-console-consumer --bootstrap-server localhost:9092 --topic jsontest.replica --from-beginning

```                

* https://medium.com/@xfor/express-with-typescript-setup-8d4863e4317e

```bash
cd /Users/rajeshpradeshik/projects/bb-poc/kafka-all/express-boilerplate
code .
npm run dev:watch


# http --form POST api.example.org/person/1 name='John Smith' email=john@example.org cv=@~/Documents/cv.txt
http -v GET http://localhost:3000/api/v1/test
http -v --form POST http://localhost:3000/topics/jsontest.replica name='John Smith' email=john@example.org
```