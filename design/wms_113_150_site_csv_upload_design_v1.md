* https://xyleminc.atlassian.net/browse/WMS-113
* This is the flow or api or data structure I am implementing
* UI can use following:
 
### (API.1)  
* Note: even if all records were persisted, the above url, still gets empty file
* Request: POST /api/v1/site/upload
* body: csv file content
* Response:
```text
Body: Hateos way ! (for 201 success) {
    totalRecords : 1000,
    validRecords : 920,
    invalidRecords : 80,
    links: [
            { 
                "href": "{id}/invalid",
                "ref":  "invalid",
                "type": "GET"
            }
            { 
                "href": "{id}/summary",
                "ref":  "summary",
                "type": "GET"
            }                
        ]
}

``` 

*Status: 201, 
    * if able to parse Csv (even if all records fail validation)
    * because we are going to persist the generated csv with last column has "validation details"
*Status: 400 
    * if not a csv, 
    * or file exceeds size limit
*Status: 500
    * if application error while processing    

#### Process Site Csv Data
* parse 1000 records in one batch
* validation failed records gets written to a csv file, with that line data
    - and at end gets appended with comment as 
    - "length of siteId is greater than 10|insertDate is missing date in yyyy-mm-dd format" 
* use Mongo batch to insert 1000 records into mongo new collection "prp_site" utility collection
* Write the Invalid records in file and persist on s3 (utility s3 location)
    - user can download using below API.2
* persistence changes:
    - prp_site (new utility collection) = insert/update based on siteId of given CsvData
    - file_processing_details (existing utility collection) = insert file name and path of s3 file storage
    - ingest_files (existing utility collection) = summary of sucess/error records
    
### API.2)
* GET /api/v1/site/upload{id}/invalid
* Response: body will be csv data
* contentType=application/csv
* filename attachement: invalid_records_penn_site_data_upload_20190610_234558.csv   
   
### API.3)
* GET /api/v1/site/upload
* Response : Json Array of "above Summary"

### API.4)
* GET /api/v1/site/upload/{id}/summary
* Response : Json Body as follows
```text
body: {
    sourceFilename: 'penn_site_data_upload_20190610_234558.csv',
    invalidRecordsResource: '/api/v1/site/upload/{id}',
    processStartTS: "2019-02-02T01:25:00Z",
    processEndTS: "2019-02-02T02:35:00Z",
        validationSummary:{
         success : 980
         fail: 20;
        }
        processSummary: {
        success:
        fail: 0
        }
}   
```
