#### What I did
* I did not spend time much with direct kmz to render, instead did with kml using OpenLayers
* I unzipped the kml file reduced from 20MB file to 3MB file, by deleting "Placemark" nodes from kml files
* If interested, the source fiels are here https://bitbucket.org/rpradesh/rpradesh.bitbucket.io/src/master/design/files/

#### Render with Pipes
* This link renders with pipes: https://codepen.io/rajeshpv/pen/xNeKdR 
* You can zoom to blue dot which renders like [follows](files/screen/hendersonville_with_pipes_blue_dot.png) (notice red highlighted blue dot), yes that is Hendersenville :)
* If you zoom to Handersonville, which renders "pipes" as [follows](files/screen/hendersonville_with_pipes.png)

#### Hiding Pipes Layer
* This link renders with-OUT pipes:  https://codepen.io/rajeshpv/pen/dELbrm
* You can zoom to Handersonville, which renders as [follows](files/screen/hendersonville_without_pipes.png) (notice the thin blue lines gone) 

#### What I have learnt
* The customer given files will/may have other un-needed elements from esri/any software
* I think running through filter node elements and then persist/compress the file seems liek a choice
* If manual upload is done, it is easy to wrap an ui at kmz/kml file upload which gives a chance to see the parsed content with  maps